<?php
//verifica se a requisção está sendo feita realemente pelo servidor ou por uma ação malicioasa

if (isset($_SERVER["HTTP_X_REQUESTED_WITH"]) && $_SERVER["HTTP_X_REQUESTED_WITH"] === "XMLHttpRequest") {
    //recebe somente números no input
    $func = filter_input(INPUT_POST, 'func', FILTER_SANITIZE_NUMBER_INT);

    //verifica se possui grupo vinculado antes de exluir
    if ($func == '1') {
        require("db_public.php");
        if ($conn) {
            $cod = filter_input(INPUT_POST, 'cod', FILTER_SANITIZE_NUMBER_INT);
            $sql = "SELECT
                cod_uni_prod 
                FROM produto_usuario 
                WHERE cod_prod = ?";
            $stmt = $conn->prepare($sql);
            $stmt->bindValue(1, $cod);
            $stmt->execute();
            if ($stmt->rowCount() > 0) {
                echo json_encode("1");
                return;
            } else {
                echo json_encode("0");
                return [];
            }
        }
        echo NULL;
    }
    //verifica se possui grupo vinculado antes de exluir
    if ($func == '2') {
        require("db_public.php");
        if ($conn) {
            $cod = filter_input(INPUT_POST, 'cod', FILTER_SANITIZE_NUMBER_INT);
            $sql = "DELETE
                FROM produtos_tabela 
                WHERE cod_prod = ?";
            $stmt = $conn->prepare($sql);
            $stmt->bindValue(1, $cod);
            $stmt->execute();
            if ($stmt->rowCount() > 0) {
                echo json_encode("0");
                return;
            } else {
                echo json_encode("1");
                return [];
            }
        }
        echo NULL;
    }
}
