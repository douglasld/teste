<?php

if (isset($_SERVER["HTTP_X_REQUESTED_WITH"]) && $_SERVER["HTTP_X_REQUESTED_WITH"] === "XMLHttpRequest") {
    $func = filter_input(INPUT_POST, 'func', FILTER_SANITIZE_NUMBER_INT);
    if ($func == '1') {
        require("db_public.php");
        if ($conn) {
            $cnpj_estab = filter_input(INPUT_POST, 'cnpj_estab', FILTER_SANITIZE_NUMBER_INT);
            if ($cnpj_estab) {
                $sql = "SELECT unid_negid, unid_negdescricao FROM unid_neg WHERE unid_negcnpj_estab = ? AND unid_negind_ativo = ?";
                $stmt = $conn->prepare($sql);
                $stmt->bindValue(1, $cnpj_estab);
                $stmt->bindValue(2, "0");
                $stmt->execute();
                echo json_encode($stmt->fetchAll());
                return;
            }
            echo NULL;
        }
    }

    if ($func == '2') {
        require("db_public.php");
        if ($conn) {
            $cnpj_estab = filter_input(INPUT_POST, 'cnpj_estab', FILTER_SANITIZE_NUMBER_INT);
            if ($cnpj_estab) {
                $sql = "SELECT user_estabcod_user, user_estabnome_user FROM user_estab WHERE user_estabcnpj_matriz = ? AND user_estabind_ativo = ?";
                $stmt = $conn->prepare($sql);
                $stmt->bindValue(1, $cnpj_estab);
                $stmt->bindValue(2, "0");
                $stmt->execute();
                echo json_encode($stmt->fetchAll());
                return;
            }
            echo NULL;
        }
    }

    if ($func == '3') {
        require("db_public.php");
        if ($conn) {
            $cnpj_estab = filter_input(INPUT_POST, 'cnpj_estab', FILTER_SANITIZE_NUMBER_INT);
            if ($cnpj_estab) {
                $sql = "SELECT user_estabcod_user, user_estabnome_user FROM user_estab WHERE user_estabcnpj_matriz = ? AND user_estabind_ativo = ? AND user_estabrecebe_metas = ?";
                $stmt = $conn->prepare($sql);
                $stmt->bindValue(1, $cnpj_estab);
                $stmt->bindValue(2, "0");
                $stmt->bindValue(3, "1");
                $stmt->execute();
                echo json_encode($stmt->fetchAll());
                return;
            }
            echo NULL;
        }
    }

    if ($func == '4') {
        require("db_public.php");
        if ($conn) {
            $cnpj_estab = filter_input(INPUT_POST, 'cnpj_estab', FILTER_SANITIZE_NUMBER_INT);
            if ($cnpj_estab) {
                $sql = "SELECT grupo_prodcod_grupo_prod, grupo_proddescricao FROM grupo_prod WHERE grupo_prodcnpj_estab = ? AND grupo_prodind_ativo = ?";
                $stmt = $conn->prepare($sql);
                $stmt->bindValue(1, $cnpj_estab);
                $stmt->bindValue(2, "0");
                $stmt->execute();
                echo json_encode($stmt->fetchAll());
                json_last_error();
                return;
            }
            echo NULL;
        }
    }

    if ($func == '5') {
        require("db_public.php");
        if ($conn) {
            $cnpj_estab = filter_input(INPUT_POST, 'cnpj_estab', FILTER_SANITIZE_NUMBER_INT);
            $cod_gprod = filter_input(INPUT_POST, 'cod_gprod', FILTER_SANITIZE_NUMBER_INT);
            error_log($cod_gprod);
            if ($cnpj_estab) {
                $sql = "SELECT produtocod_produto, produtodescricao FROM produto WHERE produtocnpj_estab = ? AND produtoind_ativo = ? AND produtocod_grup_prod = ?";
                $stmt = $conn->prepare($sql);
                $stmt->bindValue(1, $cnpj_estab);
                $stmt->bindValue(2, "0");
                $stmt->bindValue(3, $cod_gprod);
                $stmt->execute();
                echo json_encode($stmt->fetchAll());
                return;
            }
            echo NULL;
        }
    }

    if ($func == '6') {
        require("db_public.php");
        if ($conn) {
            $cnpj_estab = filter_input(INPUT_POST, 'cnpj_estab', FILTER_SANITIZE_NUMBER_INT);
            if ($cnpj_estab) {
                $sql = "SELECT grupo_prodcod_grupo_prod, grupo_proddescricao FROM grupo_prod WHERE grupo_prodcnpj_estab = ? AND grupo_prodind_ativo = ?";
                $stmt = $conn->prepare($sql);
                $stmt->bindValue(1, $cnpj_estab);
                $stmt->bindValue(2, "0");
                $stmt->execute();
                echo json_encode($stmt->fetchAll());
                return;
            }
            echo NULL;
        }
    }

    if ($func == '7') {
        require("db_public.php");
        if ($conn) {
            $cnpj_estab = filter_input(INPUT_POST, 'cnpj_estab', FILTER_SANITIZE_NUMBER_INT);
            if ($cnpj_estab) {
                $sql = "SELECT unid_medcod_unid_med, unid_meddescricao FROM unid_med WHERE unid_medcnpj_estab = ? AND unid_medind_ativo = ?";
                $stmt = $conn->prepare($sql);
                $stmt->bindValue(1, $cnpj_estab);
                $stmt->bindValue(2, "0");
                $stmt->execute();
                echo json_encode($stmt->fetchAll());
                return;
            }
            echo NULL;
        }
    }

    if ($func == '8') {
        require("db_public.php");
        if ($conn) {
            $cnpj_estab = filter_input(INPUT_POST, 'cnpj_estab', FILTER_SANITIZE_NUMBER_INT);
            $cod_meta = filter_input(INPUT_POST, 'cod_meta', FILTER_SANITIZE_NUMBER_INT);
            if ($cnpj_estab) {
                $sql = "SELECT metacod_meta, metadescricao FROM meta WHERE metacnpj_estab = ? AND metacod_meta = ? AND metaind_ativo = ?";
                $stmt = $conn->prepare($sql);
                $stmt->bindValue(1, $cnpj_estab);
                $stmt->bindValue(2, $cod_meta);
                $stmt->bindValue(3, "0");
                $stmt->execute();
                echo json_encode($stmt->fetchAll());
                return;
            }
            echo NULL;
        }
    }

    if ($func == '9') {
        require("db_public.php");
        if ($conn) {
            $cnpj_estab = filter_input(INPUT_POST, 'cnpj_estab', FILTER_SANITIZE_NUMBER_INT);
            if ($cnpj_estab) {
                $sql = "SELECT user_perfilcod_user, user_perfilnome_completo FROM user_perfil WHERE user_perfilcnpj_matriz = ? AND user_perfilind_ativo = ? AND user_perfilind_presidente =?";
                $stmt = $conn->prepare($sql);
                $stmt->bindValue(1, $cnpj_estab);
                $stmt->bindValue(2, "0");
                $stmt->bindValue(3, "1");
                $stmt->execute();
                echo json_encode($stmt->fetchAll());
                return;
            }
            echo NULL;
        }
    }

    if ($func == '10') {
        require("db_public.php");
        if ($conn) {
            $cnpj_estab = filter_input(INPUT_POST, 'cnpj_estab', FILTER_SANITIZE_NUMBER_INT);
            if ($cnpj_estab) {
                $sql = "SELECT user_perfilcod_user, user_perfilnome_completo FROM user_perfil WHERE user_perfilcnpj_matriz = ? AND user_perfilind_ativo = ? AND user_perfilind_diretor =?";
                $stmt = $conn->prepare($sql);
                $stmt->bindValue(1, $cnpj_estab);
                $stmt->bindValue(2, "0");
                $stmt->bindValue(3, "1");
                $stmt->execute();
                echo json_encode($stmt->fetchAll());
                return;
            }
            echo NULL;
        }
    }

    if ($func == '11') {
        require("db_public.php");
        if ($conn) {
            $cnpj_estab = filter_input(INPUT_POST, 'cnpj_estab', FILTER_SANITIZE_NUMBER_INT);
            if ($cnpj_estab) {
                $sql = "SELECT user_perfilcod_user, user_perfilnome_completo FROM user_perfil WHERE user_perfilcnpj_matriz = ? AND user_perfilind_ativo = ? AND user_perfilind_gerente =?";
                $stmt = $conn->prepare($sql);
                $stmt->bindValue(1, $cnpj_estab);
                $stmt->bindValue(2, "0");
                $stmt->bindValue(3, "1");
                $stmt->execute();
                echo json_encode($stmt->fetchAll());
                return;
            }
            echo NULL;
        }
    }
}
