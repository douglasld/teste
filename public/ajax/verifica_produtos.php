<?php
//ESSA PAGINA INFLUENCIA EM TODAS AS PÁGINAS DE VENDAS QUE POSSUEM FUNCIONALIDADES PADROES COMPARTILHAdas
if (isset($_SERVER["HTTP_X_REQUESTED_WITH"]) && $_SERVER["HTTP_X_REQUESTED_WITH"] === "XMLHttpRequest") {
    //recebe somente números no input
    $func = filter_input(INPUT_POST, 'func', FILTER_SANITIZE_NUMBER_INT);

    if ($func == '1') {
        require("db_public.php");
        if ($conn) {
            $nome_prod = filter_input(INPUT_POST, 'nome_prod', FILTER_SANITIZE_STRING);
            $cod_loja = filter_input(INPUT_POST, 'cod_loja', FILTER_SANITIZE_NUMBER_INT);
            if ($cod_loja) {
                $sql = "SELECT
                cod_uni_prod 
                FROM produto_usuario 
                WHERE cod_loja = ? AND 
                nome_prod_v = ?";
                $stmt = $conn->prepare($sql);
                $stmt->bindValue(1, $cod_loja);
                $stmt->bindValue(2, $nome_prod);
                $stmt->execute();
                if ($stmt->rowCount() > 0) {
                    echo json_encode("1");
                    return;
                } else {
                    echo json_encode("0");
                    return [];
                }
            }
            echo NULL;
        }
    }

    if ($func == '2') {
        require("db_public.php");
        if ($conn) {
            $nome_prod = filter_input(INPUT_POST, 'nome_prod', FILTER_SANITIZE_STRING);
            $cod_loja = filter_input(INPUT_POST, 'cod_loja', FILTER_SANITIZE_NUMBER_INT);
            if ($cod_loja) {
                $sql = "SELECT
                cod_uni_prod 
                FROM produto_usuario 
                WHERE cod_loja = ? AND 
                nome_prod_v = ?";
                $stmt = $conn->prepare($sql);
                $stmt->bindValue(1, $cod_loja);
                $stmt->bindValue(2, $nome_prod);
                $stmt->execute();
                if ($stmt->rowCount() > 0) {
                    echo json_encode("1");
                    return;
                } else {
                    echo json_encode("0");
                    return [];
                }
            }
            echo NULL;
        }
    }
}
