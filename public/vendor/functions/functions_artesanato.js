/* ---------------------------------------------------------------INSERÇÃO DE PRODUTOS ------------------------------------------------------------------------*/

//prview foto 1
function readURL(input) {
  var extPermitidas = ["jpg", "png", "jpeg", "webp"];
  var extArquivo = input.value.split(".").pop();
  if (
    typeof extPermitidas.find(function (ext) {
      return extArquivo == ext;
    }) == "undefined"
  ) {
    Swal.fire({
      icon: "error",
      title: "",
      showClass: {
        popup: "animate__animated animate__bounceInUp",
      },
      hideClass: {
        popup: "animate__animated animate__bounceOutDown",
      },
      text: "Extenção " + extArquivo + " não suportada!",
    });
    $("#ft1").val("");
    event.defaultPrevented();
    return;
  } else {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
        $("#blah1").attr("src", e.target.result);
      };
      reader.readAsDataURL(input.files[0]);
    }
  }
}

//prview foto 2
function readURLL(input) {
  var extPermitidas = ["jpg", "png", "jpeg", "webp"];
  var extArquivo = input.value.split(".").pop();
  if (
    typeof extPermitidas.find(function (ext) {
      return extArquivo == ext;
    }) == "undefined"
  ) {
    Swal.fire({
      icon: "error",
      title: "",
      showClass: {
        popup: "animate__animated animate__bounceInUp",
      },
      hideClass: {
        popup: "animate__animated animate__bounceOutDown",
      },
      text: "Extenção " + extArquivo + " não suportada!",
    });
    $("#ft1").val("");
    event.defaultPrevented();
    return;
  } else {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
        $("#blah2").attr("src", e.target.result);
      };
      reader.readAsDataURL(input.files[0]);
    }
  }
}

//prview foto 3
function readURLLL(input) {
  var extPermitidas = ["jpg", "png", "jpeg", "webp"];
  var extArquivo = input.value.split(".").pop();
  if (
    typeof extPermitidas.find(function (ext) {
      return extArquivo == ext;
    }) == "undefined"
  ) {
    Swal.fire({
      icon: "error",
      title: "",
      showClass: {
        popup: "animate__animated animate__bounceInUp",
      },
      hideClass: {
        popup: "animate__animated animate__bounceOutDown",
      },
      text: "Extenção " + extArquivo + " não suportada!",
    });
    $("#ft1").val("");
    event.defaultPrevented();
    return;
  } else {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
        $("#blah3").attr("src", e.target.result);
      };
      reader.readAsDataURL(input.files[0]);
    }
  }
}

//prview foto 4
function readURLLLL(input) {
  var extPermitidas = ["jpg", "png", "jpeg", "webp"];
  var extArquivo = input.value.split(".").pop();
  if (
    typeof extPermitidas.find(function (ext) {
      return extArquivo == ext;
    }) == "undefined"
  ) {
    Swal.fire({
      icon: "error",
      title: "",
      showClass: {
        popup: "animate__animated animate__bounceInUp",
      },
      hideClass: {
        popup: "animate__animated animate__bounceOutDown",
      },
      text: "Extenção " + extArquivo + " não suportada!",
    });
    $("#ft1").val("");
    event.defaultPrevented();
    return;
  } else {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
        $("#blah4").attr("src", e.target.result);
      };
      reader.readAsDataURL(input.files[0]);
    }
  }
}

//prview foto 5
function readURLLLLL(input) {
  var extPermitidas = ["jpg", "png", "jpeg", "webp"];
  var extArquivo = input.value.split(".").pop();
  if (
    typeof extPermitidas.find(function (ext) {
      return extArquivo == ext;
    }) == "undefined"
  ) {
    Swal.fire({
      icon: "error",
      title: "",
      showClass: {
        popup: "animate__animated animate__bounceInUp",
      },
      hideClass: {
        popup: "animate__animated animate__bounceOutDown",
      },
      text: "Extenção " + extArquivo + " não suportada!",
    });
    $("#ft1").val("");
    event.defaultPrevented();
    return;
  } else {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
        $("#blah5").attr("src", e.target.result);
      };
      reader.readAsDataURL(input.files[0]);
    }
  }
}

//desabilita garantia
function verifica(value) {
  var garantia_meses = document.getElementById("garantia_meses");

  if (value == 1) {
    garantia_meses.disabled = false;
  } else if (value == 2) {
    garantia_meses.disabled = true;
    garantia_meses.value = 0;
  }
}


//calcula centimetros cubicos do produto
function calcularcm3() {
  var valor1 = parseInt(document.getElementById("altura").value, 10);
  var valor2 = parseInt(document.getElementById("largura").value, 10);
  var valor3 = parseInt(document.getElementById("comprimento").value, 10);
  document.getElementById("resultcm3").value =
    (valor1 * valor2 * valor3) / 6000;
}

//adição de inputs de cores vendas
var count = 2;
$("#add_color").click(function () {
  $("#prod_formm").append(
    " <div class='col-md-12' id='campo" +
    ++count +
    "'><div class='row'> <div class='form-group col-md-4'><select class='form-control  select2bs4' id='cores' name='cores[]' require=''><option disabled selected value=''>Selecione</option> <option value='1'>Azul</option><option value='2'>Verde</option> <option value='3'>Vermelho</option><option value='4'>Preto</option><option value='5' style='color: black;'>Branco</option><option value='6'>Marrom</option> <option value='7'>Cinza</option> <option value='8'>Rosa</option><option value='9' style='color: black;'>Amarelo</option></select></div><div class='form-group col-md-6'><input type='number' min=1 name='qtd_prod[]' id='qtd_prod[]' class='form-control' class='btn btn-outline-primary' required='' placeholder='Quantidade disponível *'></input> </div> <div class='form-group col-md-1'><button type='button' id='" +
    count +
    "'class='apagar btn btn-outline-primary'>-</button></div></div></div>"
  );
  $("#prod_formm").on("click", ".apagar", function () {
    var btn_id = $(this).attr("id");
    $("#campo" + btn_id + "").remove();
  });
});

// Summernote do textearea do prodtuo
$('.textarea').summernote({
  placeholder: 'Digite a descrição aqui!',
  tabsize: 3,
  height: 100,
  minHeight: 300, // set minimum height of editor
  maxHeight: 300,
  toolbar: [
    ['font', ['bold', 'underline', 'clear']],
    ['color', ['color']],
    ['para', ['ul', 'ol', 'paragraph']],
  ],
});

//coolca máscaras nos camposos
$(document).ready(function () {
  $('#valor_produto').mask('000,00', {
    reverse: true
  });
});

$(document).ready(function () {
  $('#valor_final').mask('000,00', {
    reverse: true
  });
});
$(document).ready(function () {
  $('#peso').mask('000,00', {
    reverse: true
  });
});

$(document).ready(function () {
  $('#altura').mask('000,00', {
    reverse: true
  });
});
$(document).ready(function () {
  $('#largura').mask('000,00', {
    reverse: true
  });
});
$(document).ready(function () {
  $('#comprimento').mask('000,00', {
    reverse: true
  });
});
$(document).ready(function () {
  $('#comprimento').mask('000,00', {
    reverse: true
  });
});

//função responsável pelo delay
function delay(callback, ms) {
  var timer = 0;
  return function () {
    var context = this,
      args = arguments;
    clearTimeout(timer);
    timer = setTimeout(function () {
      callback.apply(context, args);
    }, ms || 0);
  };
}




