<?php

namespace App;

use App\Core\Model;

class Auth
{
    public static function Login($email, $senha)
    {
        $sql = "SELECT * FROM usuario WHERE email = ? AND senha = ? ";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $email);
        $stmt->bindValue(2, $senha);
        $stmt->execute();

        if ($stmt->rowCount() >= 1) {
            $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
            $_SESSION['logado'] = true;
            $_SESSION['userId'] = $resultado['cod_user'];
            $_SESSION['userNome'] = $resultado['nome'];
            $_SESSION['sobrenome'] = $resultado['sobrenome'];
            $_SESSION['permissao'] = $resultado['permissao'];
            $_SESSION['prod'] = 'public/';  
            
            /*  $_SESSION['userFoto'] = $resultado['user_perfilfoto']; */
            $data['mensagem'] = ['mensagem' => '3 - Bem Vindo(a)!'];
            header('Location: /home/loja_geral/');
        } else {
            return "3 - Credenciais inválidas!";
        }
    }

    public static function Cadastrar($nome, $sobrenome, $datanasc, $genero, $email, $senha, $confsenha, $dt_criacao, $hr_criacao)
    {

        $sql = "SELECT email FROM usuario WHERE email = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $email);
        $stmt->execute();
        if ($stmt->rowCount() >= 1) {
            return "3 - E-mail já está em uso!";
        }

        $sql = "INSERT INTO usuario (nome, sobrenome, data_nasc, email, genero, senha, permissao, ban_nv, status_conta, dt_criacao, hr_criacao)VALUES(?,?,?,?,?,?,?,?,?,?,?)";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $nome);
        $stmt->bindValue(2, $sobrenome);
        $stmt->bindValue(3, $datanasc);
        $stmt->bindValue(4, $email);
        $stmt->bindValue(5, $genero);
        $stmt->bindValue(6, $senha);
        $stmt->bindValue(7, 'C4CA4238A0B923820DCC509A6F75849B');
        $stmt->bindValue(8, '618A790F8DF94B8F4A565DE543B34979');
        $stmt->bindValue(9, '909A54BEE9F6034E55CE03F43EC5BAC5');
        $stmt->bindValue(10, $dt_criacao);
        $stmt->bindValue(11, $hr_criacao);
        if ($stmt->execute()) {
            return "1 - Cadastrado com sucesso! Verifique o e-mail: $email para ativação da conta";
        } else {
            return "3 - Erro ao efetuar o cadastro!";
        }
    }

    public static function Logout()
    {
        session_destroy();
        header('Location: /home/login');
    }

    public static function checkLogin()
    {
        if (!isset($_SESSION['logado'])) {
            header('Location: /home/login');
            die;
        }
    }
}
