<?php

use App\Core\Model;

class UserEstab extends Model
{

    public $user_estabcnpj_matriz;
    public $user_estabcnpj_estab;
    public $user_estabcod_user;
    public $user_estabnome_user;
    public $user_estabvisao_global;
    public $user_estabvisao_gerencial;
    public $user_estabvisao_vendas;
    public $user_estabind_ativo;
    public $user_estabind_origem;
    public $user_estabrecebe_metas;
    public $user_estabdt_criacao;
    public $user_estabhr_criacao;
    public $user_estabdt_altera;
    public $user_estabhr_altera;
    public $user_estabuser_criacao;
    public $user_estabuser_altera;

    public function save()
    {

        $sql = "INSERT INTO user_estab(user_estabcnpj_matriz,user_estabcnpj_estab,user_estabcod_user,user_estabnome_user,user_estabvisao_global,user_estabvisao_gerencial,user_estabvisao_vendas,user_estabind_ativo,user_estabind_origem,user_estabrecebe_metas,user_estabdt_criacao,user_estabhr_criacao,user_estabdt_altera,user_estabhr_altera,user_estabuser_criacao,user_estabuser_altera)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $this->user_estabcnpj_matriz);
        $stmt->bindValue(2, $this->user_estabcnpj_estab);
        $stmt->bindValue(3, $this->user_estabcod_user);
        $stmt->bindValue(4, $this->user_estabnome_user);
        $stmt->bindValue(5, $this->user_estabvisao_global);
        $stmt->bindValue(6, $this->user_estabvisao_gerencial);
        $stmt->bindValue(7, $this->user_estabvisao_vendas);
        $stmt->bindValue(8, $this->user_estabind_ativo);
        $stmt->bindValue(9, $this->user_estabind_origem);
        $stmt->bindValue(10, $this->user_estabrecebe_metas);
        $stmt->bindValue(11, $this->user_estabdt_criacao);
        $stmt->bindValue(12, $this->user_estabhr_criacao);
        $stmt->bindValue(13, $this->user_estabdt_altera);
        $stmt->bindValue(14, $this->user_estabhr_altera);
        $stmt->bindValue(15, $this->user_estabuser_criacao);
        $stmt->bindValue(16, $this->user_estabuser_altera);

        if ($stmt->execute()) :
            return "Cadastrado com sucesso";
        else :
            return "Erro ao cadastrar";
        endif;
    }

    public function update($id)
    {
        $sql = "UPDATE user_estab SET user_estabvisao_global = ?,user_estabvisao_gerencial = ?,user_estabuser_estabvisao_vendas = ?,user_estabuser_estabind_ativo = ?,user_estabind_origem = ?,user_estabrecebe_metas = ?,user_estabdt_altera = ?,user_estabhr_altera = ?,user_estabuser_altera = ? WHERE user_estabid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $this->user_estabvisao_global);
        $stmt->bindValue(2, $this->user_estabvisao_gerencial);
        $stmt->bindValue(3, $this->user_estabvisao_vendas);
        $stmt->bindValue(4, $this->user_estabind_ativo);
        $stmt->bindValue(5, $this->user_estabind_origem);
        $stmt->bindValue(6, $this->user_estabrecebe_metas);
        $stmt->bindValue(7, $this->user_estabdt_altera);
        $stmt->bindValue(8, $this->user_estabhr_altera);
        $stmt->bindValue(9, $this->user_estabuser_altera);
        $stmt->bindValue(10, $id);

        if ($stmt->execute()) :
            return "Atualizado com sucesso";
        else :
            return "Erro ao atualizar";
        endif;
    }

    public function delete($id)
    {
        $sql = "DELETE FROM user_estab WHERE user_estabid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $id);

        if ($stmt->execute()) :
            return "Excluido com sucesso";
        else :
            return "Erro ao excluir";
        endif;
    }

    public function getAll()
    {
        $sql = "SELECT * FROM user_estab a LEFT JOIN user_perfil b ON b.user_perfilid = a.user_estabcod_user";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        } else {
            return [];
        }
    }

    public function findId($id)
    {
        $sql = "SELECT * FROM user_estab LEFT JOIN user_perfil a ON a.user_perfilcod_user = user_estabcod_user WHERE user_estabid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
            return $resultado;
        } else {
            return [];
        }
    }

    public function findCod($id)
    {
        $sql = "SELECT * FROM user_estab a LEFT JOIN user_perfil b ON b.user_perfilcod_user = a.user_estabcod_user WHERE a.user_estabcod_user = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        } else {
            return [];
        }
    }

    public function BuscageralMatriz($matriz)
    {
        $sql = "SELECT * FROM user_estab WHERE user_estabcnpj_matriz = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $matriz);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $resultado;
    }
    public function FindCodUser($cod_user)
    {
        $sql = "SELECT * FROM user_estab WHERE user_estabcod_user = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $cod_user);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $resultado;
    }

    public function get_auto_cnpj($matriz, $cod)
    {
        $sql = "SELECT user_estabcnpj_estab FROM user_estab WHERE user_estabcod_user = ? AND user_estabcnpj_matriz = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $cod);
        $stmt->bindValue(2, $matriz);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $resultado;
    }
}
