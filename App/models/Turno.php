<?php

use App\Core\Model;

class Turno extends Model
{

    public $turnocnpj_estab;
    public $turnodescricao;
    public $turnoind_ativo;
    public $turnoind_origem;
    public $turnodt_criacao;
    public $turnohr_criacao;
    public $turnouser_criacao;
    public $turnodt_altera;
    public $turnohr_altera;
    public $turnouser_altera;
    public $turnohr_ini;
    public $turnohr_fim;
    public $turnoperc_red_meta_turno;

    public function save()
    {

        $sql = "INSERT INTO turno(turnocnpj_estab,turnodescricao,turnoind_ativo,turnoind_origem,turnodt_criacao,turnohr_criacao,turnouser_criacao,turnodt_altera,turnohr_altera,turnouser_altera,turnohr_ini,turnohr_fim,turnoperc_red_meta_turno)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$this->turnocnpj_estab);
        $stmt->bindValue(2,$this->turnodescricao);
        $stmt->bindValue(3,$this->turnoind_ativo);
        $stmt->bindValue(4,$this->turnoind_origem);
        $stmt->bindValue(5,$this->turnodt_criacao);
        $stmt->bindValue(6,$this->turnohr_criacao);
        $stmt->bindValue(7,$this->turnouser_criacao);
        $stmt->bindValue(8,$this->turnodt_altera);
        $stmt->bindValue(9,$this->turnohr_altera);
        $stmt->bindValue(10,$this->turnouser_altera);
        $stmt->bindValue(11,$this->turnohr_ini);
        $stmt->bindValue(12,$this->turnohr_fim);
        $stmt->bindValue(13,$this->turnoperc_red_meta_turno);

        if($stmt->execute()):
            return "Cadastrado com sucesso";
        else:
            return "Erro ao cadastrar";
        endif;

    }

    public function update($id)
    {
        $sql = "UPDATE turno SET turnocnpj_estab=?,turnodescricao=?,turnoind_ativo=?,turnoind_origem=?,turnodt_altera=?,turnohr_altera=?,turnouser_altera=?,turnohr_ini=?,turnohr_fim=?,turnoperc_red_meta_turno=? WHERE turnoid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$this->turnocnpj_estab);
        $stmt->bindValue(2,$this->turnodescricao);
        $stmt->bindValue(3,$this->turnoind_ativo);
        $stmt->bindValue(4,$this->turnoind_origem);
        $stmt->bindValue(5,$this->turnodt_altera);
        $stmt->bindValue(6,$this->turnohr_altera);
        $stmt->bindValue(7,$this->turnouser_altera);
        $stmt->bindValue(8,$this->turnohr_ini);
        $stmt->bindValue(9,$this->turnohr_fim);
        $stmt->bindValue(10,$this->turnoperc_red_meta_turno);
        $stmt->bindValue(11,$id);

        if($stmt->execute()):
            return "Atualizado com sucesso";
        else:
            return "Erro ao atualizar";
        endif;
    }

    public function delete($id)
    {
        $sql = "DELETE FROM turno WHERE turnoid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$id);

        if($stmt->execute()):
            return "Excluido com sucesso";
        else:
            return "Erro ao excluir";
        endif;
    }

    public function getAll()
    {
        $sql = "SELECT * FROM turno";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

    public function findId($id)
    {
        $sql = "SELECT * FROM turno WHERE turnoid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$id);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

    public function ultimo($estab,$usuario){
        $sql = "SELECT * FROM turno WHERE turnocnpj_estab = ? AND turnouser_criacao = ? ORDER BY turnoid DESC";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$estab);
        $stmt->bindValue(2,$usuario);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }
}





