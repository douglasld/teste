<?php

use App\Core\Model;

class ResUnidNeg extends Model
{
    public $res_unid_negcnpj_estab;
    public $res_unid_negvl_meta;
    public $res_unid_negvl_minimo;
    public $res_unid_negvl_variavel;
    public $res_unid_negvl_adic;
    public $res_unid_negvl_bonus;
    public $res_unid_negdt_movto;
    public $res_unid_negcod_unid_neg;
    public $res_unid_negvl_real;

    public function save()
    {
        $sql = "INSERT INTO res_unid_neg(res_unid_negcnpj_estab,res_unid_negdt_movto,res_unid_negcod_unid_neg,res_unid_negvl_real)VALUES(?,?,?,?)";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$this->res_unid_negcnpj_estab);
        $stmt->bindValue(2,$this->res_unid_negdt_movto);
        $stmt->bindValue(3,$this->res_unid_negcod_unid_neg);
        $stmt->bindValue(4,$this->res_unid_negvl_real);

        if($stmt->execute()):
            return "Cadastrado com sucesso";
        else:
            return "Erro ao cadastrar";
        endif;
    }

    public function update($id)
    {
        $sql = "UPDATE res_unid_neg SET res_unid_negdt_movto = ?, res_unid_negvl_real = ? WHERE res_unid_negid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$this->res_unid_negdt_movto);
        $stmt->bindValue(2,$this->res_unid_negvl_real);
        $stmt->bindValue(3,$id);

        if($stmt->execute()):
            return "Atualizado com sucesso";
        else:
            return "Erro ao atualizar";
        endif;
    }

    public function delete($id)
    {
        $sql = "DELETE FROM res_unid_neg WHERE res_unid_negid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$id);

        if($stmt->execute()):
            return "Excluido com sucesso";
        else:
            return "Erro ao excluir";
        endif;
    }

    public function deleteLimpa()
    {
        $sql = "DELETE FROM res_unid_neg";
        $stmt = Model::getConn()->prepare($sql);

        if($stmt->execute()):
            return "Excluido com sucesso";
        else:
            return "Erro ao excluir";
        endif;
    }

    public function getAll()
    {
        $sql = "SELECT * FROM res_unid_neg";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

    public function getAllEstabelecimento($id)
    {
        $sql = "SELECT * FROM res_unid_neg WHERE res_unid_negcnpj_estab = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$id);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

    public function findId($id)
    {
        $sql = "SELECT * FROM res_unid_neg WHERE res_unid_negid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$id);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

}



