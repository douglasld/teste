<?php

use App\Core\Model;

class Dashboard extends Model
{

    public function getAll($id)
    {
        $sql = "SELECT * FROM res_matriz WHERE res_matrizcnpj_matriz = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$id);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

    public function getAllEstabelecimento($id)
    {
        $sql = "SELECT * FROM meta WHERE metacnpj_estab = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        } else {
            return [];
        }
    }

    public function findId($id)
    {
        $sql = "SELECT * FROM meta a";
        $sql = $sql . " LEFT JOIN grupo_prod b ON b.grupo_prodid = a.metacod_grupo_prod";
        $sql = $sql . " LEFT JOIN produto c ON c.produtoid = a.metacod_produto";
        $sql = $sql . " LEFT JOIN unid_med d ON d.unid_medid = a.metaunid_calc";
        $sql = $sql . " LEFT JOIN empresa e ON e.empresaid = a.metacnpj_estab";
        $sql = $sql . " LEFT JOIN unid_neg f ON f.unid_negid = a.metacod_unid_neg";
        $sql = $sql . " WHERE a.metaid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
            return $resultado;
        } else {
            return [];
        }
    }

    public function getSeriesEstab($mes)
    {
        $sql = "SELECT empresa.empresanome_abrev as name, res_estab.res_estabvl_meta as series from res_estab
        left join empresa on empresa.empresacnpj_estab = res_estab.res_estabcnpj_estab";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $mes);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);

            foreach ($resultado as $key => $value) {
                $result[$key] = array((string) $value['name'], (float) $value['series']);
            }

            return $result;
        } else {
            return [];
        }
    }

    public function getComparativoEstabName($mes)
    {
        $sql = "SELECT empresa.empresanome_abrev as name from res_estab
        inner join empresa on empresa.empresacnpj_estab = res_estab.res_estabcnpj_estab";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $mes);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);

            foreach ($resultado as $key => $value) {
                $result[$key] = array((string) $value['name']);
            }

            return $result;
        } else {
            return [];
        }
    }

    public function getComparativoEstabMeta($mes)
    {
        $sql = "SELECT res_estab.res_estabvl_meta as meta from res_estab
        inner join empresa on empresa.empresacnpj_estab = res_estab.res_estabcnpj_estab";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $mes);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);

            foreach ($resultado as $key => $value) {
                $result[$key] = array((float) $value['meta']);
            }

            return $result;
        } else {
            return [];
        }
    }

    public function getComparativoEstabReal($mes)
    {
        $sql = "SELECT res_estab.res_estabvl_real as valor_real from res_estab
        inner join empresa on empresa.empresacnpj_estab = res_estab.res_estabcnpj_estab";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $mes);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);

            foreach ($resultado as $key => $value) {
                $result[$key] = array((float) $value['valor_real']);
            }

            return $result;
        } else {
            return [];
        }
    }

    /***  Engenharia de Tabelas "DrillDown"  ***/
    // Accordion Grupo de Produto
    // @data_input 
    public function estabData($mes, $estab = null)
    {
        // print_r($_GET); die;
        $periodo = $this->findPeriod($mes, 5);

        $sql = "SELECT 
                    e.empresarazao_social as estabelecimento,
                    a.res_estabvl_meta as meta_mes_atual,
                    a.res_estabvl_real as realizado_mes_atual,    
                    (a.res_estabvl_real/a.res_estabvl_meta) as percentual_mes_atual,
                    IF (a.res_estabvl_real > b.res_estabvl_real, 'fa fa-arrow-up', 'fa fa-arrow-down') as arrow_a,
                
                    b.res_estabvl_real as realizado_mes_anterior1,
                    (b.res_estabvl_real/b.res_estabvl_meta) as percentual_mes_anterior1,
                    IF (b.res_estabvl_real > c.res_estabvl_real, 'fa fa-arrow-up', 'fa fa-arrow-down') as arrow_b,
                
                     c.res_estabvl_real as realizado_mes_anterior2,
                     (c.res_estabvl_real/b.res_estabvl_meta) as percentual_mes_anterior2,
                     IF (c.res_estabvl_real > d.res_estabvl_real, 'fa fa-arrow-up', 'fa fa-arrow-down') as arrow_c,
                
                     d.res_estabvl_real as realizado_mes_anterior3,
                     (d.res_estabvl_real/d.res_estabvl_meta) as percentual_mes_anterior3,
                     IF (d.res_estabvl_real > f.res_estabvl_real, 'fa fa-arrow-up', 'fa fa-arrow-down') as arrow_d
                
                FROM 
                    res_estab AS a
                    LEFT JOIN res_estab AS b ON a.res_estabcnpj_estab = b.res_estabcnpj_estab
                    LEFT JOIN res_estab AS c ON b.res_estabcnpj_estab = c.res_estabcnpj_estab
                    LEFT JOIN res_estab AS d ON c.res_estabcnpj_estab = d.res_estabcnpj_estab
                    LEFT JOIN empresa AS e ON a.res_estabcnpj_estab = e.empresacnpj_estab
                    LEFT JOIN res_estab AS f ON d.res_estabcnpj_estab = f.res_estabcnpj_estab 
                WHERE 
                    a.res_estabcnpj_estab = 1 or a.res_estabcnpj_estab = 2
                AND a.res_estabdt_movto BETWEEN CAST('2020-4-01' AS DATE) AND CAST('2020-04-20' AS DATE) IS NOT NULL
                AND b.res_estabdt_movto BETWEEN CAST('2020-3-01' AS DATE) AND CAST('2020-03-20' AS DATE)IS NOT NULL
                AND c.res_estabdt_movto BETWEEN CAST('2020-2-01' AS DATE) AND CAST('2020-02-20' AS DATE)IS NOT NULL
                AND d.res_estabdt_movto BETWEEN CAST('2020-1-01' AS DATE) AND CAST('2020-01-20' AS DATE)IS NOT NULL
                -- AND f.res_estabdt_movto BETWEEN CAST('2020-5-01' AS DATE) AND CAST('2020-05-31' AS DATE)";

        $stmt = Model::getConn()->prepare($sql);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        } else {
            return [];
        }
    }

    // Accordion Grupo de Produto
    public function grupo_produtoData($mes)
    {

        $periodos = $this->findPeriod($mes, 5);

        $sql = "SELECT 
                    e.grupo_proddescricao                         as grupo,
                    a.res_gru_prodvl_meta                         as meta_mes_atual,
                    a.res_gru_prodvl_real                         as realizado_mes_atual,
                    (a.res_gru_prodvl_real/a.res_gru_prodvl_meta)*100 as percentual_mes_atual,
                    IF (a.res_gru_prodvl_real > b.res_gru_prodvl_real, 'fa fa-arrow-up', 'fa fa-arrow-down') as arrow_a,
                    b.res_gru_prodvl_real                         as realizado_mes_anterior1,
                    (b.res_gru_prodvl_real/b.res_gru_prodvl_meta)*100 as percentual_mes_anterior1,
                    IF (b.res_gru_prodvl_real > c.res_gru_prodvl_real, 'fa fa-arrow-up', 'fa fa-arrow-down') as arrow_b,
                    c.res_gru_prodvl_real                         as realizado_mes_anterior2,
                    (c.res_gru_prodvl_real/b.res_gru_prodvl_meta)*100 as percentual_mes_anterior2,
                    IF (c.res_gru_prodvl_real > d.res_gru_prodvl_real, 'fa fa-arrow-up', 'fa fa-arrow-down') as arrow_c,
                    d.res_gru_prodvl_real                         as realizado_mes_anterior3,
                    (d.res_gru_prodvl_real/d.res_gru_prodvl_meta)*100 as percentual_mes_anterior3,
                    IF (d.res_gru_prodvl_real > f.res_gru_prodvl_real, 'fa fa-arrow-up', 'fa fa-arrow-down') as arrow_d
                FROM 
                    res_gru_prod AS a
                    LEFT JOIN res_gru_prod AS b ON a.res_gru_prodcnpj_estab = b.res_gru_prodcnpj_estab
                    LEFT JOIN res_gru_prod AS c ON a.res_gru_prodcnpj_estab = c.res_gru_prodcnpj_estab
                    LEFT JOIN res_gru_prod AS d ON a.res_gru_prodcnpj_estab = c.res_gru_prodcnpj_estab
                    INNER JOIN grupo_prod AS e ON a.res_gru_prodcnpj_estab = e.grupo_prodcnpj_estab
                    LEFT JOIN res_gru_prod AS f ON a.res_gru_prodcnpj_estab = f.res_gru_prodcnpj_estab
                WHERE 
                    a.res_gru_prodcnpj_estab = '{$_SESSION['estab']}'
                    AND a.res_gru_proddt_movto BETWEEN CAST('{$periodos[1][0]}' AS DATE) AND CAST('{$periodos[1][1]}' AS DATE)
                    AND b.res_gru_proddt_movto BETWEEN CAST('{$periodos[2][0]}' AS DATE) AND CAST('{$periodos[2][1]}' AS DATE)
                    AND c.res_gru_proddt_movto BETWEEN CAST('{$periodos[3][0]}' AS DATE) AND CAST('{$periodos[3][1]}' AS DATE)
                    AND d.res_gru_proddt_movto BETWEEN CAST('{$periodos[4][0]}' AS DATE) AND CAST('{$periodos[4][1]}' AS DATE)
                    AND f.res_gru_proddt_movto BETWEEN CAST('{$periodos[0][0]}' AS DATE) AND CAST('{$periodos[0][1]}' AS DATE)";

        $stmt = Model::getConn()->prepare($sql);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        } else {
            return [];
        }
    }

    // Accordion Unidade de Negócio
    public function unid_negData($mes)
    {

        $periodos = $this->findPeriod($mes, 5);

        $sql = "SELECT 
	            	b.unid_negdescricao                         as unid_neg,
	            	a.res_unid_negvl_real                         as meta_mes_atual,
	            	a.res_unid_negvl_meta                         as realizado_mes_atual,
	            	(a.res_unid_negvl_real/a.res_unid_negvl_meta)*100 as percentual_mes_atual,
	            	IF (a.res_unid_negvl_real > c.res_unid_negvl_real, 'fa fa-arrow-up', 'fa fa-arrow-down') as arrow_a,

	            	c.res_unid_negvl_real                         as realizado_mes_anterior1,
	            	(c.res_unid_negvl_real/c.res_unid_negvl_meta)*100 as percentual_mes_anterior1,
	            	IF (c.res_unid_negvl_real > d.res_unid_negvl_real, 'fa fa-arrow-up', 'fa fa-arrow-down') as arrow_b,

	            	d.res_unid_negvl_real                         as realizado_mes_anterior2,
	            	(d.res_unid_negvl_real/d.res_unid_negvl_meta)*100 as percentual_mes_anterior2,
	            	IF (d.res_unid_negvl_real > e.res_unid_negvl_real, 'fa fa-arrow-up', 'fa fa-arrow-down') as arrow_c,

	            	e.res_unid_negvl_real                         as realizado_mes_anterior3,
	            	(e.res_unid_negvl_real/e.res_unid_negvl_meta)*100 as percentual_mes_anterior3
	            	-- IF (e.res_unid_negvl_real > f.res_unid_negvl_real, 'fa fa-arrow-up', 'fa fa-arrow-down') as arrow_d
	            FROM 
	            	res_unid_neg AS a
	            	INNER JOIN unid_neg AS b ON a.res_unid_negcnpj_estab = b.unid_negcnpj_estab
	            	LEFT JOIN res_unid_neg AS c ON a.res_unid_negcnpj_estab = c.res_unid_negcnpj_estab
	            	LEFT JOIN res_unid_neg AS d ON a.res_unid_negcnpj_estab = d.res_unid_negcnpj_estab
	            	LEFT JOIN res_unid_neg AS e ON a.res_unid_negcnpj_estab = e.res_unid_negcnpj_estab
	            	-- LEFT JOIN res_unid_neg AS f ON a.res_unid_negcnpj_estab = f.res_unid_negcnpj_estab
	            WHERE 
	            	a.res_unid_negcnpj_estab = 1
	            	AND a.res_unid_negdt_movto BETWEEN CAST('2020-04-01' AS DATE) AND CAST('2020-04-20' AS DATE)
	            	-- b : campo do nome
	            	AND c.res_unid_negdt_movto BETWEEN CAST('2020-03-01' AS DATE) AND CAST('2020-03-20' AS DATE)
	            	-- AND d.res_unid_negdt_movto BETWEEN CAST('2020-02-01' AS DATE) AND CAST('2020-02-20' AS DATE)
	            	-- AND e.res_unid_negdt_movto BETWEEN CAST('2020-01-01' AS DATE) AND CAST('2020-01-20' AS DATE)
	            	-- AND f.res_unid_negdt_movto BETWEEN CAST('2020-04-01' AS DATE) AND CAST('2020-04-20' AS DATE)";

        $stmt = Model::getConn()->prepare($sql);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        } else {
            return [];
        }
    }

    // Accordion Gerentes
    public function gerentesData($mes)
    {

        $periodos = $this->findPeriod($mes, 5);

        $sql = "SELECT 
                    b.user_perfilnome_completo                    as gerente,
                    a.res_gerentevl_meta                         as meta_mes_atual,
                    a.res_gerentevl_meta                         as realizado_mes_atual,
                    (a.res_gerentevl_real/a.res_gerentevl_meta)*100 as percentual_mes_atual,
                    IF (a.res_gerentevl_meta > c.res_gerentevl_real, 'fa fa-arrow-up', 'fa fa-arrow-down') as arrow_a,
                
                    c.res_gerentevl_real                         as realizado_mes_anterior1,
                    (c.res_gerentevl_real/c.res_gerentevl_meta)*100 as percentual_mes_anterior1,
                    IF (c.res_gerentevl_real > d.res_gerentevl_real, 'fa fa-arrow-up', 'fa fa-arrow-down') as arrow_b,
                
                    d.res_gerentevl_real                         as realizado_mes_anterior2,
                    (d.res_gerentevl_real/d.res_gerentevl_meta)*100 as percentual_mes_anterior2,
                    IF (d.res_gerentevl_real > e.res_gerentevl_real, 'fa fa-arrow-up', 'fa fa-arrow-down') as arrow_c,
                
                    e.res_gerentevl_real                         as realizado_mes_anterior3,
                    (e.res_gerentevl_real/e.res_gerentevl_meta)*100 as percentual_mes_anterior3,
                    IF (e.res_gerentevl_real > f.res_gerentevl_real, 'fa fa-arrow-up', 'fa fa-arrow-down') as arrow_d
                FROM 
                    res_gerente AS a
                    INNER JOIN user_perfil AS b ON a.res_gerentecod_gerente = b.user_perfilcod_user
                    LEFT JOIN res_gerente AS c ON a.res_gerentecod_gerente = c.res_gerentecod_gerente
                    LEFT JOIN res_gerente AS d ON a.res_gerentecod_gerente = d.res_gerentecod_gerente
                    LEFT JOIN res_gerente AS e ON a.res_gerentecod_gerente = e.res_gerentecod_gerente
                    LEFT JOIN res_gerente AS f ON a.res_gerentecod_gerente = f.res_gerentecod_gerente
                WHERE 
                    a.res_gerentecnpj_estab = 1
                    AND a.res_gerentedt_movto BETWEEN CAST('2020-03-01' AS DATE) AND CAST('2020-04-20' AS DATE) IS NOT NULL
                    -- b : campo do nome
                    AND c.res_gerentedt_movto BETWEEN CAST('2020-05-01' AS DATE) AND CAST('2020-02-20' AS DATE) IS NOT NULL
                    AND d.res_gerentedt_movto BETWEEN CAST('2020-03-01' AS DATE) AND CAST('2020-04-20' AS DATE) IS NOT NULL
                    AND e.res_gerentedt_movto BETWEEN CAST('2020-03-01' AS DATE) AND CAST('2020-04-20' AS DATE) IS NOT NULL
                    AND f.res_gerentedt_movto BETWEEN CAST('2020-03-01' AS DATE) AND CAST('2020-04-20' AS DATE)
                    GROUP BY b.user_perfilnome_completo";

        $stmt = Model::getConn()->prepare($sql);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        } else {
            return [];
        }
    }

    // Accordion Vendedores
    public function vendedoresData($mes)
    {

        $periodos = $this->findPeriod($mes, 5);

        $sql = "SELECT 
                    b.user_perfilnome_completo                    as vendedor,
                    a.res_vendedorvl_meta                         as meta_mes_atual,
                    a.res_vendedorvl_meta                         as realizado_mes_atual,
                    (a.res_vendedorvl_real/a.res_vendedorvl_meta)*100 as percentual_mes_atual,
                    IF (a.res_vendedorvl_meta > c.res_vendedorvl_real, 'fa fa-arrow-up', 'fa fa-arrow-down') as arrow_a,

                    c.res_vendedorvl_real                         as realizado_mes_anterior1,
                    (c.res_vendedorvl_real/c.res_vendedorvl_meta)*100 as percentual_mes_anterior1,
                    IF (c.res_vendedorvl_real > d.res_vendedorvl_real, 'fa fa-arrow-up', 'fa fa-arrow-down') as arrow_b,

                    d.res_vendedorvl_real                         as realizado_mes_anterior2,
                    (d.res_vendedorvl_real/d.res_vendedorvl_meta)*100 as percentual_mes_anterior2,
                    IF (d.res_vendedorvl_real > e.res_vendedorvl_real, 'fa fa-arrow-up', 'fa fa-arrow-down') as arrow_c,

                    e.res_vendedorvl_real                         as realizado_mes_anterior3,
                    (e.res_vendedorvl_real/e.res_vendedorvl_meta)*100 as percentual_mes_anterior3,
                    IF (e.res_vendedorvl_real > f.res_vendedorvl_real, 'fa fa-arrow-up', 'fa fa-arrow-down') as arrow_d
                FROM 
                    res_vendedor AS a
                    INNER JOIN user_perfil AS b ON a.res_vendedorcod_vendedor = b.user_perfilcod_user
                    LEFT JOIN res_vendedor AS c ON a.res_vendedorcod_gerente = c.res_vendedorcod_gerente
                    LEFT JOIN res_vendedor AS d ON a.res_vendedorcod_gerente = d.res_vendedorcod_gerente
                    LEFT JOIN res_vendedor AS e ON a.res_vendedorcod_gerente = e.res_vendedorcod_gerente
                    LEFT JOIN res_vendedor AS f ON a.res_vendedorcod_gerente = f.res_vendedorcod_gerente
                WHERE 
                    a.res_vendedorcnpj_estab = 1
                    AND a.res_vendedordt_movto BETWEEN CAST('2020-03-01' AS DATE) AND CAST('2020-04-20' AS DATE) IS NOT NULL
                    -- b : campo do nome
                    AND c.res_vendedordt_movto BETWEEN CAST('2020-05-01' AS DATE) AND CAST('2020-02-20' AS DATE) IS NOT NULL
                    AND d.res_vendedordt_movto BETWEEN CAST('2020-03-01' AS DATE) AND CAST('2020-04-20' AS DATE) IS NOT NULL
                    AND e.res_vendedordt_movto BETWEEN CAST('2020-03-01' AS DATE) AND CAST('2020-04-20' AS DATE) IS NOT NULL
                    AND f.res_vendedordt_movto BETWEEN CAST('2020-03-01' AS DATE) AND CAST('2020-04-20' AS DATE)
                    GROUP BY b.user_perfilnome_completo";

        $stmt = Model::getConn()->prepare($sql);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        } else {
            return [];
        }
    }

    // Accordion Grupo de Produtos por Vendedor
    public function grupo_produto_vendedoresData($mes)
    {

        $periodos = $this->findPeriod($mes, 5);

        $sql = "SELECT 
                    b.user_perfilnome_completo                   as vendedor,
                    a.res_gerentevl_meta                         as meta_mes_atual,
                    a.res_gerentevl_meta                         as realizado_mes_atual,
                    (a.res_gerentevl_real/a.res_gerentevl_meta)*100 as percentual_mes_atual,
                    IF (a.res_gerentevl_meta > c.res_gerentevl_real, 'fa fa-arrow-up', 'fa fa-arrow-down') as arrow_a,

                    c.res_gerentevl_real                         as realizado_mes_anterior1,
                    (c.res_gerentevl_real/c.res_gerentevl_meta)*100 as percentual_mes_anterior1,
                    IF (c.res_gerentevl_real > d.res_gerentevl_real, 'fa fa-arrow-up', 'fa fa-arrow-down') as arrow_b,

                    d.res_gerentevl_real                         as realizado_mes_anterior2,
                    (d.res_gerentevl_real/d.res_gerentevl_meta)*100 as percentual_mes_anterior2,
                    IF (d.res_gerentevl_real > e.res_gerentevl_real, 'fa fa-arrow-up', 'fa fa-arrow-down') as arrow_c,

                    e.res_gerentevl_real                         as realizado_mes_anterior3,
                    (e.res_gerentevl_real/e.res_gerentevl_meta)*100 as percentual_mes_anterior3,
                    IF (e.res_gerentevl_real > f.res_gerentevl_real, 'fa fa-arrow-up', 'fa fa-arrow-down') as arrow_d
                FROM 
                    res_gerente AS a
                    INNER JOIN user_perfil AS b ON a.res_gerentecod_gerente = b.user_perfilcod_user
                    LEFT JOIN res_gerente AS c ON a.res_gerentecod_gerente = c.res_gerentecod_gerente
                    LEFT JOIN res_gerente AS d ON a.res_gerentecod_gerente = d.res_gerentecod_gerente
                    LEFT JOIN res_gerente AS e ON a.res_gerentecod_gerente = e.res_gerentecod_gerente
                    LEFT JOIN res_gerente AS f ON a.res_gerentecod_gerente = f.res_gerentecod_gerente
                WHERE 
                    a.res_gerentecnpj_estab = 1
                    AND a.res_gerentedt_movto BETWEEN CAST('2020-03-01' AS DATE) AND CAST('2020-04-20' AS DATE) IS NOT NULL
                    -- b : campo do nome
                    AND c.res_gerentedt_movto BETWEEN CAST('2020-05-01' AS DATE) AND CAST('2020-02-20' AS DATE) IS NOT NULL
                    AND d.res_gerentedt_movto BETWEEN CAST('2020-03-01' AS DATE) AND CAST('2020-04-20' AS DATE) IS NOT NULL
                    AND e.res_gerentedt_movto BETWEEN CAST('2020-03-01' AS DATE) AND CAST('2020-04-20' AS DATE) IS NOT NULL
                    AND f.res_gerentedt_movto BETWEEN CAST('2020-03-01' AS DATE) AND CAST('2020-04-20' AS DATE)
                    GROUP BY b.user_perfilnome_completo";

        $stmt = Model::getConn()->prepare($sql);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        } else {
            return [];
        }
    }

    // Accordion Produtos
    public function produtosData($mes)
    {

        $periodos = $this->findPeriod($mes, 5);

        $sql = "SELECT 
                    b.user_perfilnome_completo                   as produtos,
                    a.res_gerentevl_meta                         as meta_mes_atual,
                    a.res_gerentevl_meta                         as realizado_mes_atual,
                    (a.res_gerentevl_real/a.res_gerentevl_meta)*100 as percentual_mes_atual,
                    IF (a.res_gerentevl_meta > c.res_gerentevl_real, 'fa fa-arrow-up', 'fa fa-arrow-down') as arrow_a,

                    c.res_gerentevl_real                         as realizado_mes_anterior1,
                    (c.res_gerentevl_real/c.res_gerentevl_meta)*100 as percentual_mes_anterior1,
                    IF (c.res_gerentevl_real > d.res_gerentevl_real, 'fa fa-arrow-up', 'fa fa-arrow-down') as arrow_b,

                    d.res_gerentevl_real                         as realizado_mes_anterior2,
                    (d.res_gerentevl_real/d.res_gerentevl_meta)*100 as percentual_mes_anterior2,
                    IF (d.res_gerentevl_real > e.res_gerentevl_real, 'fa fa-arrow-up', 'fa fa-arrow-down') as arrow_c,

                    e.res_gerentevl_real                         as realizado_mes_anterior3,
                    (e.res_gerentevl_real/e.res_gerentevl_meta)*100 as percentual_mes_anterior3,
                    IF (e.res_gerentevl_real > f.res_gerentevl_real, 'fa fa-arrow-up', 'fa fa-arrow-down') as arrow_d
                FROM 
                    res_gerente AS a
                    INNER JOIN user_perfil AS b ON a.res_gerentecod_gerente = b.user_perfilcod_user
                    LEFT JOIN res_gerente AS c ON a.res_gerentecod_gerente = c.res_gerentecod_gerente
                    LEFT JOIN res_gerente AS d ON a.res_gerentecod_gerente = d.res_gerentecod_gerente
                    LEFT JOIN res_gerente AS e ON a.res_gerentecod_gerente = e.res_gerentecod_gerente
                    LEFT JOIN res_gerente AS f ON a.res_gerentecod_gerente = f.res_gerentecod_gerente
                WHERE 
                    a.res_gerentecnpj_estab = 1
                    AND a.res_gerentedt_movto BETWEEN CAST('2020-03-01' AS DATE) AND CAST('2020-04-20' AS DATE) IS NOT NULL
                    -- b : campo do nome
                    AND c.res_gerentedt_movto BETWEEN CAST('2020-05-01' AS DATE) AND CAST('2020-02-20' AS DATE) IS NOT NULL
                    AND d.res_gerentedt_movto BETWEEN CAST('2020-03-01' AS DATE) AND CAST('2020-04-20' AS DATE) IS NOT NULL
                    AND e.res_gerentedt_movto BETWEEN CAST('2020-03-01' AS DATE) AND CAST('2020-04-20' AS DATE) IS NOT NULL
                    AND f.res_gerentedt_movto BETWEEN CAST('2020-03-01' AS DATE) AND CAST('2020-04-20' AS DATE)
                    GROUP BY b.user_perfilnome_completo";

        $stmt = Model::getConn()->prepare($sql);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        } else {
            return [];
        }
    }

    // encontra periodo conforme data informada no input DATE
    function findPeriod($date, $period)
    {

        $year_input = date("Y", strtotime($date));
        // pega ultimos "-$period" meses para comparações dos dados
        $previousMonths = array_map('intval', range(-$period, -1));

        foreach ($previousMonths as $key => $value) {
            $months[$key] = abs($value);
            $firstDayOfMonth[$key] = $year_input . $value . "-01";
            $lastDayofMonth[$key] = date("Y-m-t", strtotime($firstDayOfMonth[$key]));
            $return[$key] = array($firstDayOfMonth[$key], $lastDayofMonth[$key]);
        }
        return $return;
    }

    // transforma meses anteriores em string
    function getMonthStr($date)
    {
        return date("F", strtotime("$date months"));
    }
}
