<?php

use App\Core\Model;

class ResVendedor extends Model
{

    public $res_vendedorcnpj_estab;
    public $res_vendedorvl_meta;
    public $res_vendedorvl_minimo;
    public $res_vendedorvl_variavel;
    public $res_vendedorvl_adic;
    public $res_vendedorvl_bonus;
    public $res_vendedordt_movto;
    public $res_vendedorcod_unid_neg;
    public $res_vendedorcod_gerente;
    public $res_vendedorcod_vendedor;
    public $res_vendedorvl_real;

    public function save()
    {

        $sql = "INSERT INTO res_vendedor(res_vendedorcnpj_estab,res_vendedordt_movto,res_vendedorcod_unid_neg,res_vendedorcod_gerente,res_vendedorcod_vendedor,res_vendedorvl_real)VALUES(?,?,?,?,?,?)";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$this->res_vendedorcnpj_estab);
        $stmt->bindValue(2,$this->res_vendedordt_movto);
        $stmt->bindValue(3,$this->res_vendedorcod_unid_neg);
        $stmt->bindValue(4,$this->res_vendedorcod_gerente);
        $stmt->bindValue(5,$this->res_vendedorcod_vendedor);
        $stmt->bindValue(6,$this->res_vendedorvl_real);

        if($stmt->execute()):
            return "Cadastrado com sucesso";
        else:
            return "Erro ao cadastrar";
        endif;

    }

    public function update($id)
    {
        $sql = "UPDATE res_vendedor SET res_vendedorcnpj_estab = ?,res_vendedordt_movto = ?,res_vendedorcod_unid_neg = ?,res_vendedorcod_gerente = ?,res_vendedorcod_vendedor = ?,res_vendedorvl_real = ? WHERE res_vendedorid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$this->res_vendedorcnpj_estab);
        $stmt->bindValue(2,$this->res_vendedordt_movto);
        $stmt->bindValue(3,$this->res_vendedorcod_unid_neg);
        $stmt->bindValue(4,$this->res_vendedorcod_gerente);
        $stmt->bindValue(5,$this->res_vendedorcod_vendedor);
        $stmt->bindValue(6,$this->res_vendedorvl_real);
        $stmt->bindValue(7,$id);

        if($stmt->execute()):
            return "Atualizado com sucesso";
        else:
            return "Erro ao atualizar";
        endif;
    }

    public function delete($id)
    {
        $sql = "DELETE FROM res_vendedor WHERE res_vendedorid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$id);

        if($stmt->execute()):
            return "Excluido com sucesso";
        else:
            return "Erro ao excluir";
        endif;
    }

    public function deleteLimpa()
    {
        $sql = "DELETE FROM res_vendedor";
        $stmt = Model::getConn()->prepare($sql);

        if($stmt->execute()):
            return "Excluido com sucesso";
        else:
            return "Erro ao excluir";
        endif;
    }

    public function getAll()
    {
        $sql = "SELECT * FROM res_vendedor";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

    public function getAllEstabelecimento($id)
    {
        $sql = "SELECT * FROM res_vendedor WHERE res_vendedorcnpj_estab = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$id);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

    public function findId($id)
    {
        $sql = "SELECT * FROM res_vendedor WHERE res_vendedorid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$id);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

}



