<?php

use App\Core\Model;

class MetaLog extends Model
{

    public $meta_logcnpj_estab;
    public $meta_logcod_meta;
    public $meta_logdt_emis;
    public $meta_loghr_emis;
    public $meta_loguser_emis;
    public $meta_logtitulo;

    public function save()
    {
        $sql = "INSERT INTO meta_log(meta_logcnpj_estab,meta_logcod_meta,meta_logdt_emis,meta_loghr_emis,meta_loguser_emis,meta_logtitulo)VALUES(?,?,?,?,?,?)";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$this->meta_logcnpj_estab);
        $stmt->bindValue(2,$this->meta_logcod_meta);
        $stmt->bindValue(3,$this->meta_logdt_emis);
        $stmt->bindValue(4,$this->meta_loghr_emis);
        $stmt->bindValue(5,$this->meta_loguser_emis);
        $stmt->bindValue(6,$this->meta_logtitulo);


        if($stmt->execute()):
            return "Cadastrado com sucesso";
        else:
            return "Erro ao cadastrar Log";
        endif;

    }

    public function getAll()
    {
        $sql = "SELECT * FROM meta_log";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

    public function findId($id)
    {
        $sql = "SELECT * FROM meta_log WHERE meta_logcod_meta = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$id);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }
}


