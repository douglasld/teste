<?php

use App\Core\Model;

class Venda extends Model
{

    public $vendascnpj_estab;
    public $vendascod_meta;
    public $vendasdescricao;
    public $vendasserie_docto;
    public $vendasnr_docto;
    public $vendasdt_docto;
    public $vendascod_produto;
    public $vendasqtvendida;
    public $vendascod_unid_med;
    public $vendasvl_unit;
    public $vendasvl_total;
    public $vendasnome_vendedor;
    public $vendasind_ativo;
    public $vendasind_origem;
    public $vendasuser_criacao;
    public $vendasdt_criacao;
    public $vendasuser_altera;
    public $vendasdt_altera;
    public $vendascod_unid_neg;
    public $vendashr_altera;
    public $vendashr_criacao;
    public $vendasgrupoprod;
    public $vendasgerente;

    public function save()
    {

        $sql = "INSERT INTO vendas(vendascnpj_estab,vendascod_meta,vendasdescricao,vendasserie_docto,vendasnr_docto,vendasdt_docto,vendascod_produto,vendasqt_vendida,vendascod_unid_med,vendasvl_unit,vendasvl_total,vendasnome_vendedor,vendasind_ativo,vendasind_origem,vendasuser_criacao,vendasdt_criacao,vendasuser_altera,vendasdt_altera,vendascod_unid_neg,vendashr_altera,vendashr_criacao,vendasgerente,vendasgrupoprod)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$this->vendascnpj_estab);
        $stmt->bindValue(2,$this->vendascod_meta);
        $stmt->bindValue(3,$this->vendasdescricao);
        $stmt->bindValue(4,$this->vendasserie_docto);
        $stmt->bindValue(5,$this->vendasnr_docto);
        $stmt->bindValue(6,$this->vendasdt_docto);
        $stmt->bindValue(7,$this->vendascod_produto);
        $stmt->bindValue(8,$this->vendasqtvendida);
        $stmt->bindValue(9,$this->vendascod_unid_med);
        $stmt->bindValue(10,$this->vendasvl_unit);
        $stmt->bindValue(11,$this->vendasvl_total);
        $stmt->bindValue(12,$this->vendasnome_vendedor);
        $stmt->bindValue(13,$this->vendasind_ativo);
        $stmt->bindValue(14,$this->vendasind_origem);
        $stmt->bindValue(15,$this->vendasuser_criacao);
        $stmt->bindValue(16,$this->vendasdt_criacao);
        $stmt->bindValue(17,$this->vendasuser_altera);
        $stmt->bindValue(18,$this->vendasdt_altera);
        $stmt->bindValue(19,$this->vendascod_unid_neg);
        $stmt->bindValue(20,$this->vendashr_altera);
        $stmt->bindValue(21,$this->vendashr_criacao);
        $stmt->bindValue(22,$this->vendasgerente);
        $stmt->bindValue(23,$this->vendasgrupoprod);

        if($stmt->execute()):
            return "Cadastrado com sucesso";
        else:
            return "Erro ao cadastrar";
        endif;

    }

    public function update($id)
    {

        $sql = "UPDATE vendas SET vendascnpj_estab = ?,vendascod_meta = ?,vendasdescricao = ?,vendasserie_docto = ?,vendasnr_docto = ?,vendasdt_docto = ?,vendascod_produto = ?,vendasqt_vendida = ?,vendascod_unid_med = ?,vendasvl_unit = ?,vendasvl_total = ?,vendasnome_vendedor = ?,vendasind_ativo = ?,vendasind_origem = ?,vendasuser_altera = ?,vendasdt_altera = ?,vendascod_unid_neg = ?,vendashr_altera = ?,vendasgerente = ? WHERE vendasid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$this->vendascnpj_estab);
        $stmt->bindValue(2,$this->vendascod_meta);
        $stmt->bindValue(3,$this->vendasdescricao);
        $stmt->bindValue(4,$this->vendasserie_docto);
        $stmt->bindValue(5,$this->vendasnr_docto);
        $stmt->bindValue(6,$this->vendasdt_docto);
        $stmt->bindValue(7,$this->vendascod_produto);
        $stmt->bindValue(8,$this->vendasqt_vendida);
        $stmt->bindValue(9,$this->vendascod_unid_med);
        $stmt->bindValue(10,$this->vendasvl_unit);
        $stmt->bindValue(11,$this->vendasvl_total);
        $stmt->bindValue(12,$this->vendasnome_vendedor);
        $stmt->bindValue(13,$this->vendasind_ativo);
        $stmt->bindValue(14,$this->vendasind_origem);
        $stmt->bindValue(15,$this->vendasuser_altera);
        $stmt->bindValue(16,$this->vendasdt_altera);
        $stmt->bindValue(17,$this->vendascod_unid_neg);
        $stmt->bindValue(18,$this->vendashr_altera);
        $stmt->bindValue(19,$this->vendasgerente);
        $stmt->bindValue(20,$id);



        if($stmt->execute()):
            return "Atualizado com sucesso";
        else:
            return "Erro ao atualizar";
        endif;
    }

    public function delete($id)
    {
        $sql = "DELETE FROM vendas WHERE vendasid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$id);

        if($stmt->execute()):
            return "Excluido com sucesso";
        else:
            return "Erro ao excluir";
        endif;
    }

    public function getAll()
    {
        $sql = "SELECT * FROM vendas";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

    public function findId($id)
    {
        $sql = "SELECT * FROM vendas WHERE vendasid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$id);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

    public function ultima($estab, $usuario){
        $sql = "SELECT * FROM vendas WHERE vendascnpj_estab = ? AND vendasuser_criacao = ? ORDER BY vendasid DESC";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$estab);
        $stmt->bindValue(2,$usuario);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

    //Valor total das vendas por estabelecimento

    public function somaVendasEstabelecimento($estab){
        $sql = "SELECT sum(vendasvl_total) as soma from vendas where vendascnpj_estab = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$estab);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $resultado;
    }

    //Valor total de vendas por produto/estabelecimento

    public function somaVendasEstabelecimentoProduto($estab,$produto){
        $sql = "SELECT sum(vendasvl_total) as soma from vendas where vendascnpj_estab = ? AND vendascod_produto = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$estab);
        $stmt->bindValue(2,$produto);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $resultado;
    }

    //vendas por data

    public function somaVendasData($data){
        $sql = "SELECT sum(vendasvl_total) as soma from vendas where vendasdt_docto = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$data);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $resultado;
    }


    public function somaVendasEstabelecimentoProdutoData($estab,$produto,$data,$gerente,$unidade,$vendedor,$grupo){
        $sql = "SELECT sum(vendasvl_total) as soma from vendas a";
        $sql = $sql." WHERE a.vendascnpj_estab = ? AND a.vendascod_produto = ? AND a.vendasdt_docto = ?";
        $sql = $sql." AND a.vendasgerente = ? AND a.vendascod_unid_neg = ? AND a.vendasnome_vendedor = ?";
        $sql = $sql." AND a.vendasgrupoprod = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$estab);
        $stmt->bindValue(2,$produto);
        $stmt->bindValue(3,$data);
        $stmt->bindValue(4,$gerente);
        $stmt->bindValue(5,$unidade);
        $stmt->bindValue(6,$vendedor);
        $stmt->bindValue(7,$grupo);
        //$stmt->bindValue(7,$grupo);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $resultado;
    }

    public function somaVendedorDataEstabUnidGerente($vendedor,$data,$estab,$unidade,$gerente){
        $sql = "SELECT sum(vendasvl_total) as soma from vendas a";
        $sql = $sql." WHERE a.vendasnome_vendedor = ? AND a.vendasdt_docto = ?";
        $sql = $sql." AND a.vendascnpj_estab = ? AND a.vendascod_unid_neg = ?";
        $sql = $sql." AND a.vendasgerente = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$vendedor);
        $stmt->bindValue(2,$data);
        $stmt->bindValue(3,$estab);
        $stmt->bindValue(4,$unidade);
        $stmt->bindValue(5,$gerente);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $resultado;
}

public function somaGerenteEstabDataUnid($gerente,$estab,$data,$unidade){
        $sql = "SELECT sum(vendasvl_total) as soma from vendas a";
        $sql = $sql." WHERE a.vendasgerente = ? AND a.vendascnpj_estab = ? AND a.vendasdt_docto = ? AND a.vendascod_unid_neg = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$gerente);
        $stmt->bindValue(2,$estab);
        $stmt->bindValue(3,$data);
        $stmt->bindValue(4,$unidade);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $resultado;
}

    public function somaGrupoEstabDataUnidadeGerenteVendedor($grupo,$estab,$data,$unidade,$gerente,$vendedor){
        $sql = "SELECT sum(vendasvl_total) as soma from vendas a";
        $sql = $sql." WHERE a.vendasgrupoprod = ? AND a.vendascnpj_estab = ? AND a.vendasdt_docto = ? AND a.vendascod_unid_neg = ? AND a.vendasgerente = ? AND a.vendasnome_vendedor = ? ";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$grupo);
        $stmt->bindValue(2,$estab);
        $stmt->bindValue(3,$data);
        $stmt->bindValue(4,$unidade);
        $stmt->bindValue(5,$gerente);
        $stmt->bindValue(6,$vendedor);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $resultado;
    }


    public function somaEstabDataUnid($estab,$data,$unidade){
        $sql = "SELECT sum(vendasvl_total) as soma from vendas a";
        $sql = $sql." WHERE a.vendascnpj_estab = ?";
        $sql = $sql." AND a.vendasdt_docto = ?";
        $sql = $sql." AND a.vendascod_unid_neg = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$estab);
        $stmt->bindValue(2,$data);
        $stmt->bindValue(3,$unidade);
        //$stmt->bindValue(7,$grupo);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $resultado;
    }

    public function somaVendasEstabelecimentoData($estab,$data){
        $sql = "SELECT sum(vendasvl_total) as soma from vendas where vendascnpj_estab = ? AND vendasdt_docto = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$estab);
        $stmt->bindValue(2,$data);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $resultado;
    }

    public function somaMatrizData($matriz,$data){
        $sql = "SELECT sum(vendasvl_total) as soma from vendas";
        $sql = $sql." LEFT JOIN empresa ON empresa.empresacnpj_estab = vendas.vendascnpj_estab";
        $sql = $sql." WHERE empresa.empresacnpj_matriz = ? AND vendas.vendasdt_docto = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$matriz);
        $stmt->bindValue(2,$data);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $resultado;
    }

    public function getSomaVendasEstabelecimentoData($estab,$data){
        $sql = "SELECT * as soma from vendas where vendascnpj_estab = ? AND vendasdt_docto = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$estab);
        $stmt->bindValue(2,$data);
        $stmt->execute();
        $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $resultado->rowCount();
    }

    public function getPorEstabelecimento($estab)
    {
        $sql = "SELECT * FROM vendas a LEFT JOIN empresa b ON b.empresacnpj_estab = a.vendascnpj_estab WHERE a.vendascnpj_estab = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$estab);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

    public function getPorEstabAgrupadoData($estab)
    {
        $sql = "SELECT * FROM vendas WHERE vendascnpj_estab = ? GROUP BY vendasdt_docto,vendascod_produto,vendasgerente,vendasnome_vendedor,vendascod_unid_neg";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$estab);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

    public function agrupadoEstabDataUnid()
    {
        $sql = "SELECT * FROM vendas GROUP BY vendascnpj_estab,vendasdt_docto,vendascod_unid_neg";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

    public function agrupadoVendedorDataEstabUnidGerente()
{
    $sql = "SELECT * FROM vendas GROUP BY vendasnome_vendedor,vendasdt_docto,vendascnpj_estab,vendascod_unid_neg,vendasgerente";
    $stmt = Model::getConn()->prepare($sql);
    $stmt->execute();

    if($stmt->rowCount()>0)
    {
        $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $resultado;
    }else{
        return [];
    }
}

    public function agrupadoGrupoEstabDataUnidGerenteVendedor()
    {
        $sql = "SELECT * FROM vendas GROUP BY vendasgrupoprod,vendascnpj_estab,vendasdt_docto,vendascod_unid_neg,vendasgerente,vendasnome_vendedor";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

    public function agrupadoVendedor()
    {
        $sql = "SELECT * FROM vendas GROUP BY vendasnome_vendedor";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

    public function agrupadoDataEstabUnidGerente($id)
    {
        $sql = "SELECT * FROM vendas WHERE vendasnome_vendedor = ? GROUP BY vendasdt_docto,vendascnpj_estab,vendascod_unid_neg,vendasgerente";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$id);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

    public function agrupadoEstabelecimento()
    {
        $sql = "SELECT * FROM vendas GROUP BY vendascnpj_estab";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

    public function agrupadoEstabData()
    {
        $sql = "SELECT * FROM vendas";
        $sql = $sql." LEFT JOIN empresa ON empresa.empresacnpj_estab = vendas.vendascnpj_estab";
        $sql = $sql." GROUP BY empresa.empresacnpj_matriz,vendas.vendasdt_docto";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

    public function agrupadoGerenteEstabDataUnid()
    {
        $sql = "SELECT * FROM vendas GROUP BY vendasgerente,vendascnpj_estab,vendasdt_docto,vendascod_unid_neg";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

    public function getPorData($data)
    {
        $sql = "SELECT * FROM vendas WHERE vendasdt_docto = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$data);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

    public function getPorDataEstab($data,$estab)
    {
        $sql = "SELECT * FROM vendas WHERE vendasdt_docto = ? AND vendascnpj_estab = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$data);
        $stmt->bindValue(2,$estab);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

    public function getPorProduto($produto)
    {
        $sql = "SELECT * FROM vendas WHERE vendascod_produto = ? ORDER BY vendascnpj_estab";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$produto);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

    public function getPorProdutoEstab($produto,$estab)
    {
        $sql = "SELECT * FROM vendas WHERE vendascod_produto = ? AND vendascnpj_estab = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$produto);
        $stmt->bindValue(2,$estab);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

    public function getPorEstabProdDataAgrupadoProduto($estab,$data)
    {
        $sql = "SELECT * FROM vendas WHERE vendascnpj_estab = ? AND vendasdt_docto = ? GROUP BY vendascod_produto";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$estab);
        $stmt->bindValue(2,$data);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }
}






