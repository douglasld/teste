<?php

use App\Core\Model;

class UnidNeg extends Model
{

    public $unid_negcnpj_estab;
    public $unid_negdescricao;
    public $unid_negind_ativo;
    public $unid_negind_origem;
    public $unid_negdt_criacao;
    public $unid_neghr_criacao;
    public $unid_neguser_criacao;
    public $unid_negdt_altera;
    public $unid_neghr_altera;
    public $unid_neguser_altera;

    public function save()
    {

        $sql = "INSERT INTO unid_neg(unid_negcnpj_estab,unid_negdescricao,unid_negind_ativo,unid_negind_origem,unid_negdt_criacao,unid_neghr_criacao,unid_neguser_criacao,unid_negdt_altera,unid_neghr_altera,unid_neguser_altera)VALUES(?,?,?,?,?,?,?,?,?,?)";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$this->unid_negcnpj_estab);
        $stmt->bindValue(2,$this->unid_negdescricao);
        $stmt->bindValue(3,$this->unid_negind_ativo);
        $stmt->bindValue(4,$this->unid_negind_origem);
        $stmt->bindValue(5,$this->unid_negdt_criacao);
        $stmt->bindValue(6,$this->unid_neghr_criacao);
        $stmt->bindValue(7,$this->unid_neguser_criacao);
        $stmt->bindValue(8,$this->unid_negdt_altera);
        $stmt->bindValue(9,$this->unid_neghr_altera);
        $stmt->bindValue(10,$this->unid_neguser_altera);

        if($stmt->execute()):
            return "Cadastrado com sucesso";
        else:
            return "Erro ao cadastrar";
        endif;

    }

    public function update($id)
    {
        $sql = "UPDATE unid_neg SET unid_negcnpj_estab=?,unid_negdescricao=?,unid_negind_ativo=?,unid_negind_origem=?,unid_negdt_criacao=?,unid_neghr_criacao=?,unid_neguser_criacao=?,unid_negdt_altera=?,unid_neghr_altera=?,unid_neguser_altera=? WHERE unid_negid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$this->unid_negcnpj_estab);
        $stmt->bindValue(2,$this->unid_negdescricao);
        $stmt->bindValue(3,$this->unid_negind_ativo);
        $stmt->bindValue(4,$this->unid_negind_origem);
        $stmt->bindValue(5,$this->unid_negdt_criacao);
        $stmt->bindValue(6,$this->unid_neghr_criacao);
        $stmt->bindValue(7,$this->unid_neguser_criacao);
        $stmt->bindValue(8,$this->unid_negdt_altera);
        $stmt->bindValue(9,$this->unid_neghr_altera);
        $stmt->bindValue(10,$this->unid_neguser_altera);
        $stmt->bindValue(11,$id);

        if($stmt->execute()):
            return "Atualizado com sucesso";
        else:
            return "Erro ao atualizar";
        endif;
    }

    public function delete($id)
    {
        $sql = "DELETE FROM unid_neg WHERE unid_negid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$id);

        if($stmt->execute()):
            return "Excluido com sucesso";
        else:
            return "Erro ao excluir";
        endif;
    }

    public function getAll()
    {
        $sql = "SELECT * FROM unid_neg";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

    public function getAllEstabelecimento($estab)
    {
        $sql = "SELECT * FROM unid_neg WHERE unid_negcnpj_estab = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$estab);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

    public function findId($id)
    {
        $sql = "SELECT * FROM unid_neg a LEFT JOIN empresa b ON b.empresacnpj_estab = a.unid_negcnpj_estab WHERE a.unid_negid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$id);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

    public function ultimo($estab,$usuario){
        $sql = "SELECT * FROM unid_neg WHERE unid_negcnpj_estab = ? AND unid_neguser_criacao = ? ORDER BY unid_negid DESC";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$estab);
        $stmt->bindValue(2,$usuario);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

    
    public function getindex(){

        $sql = "SELECT * FROM unid_neg";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        } else {
            return [];
        }
    }

    public function BuscageralEstab($estab)
    {
        $sql = "SELECT * FROM unid_neg WHERE unid_negcnpj_estab = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $estab);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $resultado;
    }

    public function GetInfoID($id)
    {
        $sql = "SELECT * FROM unid_neg WHERE unid_negid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $resultado;
    }

}





