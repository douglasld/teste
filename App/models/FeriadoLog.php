<?php

use App\Core\Model;

class FeriadoLog extends Model
{

    public $feriado_logcnpj_estab;
    public $feriado_logdt_feriado;
    public $feriado_loguser_emis;
    public $feriado_logdt_emis;
    public $feriado_loghr_emis;
    public $feriado_logtitulo;


    public function save()
    {

        $sql = "INSERT INTO feriado_log(feriado_logcnpj_estab,feriado_logdt_feriado,feriado_loguser_emis,feriado_logdt_emis,feriado_loghr_emis,feriado_logtitulo)VALUES(?,?,?,?,?,?)";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $this->feriado_logcnpj_estab);
        $stmt->bindValue(2, $this->feriado_logdt_feriado);
        $stmt->bindValue(3, $this->feriado_loguser_emis);
        $stmt->bindValue(4, $this->feriado_logdt_emis);
        $stmt->bindValue(5, $this->feriado_loghr_emis);
        $stmt->bindValue(6, $this->feriado_logtitulo);

        if ($stmt->execute()):
            return "Cadastrado com sucesso";
        else:
            return "Erro ao cadastrar";
        endif;

    }


    public function getAll($dataFeriado,$cnpjEstabelecimento)
    {
        $sql = "SELECT * FROM feriado_log WHERE feriado_logdt_feriado = ? AND feriado_logcnpj_estab = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $dataFeriado);
        $stmt->bindValue(2, $cnpjEstabelecimento);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        } else {
            return [];
        }
    }

}


