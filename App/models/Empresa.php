<?php

use App\Core\Model;

class Empresa extends Model
{

    public $empresacnpj_matriz;
    public $empresacnpj_estab;
    public $empresanome_abrev;
    public $empresarazao_social;
    public $empresaramo_atividade;
    public $empresarua;
    public $empresanumero;
    public $empresacomplemento;
    public $empresacidade;
    public $empresaestado;
    public $empresapais;
    public $empresacep;
    public $empresatelefone;
    public $empresacelular;
    public $empresaurl;
    public $empresae_mail;
    public $empresae_mail_nfe;
    public $empresaind_ativo;
    public $empresalogo_marca;
    public $empresaind_origem;
    public $empresadt_criacao;
    public $empresahr_criacao;
    public $empresadt_altera;
    public $empresahr_altera;
    public $empresauser_criacao;
    public $empresauser_altera;



    public function save()
    {
        $sql = "INSERT INTO empresa(empresacnpj_matriz,empresacnpj_estab,empresanome_abrev,empresarazao_social,
        empresaramo_atividade,empresarua,empresanumero,empresacomplemento,empresacidade,empresaestado,empresapais,
        empresacep,empresatelefone,empresacelular,empresaurl,empresae_mail,empresae_mail_nfe,empresaind_ativo,empresalogo_marca,
        empresaind_origem,empresadt_criacao,empresahr_criacao,empresauser_criacao,empresabairro)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $this->empresacnpj_matriz);
        $stmt->bindValue(2, $this->empresacnpj_estab);
        $stmt->bindValue(3, $this->empresanome_abrev);
        $stmt->bindValue(4, $this->empresarazao_social);
        $stmt->bindValue(5, $this->empresaramo_atividade);
        $stmt->bindValue(6, $this->empresarua);
        $stmt->bindValue(7, $this->empresanumero);
        $stmt->bindValue(8, $this->empresacomplemento);
        $stmt->bindValue(9, $this->empresacidade);
        $stmt->bindValue(10, $this->empresaestado);
        $stmt->bindValue(11, $this->empresapais);
        $stmt->bindValue(12, $this->empresacep);
        $stmt->bindValue(13, $this->empresatelefone);
        $stmt->bindValue(14, $this->empresacelular);
        $stmt->bindValue(15, $this->empresaurl);
        $stmt->bindValue(16, $this->empresae_mail);
        $stmt->bindValue(17, $this->empresae_mail_nfe);
        $stmt->bindValue(18, $this->empresaind_ativo);
        $stmt->bindValue(19, $this->empresalogo_marca);
        $stmt->bindValue(20, $this->empresaind_origem);
        $stmt->bindValue(21, $this->empresadt_criacao);
        $stmt->bindValue(22, $this->empresahr_criacao);
        $stmt->bindValue(23, $this->empresauser_criacao);
        $stmt->bindValue(24, $this->empresabairro);
    
        if ($stmt->execute()):
            return "Cadastrado com sucesso";
        else:
            return "Erro ao cadastrar";
        endif;

    }

    public function update($id)
    {
        $sql = "UPDATE empresa SET empresacnpj_matriz = ?,empresacnpj_estab = ?,empresanome_abrev = ?,empresarazao_social = ?,empresaramo_atividade = ?,empresarua = ?,empresanumero = ?,empresacomplemento = ?,empresacidade = ?,empresaestado = ?,empresapais = ?,empresacep = ?,empresatelefone = ?,empresacelular = ?,empresaurl = ?,empresae_mail = ?,empresae_mail_nfe = ?,empresaind_ativo = ?,empresaind_origem = ?,empresadt_altera = ?,empresahr_altera = ?,empresauser_altera = ? WHERE empresaid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $this->empresacnpj_matriz);
        $stmt->bindValue(2, $this->empresacnpj_estab);
        $stmt->bindValue(3, $this->empresanome_abrev);
        $stmt->bindValue(4, $this->empresarazao_social);
        $stmt->bindValue(5, $this->empresaramo_atividade);
        $stmt->bindValue(6, $this->empresarua);
        $stmt->bindValue(7, $this->empresanumero);
        $stmt->bindValue(8, $this->empresacomplemento);
        $stmt->bindValue(9, $this->empresacidade);
        $stmt->bindValue(10, $this->empresaestado);
        $stmt->bindValue(11, $this->empresapais);
        $stmt->bindValue(12, $this->empresacep);
        $stmt->bindValue(13, $this->empresatelefone);
        $stmt->bindValue(14, $this->empresacelular);
        $stmt->bindValue(15, $this->empresaurl);
        $stmt->bindValue(16, $this->empresae_mail);
        $stmt->bindValue(17, $this->empresae_mail_nfe);
        $stmt->bindValue(18, $this->empresaind_ativo);
        $stmt->bindValue(19, $this->empresaind_origem);
        $stmt->bindValue(20, $this->empresadt_altera);
        $stmt->bindValue(21, $this->empresahr_altera);
        $stmt->bindValue(22, $this->empresauser_altera);
        $stmt->bindValue(23, $id);

        if ($stmt->execute()):
            return "Atualizado com sucesso";
        else:
            return "Erro ao atualizar";
        endif;
    }

    public function updatelogo($id)
    {
        $sql = "UPDATE empresa SET logo_marca = ? WHERE id = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $this->logo_marca);
        $stmt->bindValue(2, $id);

        if ($stmt->execute()):
            return "Atualizado com sucesso";
        else:
            return "Erro ao atualizar";
        endif;
    }

    public function delete($id)
    {
        $sql = "DELETE FROM empresa WHERE empresaid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $id);

        if ($stmt->execute()):
            return "Excluido com sucesso";
        else:
            return "Erro ao excluir";
        endif;
    }

    public function getAll()
    {
        $sql = "SELECT * FROM empresa";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        } else {
            return [];
        }
    }

    public function findId($id)
    {
        $sql = "SELECT * FROM empresa a LEFT JOIN user_perfil b on b.user_perfilid = a.empresauser_criacao LEFT JOIN user_perfil c ON c.user_perfilid = a.empresauser_altera WHERE a.empresaid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
            return $resultado;
        } else {
            return [];
        }
    }

    public function findMatriz($id)
    {
        $sql = "SELECT * FROM empresa a LEFT JOIN user_perfil b on b.user_perfilid = a.empresauser_criacao LEFT JOIN user_perfil c ON c.user_perfilid = a.empresauser_altera WHERE a.empresacnpj_matriz = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();
        $resultado = $stmt->rowCount();
        return $resultado;
    }

    public function findEstab($id)
    {
        $sql = "SELECT * FROM empresa a LEFT JOIN user_perfil b on b.user_perfilid = a.empresauser_criacao LEFT JOIN user_perfil c ON c.user_perfilid = a.empresauser_altera WHERE a.empresacnpj_estab = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();
        $resultado = $stmt->rowCount();
        return $resultado;
    }

    public function pesquisar($texto){
        $sql = "SELECT * FROM empresa WHERE empresanome_abrev LIKE ? or empresarazao_social LIKE ? COLLATE utf8_general_ci";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, "%{$texto}%");
        $stmt->bindValue(2, "%{$texto}%");
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        } else {
            return [];
        }
    }
    

    public function getPorMatriz($matriz){
        $sql = "SELECT * FROM empresa WHERE empresacnpj_matriz = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $matriz);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        } else {
            return [];
        }
    }

    public function getPorEstab($estab){
        $sql = "SELECT * FROM empresa WHERE empresacnpj_estab = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $estab);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        } else {
            return [];
        }
    }

    public function GETMATRIZ($matriz){
        $sql = "SELECT * FROM empresa WHERE empresacnpj_matriz = ? AND empresacnpj_estab = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $matriz);
        $stmt->bindValue(2, $matriz);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        } else {
            return [];
        }
    }
}