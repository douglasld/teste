<?php

use App\Core\Model;

class ResGruProd extends Model
{

    public $res_gru_prodcnpj_estab;
    public $res_gru_prodvl_meta;
    public $res_gru_prodvl_minimo;
    public $res_gru_prodvl_variavel;
    public $res_gru_prodvl_adic;
    public $res_gru_prodvl_bonus;
    public $res_gru_proddt_movto;
    public $res_gru_prodcod_unid_neg;
    public $res_gru_prodcod_gerente;
    public $res_gru_prodcod_vendedor;
    public $res_gru_prodcod_gru_prod;
    public $res_gru_prodvl_real;

    public function save()
    {

        $sql = "INSERT INTO res_gru_prod(res_gru_prodcnpj_estab,res_gru_proddt_movto,res_gru_prodcod_unid_neg,res_gru_prodcod_gerente,res_gru_prodcod_vendedor,res_gru_prodcod_gru_prod,res_gru_prodvl_real)VALUES(?,?,?,?,?,?,?)";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$this->res_gru_prodcnpj_estab);
        $stmt->bindValue(2,$this->res_gru_proddt_movto);
        $stmt->bindValue(3,$this->res_gru_prodcod_unid_neg);
        $stmt->bindValue(4,$this->res_gru_prodcod_gerente);
        $stmt->bindValue(5,$this->res_gru_prodcod_vendedor);
        $stmt->bindValue(6,$this->res_gru_prodcod_gru_prod);
        $stmt->bindValue(7,$this->res_gru_prodvl_real);

        if($stmt->execute()):
            return "Cadastrado com sucesso";
        else:
            return "Erro ao cadastrar";
        endif;

    }

    public function update($id)
    {
        $sql = "UPDATE res_gru_prod SET res_gru_prodcnpj_estab = ?,res_gru_proddt_movto = ?,res_gru_prodcod_unid_neg = ?,res_gru_prodcod_gerente = ?,res_gru_prodcod_vendedor = ?,res_gru_prodcod_gru_prod = ?,res_gru_prodvl_real = ? WHERE res_gru_prodid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$this->res_gru_prodcnpj_estab);
        $stmt->bindValue(2,$this->res_gru_proddt_movto);
        $stmt->bindValue(3,$this->res_gru_prodcod_unid_neg);
        $stmt->bindValue(4,$this->res_gru_prodcod_gerente);
        $stmt->bindValue(5,$this->res_gru_prodcod_vendedor);
        $stmt->bindValue(6,$this->res_gru_prodcod_gru_prod);
        $stmt->bindValue(7,$this->res_gru_prodvl_real);
        $stmt->bindValue(8,$id);

        if($stmt->execute()):
            return "Atualizado com sucesso";
        else:
            return "Erro ao atualizar";
        endif;
    }

    public function delete($id)
{
    $sql = "DELETE FROM res_gru_prod WHERE res_gru_prodid = ?";
    $stmt = Model::getConn()->prepare($sql);
    $stmt->bindValue(1,$id);

    if($stmt->execute()):
        return "Excluido com sucesso";
    else:
        return "Erro ao excluir";
    endif;
}

    public function deleteLimpa()
    {
        $sql = "DELETE FROM res_gru_prod";
        $stmt = Model::getConn()->prepare($sql);

        if($stmt->execute()):
            return "Excluido com sucesso";
        else:
            return "Erro ao excluir";
        endif;
    }

    public function getAll()
    {
        $sql = "SELECT * FROM res_gru_prod";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

    public function getAllEstabelecimento($id)
    {
        $sql = "SELECT * FROM res_gru_prod WHERE res_gru_prodcnpj_estab = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$id);
        $stmt->execute();
        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

    public function findId($id)
    {
        $sql = "SELECT * FROM res_gru_prod WHERE res_gru_prodid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$id);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

}




