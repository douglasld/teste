<?php

use App\Core\Model;

class ParamEstabLog extends Model
{

    public $param_estab_logdt_emis;
    public $param_estab_loghr_emis;
    public $param_estab_loguser_emis;
    public $param_estab_logtitulo;

    public function save()
    {
        $sql = "INSERT INTO param_estab_log(param_estab_logdt_emis,param_estab_loghr_emis,param_estab_loguser_emis,param_estab_logtitulo)VALUES(?,?,?,?)";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$this->param_estab_logdt_emis);
        $stmt->bindValue(2,$this->param_estab_loghr_emis);
        $stmt->bindValue(3,$this->param_estab_loguser_emis);
        $stmt->bindValue(4,$this->param_estab_logtitulo);


        if($stmt->execute()):
            return "Cadastrado com sucesso";
        else:
            return "Erro ao cadastrar Log";
        endif;

    }

    public function getAll()
    {
        $sql = "SELECT * FROM param_estab_log";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

    public function findId($id)
    {
        $sql = "SELECT * FROM param_estab_log WHERE param_estab_logid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$id);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }
}


