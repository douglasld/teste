<?php

use \App\Core\Controller;
use App\Auth;

class metas extends Controller
{

    public function index()
    {
        Auth::checkLogin();

        $metas = $this->model('Meta');
        $empresas = $this->model('Empresa');
        $dados = $empresas->getPorMatriz($_SESSION['matriz']);

        $this->view('metas/index', $dados = ['registros' => $dados]);
    }


    public function ver($id = '')
    {
        Auth::checkLogin();

        $metas = $this->model('Meta');
        $dados = $metas->getAllID($id);


        $this->view('metas/ver', $dados = ['registros' => $dados]);
    }

    public function ver_metas($id = '')
    {
        Auth::checkLogin();

        $metas = $this->model('Meta');
        $dados = $metas->getAllEstabelecimento($id);

        $this->view('metas/ver_metas', $dados = ['registros' => $dados]);
    }


    public function cadastrar()
    {
        Auth::checkLogin();
        $empresas = $this->model('Empresa');

        $mensagem = array();

        if (isset($_POST['Cadastrar'])) {

            if (empty($_POST['descricao'])) {
                $mensagem[] = "O descricao não pode ser em branco";
            } else {
                $metas = $this->model('Meta');
                $cnpj_estab = str_replace(array('.', '/', '-', '_'), '', $_POST['empresacnpj_estab']);
                $metas->metacnpj_estab = substr($cnpj_estab, 0, 14);

                $metas->metacod_meta = $_POST['cod_meta'];
                $metas->metadescricao = $_POST['descricao'];

                //trata o meta_cod_gerente
                $partes = explode(" -", $_POST['cod_gerente']);
                $metas->metacod_gerente = $partes[0];

                //trata o meta_vendedor
                $partes = explode(" -", $_POST['cod_vendedor']);
                $metas->metacod_vendedor = $partes[0];

                //trata o gru_prod
                $partes = explode(" -", $_POST['cod_grupo_prod']);
                $metas->metacod_grupo_prod = $partes[0];


                //trata o meta_produto
                $partes = explode(" -", $_POST['cod_produto']);
                $metas->metacod_produto = $partes[0];

                //trata o produto_pai
                $partes = explode(" -", $_POST['cod_grupo_prod_pai']);
                $metas->metacod_grupo_prod_pai = $partes[0];

                //trata o Unid_neg
                $partes = explode(" -", $_POST['cod_unid_neg']);
                $metas->metacod_unid_neg = $partes[0];

                //trata o unid_calc
                $partes = explode(" -", $_POST['unid_calc']);
                $metas->metaunid_calc = $partes[0];

                //trata o meta_base
                $partes = explode(" -", $_POST['cod_meta_base']);
                $metas->metacod_meta_base = $partes[0];

                $metas->metadt_meta = $_POST['dt_meta'] . '-01';
                $metas->metadt_ini_val = $_POST['dt_ini_val'];
                $metas->metadt_fim_val = $_POST['dt_fim_val'];
                $metas->metaind_ativo = $_POST['ind_ativo'];
                if ($metas->metaind_ativo == "on") {
                    $metas->metaind_ativo = "0";
                } else {
                    $metas->metaind_ativo = "1";
                }
                $metas->metaind_origem = "0";

                $metas->metavl_meta = str_replace(array('.', ','), array('', '.'), $_POST['vl_meta']);
                $metas->metavl_minimo = str_replace(array('.', ','), array('', '.'), $_POST['vl_minimo']);
                $metas->metavl_variavel = str_replace(array('.', ','), array('', '.'), $_POST['vl_variavel']);
                $metas->metavl_adic = str_replace(array('.', ','), array('', '.'), $_POST['vl_adic']);
                $metas->metavl_bonus = str_replace(array('.', ','), array('', '.'), $_POST['vl_bonus']);

                $metas->metauser_criacao = $_SESSION['userId'];
                $metas->metadt_criacao = date("Y-m-d");
                $metas->metauser_altera = $_SESSION['userId'];;
                $metas->metadt_altera = date("Y-m-d");

                $metas->metahr_altera = date("H:i:s");
                $metas->metahr_criacao =  date("H:i:s");

                $mensagem[] = $metas->save();

                $metas_log = $this->model('MetaLog');
                $cnpj_estab = str_replace(array('.', '/', '-', '_'), '', $_POST['empresacnpj_estab']);
                $metas_log->meta_logcnpj_estab = substr($cnpj_estab, 0, 14);
                $metas_log->meta_logcod_meta = $_POST['cod_meta'];
                $metas_log->meta_logdt_emis = date("Y-m-d");
                $metas_log->meta_loghr_emis = date("H:i:s");
                $metas_log->meta_loguser_emis = $_SESSION['userId'];
                $metas_log->meta_logtitulo = "Inclusão";

                $mensagem[] = $metas_log->save();
                 $this->view('metas/cadastrar', $dados = ['mensagem' => $mensagem, 'registros' => $dados]);
            }
        }

        $this->view('metas/cadastrar', $dados = ['mensagem' => $mensagem, 'registros' => $dados]);
    }

    public function editar($id)
    {
        var_dump($id);

        Auth::checkLogin();

        $mensagem = array();
        $metas = $this->model('Meta');

        if (isset($_POST['Atualizar'])) {

            $metas = $this->model('Meta');
            $cnpj_estab = str_replace(array('.', '/', '-', '_'), '', $_POST['empresacnpj_estab']);
            $metas->metacnpj_estab = substr($cnpj_estab, 0, 14);

            $metas->metacod_meta = $_POST['cod_meta'];
            $metas->metadescricao = $_POST['descricao'];

            //trata o meta_cod_gerente
            $partes = explode(" -", $_POST['cod_gerente']);
            $metas->metacod_gerente = $partes[0];

            //trata o meta_vendedor
            $partes = explode(" -", $_POST['cod_vendedor']);
            $metas->metacod_vendedor = $partes[0];

            //trata o gru_prod
            $partes = explode(" -", $_POST['cod_grupo_prod']);
            $metas->metacod_grupo_prod = $partes[0];


            //trata o meta_produto
            $partes = explode(" -", $_POST['cod_produto']);
            $metas->metacod_produto = $partes[0];

            //trata o produto_pai
            $partes = explode(" -", $_POST['cod_grupo_prod_pai']);
            $metas->metacod_grupo_prod_pai = $partes[0];

            //trata o Unid_neg
            $partes = explode(" -", $_POST['cod_unid_neg']);
            $metas->metacod_unid_neg = $partes[0];

            //trata o unid_calc
            $partes = explode(" -", $_POST['unid_calc']);
            $metas->metaunid_calc = $partes[0];

            //trata o meta_base
            $partes = explode(" -", $_POST['cod_meta_base']);
            $metas->metacod_meta_base = $partes[0];

            $metas->metadt_meta = $_POST['dt_meta'] . '-01';
            $metas->metadt_ini_val = $_POST['dt_ini_val'];
            $metas->metadt_fim_val = $_POST['dt_fim_val'];
            $metas->metaind_ativo = $_POST['ind_ativo'];
            if ($metas->metaind_ativo == "on") {
                $metas->metaind_ativo = "0";
            } else {
                $metas->metaind_ativo = "1";
            }
            $metas->metaind_origem = "0";

            $metas->metavl_meta = str_replace(array('.', ','), array('', '.'), $_POST['vl_meta']);
            $metas->metavl_minimo = str_replace(array('.', ','), array('', '.'), $_POST['vl_minimo']);
            $metas->metavl_variavel = str_replace(array('.', ','), array('', '.'), $_POST['vl_variavel']);
            $metas->metavl_adic = str_replace(array('.', ','), array('', '.'), $_POST['vl_adic']);
            $metas->metavl_bonus = str_replace(array('.', ','), array('', '.'), $_POST['vl_bonus']);

            $metas->metauser_criacao = $_SESSION['userId'];
            $metas->metadt_criacao = date("Y-m-d");
            $metas->metauser_altera = $_SESSION['userId'];;
            $metas->metadt_altera = date("Y-m-d");

            $metas->metahr_altera = date("H:i:s");
            $metas->metahr_criacao =  date("H:i:s");

       
            $mensagem[] = $metas->update($id);

            $metas_log = $this->model('MetaLog');
            $metas_log->meta_logcnpj_estab = $metas->metacnpj_estab;
            $metas_log->meta_logcod_meta = $_POST['cod_meta'];
            $metas_log->meta_logdt_emis = date("Y-m-d");
            $metas_log->meta_loghr_emis = date("H:i:s");
            $metas_log->meta_loguser_emis = $_SESSION['userId'];
            $metas_log->meta_logtitulo = "Inclusão";

            $mensagem[] = $metas_log->save();
        }
        $dados = $metas->getAllID($id);

        $this->view('metas/editar', $dados = ['mensagem' => $mensagem, 'registros' => $dados]);
    }


    public function excluir($id = '')
    {
        Auth::checkLogin();
        $mensagem = array();
        $metas = $this->model('Meta');

        $mensagem[] = $metas->delete($id);

        $dados = $metas->getAll();

        $this->view('metas/index', $dados = ['registros' => $dados, 'mensagem' => $mensagem]);
    }
}
