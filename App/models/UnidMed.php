<?php

use App\Core\Model;

class UnidMed extends Model
{

    public $unid_medcnpj_estab;
    public $unid_medcod_unid_med;
    public $unid_meddescricao;
    public $unid_medind_ativo;
    public $unid_medind_origem;
    public $unid_meddt_criacao;
    public $unid_medhr_criacao;
    public $unid_meduser_criacao;
    public $unid_meddt_altera;
    public $unid_medhr_altera;
    public $unid_meduser_altera;

    public function save()
    {

        $sql = "INSERT INTO unid_med(unid_medcnpj_estab,unid_medcod_unid_med,unid_meddescricao,unid_medind_ativo,unid_medind_origem,unid_meddt_criacao,unid_medhr_criacao,unid_meduser_criacao,unid_meddt_altera,unid_medhr_altera,unid_meduser_altera)VALUES(?,?,?,?,?,?,?,?,?,?,?)";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $this->unid_medcnpj_estab);
        $stmt->bindValue(2, $this->unid_medcod_unid_med);
        $stmt->bindValue(3, $this->unid_meddescricao);
        $stmt->bindValue(4, $this->unid_medind_ativo);
        $stmt->bindValue(5, $this->unid_medind_origem);
        $stmt->bindValue(6, $this->unid_meddt_criacao);
        $stmt->bindValue(7, $this->unid_medhr_criacao);
        $stmt->bindValue(8, $this->unid_meduser_criacao);
        $stmt->bindValue(9, $this->unid_meddt_altera);
        $stmt->bindValue(10, $this->unid_medhr_altera);
        $stmt->bindValue(11, $this->unid_meduser_altera);

        if ($stmt->execute()) :
            return "Cadastrado com sucesso";
        else :
            return "Erro ao cadastrar";
        endif;
    }

    public function update($id)
    {
        $sql = "UPDATE unid_med SET unid_medcnpj_estab = ?,unid_medcod_unid_med = ?,unid_meddescricao = ?,unid_medind_ativo = ?,unid_medind_origem = ?,unid_meddt_altera = ?,unid_medhr_altera = ?,unid_meduser_altera = ? WHERE unid_medid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $this->unid_medcnpj_estab);
        $stmt->bindValue(2, $this->unid_medcod_unid_med);
        $stmt->bindValue(3, $this->unid_meddescricao);
        $stmt->bindValue(4, $this->unid_medind_ativo);
        $stmt->bindValue(5, $this->unid_medind_origem);
        $stmt->bindValue(6, $this->unid_meddt_altera);
        $stmt->bindValue(7, $this->unid_medhr_altera);
        $stmt->bindValue(8, $this->unid_meduser_altera);
        $stmt->bindValue(9, $id);

        if ($stmt->execute()) :
            return "Atualizado com sucesso";
        else :
            return "Erro ao atualizar";
        endif;
    }

    public function delete($id)
    {
        $sql = "DELETE FROM unid_med WHERE unid_medid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $id);

        if ($stmt->execute()) :
            return "Excluido com sucesso";
        else :
            return "Erro ao excluir";
        endif;
    }

    public function getAll()
    {
        $sql = "SELECT * FROM unid_med";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        } else {
            return [];
        }
    }

    public function getAllEstabelecimento($estab)
    {
        $sql = "SELECT * FROM unid_med WHERE unid_medcnpj_estab = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $estab);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        } else {
            return [];
        }
    }

    public function findId($id)
    {
        $sql = "SELECT * FROM unid_med WHERE unid_medid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
            return $resultado;
        } else {
            return [];
        }
    }

    public function ultimo($estab, $usuario)
    {
        $sql = "SELECT * FROM unid_med WHERE unid_medcnpj_estab = ? AND unid_meduser_criacao = ? ORDER BY unid_medid DESC";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $estab);
        $stmt->bindValue(2, $usuario);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
            return $resultado;
        } else {
            return [];
        }
    }

    public function BuscageralEstab($estab)
    {
        $sql = "SELECT * FROM unid_med WHERE unid_medcnpj_estab = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $estab);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $resultado;
    }
}
