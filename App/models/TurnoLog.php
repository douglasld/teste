<?php

use App\Core\Model;

class TurnoLog extends Model
{

    public $turno_logcnpj_estab;
    public $turno_loguser_emis;
    public $turno_logdt_emis;
    public $turno_loghr_emis;
    public $turno_logtitulo;
    public $turno_loghr_ini;
    public $turno_loghr_fim;

    public function save()
    {

        $sql = "INSERT INTO turno_log(turno_logcnpj_estab,turno_loguser_emis,turno_logdt_emis,turno_loghr_emis,turno_logtitulo,turno_loghr_ini,turno_loghr_fim)VALUES(?,?,?,?,?,?,?)";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $this->turno_logcnpj_estab);
        $stmt->bindValue(2, $this->turno_loguser_emis);
        $stmt->bindValue(3, $this->turno_logdt_emis);
        $stmt->bindValue(4, $this->turno_loghr_emis);
        $stmt->bindValue(5, $this->turno_logtitulo);
        $stmt->bindValue(6, $this->turno_loghr_ini);
        $stmt->bindValue(7, $this->turno_loghr_fim);

        if ($stmt->execute()):
            return "Cadastrado com sucesso";
        else:
            return "Erro ao cadastrar";
        endif;

    }


    public function getAll($estabelecimento,$horaInicio,$horaFinal)
    {
        $sql = "SELECT * FROM turno_log WHERE turno_logcnpj_estab = ? AND turno_loghr_ini = ? AND turno_loghr_fim = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $estabelecimento);
        $stmt->bindValue(2, $horaInicio);
        $stmt->bindValue(3, $horaFinal);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        } else {
            return [];
        }
    }

}



