<?php

use App\Core\Model;

class ParamMetaLog extends Model
{
    public $param_meta_logdt_emis;
    public $param_meta_loghr_emis;
    public $param_meta_loguser_emis;
    public $param_meta_logtitulo;

    public function save()
    {
        $sql = "INSERT INTO param_meta_log(param_meta_logdt_emis,param_meta_loghr_emis,param_meta_loguser_emis,param_meta_logtitulo)VALUES(?,?,?,?,?,?)";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$this->param_meta_logdt_emis);
        $stmt->bindValue(2,$this->param_meta_loghr_emis);
        $stmt->bindValue(3,$this->param_meta_loguser_emis);
        $stmt->bindValue(4,$this->param_meta_logtitulo);

        if($stmt->execute()):
            return "Cadastrado com sucesso";
        else:
            return "Erro ao cadastrar Log";
        endif;

    }

    public function getAll()
    {
        $sql = "SELECT * FROM param_meta_log";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

    public function findId($id)
    {
        $sql = "SELECT * FROM param_meta_log WHERE param_meta_logid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$matriz);
        $stmt->bindValue(2,$estab);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }
}



