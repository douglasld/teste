<?php

use App\Core\Model;

class VendaLog extends Model
{

    public $vendas_logcnpj_estab;
    public $vendas_logserie_docto;
    public $vendas_logdt_emis;
    public $vendas_loghr_emis;
    public $vendas_loguser_emis;
    public $vendas_logtitulo;
    public $vendas_lognr_docto;
    public $vendas_logdt_docto;

    public function save()
    {

        $sql = "INSERT INTO vendas_log(vendas_logcnpj_estab,vendas_logserie_docto,vendas_logdt_emis,vendas_loghr_emis,vendas_loguser_emis,vendas_logtitulo,vendas_lognr_docto,vendas_logdt_docto)VALUES(?,?,?,?,?,?,?,?)";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $this->vendas_logcnpj_estab);
        $stmt->bindValue(2, $this->vendas_logserie_docto);
        $stmt->bindValue(3, $this->vendas_logdt_emis);
        $stmt->bindValue(4, $this->vendas_loghr_emis);
        $stmt->bindValue(5, $this->vendas_loguser_emis);
        $stmt->bindValue(6, $this->vendas_logtitulo);
        $stmt->bindValue(7, $this->vendas_lognr_docto);
        $stmt->bindValue(8, $this->vendas_logdt_docto);

        if ($stmt->execute()):
            return "Cadastrado com sucesso";
        else:
            return "Erro ao cadastrar";
        endif;

    }


    public function getAll($estabelecimento,$numeroDocto,$serieDocto,$dataDocto)
    {
        $sql = "SELECT * FROM vendas_log WHERE vendas_logcnpj_estab = ? AND vendas_lognr_docto = ? AND vendas_logserie_docto = ? AND vendas_logdt_docto = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $estabelecimento);
        $stmt->bindValue(2, $numeroDocto);
        $stmt->bindValue(3, $serieDocto);
        $stmt->bindValue(4, $dataDocto);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        } else {
            return [];
        }
    }

}




