<?php

use App\Core\Model;

class ParamEstab extends Model
{

    public $param_estabcnpj_estab;
    public $param_estabdt_criacao;
    public $param_estabhr_criacao;
    public $param_estabuser_criacao;
    public $param_estabdt_altera;
    public $param_estabhr_altera;
    public $param_estabuser_altera;
    public $param_estabperc_red_meta_feriado;
    public $param_estabvl_bonus_extra;
    public $param_estabperc_bonus_extra;
    public $param_estabind_ativo;

    public function save()
    {

        $sql = "INSERT INTO param_estab(param_estabcnpj_estab,param_estabdt_criacao,param_estabhr_criacao,param_estabuser_criacao,param_estabdt_altera,param_estabhr_altera,param_estabuser_altera,param_estabperc_red_meta_feriado,param_estabvl_bonus_extra,param_estabperc_bonus_extra,param_estabind_ativo)VALUES(?,?,?,?,?,?,?,?,?,?,?)";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$this->param_estabcnpj_estab);
        $stmt->bindValue(2,$this->param_estabdt_criacao);
        $stmt->bindValue(3,$this->param_estabhr_criacao);
        $stmt->bindValue(4,$this->param_estabuser_criacao);
        $stmt->bindValue(5,$this->param_estabdt_altera);
        $stmt->bindValue(6,$this->param_estabhr_altera);
        $stmt->bindValue(7,$this->param_estabuser_altera);
        $stmt->bindValue(8,$this->param_estabperc_red_meta_feriado);
        $stmt->bindValue(9,$this->param_estabvl_bonus_extra);
        $stmt->bindValue(10,$this->param_estabperc_bonus_extra);
        $stmt->bindValue(11,$this->param_estabind_ativo);

        if($stmt->execute()):
            return "Cadastrado com sucesso";
        else:
            return "Erro ao cadastrar";
        endif;
    }

    public function update($id)
    {
        $sql = "UPDATE param_estab SET param_estabcnpj_estab=?,param_estabdt_altera=?,param_estabhr_altera=?,param_estabuser_altera=?,param_estabperc_red_meta_feriado=?,param_estabvl_bonus_extra=?,param_estabperc_bonus_extra=?,param_estabind_ativo=? WHERE param_estabid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$this->param_estabcnpj_estab);
        $stmt->bindValue(2,$this->param_estabdt_altera);
        $stmt->bindValue(3,$this->param_estabhr_altera);
        $stmt->bindValue(4,$this->param_estabuser_altera);
        $stmt->bindValue(5,$this->param_estabperc_red_meta_feriado);
        $stmt->bindValue(6,$this->param_estabvl_bonus_extra);
        $stmt->bindValue(7,$this->param_estabperc_bonus_extra);
        $stmt->bindValue(8,$this->param_estabind_ativo);
        $stmt->bindValue(9,$id);

        if($stmt->execute()):
            return "Atualizado com sucesso";
        else:
            return "Erro ao atualizar";
        endif;
    }

    public function delete($id)
    {
        $sql = "DELETE FROM param_estab WHERE param_estabid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$id);

        if($stmt->execute()):
            return "Excluido com sucesso";
        else:
            return "Erro ao excluir";
        endif;
    }

    public function getAll()
    {
        $sql = "SELECT * FROM param_estab";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

    public function findId($id)
    {
        $sql = "SELECT * FROM param_estab WHERE param_estabid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$id);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }
    
    public function getAllEstabelecimento($estab){
        $sql = "SELECT * FROM param_estab WHERE param_estabcnpj_estab = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$estab);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }

    }
    public function BuscageralEstab($estab)
    {
        $sql = "SELECT * FROM param_estab WHERE param_estabcnpj_estab = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $estab);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $resultado;
    }
}

