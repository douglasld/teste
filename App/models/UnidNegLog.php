<?php

use App\Core\Model;

class UnidNegLog extends Model
{
    public $unid_neg_logcnpj_estab;
    public $unid_neg_logdt_emis;
    public $unid_neg_loghr_emis;
    public $unid_neg_loguser_emis;
    public $unid_neg_logtitulo;

    public function save()
    {

        $sql = "INSERT INTO unid_neg_log(unid_neg_logcnpj_estab,unid_neg_logdt_emis,unid_neg_loghr_emis,unid_neg_loguser_emis,unid_neg_logtitulo)VALUES(?,?,?,?,?)";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$this->unid_neg_logcnpj_estab);
        $stmt->bindValue(2,$this->unid_neg_logdt_emis);
        $stmt->bindValue(3,$this->unid_neg_loghr_emis);
        $stmt->bindValue(4,$this->unid_neg_loguser_emis);
        $stmt->bindValue(5,$this->unid_neg_logtitulo);

        if($stmt->execute()):
            return "Cadastrado com sucesso";
        else:
            return "Erro ao cadastrar";
        endif;

    }

    public function update($id)
    {
        $sql = "UPDATE unid_neg_log SET unid_neg_logcnpj_estab=?,unid_neg_logdt_emis=?,unid_neg_loghr_emis=?,unid_neg_loguser_emis=?,unid_neg_logtitulo=? WHERE unid_neg_logid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$this->unid_neg_logcnpj_estab);
        $stmt->bindValue(2,$this->unid_neg_logdt_emis);
        $stmt->bindValue(3,$this->unid_neg_loghr_emis);
        $stmt->bindValue(4,$this->unid_neg_loguser_emis);
        $stmt->bindValue(5,$this->unid_neg_logtitulo);
        $stmt->bindValue(6,$id);

        if($stmt->execute()):
            return "Atualizado com sucesso";
        else:
            return "Erro ao atualizar";
        endif;
    }

    public function delete($id)
    {
        $sql = "DELETE FROM unid_neg_log WHERE unid_neg_logid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$id);

        if($stmt->execute()):
            return "Excluido com sucesso";
        else:
            return "Erro ao excluir";
        endif;
    }

    public function getAll()
    {
        $sql = "SELECT * FROM unid_neg_log";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

    public function findId($id)
    {
        $sql = "SELECT * FROM unid_neg_log WHERE unid_neg_logid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$id);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

}






