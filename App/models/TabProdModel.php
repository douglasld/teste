<?php

use App\Core\Model;

class TabProdModel extends Model
{

    public $nome_prod;
    public $cod_cat_p;
    public $cod_cat_f;
    public $ind_ativo;
    public $user_criacao;
    public $dt_criacao;
    public $hr_criacao;

    public function save()
    {
        $sql = "INSERT INTO produtos_tabela(nome_prod,cod_cat_p,cod_cat_f,ind_ativo,user_criacao,dt_criacao,hr_criacao)VALUES(?,?,?,?,?,?,?)";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $this->nome_prod);
        $stmt->bindValue(2, $this->cod_cat_p);
        $stmt->bindValue(3, $this->cod_cat_f);
        $stmt->bindValue(4, $this->ind_ativo);
        $stmt->bindValue(5, $this->user_criacao);
        $stmt->bindValue(6, $this->dt_criacao);
        $stmt->bindValue(7, $this->hr_criacao);


        if ($stmt->execute()) :
            return "1 - Produto cadastrado com sucesso!";
        else :
            return "3 - Erro ao processar cadastro!";
        endif;
    }

    public function update($id)
    {
        $sql = "UPDATE produtos_tabela SET nome_prod = ?,cod_cat_p = ?,cod_cat_f = ?,ind_ativo = ? WHERE cod_prod = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $this->nome_prod);
        $stmt->bindValue(2, $this->cod_cat_p);
        $stmt->bindValue(3, $this->cod_cat_f);
        $stmt->bindValue(4, $this->ind_ativo);
        $stmt->bindValue(5, $id);

        if ($stmt->execute()) :
            return "1 - Categoria atualizada com sucesso";
        else :
            return "3 - Erro ao atualizar";
        endif;
    }

    public function delete($id)
    {
        $sql = "DELETE FROM produtos_tabela WHERE cod_prod = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $id);

        if ($stmt->execute()) :
            return "5 - Produto excluido com sucesso!";
        else :
            return "4 - Erro ao excluir o produto!";
        endif;
    }

    //busca ultimo para adc +
    public function GetUltimoCod()
    {
        $sql = "SELECT MAX(cod_prod) as last_cod FROM produtos_tabela";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $resultado;
    }

    //busca all para lista
    public function getAll()
    {
        $sql = "SELECT * FROM produtos_tabela";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        } else {
            return [];
        }
    }

    //busca cat por id
    public function getId($id)
    {
        $sql = "SELECT * FROM produtos_tabela WHERE cod_prod = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        } else {
            return [];
        }
    }
}
