<?php

use App\Core\Model;

class ResGerente extends Model
{

    public $res_gerentecnpj_estab;
    public $res_gerentevl_meta;
    public $res_gerentevl_minimo;
    public $res_gerentevl_variavel;
    public $res_gerentevl_adic;
    public $res_gerentevl_bonus;
    public $res_gerentedt_movto;
    public $res_gerentecod_unid_neg;
    public $res_gerentecod_gerente;
    public $res_gerentevl_real;

    public function save()
    {

        $sql = "INSERT INTO res_gerente(res_gerentecnpj_estab,res_gerentedt_movto,res_gerentecod_unid_neg,res_gerentecod_gerente,res_gerentevl_real)VALUES(?,?,?,?,?)";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $this->res_gerentecnpj_estab);
        $stmt->bindValue(2, $this->res_gerentedt_movto);
        $stmt->bindValue(3, $this->res_gerentecod_unid_neg);
        $stmt->bindValue(4, $this->res_gerentecod_gerente);
        $stmt->bindValue(5, $this->res_gerentevl_real);

        if ($stmt->execute()) :
            return "Cadastrado com sucesso";
        else :
            return "Erro ao cadastrar";
        endif;
    }

    public function update($id)
    {
        $sql = "UPDATE res_gerente SET res_gerentecnpj_estab = ?,res_gerentedt_movto = ?,res_gerentecod_unid_neg = ?,res_gerentecod_gerente = ?,res_gerentevl_real = ? WHERE res_gerenteid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $this->res_gerentecnpj_estab);
        $stmt->bindValue(2, $this->res_gerentedt_movto);
        $stmt->bindValue(3, $this->res_gerentecod_unid_neg);
        $stmt->bindValue(4, $this->res_gerentecod_gerente);
        $stmt->bindValue(5, $this->res_gerentevl_real);
        $stmt->bindValue(6, $id);

        if ($stmt->execute()) :
            return "Atualizado com sucesso";
        else :
            return "Erro ao atualizar";
        endif;
    }

    public function delete($id)
    {
        $sql = "DELETE FROM res_gerente WHERE res_gerenteid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $id);

        if ($stmt->execute()) :
            return "Excluido com sucesso";
        else :
            return "Erro ao excluir";
        endif;
    }

    public function deleteLimpa()
    {
        $sql = "DELETE FROM res_gerente";
        $stmt = Model::getConn()->prepare($sql);

        if ($stmt->execute()) :
            return "Excluido com sucesso";
        else :
            return "Erro ao excluir";
        endif;
    }

    public function getAll()
    {
        $sql = "SELECT * FROM res_gerente";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        } else {
            return [];
        }
    }

    public function getAllEstabelecimento($id)
    {
        $sql = "SELECT * FROM res_gerente WHERE res_gerentecnpj_estab = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        } else {
            return [];
        }
    }

    public function findId($id)
    {
        $sql = "SELECT * FROM res_gerente WHERE res_gerenteid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
            return $resultado;
        } else {
            return [];
        }
    }

}
