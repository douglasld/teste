<?php

use App\Core\Model;

class GrupoProd extends Model
{

    public $grupo_prodcnpj_estab;
    public $grupo_prodcod_grupo_prod;
    public $grupo_proddescricao;
    public $grupo_prodind_ativo;
    public $grupo_prodind_origem;
    public $grupo_proddt_criacao;
    public $grupo_prodhr_criacao;
    public $grupo_produser_criacao;
    public $grupo_proddt_altera;
    public $grupo_prodhr_altera;
    public $grupo_produser_altera;

    public function save()
    {

        $sql = "INSERT INTO grupo_prod(grupo_prodcnpj_estab,grupo_prodcod_grupo_prod,grupo_proddescricao,grupo_prodind_ativo,grupo_prodind_origem,grupo_proddt_criacao,grupo_prodhr_criacao,grupo_produser_criacao,grupo_proddt_altera,grupo_prodhr_altera,grupo_produser_altera)VALUES(?,?,?,?,?,?,?,?,?,?,?)";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$this->grupo_prodcnpj_estab);
        $stmt->bindValue(2,$this->grupo_prodcod_grupo_prod);
        $stmt->bindValue(3,$this->grupo_proddescricao);
        $stmt->bindValue(4,$this->grupo_prodind_ativo);
        $stmt->bindValue(5,$this->grupo_prodind_origem);
        $stmt->bindValue(6,$this->grupo_proddt_criacao);
        $stmt->bindValue(7,$this->grupo_prodhr_criacao);
        $stmt->bindValue(8,$this->grupo_produser_criacao);
        $stmt->bindValue(9,$this->grupo_proddt_altera);
        $stmt->bindValue(10,$this->grupo_prodhr_altera);
        $stmt->bindValue(11,$this->grupo_produser_altera);

        if($stmt->execute()):
            return "Cadastrado com sucesso";
        else:
            return "Erro ao cadastrar";
        endif;

    }


    public function update($id)
    {
        $sql = "UPDATE grupo_prod SET grupo_prodcnpj_estab = ?,grupo_prodcod_grupo_prod = ?,grupo_proddescricao = ?,grupo_prodind_ativo = ?,grupo_prodind_origem = ?,grupo_proddt_altera = ?,grupo_prodhr_altera = ?,grupo_produser_altera = ? WHERE grupo_prodid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$this->grupo_prodcnpj_estab);
        $stmt->bindValue(2,$this->grupo_prodcod_grupo_prod);
        $stmt->bindValue(3,$this->grupo_proddescricao);
        $stmt->bindValue(4,$this->grupo_prodind_ativo);
        $stmt->bindValue(5,$this->grupo_prodind_origem);
        $stmt->bindValue(6,$this->grupo_proddt_altera);
        $stmt->bindValue(7,$this->grupo_prodhr_altera);
        $stmt->bindValue(8,$this->grupo_produser_altera);
        $stmt->bindValue(9,$id);

        if($stmt->execute()):
            return "Atualizado com sucesso";
        else:
            return "Erro ao atualizar";
        endif;
    }

    public function delete($id)
    {
        $sql = "DELETE FROM grupo_prod WHERE grupo_prodid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$id);

        if($stmt->execute()):
            return "Excluido com sucesso";
        else:
            return "Erro ao excluir";
        endif;
    }

    public function getAll()
    {
        $sql = "SELECT * FROM grupo_prod";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

    public function findId($id)
    {
        $sql = "SELECT * FROM grupo_prod WHERE grupo_prodid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$id);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

    public function ultimoCadastro($usuario,$estab){
        $sql = "SELECT * FROM grupo_prod WHERE grupo_produser_criacao = ? AND grupo_prodcnpj_estab = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$usuario);
        $stmt->bindValue(2,$estab);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

    public function getPorMatriz($matriz){
        $sql = "SELECT * FROM grupo_prod WHERE grupo_prodcnpj_estab = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $matriz);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        } else {
            return [];
        }
    }

    public function contaCadastro(){
        $sql = "SELECT * FROM grupo_prod";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->execute();
        $resultado = $stmt->rowCount();
        return $resultado;
    }

    public function getAllEstabelecimento($estab){
        $sql = "SELECT * FROM grupo_prod WHERE grupo_prodcnpj_estab = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$estab);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }

    }

    public function pesquisar($texto){
        $sql = "SELECT * FROM grupo_prod a ";
        $sql = $sql." WHERE a.grupo_proddescricao LIKE ? ";
        $sql = $sql." COLLATE utf8_general_ci";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, "%{$texto}%");
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        } else {
            return [];
        }
    }

    public function BuscageralEstab($estab)
    {
        var_dump($estab);
        $sql = "SELECT * FROM grupo_prod WHERE grupo_prodcnpj_estab = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$estab);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $resultado;
    }
}
