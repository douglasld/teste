<?php

use App\Core\Model;

class UserPerfil extends Model
{
    public $user_perfilcnpj_matriz;
    public $user_perfilcod_user;
    public $user_perfilnome_user;
    public $user_perfilnome_completo;
    public $user_perfilsenha;
    public $user_perfildias_validade;
    public $user_perfile_mail;
    public $user_perfilrua;
    public $user_perfilnumero;
    public $user_perfilcomplemento;
    public $user_perfilbairro;
    public $user_perfilcidade;
    public $user_perfilestado;
    public $user_perfilpais;
    public $user_perfilcep;
    public $user_perfiltelefone;
    public $user_perfilcelular;
    public $user_perfiladm;
    public $user_perfilgestor;
    public $user_perfilind_ativo;
    public $user_perfilind_origem;
    public $user_perfilfoto;
    public $user_perfildt_criacao;
    public $user_perfilhr_criacao;
    public $user_perfildt_altera;
    public $user_perfilhr_altera;
    public $user_perfilind_presidente;
    public $user_perfilcod_presidente;
    public $user_perfilind_diretor;
    public $user_perfilcod_diretor;
    public $user_perfilind_gerente;
    public $user_perfilcod_gerente;
    public $user_perfilind_coordenador;
    public $user_perfilcod_coordenador;

    public function save()
    {
        $sql = "INSERT INTO user_perfil(user_perfilcnpj_matriz,user_perfilcod_user,user_perfilnome_user,user_perfilnome_completo,user_perfilsenha,
        user_perfildias_validade,user_perfile_mail,user_perfilrua,user_perfilnumero,user_perfilcomplemento,user_perfilbairro,user_perfilcidade,
        user_perfilestado,user_perfilpais,user_perfilcep,user_perfiltelefone,user_perfilcelular,user_perfiladm,user_perfilgestor,user_perfilind_ativo,
        user_perfilind_origem,user_perfilfoto,user_perfildt_criacao,user_perfilhr_criacao,user_perfildt_altera,user_perfilhr_altera,
        user_perfilind_presidente,user_perfilcod_presidente,user_perfilind_diretor,user_perfilcod_diretor,user_perfilind_gerente,user_perfilcod_gerente,
        user_perfilind_coordenador,user_perfilcod_coordenador)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$this->user_perfilcnpj_matriz);
        $stmt->bindValue(2,$this->user_perfilcod_user);
        $stmt->bindValue(3,$this->user_perfilnome_user);
        $stmt->bindValue(4,$this->user_perfilnome_completo);
        $stmt->bindValue(5,$this->user_perfilsenha);
        $stmt->bindValue(6,$this->user_perfildias_validade);
        $stmt->bindValue(7,$this->user_perfile_mail);
        $stmt->bindValue(8,$this->user_perfilrua);
        $stmt->bindValue(9,$this->user_perfilnumero);
        $stmt->bindValue(10,$this->user_perfilcomplemento);
        $stmt->bindValue(11,$this->user_perfilbairro);
        $stmt->bindValue(12,$this->user_perfilcidade);
        $stmt->bindValue(13,$this->user_perfilestado);
        $stmt->bindValue(14,$this->user_perfilpais);
        $stmt->bindValue(15,$this->user_perfilcep);
        $stmt->bindValue(16,$this->user_perfiltelefone);
        $stmt->bindValue(17,$this->user_perfilcelular);
        $stmt->bindValue(18,$this->user_perfiladm);
        $stmt->bindValue(19,$this->user_perfilgestor);
        $stmt->bindValue(20,$this->user_perfilind_ativo);
        $stmt->bindValue(21,$this->user_perfilind_origem);
        $stmt->bindValue(22,$this->user_perfilfoto);
        $stmt->bindValue(23,$this->user_perfildt_criacao);
        $stmt->bindValue(24,$this->user_perfilhr_criacao);
        $stmt->bindValue(25,$this->user_perfildt_altera);
        $stmt->bindValue(26,$this->user_perfilhr_altera);
        $stmt->bindValue(27,$this->user_perfilind_presidente);
        $stmt->bindValue(28,$this->user_perfilcod_presidente);
        $stmt->bindValue(29,$this->user_perfilind_diretor);
        $stmt->bindValue(30,$this->user_perfilcod_diretor);
        $stmt->bindValue(31,$this->user_perfilind_gerente);
        $stmt->bindValue(32,$this->user_perfilcod_gerente);
        $stmt->bindValue(33,$this->user_perfilind_coordenador);
        $stmt->bindValue(34,$this->user_perfilcod_coordenador);

        var_dump($stmt->execute());
                if($stmt->execute()):
            return "Cadastrado com sucesso";
        else:
            return "Erro ao cadastrar";
        endif;

    }

    public function update($id)
    {
        $sql = "UPDATE user_perfil SET user_perfilcnpj_matriz = ?,user_perfilcod_user= ?,user_perfilnome_user= ?,user_perfilnome_completo= ?,user_perfildias_validade= ?,user_perfile_mail= ?,user_perfilrua= ?,user_perfilnumero= ?,user_perfilcomplemento= ?,user_perfilbairro= ?,user_perfilcidade= ?,user_perfilestado= ?,user_perfilpais= ?,user_perfilcep= ?,user_perfiltelefone= ?,user_perfilcelular= ?,user_perfiladm= ?,user_perfilgestor= ?,user_perfilind_ativo= ?,user_perfilind_origem= ?,user_perfildt_altera= ?,user_perfilhr_altera= ? WHERE user_perfilid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$this->user_perfilcnpj_matriz);
        $stmt->bindValue(2,$this->user_perfilcod_user);
        $stmt->bindValue(3,$this->user_perfilnome_user);
        $stmt->bindValue(4,$this->user_perfilnome_completo);
        $stmt->bindValue(5,$this->user_perfildias_validade);
        $stmt->bindValue(6,$this->user_perfile_mail);
        $stmt->bindValue(7,$this->user_perfilrua);
        $stmt->bindValue(8,$this->user_perfilnumero);
        $stmt->bindValue(9,$this->user_perfilcomplemento);
        $stmt->bindValue(10,$this->user_perfilbairro);
        $stmt->bindValue(11,$this->user_perfilcidade);
        $stmt->bindValue(12,$this->user_perfilestado);
        $stmt->bindValue(13,$this->user_perfilpais);
        $stmt->bindValue(14,$this->user_perfilcep);
        $stmt->bindValue(15,$this->user_perfiltelefone);
        $stmt->bindValue(16,$this->user_perfilcelular);
        $stmt->bindValue(17,$this->user_perfiladm);
        $stmt->bindValue(18,$this->user_perfilgestor);
        $stmt->bindValue(19,$this->user_perfilind_ativo);
        $stmt->bindValue(20,$this->user_perfilind_origem);
        $stmt->bindValue(21,$this->user_perfildt_altera);
        $stmt->bindValue(22,$this->user_perfilhr_altera);
        $stmt->bindValue(23,$id);       

        if($stmt->execute()):
            return "Atualizado com sucesso";
        else:
            return "Erro ao atualizar";
        endif;
    }

    public function delete($id)
    {
        $sql = "DELETE FROM user_perfil WHERE user_perfilid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$id);

        if($stmt->execute()):
            return "Excluido com sucesso";
        else:
            return "Erro ao excluir";
        endif;
    }

    public function getAll()
    {
        $sql = "SELECT * FROM user_perfil";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

    public function getAllMatriz($id)
    {
        $sql = "SELECT * FROM user_perfil WHERE user_perfilcnpj_matriz = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$id);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

    public function contaMatrizEmail($matriz,$email)
{
    $sql = "SELECT * FROM user_perfil WHERE user_perfilcnpj_matriz = ? AND 	user_perfile_mail = ?";
    $stmt = Model::getConn()->prepare($sql);
    $stmt->bindValue(1,$matriz);
    $stmt->bindValue(2,$email);
    $stmt->execute();
    $resultado = $stmt->rowCount();
    return $resultado;
}

    public function contaGeral()
    {
        $sql = "SELECT * FROM user_perfil";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->execute();
        $resultado = $stmt->rowCount();
        return $resultado;
    }

    public function findId($id)
    {
        $sql = "SELECT * FROM user_perfil WHERE user_perfilid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$id);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

    public function findCod($id)
{
    $sql = "SELECT * FROM user_perfil WHERE user_perfilcod_user = ?";
    $stmt = Model::getConn()->prepare($sql);
    $stmt->bindValue(1,$id);
    $stmt->execute();

    if($stmt->rowCount()>0)
    {
        $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $resultado;
    }else{
        return [];
    }
}

    public function contaEmail($email)
    {
        $sql = "SELECT * FROM user_perfil WHERE user_perfile_mail = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$email);
        $stmt->execute();
        $resultado = $stmt->rowCount();
        return $resultado;
    }

    public function updateSenha($id){
        $sql = "UPDATE user_perfil SET user_perfilsenha = ? WHERE user_perfilid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$this->user_perfilsenha);
        $stmt->bindValue(2,$id);

        if($stmt->execute()):
            return "Atualizado com sucesso";
        else:
            return "Erro ao atualizar";
        endif;
    }

    public function updatefoto($id){
        $sql = "UPDATE user_perfil SET user_perfilfoto = ? WHERE user_perfilid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$this->user_perfilfoto);
        $stmt->bindValue(2,$id);

        if($stmt->execute()):
            return "Atualizado com sucesso";
        else:
            return "Erro ao atualizar";
        endif;
    }

    public function ultimoCadastro($email,$empresa){
        $sql = "SELECT * FROM user_perfil WHERE user_perfile_mail = ? AND user_perfilcnpj_matriz = ? ORDER BY user_perfilid DESC";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$email);
        $stmt->bindValue(2,$empresa);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
            
            return $resultado;
        }else{
            return [];
        }
    }

    public function pesquisar($texto){
        $sql = "SELECT * FROM user_perfil WHERE user_perfilnome_user LIKE ? or user_perfilnome_completo LIKE ? COLLATE utf8_general_ci";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, "%{$texto}%");
        $stmt->bindValue(2, "%{$texto}%");
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        } else {
            return [];
        }
    }

    public function getAllLogs($id)
    {
        $sql = "SELECT * FROM user_perfil_log";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->execute();
        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }       
    }

    
    public function getAllEstabelecimentos($id)
    {
        $sql = "SELECT * FROM user_estab a LEFT JOIN user_perfil b ON b.user_perfilid = a.user_estabcod_user";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->execute();
        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

    public function getultuser($matriz)
    {
        $sql = "SELECT MAX(user_perfilcod_user) as max_cod FROM user_perfil WHERE user_perfilcnpj_matriz = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $matriz);
        $stmt->execute();
        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

    public function getultusercod($cod)
    {
        $sql = "SELECT user_perfilnome_user FROM user_perfil WHERE user_perfilcod_user = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $cod);
        $stmt->execute();
        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

  
}
