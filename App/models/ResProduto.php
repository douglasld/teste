<?php

use App\Core\Model;

class ResProduto extends Model
{
    public $res_produtocnpj_estab;
    public $res_produtovl_meta;
    public $res_produtovl_minimo;
    public $res_produtovl_variavel;
    public $res_produtovl_adic;
    public $res_produtovl_bonus;
    public $res_produtodt_movto;
    public $res_produtocod_unid_neg;
    public $res_produtocod_gerente;
    public $res_produtocod_vendedor;
    public $res_produtocod_gru_prod;
    public $res_produtocod_produto;
    public $res_produtovl_real;

    public function save()
    {

        $sql = "INSERT INTO res_produto(res_produtocnpj_estab,res_produtodt_movto,res_produtocod_unid_neg,res_produtocod_gerente,res_produtocod_vendedor,res_produtocod_gru_prod,res_produtocod_produto,res_produtovl_real)VALUES(?,?,?,?,?,?,?,?)";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$this->res_produtocnpj_estab);
        $stmt->bindValue(2,$this->res_produtodt_movto);
        $stmt->bindValue(3,$this->res_produtocod_unid_neg);
        $stmt->bindValue(4,$this->res_produtocod_gerente);
        $stmt->bindValue(5,$this->res_produtocod_vendedor);
        $stmt->bindValue(6,$this->res_produtocod_gru_prod);
        $stmt->bindValue(7,$this->res_produtocod_produto);
        $stmt->bindValue(8,$this->res_produtovl_real);

        if($stmt->execute()):
            return "Cadastrado com sucesso";
        else:
            return "Erro ao cadastrar";
        endif;

    }

    public function update($id)
    {
        $sql = "UPDATE res_produto SET res_produtocnpj_estab = ?,res_produtodt_movto = ?,res_produtocod_unid_neg = ?,res_produtocod_gerente = ?,res_produtocod_vendedor = ?,res_produtocod_gru_prod = ?,res_produtocod_produto = ?,res_produtovl_real = ? WHERE res_produtoid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$this->res_produtocnpj_estab);
        $stmt->bindValue(2,$this->res_produtodt_movto);
        $stmt->bindValue(3,$this->res_produtocod_unid_neg);
        $stmt->bindValue(4,$this->res_produtocod_gerente);
        $stmt->bindValue(5,$this->res_produtocod_vendedor);
        $stmt->bindValue(6,$this->res_produtocod_gru_prod);
        $stmt->bindValue(7,$this->res_produtocod_produto);
        $stmt->bindValue(8,$this->res_produtovl_real);
        $stmt->bindValue(9,$id);

        if($stmt->execute()):
            return "Atualizado com sucesso";
        else:
            return "Erro ao atualizar";
        endif;
    }

    public function delete($id)
    {
        $sql = "DELETE FROM res_produto WHERE res_produtoid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$id);

        if($stmt->execute()):
            return "Excluido com sucesso";
        else:
            return "Erro ao excluir";
        endif;
    }

    public function deleteLimpa()
    {
        $sql = "DELETE FROM res_produto";
        $stmt = Model::getConn()->prepare($sql);

        if($stmt->execute()):
            return "Excluido com sucesso";
        else:
            return "Erro ao excluir";
        endif;
    }

    public function deleteProduto($prod)
    {
        $sql = "DELETE FROM res_produto WHERE res_produtocod_produto = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$prod);

        if($stmt->execute()):
            return "Excluido com sucesso";
        else:
            return "Erro ao excluir";
        endif;
    }

    public function deleteEstab($estab)
    {
        $sql = "DELETE FROM res_produto WHERE res_produtocnpj_estab = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$estab);

        if($stmt->execute()):
            return "Excluido com sucesso";
        else:
            return "Erro ao excluir";
        endif;
    }

    public function getAll()
    {
        $sql = "SELECT * FROM res_produto";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

    public function getAllEstabelecimento($id)
    {
        $sql = "SELECT * FROM res_produto WHERE res_produtocnpj_estab = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$id);
        $stmt->execute();

        
        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

    public function findId($id)
    {
        $sql = "SELECT * FROM res_produto WHERE res_produtoid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$id);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

    public function verificaResumo($estab,$data,$produto)
    {
        $sql = "SELECT * FROM res_estab WHERE res_estabcnpj_estab = ? AND res_estabdt_movto = ? AND res_produtocod_produto = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$estab);
        $stmt->bindValue(2,$data);
        $stmt->bindValue(3,$produto);
        $stmt->execute();
        $resultado = $stmt->rowCount();
        return $resultado;
    }

    public function atualizaResumo($id)
    {
        $sql = "UPDATE res_produto SET res_produtocnpj_estab = ?,res_produtovl_meta = ?,res_produtovl_minimo = ?,res_produtovl_variavel = ?,res_produtovl_adic = ?,res_produtovl_bonus = ?,res_produtodt_movto = ?,res_produtocod_unid_neg = ?,res_produtocod_gerente = ?,res_produtocod_vendedor = ?,res_produtocod_gru_prod = ?,res_produtocod_produto = ?,res_produtovl_real = ? WHERE res_produtoid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$this->res_produtocnpj_estab);
        $stmt->bindValue(2,$this->res_produtodt_movto);
        $stmt->bindValue(3,$this->res_produtocod_unid_neg);
        $stmt->bindValue(4,$this->res_produtocod_gerente);
        $stmt->bindValue(5,$this->res_produtocod_vendedor);
        $stmt->bindValue(6,$this->res_produtocod_gru_prod);
        $stmt->bindValue(7,$this->res_produtocod_produto);
        $stmt->bindValue(8,$this->res_produtovl_real);
        $stmt->bindValue(9,$id);

        if($stmt->execute()):
            return "Atualizado com sucesso";
        else:
            return "Erro ao atualizar";
        endif;
    }

    public function localiza($estab,$data,$produto)
    {
        $sql = "SELECT * FROM res_estab WHERE res_estabcnpj_estab = ? AND res_estabdt_movto = ? AND res_produtocod_produto = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$estab);
        $stmt->bindValue(2,$data);
        $stmt->bindValue(3,$data);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

}





