<?php

use App\Core\Model;

class Meta extends Model
{
    public $metacnpj_estab;
    public $metacod_meta;
    public $metadescricao;
    public $metacod_gerente;
    public $metacod_vendedor;
    public $metacod_grupo_prod;
    public $metacod_produto;
    public $metacod_grupo_prod_pai;
    public $metaunid_calc;
    public $metavl_meta;
    public $metavl_minimo;
    public $metavl_variavel;
    public $metavl_adic;
    public $metavl_bonus;
    public $metacod_meta_base;
    public $metadt_meta;
    public $metadt_ini_val;
    public $metadt_fim_val;
    public $metaind_ativo;
    public $metaind_origem;
    public $metauser_criacao;
    public $metadt_criacao;
    public $metauser_altera;
    public $metadt_altera;
    public $metacod_unid_neg;
    public $metahr_altera;
    public $metahr_criacao;

    public function save()
    {

        $sql = "INSERT INTO meta(metacnpj_estab,metacod_meta,metadescricao,metacod_gerente,metacod_vendedor,metacod_grupo_prod,metacod_produto,metacod_grupo_prod_pai,metaunid_calc,metavl_meta,metavl_minimo,metavl_variavel,metavl_adic,metavl_bonus,metacod_meta_base,metadt_meta,metadt_ini_val,metadt_fim_val,metaind_ativo,metaind_origem,metauser_criacao,metadt_criacao,metauser_altera,metadt_altera,metacod_unid_neg,metahr_altera,metahr_criacao)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $this->metacnpj_estab);
        $stmt->bindValue(2, $this->metacod_meta);
        $stmt->bindValue(3, $this->metadescricao);
        $stmt->bindValue(4, $this->metacod_gerente);
        $stmt->bindValue(5, $this->metacod_vendedor);
        $stmt->bindValue(6, $this->metacod_grupo_prod);
        $stmt->bindValue(7, $this->metacod_produto);
        $stmt->bindValue(8, $this->metacod_grupo_prod_pai);
        $stmt->bindValue(9, $this->metaunid_calc);
        $stmt->bindValue(10, $this->metavl_meta);
        $stmt->bindValue(11, $this->metavl_minimo);
        $stmt->bindValue(12, $this->metavl_variavel);
        $stmt->bindValue(13, $this->metavl_adic);
        $stmt->bindValue(14, $this->metavl_bonus);
        $stmt->bindValue(15, $this->metacod_meta_base);
        $stmt->bindValue(16, $this->metadt_meta);
        $stmt->bindValue(17, $this->metadt_ini_val);
        $stmt->bindValue(18, $this->metadt_fim_val);
        $stmt->bindValue(19, $this->metaind_ativo);
        $stmt->bindValue(20, $this->metaind_origem);
        $stmt->bindValue(21, $this->metauser_criacao);
        $stmt->bindValue(22, $this->metadt_criacao);
        $stmt->bindValue(23, $this->metauser_altera);
        $stmt->bindValue(24, $this->metadt_altera);
        $stmt->bindValue(25, $this->metacod_unid_neg);
        $stmt->bindValue(26, $this->metahr_altera);
        $stmt->bindValue(27, $this->metahr_criacao);


        if ($stmt->execute()) :
            return "Cadastrado com sucesso";
        else :
            return "Erro ao cadastrar";
        endif;
    }

    public function update($id)
    {
        $sql = "UPDATE meta SET metacnpj_estab = ?,metacod_meta = ?,metadescricao = ?,metacod_gerente = ?,metacod_vendedor = ?,metacod_grupo_prod = ?,metacod_produto = ?,metacod_grupo_prod_pai = ?,metaunid_calc = ?,metavl_meta = ?,metavl_minimo = ?,metavl_variavel = ?,metavl_adic = ?,metavl_bonus = ?,metacod_meta_base = ?,metadt_meta = ?,metadt_ini_val = ?,metadt_fim_val = ?,metaind_ativo = ?,metaind_origem = ?,metauser_altera = ?,metadt_altera = ?,metacod_unid_neg = ?,metahr_altera =? WHERE metaid = ?";
        $stmt = Model::getConn()->prepare($sql);

        $stmt->bindValue(1, $this->metacnpj_estab);
        $stmt->bindValue(2, $this->metacod_meta);
        $stmt->bindValue(3, $this->metadescricao);
        $stmt->bindValue(4, $this->metacod_gerente);
        $stmt->bindValue(5, $this->metacod_vendedor);
        $stmt->bindValue(6, $this->metacod_grupo_prod);
        $stmt->bindValue(7, $this->metacod_produto);
        $stmt->bindValue(8, $this->metacod_grupo_prod_pai);
        $stmt->bindValue(9, $this->metaunid_calc);
        $stmt->bindValue(10, $this->metavl_meta);
        $stmt->bindValue(11, $this->metavl_minimo);
        $stmt->bindValue(12, $this->metavl_variavel);
        $stmt->bindValue(13, $this->metavl_adic);
        $stmt->bindValue(14, $this->metavl_bonus);
        $stmt->bindValue(15, $this->metacod_meta_base);
        $stmt->bindValue(16, $this->metadt_meta);
        $stmt->bindValue(17, $this->metadt_ini_val);
        $stmt->bindValue(18, $this->metadt_fim_val);
        $stmt->bindValue(19, $this->metaind_ativo);
        $stmt->bindValue(20, $this->metaind_origem);
        $stmt->bindValue(21, $this->metauser_altera);
        $stmt->bindValue(22, $this->metadt_altera);
        $stmt->bindValue(23, $this->metacod_unid_neg);
        $stmt->bindValue(24, $this->metahr_altera);
        $stmt->bindValue(25, $id);       
        
        if ($stmt->execute()) :
            return "Atualizado com sucesso";
        else :
            return "Erro ao atualizar";
        endif;
    }

    public function delete($id)
    {
        $sql = "DELETE FROM meta WHERE metaid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $id);

        if ($stmt->execute()) :
            return "Excluido com sucesso";
        else :
            return "Erro ao excluir";
        endif;
    }

    public function getAll()
    {
        $sql = "SELECT * FROM meta";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        } else {
            return [];
        }
    }

    public function getAllEstabelecimento($id)
    {
        
        $sql = "SELECT * FROM meta WHERE metacnpj_estab = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();


        if ($stmt->rowCount() > 0) {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        } else {
            return [];
        }
    }

    public function getAllID($id)
    {
        
        $sql = "SELECT * FROM meta WHERE metaid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();


        if ($stmt->rowCount() > 0) {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        } else {
            return [];
        }
    }

    public function findId($id)
    {
        $sql = "SELECT * FROM meta a";
        $sql = $sql . " LEFT JOIN grupo_prod b ON b.grupo_prodid = a.metacod_grupo_prod";
        $sql = $sql . " LEFT JOIN produto c ON c.produtoid = a.metacod_produto";
        $sql = $sql . " LEFT JOIN unid_med d ON d.unid_medid = a.metaunid_calc";
        $sql = $sql . " LEFT JOIN empresa e ON e.empresaid = a.metacnpj_estab";
        $sql = $sql . " LEFT JOIN unid_neg f ON f.unid_negid = a.metacod_unid_neg";
        $sql = $sql . " WHERE a.metaid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
            return $resultado;
        } else {
            return [];
        }
    }

    public function ultimo($estab)
    {
        $sql = "SELECT * FROM meta WHERE metacnpj_estab = ? ORDER BY metaid DESC";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $estab);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
            return $resultado;
        } else {
            return [];
        }
    }

    public function ultimoJson($estab)
    {
        $sql = "SELECT * FROM meta WHERE metacnpj_estab = ? ORDER BY metaid DESC";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $estab);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
            return json_encode($resultado);
        } else {
            return [];
        }
    }

    //Consultas Montar Resumos por estabelecimento
    public function somaValorTotalMeta($estab)
    {
        $sql = "SELECT sum(metavl_meta) as soma from meta where metacnpj_estab = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $estab);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $resultado;
    }

    public function somaValorMinimoMeta($estab)
    {
        $sql = "SELECT sum(metavl_minimo) as soma from meta where metacnpj_estab = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $estab);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $resultado;
    }

    public function somaValorVariavelMeta($estab)
    {
        $sql = "SELECT sum(metavl_variavel) as soma from meta where metacnpj_estab = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $estab);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $resultado;
    }

    public function somaValorAdicionalMeta($estab)
    {
        $sql = "SELECT sum(metavl_adic) as soma from meta where metacnpj_estab = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $estab);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $resultado;
    }

    public function somaValorBonusMeta($estab)
    {
        $sql = "SELECT sum(metavl_bonus) as soma from meta where metacnpj_estab = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $estab);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $resultado;
    }
    //Consultas Montar Resumos por estabelecimento

    //Consultas Montar Resumos por PRODUTO
    public function somaValorPorProduto($estab, $produto)
    {
        $sql = "SELECT sum(metavl_meta) as soma from meta where metacnpj_estab = ? AND metacod_produto = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $estab);
        $stmt->bindValue(2, $produto);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $resultado;
    }

    public function somaValorMinimoPorProduto($estab, $produto)
    {
        $sql = "SELECT sum(metavl_minimo) as soma from meta where metacnpj_estab = ? AND metacod_produto = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $estab);
        $stmt->bindValue(2, $produto);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $resultado;
    }

    public function somaValorVariavelPorProduto($estab, $produto)
    {
        $sql = "SELECT sum(metavl_variavel) as soma from meta where metacnpj_estab = ? AND metacod_produto = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $estab);
        $stmt->bindValue(2, $produto);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $resultado;
    }

    public function somaValorAdicionalPorProduto($estab, $produto)
    {
        $sql = "SELECT sum(metavl_adic) as soma from meta where metacnpj_estab = ? AND metacod_produto = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $estab);
        $stmt->bindValue(2, $produto);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $resultado;
    }

    public function somaValorBonusPorProduto($estab, $produto)
    {
        $sql = "SELECT sum(metavl_bonus) as soma from meta where metacnpj_estab = ? AND metacod_produto = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $estab);
        $stmt->bindValue(2, $produto);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $resultado;
    }

    //Consultas Montar Resumos por PRODUTO

    //Consultas Montar Resumos por GERENTE
    public function somaValorTotalGerente($gerente, $estab, $unidade)
    {
        $sql = "SELECT sum(metavl_meta) as soma from meta where metacod_gerente = ? AND metacnpj_estab = ? AND metacod_unid_neg  = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $gerente);
        $stmt->bindValue(2, $estab);
        $stmt->bindValue(3, $unidade);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $resultado;
    }

    public function somaValorMinimoGerente($gerente, $estab, $unidade)
    {
        $sql = "SELECT sum(metavl_minimo) as soma from meta where metacod_gerente = ? AND metacnpj_estab = ? AND metacod_unid_neg  = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $gerente);
        $stmt->bindValue(2, $estab);
        $stmt->bindValue(3, $unidade);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $resultado;
    }

    public function somaValorVariavelGerente($gerente, $estab, $unidade)
    {
        $sql = "SELECT sum(metavl_variavel) as soma from meta where metacod_gerente = ? AND metacnpj_estab = ? AND metacod_unid_neg  = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $gerente);
        $stmt->bindValue(2, $estab);
        $stmt->bindValue(3, $unidade);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $resultado;
    }

    public function somaValorAdicionalGerente($gerente, $estab, $unidade)
    {
        $sql = "SELECT sum(metavl_adic) as soma from meta where metacod_gerente = ? AND metacnpj_estab = ? AND metacod_unid_neg  = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $gerente);
        $stmt->bindValue(2, $estab);
        $stmt->bindValue(3, $unidade);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $resultado;
    }

    public function somaValorBonusGerente($gerente, $estab, $unidade)
    {
        $sql = "SELECT sum(metavl_bonus) as soma from meta where metacod_gerente = ? AND metacnpj_estab = ? AND metacod_unid_neg  = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $gerente);
        $stmt->bindValue(2, $estab);
        $stmt->bindValue(3, $unidade);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $resultado;
    }
    //Consultas Montar Resumos por GERENTE

    //Consultas Montar Resumos por VENDEDOR
    public function somaValorTotalVendedor($vendedor, $estab, $unidade)
    {
        $sql = "SELECT sum(metavl_meta) as soma from meta where metacod_vendedor = ? AND metacnpj_estab = ? AND metacod_unid_neg  = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $vendedor);
        $stmt->bindValue(2, $estab);
        $stmt->bindValue(3, $unidade);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $resultado;
    }

    public function somaValorTotalVendedorMinimo($vendedor, $estab, $unidade)
    {
        $sql = "SELECT sum(metavl_minimo) as soma from meta where metacod_vendedor = ? AND metacnpj_estab = ? AND metacod_unid_neg  = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $vendedor);
        $stmt->bindValue(2, $estab);
        $stmt->bindValue(3, $unidade);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $resultado;
    }

    public function somaValorTotalVendedorVariavel($vendedor, $estab, $unidade)
    {
        $sql = "SELECT sum(metavl_variavel) as soma from meta where metacod_vendedor = ? AND metacnpj_estab = ? AND metacod_unid_neg  = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $vendedor);
        $stmt->bindValue(2, $estab);
        $stmt->bindValue(3, $unidade);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $resultado;
    }

    public function somaValorTotalVendedorAdicional($vendedor, $estab, $unidade)
    {
        $sql = "SELECT sum(metavl_adic) as soma from meta where metacod_vendedor = ? AND metacnpj_estab = ? AND metacod_unid_neg  = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $vendedor);
        $stmt->bindValue(2, $estab);
        $stmt->bindValue(3, $unidade);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $resultado;
    }

    public function somaValorTotalVendedorBonus($vendedor, $estab, $unidade)
    {
        $sql = "SELECT sum(metavl_bonus) as soma from meta where metacod_vendedor = ? AND metacnpj_estab = ? AND metacod_unid_neg  = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $vendedor);
        $stmt->bindValue(2, $estab);
        $stmt->bindValue(3, $unidade);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $resultado;
    }
    //Consultas Montar Resumos por VENDEDOR

    //Consultas Montar Resumos por META

    public function somaUnidValorTotal($id)
    {
        $sql = "SELECT sum(metavl_meta) as soma from meta where metacod_unid_neg = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $resultado;
    }

    public function somaUnidValorMinimo($id)
    {
        $sql = "SELECT sum(metavl_meta) as soma from meta where metacod_unid_neg = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $resultado;
    }

    public function somaUnidValorVariavel($id)
    {
        $sql = "SELECT sum(metavl_meta) as soma from meta where metacod_unid_neg = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $resultado;
    }

    public function somaUnidValorAdicional($id)
    {
        $sql = "SELECT sum(metavl_meta) as soma from meta where metacod_unid_neg = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $resultado;
    }

    public function somaUnidValorBonus($id)
    {
        $sql = "SELECT sum(metavl_meta) as soma from meta where metacod_unid_neg = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $resultado;
    }

    //Consultas Montar Resumos por META


    //Consultas Montar Resumos por GRUPO PRODUTO
    public function somaValorPorGrupo($estab, $grupo, $unid)
    {
        $sql = "SELECT sum(metavl_meta) as soma from meta where metacnpj_estab = ? AND metacod_produto = ? AND metacod_unid_neg = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $estab);
        $stmt->bindValue(2, $grupo);
        $stmt->bindValue(3, $unid);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $resultado;
    }

    public function somaValorMinimoPorGrupo($estab, $grupo, $unid)
    {
        $sql = "SELECT sum(metavl_minimo) as soma from meta where metacnpj_estab = ? AND metacod_produto = ? AND metacod_unid_neg = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $estab);
        $stmt->bindValue(2, $grupo);
        $stmt->bindValue(3, $unid);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $resultado;
    }

    public function somaValorVariavelPorGrupo($estab, $grupo, $unid)
    {
        $sql = "SELECT sum(metavl_variavel) as soma from meta where metacnpj_estab = ? AND metacod_produto = ? AND metacod_unid_neg = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $estab);
        $stmt->bindValue(2, $grupo);
        $stmt->bindValue(3, $unid);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $resultado;
    }

    public function somaValorAdicionalPorGrupo($estab, $grupo, $unid)
    {
        $sql = "SELECT sum(metavl_adic) as soma from meta where metacnpj_estab = ? AND metacod_produto = ? AND metacod_unid_neg = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $estab);
        $stmt->bindValue(2, $grupo);
        $stmt->bindValue(3, $unid);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $resultado;
    }

    public function somaValorBonusPorGrupo($estab, $grupo, $unid)
    {
        $sql = "SELECT sum(metavl_bonus) as soma from meta where metacnpj_estab = ? AND metacod_produto = ? AND metacod_unid_neg = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $estab);
        $stmt->bindValue(2, $grupo);
        $stmt->bindValue(3, $unid);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $resultado;
    }

    //Consultas Montar Resumos por GRUPO PRODUTO

    //Consultas Montar Resumos por MATRIZ
    public function somaValorMatriz($matriz)
    {
        $sql = "SELECT sum(metavl_meta) as soma from meta a LEFT JOIN empresa b ON b.empresacnpj_estab = a.metacnpj_estab where b.empresacnpj_matriz = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $matriz);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $resultado;
    }

    public function somaValorMinimoMatriz($matriz)
    {
        $sql = "SELECT sum(metavl_minimo) as soma from meta a LEFT JOIN empresa b ON b.empresacnpj_estab = a.metacnpj_estab where b.empresacnpj_matriz = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $matriz);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $resultado;
    }

    public function somaValorVariavelMatriz($matriz)
    {
        $sql = "SELECT sum(metavl_variavel) as soma from meta a LEFT JOIN empresa b ON b.empresacnpj_estab = a.metacnpj_estab where b.empresacnpj_matriz = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $matriz);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $resultado;
    }

    public function somaValorAdicionalMatriz($matriz)
    {
        $sql = "SELECT sum(metavl_adic) as soma from meta a LEFT JOIN empresa b ON b.empresacnpj_estab = a.metacnpj_estab where b.empresacnpj_matriz = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $matriz);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $resultado;
    }

    public function somaValorBonusMatriz($matriz)
    {
        $sql = "SELECT sum(metavl_bonus) as soma from meta a LEFT JOIN empresa b ON b.empresacnpj_estab = a.metacnpj_estab where b.empresacnpj_matriz = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $matriz);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $resultado;
    }
    //Consultas Montar Resumos por MATRIZ

    public function GetUltimaMeta($estab)
    {
        $sql = "SELECT MAX(metacod_meta) as last_meta FROM meta WHERE metacnpj_estab = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $estab);
        $stmt->execute();
        $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        $val = $resultado['0']['last_meta'];
        return $val + 1;
    }
}
