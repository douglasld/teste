<?php

use App\Core\Model;

class UnidMedLog extends Model
{

    public $unid_med_logcnpj_estab;
    public $unid_med_logcod_unid_med;
    public $unid_med_logdt_emis;
    public $unid_med_loghr_emis;
    public $unid_med_loguser_emis;
    public $unid_med_logtitulo;


    public function save()
    {
        $sql = "INSERT INTO unid_med_log(unid_med_logcnpj_estab,unid_med_logcod_unid_med,unid_med_logdt_emis,unid_med_loghr_emis,unid_med_loguser_emis,unid_med_logtitulo)VALUES(?,?,?,?,?,?)";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$this->unid_med_logcnpj_estab);
        $stmt->bindValue(2,$this->unid_med_logcod_unid_med);
        $stmt->bindValue(3,$this->unid_med_logdt_emis);
        $stmt->bindValue(4,$this->unid_med_loghr_emis);
        $stmt->bindValue(5,$this->unid_med_loguser_emis);
        $stmt->bindValue(6,$this->unid_med_logtitulo);


        if($stmt->execute()):
            return "Log Cadastrado com sucesso";
        else:
            return "Erro ao cadastrar Log";
        endif;

    }

    public function getAll()
    {
        $sql = "SELECT * FROM unid_med_log LEFT JOIN user_perfil a ON a.user_perfilid = unid_med_loguser_emis";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

    public function findId($id)
    {
        $sql = "SELECT * FROM unid_med_log a LEFT JOIN unid_med b ON b.unid_medid = a.unid_med_logcod_unid_med LEFT JOIN user_perfil c ON c.user_perfilid = a.unid_med_loguser_emis  WHERE a.unid_med_logcod_unid_med = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$id);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }


}



