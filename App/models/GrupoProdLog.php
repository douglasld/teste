<?php

use App\Core\Model;

class GrupoProdLog extends Model
{

    public $grupo_prod_logcnpj_estab;
    public $grupo_prod_logcod_grupo_prod;
    public $grupo_prod_logdt_emis;
    public $grupo_prod_loghr_emis;
    public $grupo_prod_loguser_emis;
    public $grupo_prod_logtitulo;

    public function save()
    {
        $sql = "INSERT INTO grupo_prod_log(grupo_prod_logcnpj_estab,grupo_prod_logcod_grupo_prod,grupo_prod_logdt_emis,grupo_prod_loghr_emis,grupo_prod_loguser_emis,grupo_prod_logtitulo)VALUES(?,?,?,?,?,?)";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$this->grupo_prod_logcnpj_estab);
        $stmt->bindValue(2,$this->grupo_prod_logcod_grupo_prod);
        $stmt->bindValue(3,$this->grupo_prod_logdt_emis);
        $stmt->bindValue(4,$this->grupo_prod_loghr_emis);
        $stmt->bindValue(5,$this->grupo_prod_loguser_emis);
        $stmt->bindValue(6,$this->grupo_prod_logtitulo);

        if($stmt->execute()):
           return "Cadastrado com sucesso";
        else:
            return "Erro ao cadastrar";
        endif;

    }

    public function getAll()
    {
        $sql = "SELECT * FROM grupo_prod_log";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

    public function findId($id)
    {
        $sql = "SELECT * FROM grupo_prod_log a LEFT JOIN user_perfil b ON b.user_perfilid = a.grupo_prod_loguser_emis WHERE a.grupo_prod_logcod_grupo_prod = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$id);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }


}

