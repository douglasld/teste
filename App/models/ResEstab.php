<?php

use App\Core\Model;

class ResEstab extends Model
{

    public $res_estabcnpj_estab;
    public $res_estabvl_meta;
    public $res_estabvl_minimo;
    public $res_estabvl_variavel;
    public $res_estabvl_adic;
    public $res_estabvl_bonus;
    public $res_estabdt_movto;
    public $res_estabvl_real;

    public function save()
    {

        $sql = "INSERT INTO res_estab(res_estabcnpj_estab,res_estabdt_movto,res_estabvl_real)VALUES(?,?,?)";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$this->res_estabcnpj_estab);
        $stmt->bindValue(2,$this->res_estabdt_movto);
        $stmt->bindValue(3,$this->res_estabvl_real);

        if($stmt->execute()):
            return "Cadastrado com sucesso";
        else:
            return "Erro ao cadastrar";
        endif;

    }

    public function update($id)
    {
        $sql = "UPDATE res_estab SET res_estabdt_movto = ?,res_estabvl_real = ? WHERE res_estabid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$this->res_estabdt_movto);
        $stmt->bindValue(2,$this->res_estabvl_real);
        $stmt->bindValue(3,$id);

        if($stmt->execute()):
            return "Atualizado com sucesso";
        else:
            return "Erro ao atualizar";
        endif;
    }

    public function delete($id)
    {
        $sql = "DELETE FROM res_estab WHERE res_estabid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$id);

        if($stmt->execute()):
            return "Excluido com sucesso";
        else:
            return "Erro ao excluir";
        endif;
    }

    public function deleteEstab($estab)
    {
        $sql = "DELETE FROM res_estab WHERE res_estabcnpj_estab = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$estab);

        if($stmt->execute()):
            return "Excluido com sucesso";
        else:
            return "Erro ao excluir";
        endif;
    }

    public function getAll()
    {
        $sql = "SELECT * FROM res_estab";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

    public function getAllEstabelecimento($id)
    {
        $sql = "SELECT * FROM res_estab WHERE res_estabcnpj_estab = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$id);
        $stmt->execute();

       
        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

    public function findId($id)
    {
        $sql = "SELECT * FROM res_estab WHERE res_estabid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$id);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

    public function localizaData($estab,$data)
    {
        $sql = "SELECT * FROM res_estab WHERE res_estabcnpj_estab = ? AND res_estabdt_movto = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$estab);
        $stmt->bindValue(2,$data);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

    public function verificaData($estab,$data)
    {
        $sql = "SELECT * FROM res_estab WHERE res_estabcnpj_estab = ? AND res_estabdt_movto = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$estab);
        $stmt->bindValue(2,$data);
        $stmt->execute();
        $resultado = $stmt->rowCount();
        return $resultado;
    }

    public function atualizaResumo($id)
    {
        $sql = "UPDATE res_estab SET res_estabvl_real = ?, res_estabvl_meta = ?,res_estabvl_minimo = ?,res_estabvl_variavel = ?,res_estabvl_adic = ?,res_estabvl_bonus = ? WHERE res_estabid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$this->res_estabvl_real);
        $stmt->bindValue(2,$this->res_estabvl_meta);
        $stmt->bindValue(3,$this->res_estabvl_minimo);
        $stmt->bindValue(4,$this->res_estabvl_variavel);
        $stmt->bindValue(5,$this->res_estabvl_adic);
        $stmt->bindValue(6,$this->res_estabvl_bonus);
        $stmt->bindValue(7,$id);

        if($stmt->execute()):
            return "Valor Atualizado com sucesso";
        else:
            return "Erro ao atualizar";
        endif;
    }

    public function gravaResumo($data,$estab)
    {
        $sql = "INSERT INTO res_estab (res_estabvl_real,res_estabdt_movto,res_estabcnpj_estab)VALUES(?,?,?)";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$this->res_estabvl_real);
        $stmt->bindValue(2,$data);
        $stmt->bindValue(3,$estab);

        if($stmt->execute()):
            return "Valor Atualizado com sucesso";
        else:
            return "Erro ao atualizar";
        endif;
    }
}


