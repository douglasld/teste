<?php

use App\Core\Model;

class VendProds extends Model
{
    public $cod_prod;
    public $cod_loja;
    public $nome_prod_v;
    public $desc_prod;
    public $val_prod;
    public $porc_serv;
    public $cor_azul;
    public $cor_verde;
    public $cor_vermelho;
    public $cor_preto;
    public $cor_branco;
    public $cor_marrom;
    public $cor_cinza;
    public $cor_rosa;
    public $cor_amarelo;
    public $qtd_cor_azul;
    public $qtd_cor_verde;
    public $qtd_cor_vermelho;
    public $qtd_cor_preto;
    public $qtd_cor_branco;
    public $qtd_cor_marrom;
    public $qtd_cor_cinza;
    public $qtd_cor_rosa;
    public $qtd_cor_amarelo;
    public $cod_cat_p;
    public $cod_cat_f;
    public $materiais;
    public $cod_foto_1;
    public $cod_foto_2;
    public $cod_foto_3;
    public $cod_foto_4;
    public $cod_foto_5;
    public $peso;
    public $unidade_peso;
    public $altura;
    public $largura;
    public $comprimento;
    public $fragilidade;
    public $retirada_mao;
    public $garantia_opcao;
    public $garantia_meses;
    public $estado_prod;
    public $dt_criacao;
    public $hr_criacao;
    public $cm3;

    public function save()
    {

        $sql = "INSERT INTO produto_usuario(cod_prod,cod_loja,nome_prod_v,desc_prod,val_prod,porc_serv,cor_azul,cor_verde,cor_vermelho,
        cor_preto,cor_branco,cor_marrom,cor_cinza,cor_rosa,cor_amarelo,qtd_cor_azul,qtd_cor_verde,qtd_cor_vermelho,qtd_cor_preto,
        qtd_cor_branco,qtd_cor_marrom,qtd_cor_cinza,qtd_cor_rosa,qtd_cor_amarelo,cod_cat_p,cod_cat_f,cm3,materiais,cod_foto_1,
        cod_foto_2,cod_foto_3,cod_foto_4,cod_foto_5,peso,unidade_peso,altura,largura,comprimento,fragilidade,retirada_mao,
        garantia_opcao,garantia_meses,estado_prod,confirma_responsabilidade,status_prod   
        )VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $this->cod_prod);
        $stmt->bindValue(2, $this->cod_loja);
        $stmt->bindValue(3, $this->nome_prod_v);
        $stmt->bindValue(4, $this->desc_prod);
        $stmt->bindValue(5, $this->val_prod);
        $stmt->bindValue(6, $this->porc_serv);
        $stmt->bindValue(7, $this->cor_azul);
        $stmt->bindValue(8, $this->cor_verde);
        $stmt->bindValue(9, $this->cor_vermelho);
        $stmt->bindValue(10, $this->cor_preto);
        $stmt->bindValue(11, $this->cor_branco);
        $stmt->bindValue(12, $this->cor_marrom);
        $stmt->bindValue(13, $this->cor_cinza);
        $stmt->bindValue(14, $this->cor_rosa);
        $stmt->bindValue(15, $this->cor_amarelo);
        $stmt->bindValue(16, $this->qtd_cor_azul);
        $stmt->bindValue(17, $this->qtd_cor_verde);
        $stmt->bindValue(18, $this->qtd_cor_vermelho);
        $stmt->bindValue(19, $this->qtd_cor_preto);
        $stmt->bindValue(20, $this->qtd_cor_branco);
        $stmt->bindValue(21, $this->qtd_cor_marrom);
        $stmt->bindValue(22, $this->qtd_cor_cinza);
        $stmt->bindValue(23, $this->qtd_cor_rosa);
        $stmt->bindValue(24, $this->qtd_cor_amarelo);
        $stmt->bindValue(25, $this->cod_cat_p);
        $stmt->bindValue(26, $this->cod_cat_f);
        $stmt->bindValue(27, $this->cm3);
        $stmt->bindValue(28, $this->materiais);
        $stmt->bindValue(29, $this->cod_foto_1);
        $stmt->bindValue(30, $this->cod_foto_2);
        $stmt->bindValue(31, $this->cod_foto_3);
        $stmt->bindValue(32, $this->cod_foto_4);
        $stmt->bindValue(33, $this->cod_foto_5);
        $stmt->bindValue(34, $this->peso);
        $stmt->bindValue(35, $this->unidade_peso);
        $stmt->bindValue(36, $this->altura);
        $stmt->bindValue(37, $this->largura);
        $stmt->bindValue(38, $this->comprimento);
        $stmt->bindValue(39, $this->fragilidade);
        $stmt->bindValue(40, $this->retirada_mao);
        $stmt->bindValue(41, $this->garantia_opcao);
        $stmt->bindValue(42, $this->garantia_meses);
        $stmt->bindValue(43, $this->estado_prod);
        $stmt->bindValue(44, "1");
        $stmt->bindValue(45, "1");
        if ($stmt->execute()) :
            return "1 - Produto cadastrado com sucesso";
        else :
            return "3 - Erro ao cadastrar produto";
        endif;
    }

    public function update($id)
    {


        $sql = "UPDATE produto_usuario SET nome_prod_v = ?,desc_prod = ?,val_prod = ?,cor_azul = ?,
        cor_verde = ?,cor_vermelho = ?,cor_preto = ?,cor_branco = ?,cor_marrom = ?,cor_cinza = ?,
        cor_rosa = ?,cor_amarelo = ?,qtd_cor_azul = ?,qtd_cor_verde = ?,qtd_cor_vermelho = ?,qtd_cor_preto = ?,
        qtd_cor_branco = ?,qtd_cor_marrom = ?,qtd_cor_cinza = ?,qtd_cor_rosa = ?,qtd_cor_amarelo = ?,cm3 = ?,
        cod_foto_1 = ?,cod_foto_2 = ?,cod_foto_3 = ?,cod_foto_4 = ?,cod_foto_5 = ?,peso = ?,unidade_peso = ?,altura = ?,
        largura = ?,comprimento = ?,fragilidade = ?,retirada_mao = ?,garantia_opcao = ?,garantia_meses = ?,estado_prod = ?
        WHERE cod_uni_prod = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $this->nome_prod_v);
        $stmt->bindValue(2, $this->desc_prod);
        $stmt->bindValue(3, $this->val_prod);
        $stmt->bindValue(4, $this->cor_azul);
        $stmt->bindValue(5, $this->cor_verde);
        $stmt->bindValue(6, $this->cor_vermelho);
        $stmt->bindValue(7, $this->cor_preto);
        $stmt->bindValue(8, $this->cor_branco);
        $stmt->bindValue(9, $this->cor_marrom);
        $stmt->bindValue(10, $this->cor_cinza);
        $stmt->bindValue(11, $this->cor_rosa);
        $stmt->bindValue(12, $this->cor_amarelo);
        $stmt->bindValue(13, $this->qtd_cor_azul);
        $stmt->bindValue(14, $this->qtd_cor_verde);
        $stmt->bindValue(15, $this->qtd_cor_vermelho);
        $stmt->bindValue(16, $this->qtd_cor_preto);
        $stmt->bindValue(17, $this->qtd_cor_branco);
        $stmt->bindValue(18, $this->qtd_cor_marrom);
        $stmt->bindValue(19, $this->qtd_cor_cinza);
        $stmt->bindValue(20, $this->qtd_cor_rosa);
        $stmt->bindValue(21, $this->qtd_cor_amarelo);
        $stmt->bindValue(22, $this->cm3);
        $stmt->bindValue(23, $this->cod_foto_1);
        $stmt->bindValue(24, $this->cod_foto_2);
        $stmt->bindValue(25, $this->cod_foto_3);
        $stmt->bindValue(26, $this->cod_foto_4);
        $stmt->bindValue(27, $this->cod_foto_5);
        $stmt->bindValue(28, $this->peso);
        $stmt->bindValue(29, $this->unidade_peso);
        $stmt->bindValue(30, $this->altura);
        $stmt->bindValue(31, $this->largura);
        $stmt->bindValue(32, $this->comprimento);
        $stmt->bindValue(33, $this->fragilidade);
        $stmt->bindValue(34, $this->retirada_mao);
        $stmt->bindValue(35, $this->garantia_opcao);
        $stmt->bindValue(36, $this->garantia_meses);
        $stmt->bindValue(37, $this->estado_prod);
        $stmt->bindValue(38, $id);
        if ($stmt->execute()) :
            return "1 - Produto atualziado com sucesso";
        else :
            return "3 - Erro ao atualizar produto";
        endif;
    }

    public function getAll()
    {
        $sql = "SELECT nome_prod,cod_prod FROM produtos_tabela";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        } else {
            return [];
        }
    }

    public function VerificaProd($cod_prod)
    {
        $sql = "SELECT cod_prod,cod_cat_p FROM produtos_tabela WHERE cod_prod = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $cod_prod);
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        } else {
            return [];
        }
    }

    public function VerificaFotos($cod_prod)
    {
        $sql = "SELECT cod_foto_1,cod_foto_2,cod_foto_3,cod_foto_4,cod_foto_5 FROM produto_usuario WHERE cod_uni_prod = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $cod_prod);
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        } else {
            return [];
        }
    }

    public function VerificaProdEdita($id)
    {
        $sql = "SELECT cod_prod,cod_cat_p FROM produtos_tabela WHERE cod_prod = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        } else {
            return [];
        }
    }
}
