<?php

use App\Core\Model;

class EmpresaLog extends Model
{

    public $empresa_logcnpj_matriz;
    public $empresa_logcnpj_estab;
    public $empresa_logdt_emis;
    public $empresa_loghr_emis;
    public $empresa_loguser_emis;
    public $empresa_logtitulo;

    public function save()
    {
        $sql = "INSERT INTO empresa_log(empresa_logcnpj_matriz,empresa_logcnpj_estab,empresa_logdt_emis,empresa_loghr_emis,empresa_loguser_emis,empresa_logtitulo)VALUES(?,?,?,?,?,?)";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$this->empresa_logcnpj_matriz);
        $stmt->bindValue(2,$this->empresa_logcnpj_estab);
        $stmt->bindValue(3,$this->empresa_logdt_emis);
        $stmt->bindValue(4,$this->empresa_loghr_emis);
        $stmt->bindValue(5,$this->empresa_loguser_emis);
        $stmt->bindValue(6,$this->empresa_logtitulo);
    

        if($stmt->execute()):
            return "Log Cadastrado com sucesso";
        else:
            return "Log Erro ao cadastrar";
        endif;

    }

    public function getAll()
    {
        $sql = "SELECT * FROM empresa_log";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

    public function findId($id)
    {
        $sql = "SELECT * FROM empresa_log a LEFT JOIN empresa b ON b.empresacnpj_estab = a.empresa_logcnpj_estab LEFT JOIN user_perfil c ON c.user_perfilid = a.empresa_loguser_emis WHERE a.empresa_logcnpj_estab = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$id);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

    public function findCnpjEstab($cnpjestab){
        $sql = "SELECT * FROM empresa_log a LEFT JOIN empresa b ON b.empresacnpj_estab = a.empresa_logcnpj_estab LEFT JOIN user_perfil c ON c.user_perfilid = a.empresa_loguser_emis  WHERE a.empresa_logcnpj_estab = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$cnpjestab);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }
}
