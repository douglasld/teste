<?php

use App\Core\Model;

class UserEstabLog extends Model
{

    public $user_estab_logcnpj_matriz;
    public $user_estab_logcod_user;
    public $user_estab_logdt_emis;
    public $user_estab_loghr_emis;
    public $user_estab_loguser_emis;
    public $user_estab_logtitulo;
    public $user_estab_lognome_user;

    public function save()
    {
        $sql = "INSERT INTO user_estab_log(user_estab_logcnpj_matriz,user_estab_logcod_user,user_estab_logdt_emis,user_estab_loghr_emis,user_estab_loguser_emis,user_estab_logtitulo,user_estab_lognome_user)VALUES(?,?,?,?,?,?,?)";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$this->user_estab_logcnpj_matriz);
        $stmt->bindValue(2,$this->user_estab_logcod_user);
        $stmt->bindValue(3,$this->user_estab_logdt_emis);
        $stmt->bindValue(4,$this->user_estab_loghr_emis);
        $stmt->bindValue(5,$this->user_estab_loguser_emis);
        $stmt->bindValue(6,$this->user_estab_logtitulo);
        $stmt->bindValue(7,$this->user_estab_lognome_user);


        if($stmt->execute()):
            return "Cadastrado com sucesso";
        else:
            return "Erro ao cadastrar Log";
        endif;

    }

    public function getAll()
    {
        $sql = "SELECT * FROM user_estab_log";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

    public function findId($id)
    {
        $sql = "SELECT * FROM user_estab_log LEFT JOIN user_perfil b ON b.user_perfilid = user_perfil_logcod_user WHERE user_perfil_logcod_user = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$id);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }
}

