<?php

use App\Core\Model;

class ProdutoLog extends Model
{

    public $produto_logcnpj_estab;
    public $produto_logdt_emis;
    public $produto_loghr_emis;
    public $produto_loguser_emis;
    public $produto_logtitulo;
    public $produto_logcod_produto;

    public function save()
    {
        $sql = "INSERT INTO produto_log(produto_logcnpj_estab,produto_logdt_emis,produto_loghr_emis,produto_loguser_emis,produto_logtitulo,produto_logcod_produto)VALUES(?,?,?,?,?,?)";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$this->produto_logcnpj_estab);
        $stmt->bindValue(2,$this->produto_logdt_emis);
        $stmt->bindValue(3,$this->produto_loghr_emis);
        $stmt->bindValue(4,$this->produto_loguser_emis);
        $stmt->bindValue(5,$this->produto_logtitulo);
        $stmt->bindValue(6,$this->produto_logcod_produto);

        if($stmt->execute()):
            return "Cadastrado com sucesso";
        else:
            return "Erro ao cadastrar";
        endif;

    }

    public function getAll()
    {
        $sql = "SELECT * FROM produto_log a LEFT JOIN user_perfil b ON b.user_perfilid = a.produto_loguser_emis";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

    public function findId($id)
    {
        $sql = "SELECT * FROM produto_log a LEFT JOIN user_perfil b ON b.user_perfilid = a.produto_loguser_emis WHERE a.produto_logcod_produto = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$id);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }


}


