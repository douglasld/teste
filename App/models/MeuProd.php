<?php

use App\Core\Model;

class MeuProd extends Model
{

    public $produtocnpj_estab;
    public $produtocod_produto;
    public $produtodescricao;
    public $produtocod_unid_med;
    public $produtoind_ativo;
    public $produtoind_origem;
    public $produtodt_criacao;
    public $produtohr_criacao;
    public $produtouser_criacao;
    public $produtodt_altera;
    public $produtohr_altera;
    public $produtouser_altera;

    public function save()
    {

        $sql = "INSERT INTO produto(produtocnpj_estab,produtocod_produto,produtodescricao,produtocod_unid_med,produtoind_ativo,produtoind_origem,produtodt_criacao,produtohr_criacao,produtouser_criacao,produtodt_altera,produtohr_altera,produtouser_altera)VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $this->produtocnpj_estab);
        $stmt->bindValue(2, $this->produtocod_produto);
        $stmt->bindValue(3, $this->produtodescricao);
        $stmt->bindValue(4, $this->produtocod_unid_med);
        $stmt->bindValue(5, $this->produtoind_ativo);
        $stmt->bindValue(6, $this->produtoind_origem);
        $stmt->bindValue(7, $this->produtodt_criacao);
        $stmt->bindValue(8, $this->produtohr_criacao);
        $stmt->bindValue(9, $this->produtouser_criacao);
        $stmt->bindValue(10, $this->produtodt_altera);
        $stmt->bindValue(11, $this->produtohr_altera);
        $stmt->bindValue(12, $this->produtouser_altera);

        if ($stmt->execute()) :
            return "Cadastrado com sucesso";
        else :
            return "Erro ao cadastrar";
        endif;
    }

    public function update($id)
    {
        $sql = "UPDATE produto SET produtocnpj_estab = ?,produtocod_produto = ?,produtodescricao = ?,produtocod_unid_med = ?,produtoind_ativo = ?,produtoind_origem = ?,produtodt_altera = ?,produtohr_altera = ?,produtouser_altera = ? WHERE produtoid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $this->produtocnpj_estab);
        $stmt->bindValue(2, $this->produtocod_produto);
        $stmt->bindValue(3, $this->produtodescricao);
        $stmt->bindValue(4, $this->produtocod_unid_med);
        $stmt->bindValue(5, $this->produtoind_ativo);
        $stmt->bindValue(6, $this->produtoind_origem);
        $stmt->bindValue(7, $this->produtodt_altera);
        $stmt->bindValue(8, $this->produtohr_altera);
        $stmt->bindValue(9, $this->produtouser_altera);
        $stmt->bindValue(10, $id);

        if ($stmt->execute()) :
            return "Atualizado com sucesso";
        else :
            return "Erro ao atualizar";
        endif;
    }

    public function excluir($id)
    {
        $sql = "UPDATE produto_usuario SET status_prod = ? WHERE cod_uni_prod = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, '6');
        $stmt->bindValue(2, $id);

        if ($stmt->execute()) :
            return "5 - Produto excluido com sucesso!";
        else :
            return "4 - Erro ao excluir o Produto!";
        endif;
    }

    public function ocultar($id)
    {
        $sql = "UPDATE produto_usuario SET status_prod = ? WHERE cod_uni_prod = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, '2');
        $stmt->bindValue(2, $id);

        if ($stmt->execute()) :
            return "1 - Produto ocultado com sucesso!";
        else :
            return "2 - Erro ao ocultar o Produto!";
        endif;
    }

    public function desocultar($id)
    {
        $sql = "UPDATE produto_usuario SET status_prod = ? WHERE cod_uni_prod = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, '1');
        $stmt->bindValue(2, $id);

        if ($stmt->execute()) :
            return "1 - Produto desocultado com sucesso!";
        else :
            return "2 - Erro ao desocultar o Produto!";
        endif;
    }

    public function getPorMatriz($matriz)
    {
        $sql = "SELECT * FROM produto WHERE produtocnpj_estab = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $matriz);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        } else {
            return [];
        }
    }

    public function getAll()
    {
        $sql = "SELECT * FROM produto a LEFT JOIN unid_med b on b.unid_medid = a.produtocod_unid_med";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        } else {
            return [];
        }
    }

    public function getPorEstabelecimento($estab)
    {

        $sql = "SELECT * FROM produto a";
        $sql = $sql . " LEFT JOIN unid_med b on b.unid_medid = a.produtocod_unid_med";
        $sql = $sql . " WHERE produtocnpj_estab = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $estab);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        } else {
            return [];
        }
    }

    public function findId($cod)
    {
        $sql = "SELECT * FROM produto_usuario WHERE cod_uni_prod = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $cod);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
            return $resultado;
        } else {
            return [];
        }
    }


    public function ultimo($estab, $usuario)
    {
        $sql = "SELECT * FROM produto WHERE produtocnpj_estab = ? AND 	produtouser_criacao = ? ORDER BY produtoid DESC";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $estab);
        $stmt->bindValue(2, $usuario);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
            return $resultado;
        } else {
            return [];
        }
    }

    public function GetAllProdsIndex($cod_user)
    {
        $sql = "SELECT
        (qtd_cor_azul + qtd_cor_verde + qtd_cor_vermelho + qtd_cor_preto + qtd_cor_branco + qtd_cor_marrom + 
        qtd_cor_cinza + qtd_cor_rosa + qtd_cor_amarelo) as unidades,
        cod_loja,
        cod_uni_prod,
        nome_prod_v,
        val_prod,
        status_prod,
        cod_foto_1
        FROM produto_usuario 
        WHERE cod_loja = ? AND status_prod !=? AND status_prod != ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $cod_user);
        $stmt->bindValue(2, '6');
        $stmt->bindValue(3, '7');
        $stmt->execute();
        $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $resultado;
    }
}
