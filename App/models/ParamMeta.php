<?php

use App\Core\Model;

class ParamMeta extends Model
{

    public $param_metadt_criacao;
    public $param_metahr_criacao;
    public $param_metauser_criacao;
    public $param_metadt_altera;
    public $param_metahr_altera;
    public $param_metauser_altera;

    public function save()
    {

        $sql = "INSERT INTO param_meta(param_metadt_criacao,param_metahr_criacao,param_metauser_criacao,param_metadt_altera,param_metahr_altera,param_metauser_altera)VALUES(?,?,?,?,?,?)";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$this->param_metadt_criacao);
        $stmt->bindValue(2,$this->param_metahr_criacao);
        $stmt->bindValue(3,$this->param_metauser_criacao);
        $stmt->bindValue(4,$this->param_metadt_altera);
        $stmt->bindValue(5,$this->param_metahr_altera);
        $stmt->bindValue(6,$this->param_metauser_altera);

        if($stmt->execute()):
            return "Cadastrado com sucesso";
        else:
            return "Erro ao cadastrar";
        endif;

    }

    public function update($id)
    {
        $sql = "UPDATE param_meta SET param_metadt_altera = ?,param_metahr_altera = ?,param_metauser_altera = ? WHERE param_metaid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$this->param_metadt_altera);
        $stmt->bindValue(2,$this->param_metahr_altera);
        $stmt->bindValue(3,$this->param_metauser_altera);
        $stmt->bindValue(4,$id);

        if($stmt->execute()):
            return "Atualizado com sucesso";
        else:
            return "Erro ao atualizar";
        endif;
    }

    public function delete($id)
    {
        $sql = "DELETE FROM param_meta WHERE param_metaid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$id);

        if($stmt->execute()):
            return "Excluido com sucesso";
        else:
            return "Erro ao excluir";
        endif;
    }

    public function getAll()
    {
        $sql = "SELECT * FROM param_meta LEFT JOIN user_perfil a ON a.user_perfilid = param_metauser_criacao";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

    public function findId($id)
    {
        $sql = "SELECT * FROM param_meta WHERE param_metaid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$id);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }
}


