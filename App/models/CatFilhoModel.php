<?php

use App\Core\Model;

class CatFilhoModel extends Model
{

    public $nome_cat_f;
    public $ind_ativo;
    public $user_criacao;
    public $dt_criacao;
    public $hr_criacao;

    public function save()
    {
        $sql = "INSERT INTO prod_cat_f(nome_cat_f,ind_ativo,user_criacao,dt_criacao,hr_criacao)VALUES(?,?,?,?,?)";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $this->nome_cat_f);
        $stmt->bindValue(2, $this->ind_ativo);
        $stmt->bindValue(3, $this->user_criacao);
        $stmt->bindValue(4, $this->dt_criacao);
        $stmt->bindValue(5, $this->hr_criacao);

        if ($stmt->execute()) :
            return "1 - Categoria cadastrada com sucesso!";
        else :
            return "3 - Erro ao processar cadastro!";
        endif;
    }

    public function update($id)
    {
        $sql = "UPDATE prod_cat_f SET nome_cat_f = ?,ind_ativo = ? WHERE cod_cat_f = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $this->nome_cat_f);
        $stmt->bindValue(2, $this->ind_ativo);
        $stmt->bindValue(3, $id);

        if ($stmt->execute()) :
            return "1 - Categoria atualizada com sucesso";
        else :
            return "3 - Erro ao atualizar";
        endif;
    }

    public function delete($id)
    {
        $sql = "DELETE FROM prod_cat_f WHERE cod_cat_f = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $id);

        if ($stmt->execute()) :
            return "5 - Categoria Filho excluida com sucesso!";
        else :
            return "4 - Erro ao excluir categoria Filho";
        endif;
    }

    //busca ultimo para adc +
    public function GetUltimoCod()
    {
        $sql = "SELECT MAX(cod_cat_f) as last_cod FROM prod_cat_f";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->execute();
        $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $resultado;
    }

    //busca all para lista
    public function getAll()
    {
        $sql = "SELECT * FROM prod_cat_f";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        } else {
            return [];
        }
    }

    //busca cat por id
    public function getId($id)
    {
        $sql = "SELECT * FROM prod_cat_f WHERE cod_cat_f = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        } else {
            return [];
        }
    }

    public function getnomeId($id)
    {
        $sql = "SELECT nome_cat_f FROM prod_cat_f WHERE cod_cat_f = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        } else {
            return [];
        }
    }

    public function VerificaExcluir($id)
    {
        $sql = "SELECT cod_prod FROM produtos_tabela WHERE cod_cat_f = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();

        if ($stmt->rowCount() > 0) {
            return false;
        } else {
            return true;
        }
    }

}
