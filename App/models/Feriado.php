<?php

use App\Core\Model;

class Feriado extends Model
{

    public $feriadocnpj_estab;
    public $feriadodt_feriado;
    public $feriadodescricao;
    public $feriadoind_ativo;
    public $feriadoind_origem;
    public $feriadodt_criacao;
    public $feriadohr_criacao;
    public $feriadouser_criacao;
    public $feriadodt_altera;
    public $feriadohr_altera;
    public $feriadouser_altera;


    public function save()
    {

        $sql = "INSERT INTO feriado(feriadocnpj_estab,feriadodt_feriado,feriadodescricao,feriadoind_ativo,feriadoind_origem,feriadodt_criacao,feriadohr_criacao,feriadouser_criacao,feriadodt_altera,feriadohr_altera,feriadouser_altera)VALUES(?,?,?,?,?,?,?,?,?,?,?)";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$this->feriadocnpj_estab);
        $stmt->bindValue(2,$this->feriadodt_feriado);
        $stmt->bindValue(3,$this->feriadodescricao);
        $stmt->bindValue(4,$this->feriadoind_ativo);
        $stmt->bindValue(5,$this->feriadoind_origem);
        $stmt->bindValue(6,$this->feriadodt_criacao);
        $stmt->bindValue(7,$this->feriadohr_criacao);
        $stmt->bindValue(8,$this->feriadouser_criacao);
        $stmt->bindValue(9,$this->feriadodt_altera);
        $stmt->bindValue(10,$this->feriadohr_altera);
        $stmt->bindValue(11,$this->feriadouser_altera);

        if($stmt->execute()):
            return "Cadastrado com sucesso";
        else:
            return "Erro ao cadastrar";
        endif;

    }

    public function update($id)
    {
        $sql = "UPDATE feriado SET feriadocnpj_estab=?,feriadodt_feriado=?,feriadodescricao=?,feriadoind_ativo=?,feriadoind_origem=?,feriadodt_altera=?,feriadohr_altera=?,feriadouser_altera=? WHERE feriadoid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$this->feriadocnpj_estab);
        $stmt->bindValue(2,$this->feriadodt_feriado);
        $stmt->bindValue(3,$this->feriadodescricao);
        $stmt->bindValue(4,$this->feriadoind_ativo);
        $stmt->bindValue(5,$this->feriadoind_origem);
        $stmt->bindValue(6,$this->feriadodt_altera);
        $stmt->bindValue(7,$this->feriadohr_altera);
        $stmt->bindValue(8,$this->feriadouser_altera);
        $stmt->bindValue(9,$id);

        if($stmt->execute()):
            return "Atualizado com sucesso";
        else:
            return "Erro ao atualizar";
        endif;
    }

    public function delete($id)
    {
        $sql = "DELETE FROM feriado WHERE feriadoid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$id);

        if($stmt->execute()):
            return "Excluido com sucesso";
        else:
            return "Erro ao excluir";
        endif;
    }

    public function getAll()
    {
        $sql = "SELECT * FROM feriado";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

    public function findId($id)
    {
        $sql = "SELECT * FROM feriado WHERE feriadoid = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$id);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }
    }

    public function ultimoCadastro($usuario,$estab){
        $sql = "SELECT * FROM feriado WHERE feriadouser_criacao = ? AND feriadocnpj_estab = ?";
        $stmt = Model::getConn()->prepare($sql);
        $stmt->bindValue(1,$usuario);
        $stmt->bindValue(2,$estab);
        $stmt->execute();

        if($stmt->rowCount()>0)
        {
            $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $resultado;
        }else{
            return [];
        }

    }
}

