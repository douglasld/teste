<section>
    <div class="x_panel">
    <div class="x_title">
        <h2>Usuários [Logs]</h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <?php
        if (!empty($data['mensagem'])):

            foreach ($data['mensagem'] as $m):
                echo $m . "<br>";
            endforeach;

        endif;
        ?>

        <div class="table-responsive">
            <table id="datatable" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>cnpj_matriz</th>
                    <th>cod_user</th>
                    <th>Usuário</th>
                    <th>dt_emis</th>
                    <th>hr_emis</th>
                    <th>user_emis</th>
                    <th>titulo</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($data['registros'] as $user_perfil_log): ?>
                    <tr>
                        <td><?php echo $user_perfil_log['user_perfil_logcnpj_matriz']; ?> </td>
                        <td><?php echo $user_perfil_log['user_perfilcod_user']; ?></td>
                        <td><?php echo $user_perfil_log['user_perfilnome_user']; ?></td>
                        <td><?php echo implode("/", array_reverse(explode("-",$user_perfil_log['user_perfil_logdt_emis']))); ?></td>
                        <td><?php echo $user_perfil_log['user_perfil_loghr_emis']; ?></td>
                        <td><?php echo $user_perfil_log['user_perfil_loguser_emis']; ?></td>
                        <td><?php echo $user_perfil_log['user_perfil_logtitulo']; ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
    </section>