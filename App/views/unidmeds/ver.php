<div class="x_panel">
    <div class="x_title">
        <h2>Produto</h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <div class="">
            <ul class="to_do">
                <li><p><b>Estabelecimento: </b><?php echo $data['unid_medcnpj_estab']; ?></p></li>
                <li><p><b>Unidade Med.: </b><?php echo $data['unid_medcod_unid_med']; ?></p></li>
                <li><p><b>Descrição: </b><?php echo $data['unid_meddescricao']; ?></p></li>
                <li><p><b>Ativo: </b><?php echo $data['unid_medind_ativo'] == 0 ? "Sim" : "Não"; ?></p></li>
                <li><p><b>Origem: </b><?php echo $data['unid_medind_origem'] == 0 ? "Portal" : "Integrador"; ?></p></li>
                <li><p><b>Data de cadastro: </b><?php echo implode("/", array_reverse(explode("-",$data['unid_meddt_criacao']))); ?></p></li>
                <li><p><b>Hora de cadastro: </b><?php echo $data['unid_medhr_criacao']; ?></p></li>
                <li><p><b>Usuário de cadastro: </b><?php echo $data['unid_meduser_criacao']; ?></p></li>
                <li><p><b>Data de alteração: </b><?php echo implode("/", array_reverse(explode("-",$data['unid_meddt_altera']))); ?></p></li>
                <li><p><b>Hora de alteração: </b><?php echo $data['unid_medhr_altera']; ?></p></li>
                <li><p><b>Usuário de alteração: </b><?php echo $data['unid_meduser_altera']; ?></p></li>
            </ul>
        </div>
    </div>
</div>

