<div class="x_panel">
    <div class="x_title">
    <br>
        <h3 style="margin-left:10px;">Cadastro Unidade Medida</h3>
        <ul class="nav navbar-right panel_toolbox">
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <?php
        if (!empty($data['mensagem'])):
            foreach ($data['mensagem'] as $m):
                echo $m . "<br>";
            endforeach;
        endif;
        ?>
        <div class="container-fluid">
        <form action="/unidmeds/cadastrar" method="post" class="form-horizontal form-label-left">
        <div class="col-md-6">
                <div class="card card-default">
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>CNPJ Estabelecimento</label>
                                    <input name="cnpj_estab" id="cnpj_estab" onfocus="javascript: retirarFormatacao(this);" onblur="javascript: formatarCampo(this);" maxlength="14" type="text" class="form-control" placeholder="00.000.000/000-00" required="required">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label> Código Unidade Medida</label>
                                    <input name="cod_unid_med" type="number" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label> Descrição</label>
                                    <input name="descricao" type="text" class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" id="ind_ativo">
                                    <label class="custom-control-label" for="ind_ativo" style="margin-top:25%;">Unidade Ativa</label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" id="ind_origem">
                                    <label class="custom-control-label" for="ind_origem" style="margin-top:25%;">Origem Portal</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        </br>
                        <input type="submit" name="Cadastar" class="btn btn-primary float-right" value="Cadastrar" style="background-color: indigo; border-color: indigo; margin:6px;">
                        <a href="/unidmeds/index" class="btn btn-default float-right" style="margin:6px;">Cancelar</a>
                    </div>
        </form>
        </div>
        <script>
            function formatarCampo(campoTexto) {
                campoTexto.value = mascaraCnpj(campoTexto.value);
            }

            function retirarFormatacao(campoTexto) {
                campoTexto.value = campoTexto.value.replace(/(\.|\/|\-)/g, "");
            }

            function mascaraCnpj(valor) {
                return valor.replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/g, "\$1.\$2.\$3\/\$4\-\$5");
            }
        </script>
    </div>
</div>




