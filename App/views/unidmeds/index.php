<!-- Content Header (Page header)  -->
<section class="content-header">

    <div class="container" style="background: pink;">
        <?php
        if (!empty($data['mensagem'])) :

            foreach ($data['mensagem'] as $m) :
                echo $m . "<br>";
            endforeach;
        endif;
        // var_dump ($_SESSION);
        ?>
    </div>

    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Resumo Unid. Medida</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a style="color: indigo" href="#">Home</a></li>
                    <li class="breadcrumb-item active">Cadastro</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<!-- Main Content -->
<section class="content">
    <div class="row">
        <div class="col-12">

            <div class="card">
                <div class="card-header">
                    <div class="row form-inline">

                        <div style="margin-left: 10px"> <a href="/unidmeds/cadastrar" type="button" class="btn btn-block btn-default btn-sm">Adicionar Uni. Med.</a> </div>
                        <div class="col-md-10 float-right">
                            <div style="float:right; padding-left: 6px;" id="example1_filter" class="dataTables_filter"><label><input type="search" class="form-control form-control-sm" placeholder="Pesquisar" aria-controls="example1"></label></div>
                            <div style="float:right;" class="dataTables_length" id="example1_length"><label> Mostrar <select name="example1_length" aria-controls="example1" class="custom-select custom-select-sm form-control form-control-sm">
                                        <option value="10">10</option>
                                        <option value="25">25</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select> </label></div>
                        </div>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                        <div class="row">
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="example1" class="table table-bordered table-striped dataTable dtr-inline" role="grid" aria-describedby="example1_info">
                                    <thead>
                                        <tr role="row">
                                            <th style="width: 11%" class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">Atalhos</th> <!-- botões -->
                                            <th style="width: 10%" class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Estabelecimento.</th>
                                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Unid. MEd.</th>
                                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Descrição</th>
                                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Ativo.</th>
                                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Origem</th>
                                        </tr>
                                    </thead>
                                    <!-- tbody -->
                                    <tbody>
                                        <?php $unid_medEmpresa = $this->model('UnidMed'); ?>
                                        <?php foreach ($data['registros'] as $empresas) : ?>
                                            <?php foreach ($unid_medEmpresa->getAllEstabelecimento($empresas['empresacnpj_estab']) as $unid_med) : ?>
                                                <tr>
                                                    <td>
                                                        <a title="visualizar" href="/unidmeds/ver/<?php echo $unid_med['unid_medid']; ?>" type="button" class="btn btn-default btn-sm"><i class="fas fa-eye"></i></a> <!-- visualizar -->
                                                        <a title="editar" href="/unidmeds/editar/<?php echo $unid_med['unid_medid']; ?>" type="button" class="btn btn-default btn-sm"><i class="fas fa-edit"></i></a> <!-- editar -->
                                                        <a title="excluir" href="/unidmeds/excluir/<?php echo $unid_med['unid_medid']; ?>" type="button" class="btn btn-default btn-sm"><i class="fas fa-trash"></i></a> <!-- excluir -->
                                                        <a title="log" href="/unidmedlogs/index/<?php echo $unid_med['unid_medid']; ?>" type="button" class="btn btn-default btn-sm"><i class="fas fa-comment"></i></a> <!-- log -->
                                                    </td>

                                                    <td><?php echo $unid_med['unid_medcnpj_estab']; ?></td>
                                                    <td><?php echo $unid_med['unid_medcod_unid_med']; ?></td>
                                                    <td><?php echo $unid_med['unid_meddescricao']; ?></td>
                                                    <td><?php echo $unid_med['unid_medind_ativo'] == 0 ? "Sim" : "Não"; ?></td>
                                                    <td><?php echo $unid_med['unid_medind_origem'] == 0 ? "Portal" : "Integrador"; ?></td>
                                                </tr>
                                            <?php endforeach; ?>
                                        <?php endforeach; ?>
                                    </tbody>
                                    <!-- tbody -->
                                    <!-- <tfoot>
                                        nada por enquanto
                                    </tfoot> -->
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-5">
                                <div class="dataTables_info" id="example1_info" role="status" aria-live="polite">Mostrando 1 to 10 of 57 registros</div>
                            </div>
                            <div class="col-sm-12 col-md-7">
                                <div class="dataTables_paginate paging_simple_numbers float-right" id="example1_paginate">
                                    <ul class="pagination">
                                        <li class="paginate_button page-item previous disabled" id="example1_previous"><a style="color: indigo; background-color: white;" href="#" aria-controls="example1" data-dt-idx="0" tabindex="0" class="page-link">Anterior</a></li>
                                        <li class="paginate_button page-item active"><a style="color: white; background-color: indigo; border-color:indigo" href="#" aria-controls="example1" data-dt-idx="1" tabindex="0" class="page-link">1</a></li>
                                        <li class="paginate_button page-item "><a style="color: indigo; background-color: white;" href="#" aria-controls="example1" data-dt-idx="2" tabindex="0" class="page-link">2</a></li>
                                        <li class="paginate_button page-item "><a style="color: indigo; background-color: white;" href="#" aria-controls="example1" data-dt-idx="3" tabindex="0" class="page-link">3</a></li>
                                        <li class="paginate_button page-item "><a style="color: indigo; background-color: white;" href="#" aria-controls="example1" data-dt-idx="4" tabindex="0" class="page-link">4</a></li>
                                        <li class="paginate_button page-item "><a style="color: indigo; background-color: white;" href="#" aria-controls="example1" data-dt-idx="5" tabindex="0" class="page-link">5</a></li>
                                        <li class="paginate_button page-item "><a style="color: indigo; background-color: white;" href="#" aria-controls="example1" data-dt-idx="6" tabindex="0" class="page-link">6</a></li>
                                        <li class="paginate_button page-item next" id="example1_next"><a style="color: indigo; background-color: white;" href="#" aria-controls="example1" data-dt-idx="7" tabindex="0" class="page-link">Próximo</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col 12 -->
    </div>
    <!-- /.row -->
</section>