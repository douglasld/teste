<section class="content-header">

    <div class="container" style="background: lavender;">
        <?php
        if (!empty($data['mensagem'])) :
            foreach ($data['mensagem'] as $m) :
                echo $m . "<br>";
            endforeach;
        endif;
        $estabelecimentos = $this->model('Empresa');
        $unid_neg = $this->model('UnidNeg');
        ?>
    </div>
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h4>Cadastro de Vendedores</h4>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a style="color: indigo" href="#">Index</a></li>
                    <li class="breadcrumb-item active">Cadastro</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<div class="container-fluid">
    <form action="/resvendedors/cadastrar" method="post">
        <div class="col-md-9">
            <div class="card card-default">
            <div class="card-header">
                    <a href="/resvendedors/index" class="btn btn-default btn-md float-left" style="margin-left:5px;">Listar</a>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>CNPJ Estabelecimento</label>
                                <input class="form-control col-md-12" name="cnpj_estab" id="cnpj_estab" list="list" autocomplete="off" required="">
                                <datalist id="list">
                                    <?php foreach ($estabelecimentos->getall($_SESSION['matriz']) as $value) : ?>
                                        <?php
                                        $cnpj_cpf = $value['empresacnpj_estab'];
                                        $cnpj = preg_replace("/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/", "\$1.\$2.\$3/\$4-\$5", $cnpj_cpf);
                                        ?>
                                        <option value="<?php echo $cnpj . ' - ' . $value['empresanome_abrev']; ?>"></option>
                                    <?php endforeach; ?>
                                </datalist>
                                </input>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Unidade de Negócio</label>
                                <input class="form-control col-md-12" name="cod_unid_neg" id="cod_unid_neg" list="lista" autocomplete="off">
                                <datalist id="lista">
                                </datalist>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Gerente</label>
                                <input name="cod_gerente" id="cod_gerente" type="number" class="form-control" required="required">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label> Vendedor</label>
                                <input name="cod_vendedor" id="cod_vendedor" type="number" class="form-control" required="required">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label> Data Movimento</label>
                                <input name="dt_movto" type="date" class="form-control" required="">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label> Valor Real</label>
                                <input name="vl_real" id="vl_real" type="text" class="form-control" required="">
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <input type="submit" name="Cadastrar" class="btn btn-primary float-right" value="Salvar" style="background-color: indigo; border-color: indigo; margin:6px;">
                    <a href="/resvendedors/index" class="btn btn-orange btn-md float-right" style="margin:6px;">Cancelar</a>
                </div>
    </form>
</div>
<script>
    function formatarCampo(campoTexto) {
        campoTexto.value = mascaraCnpj(campoTexto.value);
    }

    function retirarFormatacao(campoTexto) {
        campoTexto.value = campoTexto.value.replace(/(\.|\/|\-)/g, "");
    }

    function mascaraCnpj(valor) {
        return valor.replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/g, "\$1.\$2.\$3\/\$4\-\$5");
    }
</script>
</div>
</div>

<!-- MÁSCARAS DOS INPUTS -->
<script type="text/javascript">
    //VALOR REAL
    $(document).ready(function() {
        $('#vl_real').mask('000.000.000.000.000,00', {
            reverse: true
        });
    });
    $("#vl_real").focusout(function() {
        if ($(this).val().length <= 2) {
            if ($(this).val().length != '') {
                temp = $(this).val()
                var newNum = temp + ",00"
                $(this).val(newNum)
            }
        }
    });
</script>


<script type="text/javascript">
    $(document).ready(function() {
        $('#cnpj_estab').change(function(e) {
            //limpa o campo
            $("#cod_unid_neg").val("");
            var id = $(this).val();
            //formata o cnpj
            var resultado = id.replace(/[^a-z0-9]/gi, '');
            cnpj_estab_formatado = resultado.slice(0, 14);
            $.post('/ajax/call_unidneg.php', {
                cnpj_estab: cnpj_estab_formatado
            }, function(data) {
                var cmb = '<option value=""></option>';
                $.each(data, function(index, value) {                
                    cmb = cmb + '<option value="' + value.unid_negid + ' - ' + value.unid_negdescricao + '"></option>';
                });
                $('#lista').html(cmb);
            }, 'json');
        });
    });
</script>