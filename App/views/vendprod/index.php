<body >
    <div id="all">
        <div id="content">
            <div class="container">
                <section class="bar">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="heading">
                                <br>
                                <br>
                                <center>
                                    <h2>Escreva e selecione o produto que irá vender.</h2>
                                </center>
                            </div>
                            <center>
                                <p class="lead">Escreva o nome do produto que irá vender e após, selecione o mesmo na lista exibida.</p>
                            </center>
                            <div>
                                <!-- Seleciona o produto-->
                                <center>
                                    <br>
                                    <br>
                                    <form method="POST" action="/vendprod/direciona">
                                        <center>
                                            <div class="form-group">
                                                <select class="form-control select2bs4" style="width: 100%;" name="nome_prod" required="">
                                                    <?php foreach ($data['registros'] as $value)
                                                        echo '<option value="' . $value['cod_prod'] . '">' . $value['nome_prod'] . '</option>';
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <br>
                                                <input type="submit" class="btn btn-green" value="Próxima Etapa" class="btn btn-outline-primary">
                                            </div>
                                    </form>
                                </center>
                                <br>
                                <br>
                                <br>
                                <section class="bar">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div id="accordion" role="tablist" class="mb-5">
                                                    <div class="card">
                                                        <div id="headingTwo" role="tab" class="card-header">
                                                            <center>
                                                                <h5 class="mb-0"><a data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo" class="">Não encontrei o produto que desejo vender.</a></h5>
                                                            </center>
                                                        </div>
                                                        <div id="collapseTwo" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion" class="collapse">
                                                            <div class="card-body">
                                                                <div class="row">
                                                                    <div class="col-md-5">
                                                                        <form method="POST" action="">
                                                                            <br>
                                                                            <h5>Preencha o seguinte campo:</h5>
                                                                            <input type="text" placeholder="Digite o nome do seu produto" class="form-control" required="" name="prod_gene"><br>
                                                                            <input type="submit" value="Próxima Etapa" name="submit2" class="btn btn-green">
                                                                            <form>
                                                                    </div>
                                                                    <div class="col-md-7">
                                                                        <br>
                                                                        <h5>Caso não possua o produto que deseja vender na lista apresentada, adicione um produto genérico, e então o mesmo passará por uma validação da equipe para que possamos adicionar opções para seu produto.</h5>
                                                                        <h5>**O produto será publicado pela equipe de suporte em até 24h.**</h5>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                </section>
            </div>
        </div>
    </div>
</body>

</html>