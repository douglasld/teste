<?php
echo mensagens($data['mensagem']);
?>

<body>
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="<?=URL_BASE?>vendor/css/custom.css">
    <div id="">
        <div class="heading">
            <br>
            <center>
                <h3>Escolha ao menos 3 fotos de apresentação</h3>
            </center>
        </div>
        <div id="content">
            <form method="POST" id="prod_form" action="/vendprod_artesanato/cadartesanato" enctype="multipart/form-data" onsubmit="javascript:document.getElementById('blanket').style.display = 'block';document.getElementById('aguarde').style.display = 'block';">
                <div class="container">
                    <section class="content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card card-outline card-info">
                                    <div class="card-header" data-card-widget="collapse">
                                        <h3 class="card-title">
                                            Fotos do Produto
                                            <small data-card-widget="collapse">clique para abrir</small>
                                        </h3>
                                        <!-- tools box -->
                                        <div class="card-tools">
                                            <button type="button" class="btn btn-tool btn-sm" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                                                <i class="fas fa-minus"></i></button>
                                        </div>
                                    </div>
                                    <div class="card-body pad">
                                        <div class="col-md-12">
                                            <div id="blanket"></div>
                                            <div id="aguarde">Aguarde...</div>
                                            <input id="cod_loja" type="text" style="display: none;" name="cod_loja" value="<?=$_SESSION['userId']?>">
                                            <input id="cod_prod" type="text" style="display: none;" name="cod_prod" value="<?=$data['registros']['cod_prod']?>">
                                            <div class="row col-md-12 tc">
                                                <div class="col-md-2 ft">
                                                    <img id="blah1" class="ftc" src="<?=URL_BASE?>/fotos_produtos/default_produto.svg" />
                                                    <label class="lb_ft1 ft" for="ft1"><i class="fa fa-image" aria-hidden="true"></i> Foto 1</label>
                                                </div>
                                                <div class="col-md-2 ft">
                                                    <img id="blah2" class="ftc" src="<?=URL_BASE?>/fotos_produtos/default_produto.svg" />
                                                    <label class="lb_ft2 ft" for="ft2"><i class="fa fa-image" aria-hidden="true"></i> Foto 2</label>
                                                </div>
                                                <div class="col-md-2 ft">
                                                    <img id="blah3" class="ftc" src="<?=URL_BASE?>/fotos_produtos/default_produto.svg" />
                                                    <label class="lb_ft3 ft" for="ft3"><i class="fa fa-image" aria-hidden="true"></i> Foto 3</label>
                                                </div>
                                                <div class="col-md-2 ft">
                                                    <img id="blah4" class="ftc" src="<?=URL_BASE?>/fotos_produtos/default_produto.svg" />
                                                    <label class="lb_ft4 ft" for="ft4"><i class="fa fa-image" aria-hidden="true"></i> Foto 4</label>
                                                </div>
                                                <div class="col-md-2 ft">
                                                    <img id="blah5" class="outline-button ftc" src="<?=URL_BASE?>/fotos_produtos/default_produto.svg" />
                                                    <label class="lb_ft5 ft" for="ft5"><i class="fa fa-image" aria-hidden="true"></i> Foto 5</label>
                                                </div>
                                            </div>
                                            <input style="display: none;" type='file' onchange="readURL(this);" id="ft1" name="ft1" required="" />
                                            <input style="display: none;" type='file' onchange="readURLL(this);" id="ft2" name="ft2" required="" />
                                            <input style="display: none;" type='file' onchange="readURLLL(this);" id="ft3" name="ft3" required="" />
                                            <input style="display: none;" type='file' onchange="readURLLLL(this);" id="ft4" name="ft4" />
                                            <input style="display: none;" type='file' onchange="readURLLLLL(this);" id="ft5" name="ft5" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.col-->
                        </div>
                        <!-- ./row -->
                    </section>
                    <section>
                        <center>
                            <h2 class="text-uppercase">Boas descrições geram boas vendas!</h2>
                            <h5 class="text-uppercase">Campos com * são de preenchimento obrigatório</h5>
                        </center>
                    </section>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="box" style="margin-top:2%;">
                                <h3>Informações do produto</h3>
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label for="nome_produto">Nome do produto *</label>
                                        <small hidden id="msgprod" style="color:red"></small>
                                        <input type="text" id="nome_produto_vendedor" name="nome_produto_vendedor" maxlength="50" placeholder="Colar de Quartzo" class="form-control" required="">
                                    </div>
                                    <div class="col-md-12">
                                        <section class="content">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="card card-outline card-info">
                                                        <div class="card-header">
                                                            <h3 class="card-title">
                                                                Descrição do produto
                                                                <small>Simple and fast</small>
                                                            </h3>
                                                            <!-- tools box -->
                                                            <div class="card-tools">
                                                                <button type="button" class="btn btn-tool btn-sm" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                                                                    <i class="fas fa-minus"></i></button>
                                                            </div>
                                                        </div>
                                                        <div class="card-body pad">
                                                            <div class="mb-3">
                                                                <textarea class="textarea" name="desc_produto" placeholder="" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
                                                            <p><span style="font-size: 14.4px;">Insira uma <u>descrição</u>&nbsp;do seu produto apontando as principais dúvidas que poderão ser levantadas sobre as características do mesmo.</span></p><p><br></p><ul><li><span style="font-size: 14.4px;"><b>Não</b> expor quaisquer <b>formas de <font color="#ff0000">pagamen<span style="background-color: rgb(255, 255, 255);">to</span></font></b><font color="#ff0000" styl ""><b style="background-color: rgb(255, 255, 255);">!</b></font></span></li><li><span style="font-size: 14.4px;"><b>Não</b> expor quaisquer <b>formas de <font color="#ff0000" style="background-color: rgb(255, 255, 255);">contato externo</font></b><font color="#ff0000" style="background-color: rgb(255, 255, 255);">!</font></span></li><li>Venda produtos <b>íntegros</b> e que estejam em <b>perfeitas condições de uso!</b></li><li>O <b><font color="#ff0000" style="background-color: rgb(255, 255, 255);">descumprimento das regras</font></b> poderá gerar uma <b><font color="#ff0000" style="background-color: rgb(255, 255, 255);">punição</font></b>!</li></ul><p><b>&nbsp; (não necessário apresentar dimensões ou peso do&nbsp; nesta área)</b></p>
                                                            </textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- /.col-->
                                            </div>
                                            <!-- ./row -->
                                        </section>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-5">
                                        <center><label for="valor_produto">Valor a receber *</label></center>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">R$</div>
                                            </div>
                                            <input type="text" class="form-control" id="valor_produto" name="valor_produto" onblur="calcular()" required="">
                                        </div>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <center>
                                            <label for="valor_servicos">Serviços</label>
                                            <input type="text" class="form-control" id="valor_servicos" name="valor_servicos" value="30%" readonly="">
                                        </center>
                                    </div>
                                    <div class="form-group col-md-5">
                                        <div class="col-auto">
                                            <center> <label for="valor_final">Valor Final</label></center>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text">R$</div>
                                                </div>
                                                <input type="text" class="form-control" id="valor_final" name="valor_final" readonly="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--   <div class="row">
                                    <div class="form-group col-md-12">
                                        <label for="material">Materias separados por vírgula *</label>
                                        <input type="text" name="material" class="form-control" class="btn btn-outline-primary" required="" placeholder="Fio, Quartzo, Biscuit">
                                    </div>
                                </div> -->
                                <label for="cores" style="color: #4F4F4F;font-size:14px;">Cores *</label>
                                <div class='row' id='prod_formm'>
                                    <div class='form-group col-md-4'>
                                        <select class='form-control  select2bs4' id='cores' name='cores[1]' required="">
                                            <option disabled selected value=''>Selecione</option>
                                            <option value='1'>Azul</option>
                                            <option value='2'>Verde</option>
                                            <option value='3'>Vermelho</option>
                                            <option value='4'>Preto</option>
                                            <option value='5' style='color: black;'>Branco</option>
                                            <option value='6'>Marrom</option>
                                            <option value='7'>Cinza</option>
                                            <option value='8'>Rosa</option>
                                            <option value='9' style='color: black;'>Amarelo</option>
                                        </select>
                                    </div>
                                    <div class='form-group col-md-6'>
                                        <input type='number' name='qtd_prod[1]' id='qtd_prod[1]' min=1 class='form-control' class='btn btn-outline-primary' required='' min="1" max="100" placeholder='Quantidade disponível *'>
                                    </div>
                                    <div class='form-group col-md-1'>
                                        <button type='button' id='add_color' class='btn btn-green'>+</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="box" style="margin-top:2%;">
                                <div class="row">
                                    <h3>Informações para Envio</h3>
                                    <div class="form-group col-md-6">
                                        <center><label for="peso">Peso do objeto *</label></center>
                                        <input id="peso" type="text" class="form-control" required="" name="peso" min=1 oninput="validity.valid||(value='');">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <center><label for="unidade_peso">Unidade *</label></center>
                                        <select class="form-control" id="unidade_peso" name="unidade_peso">
                                            <option selected="selected" value="1">Grama</option>
                                            <option value="2">Kilograma</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <center> <label for="altura">Altura *</label></center>
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="altura" name="altura" onblur="calcularcm3()" required="" min=1 oninput="validity.valid||(value='');">
                                            <div class="input-group-text">CM</div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <center> <label for="largura">Largura *</label></center>
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="largura" name="largura" onblur="calcularcm3()" required="" min=1 oninput="validity.valid||(value='');">
                                            <div class="input-group-text">CM</div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <center> <label for="comprimento">Comprimento *</label></center>
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="comprimento" name="comprimento" onblur="calcularcm3()" required="" min=1 oninput="validity.valid||(value='');">
                                            <div class="input-group-text">CM</div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-0">
                                        <!-- <center> <label for="cm3">CM³</label></center> -->
                                        <div class="input-group">
                                            <input type="text" hidden class="form-control" id="resultcm3" name="cm3" onblur="calcularcm3()" readonly="" required="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="fragilidade">Escolha a fragilidade do produto *</label>
                                        <select name="fragilidade" id="fragilidade" class="form-control  select2bs4" required="">
                                            <option value="1">Produto Frágil</option>
                                            <option value="2">Produto Resistente</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="retirar_mao">Aceita retirada em mãos? *</label>
                                        <select name="retirar_mao" id="retirar_mao" class="form-control  select2bs4" required="">
                                            <option value="1">Sim</option>
                                            <option value="2">Não</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <label for="garantia_opcao">Garantia</label>
                                        <select name="garantia_opcao" id="garantia_opcao" class="form-control  select2bs4" required="" onchange="verifica(this.value)">
                                            <option value="1">Sim</option>
                                            <option value="2">Não</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="garantia_meses">Meses de Garantia</label>
                                        <input type="number" name="garantia_meses" id="garantia_meses" class="form-control" required="" max="48" min="1">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="estado_produto">Estado do produto *</label>
                                        <select name="estado_produto" id="estado_produto" class="form-control  select2bs4" required="">
                                            <option value="1">Novo</option>
                                            <option value="2">Usado</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <input class="boxestilizado" type="checkbox" name="confirma_responsabilidade" id="confirma_responsabilidade" required="">
                                        <label for="confirma_responsabilidade" style="font-size: 17px;"> Confirmo à minha responsabilidade a integridade do produto e das informações mencionadas neste formulário.</label>
                                    </div>
                                </div>
                                <? var_dump($_SESSION['prod']);?>
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <input type="submit" name="enviar" class="btn btn-green" value="Cadastrar Produto">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </form>
        </div>
    </div>
    <link rel="stylesheet" href="<?= URL_BASE ?>template/plugins/summernote/summernote-bs4.css">
    <script src="<?= URL_BASE ?>template/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="<?= URL_BASE ?>template/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?= URL_BASE ?>template/dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?= URL_BASE ?>template/dist/js/demo.js"></script>
    <!-- Summernote -->
    <script src="<?= URL_BASE ?>template/plugins/summernote/summernote-bs4.min.js"></script>
    <script language="JavaScript" src='<?= URL_BASE ?>vendor/functions/functions_artesanato.js'></script>
</body>
</html>

<script>
    $('#nome_produto_vendedor').keyup(delay(function(e) {
        //recebe nome do produto
        $.post('<?= $_SESSION['prod'] ?>/ajax/verifica_produtos.php', {
                nome_prod: this.value,
                cod_loja: $("#cod_loja").val(),
                func: 1
            }, function(data) {
                if (data > 0) {
                    $("#nome_produto_vendedor")
                        .removeClass("is-valid")
                        .addClass("is-invalid");
                    $("#nome_produto_vendedor").val("");
                    Swal.fire({
                        icon: "warning",
                        title: "",
                        showClass: {
                            popup: "animate__animated animate__bounceInUp",
                        },
                        hideClass: {
                            popup: "animate__animated animate__bounceOutDown",
                        },
                        text: "Você já cadastrou este produto!",
                    });
                } else {
                    $("#nome_produto_vendedor")
                        .removeClass("is-invalid")
                        .addClass("is-valid");
                }
            },
            "json"
        );
    }, 500));
    //verifica se ja possui o produto cadastrado
</script>