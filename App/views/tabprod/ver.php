<?php
echo mensagens($data['mensagem']);

$CatPai = $this->model('CatPaisModel');
$CatFilho = $this->model('CatFilhoModel');

$data = $data['registros'];
$data = $data[0];

//recebe os nomes-valores dos inputs
$nome_catp = $CatPai->getnomeId($data['cod_cat_p']);
$nome_catf = $CatFilho->getnomeId($data['cod_cat_f']);

?>
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6 heading" style="margin-bottom: -1%;">
                <h4>Detalhes Tabela Produtos</h4>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="/tabprod/index" style="color:green">Index</a></li>
                    <li class="breadcrumb-item active">Detalhamento</li>
                </ol>
            </div>
        </div>
    </div>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="card card-purple card-outline card-outline-tabs">
                <div class="card-header p-0 pt-1 border-bottom-0">
                    <ul class="nav nav-tabs" role="tablist">
                        <a href="/tabprod/index" class="nav-link"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</a>
                        <li class="nav-item"><a class="nav-link active" href="#editar" data-toggle="tab">Editar</a></li>
                    </ul>
                </div>
                <div class="card-body">
                    <div class="tab-content">
                        <div class="active tab-pane" id="editar">
                            <section class="content">
                                <div class="row">
                                    <div class="card-body">
                                        <form action="/tabprod/editar/<?php echo $data['cod_prod']; ?>" method="post">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Nome Produto *</label>
                                                        <input type="text" class="form-control" name="nome_prod" id="nome_prod" value="<?php echo $data['nome_prod']; ?>" required="" autocomplete="off">
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label> Código Produto</label>
                                                        <input type="text" class="form-control" name="cod_prod" id="cod_prod" value="<?php echo $data['cod_prod']; ?>" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label>Categoria Pai *</label>
                                                        <select class="form-control select2bs4" style="width: 100%;" name="cod_cat_p" required="">
                                                            <?php foreach ($CatPai->getAll() as $value) {
                                                                if ($value['cod_cat_p'] == $data['cod_cat_p']) {
                                                                    echo '<option value="' . $value['cod_cat_p'] . '" selected="selected" >' . $value['nome_cat_p'] . '</option>';
                                                                } else {
                                                                    echo '<option value="' . $value['cod_cat_p'] . '">' . $value['nome_cat_p'] . '</option>';
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label>Categoria Filho *</label>
                                                        <select class="form-control select2bs4" style="width: 100%;" name="cod_cat_f" required="">
                                                            <?php foreach ($CatFilho->getAll() as $value) {
                                                                if ($value['cod_cat_f'] == $data['cod_cat_f']) {
                                                                    echo '<option value="' . $value['cod_cat_f'] . '" selected="selected" >' . $value['nome_cat_f'] . '</option>';
                                                                } else {
                                                                    echo '<option value="' . $value['cod_cat_f'] . '">' . $value['nome_cat_f'] . '</option>';
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group" style="margin-top: 17.5%;">
                                                        <input type="checkbox" id="ind_ativo" name="ind_ativo" <?php if ($data['ind_ativo'] == 0) echo "checked" ?> data-bootstrap-switch>
                                                        <label for="ind_ativo">Produto Ativo</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-footer">
                                                <input type="submit" name="Atualizar" class="btn btn-purple btn-md float-right" value="Salvar">
                                                <a href="/tabprod/index" class="btn btn-orange btn-md float-right" style="margin-right:4px;">Cancelar</a>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>