<!-- Content Header (Page header)  -->

<?php
echo mensagens($data['mensagem']);
?>
<section class="content-header">
    <div class="container" style="background: pink;">
    </div>
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6 heading" style="margin-bottom: -1%;">
                <h4>Tabela de Produtos</h4>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a style="color: green" href="tabprod/index">Index</a></li>
                    <li class="breadcrumb-item active"></li>
                </ol>
            </div>
        </div>
    </div>
</section>
<!-- Main Content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="btn-group">
                        <div> <a href="/tabprod/cadastrar" type="button" class="btn btn-block btn-green btn-sm"><i class="fas fa-plus"></i> Produto</a> </div>
                        &nbsp
                        <div> <a href="/tabprod/exportar" type="button" class="btn btn-block btn-orange btn-sm">Exportar</a> </div>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <table id="table" class="table table-bordered table-hover" style="width:100%">
                                <thead>
                                    <tr>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">Atalhos</th> <!-- botões -->
                                        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Código</th>
                                        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Nome</th>
                                        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Cat. Pai</th>
                                        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Cat. Filho</th>
                                        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Ativo</th>
                                    </tr>
                                </thead>
                                <!-- tbody -->
                                <tbody>
                                    <?php $CatPai = $this->model('CatPaisModel'); ?>
                                    <?php $CatFilho = $this->model('CatFilhoModel'); ?>
                                    <?php foreach ($data['registros'] as $value) : ?>
                                        <?php $cod_p = $CatPai->getnomeId($value['cod_cat_p']); ?>
                                        <?php $cod_f = $CatFilho->getnomeId($value['cod_cat_f']); ?>
                                        <tr>
                                            <td>
                                                <a title="Detalhar" href="/tabprod/ver/<?php echo $value['cod_prod']; ?>" type="button" class="btn btn-green btn-sm"><i class="fas fa-eye"></i></a> <!-- visualizar -->
                                                <button title="Excluir" value="<?= $value['cod_prod']; ?>" href="#" type="button" class="btn btn-danger btn-sm excluir"><i class="fas fa-trash"></i></button> <!-- excluir -->
                                            </td>
                                            <td><?php echo  $value['cod_prod']; ?></td>
                                            <td><?php echo  $value['nome_prod']; ?></td>
                                            <td><?php echo  $cod_p[0]['nome_cat_p']; ?></td>
                                            <td><?php echo  $cod_f[0]['nome_cat_f']; ?></td>
                                            <td><?php echo $value['ind_ativo'] == 0 ? "Ativo" : "Desativado"; ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
</section>
<script language="JavaScript" src='<?= URL_BASE ?>vendor/functions/functions_tabprod.js'></script>
<script>
    $('#table').on('click', '.excluir', function() {
        //seta variaveis
        var html = this;
        var cod = this.value;
        Swal.fire({
            title: "Deseja Excluir?",
            text: "Não será possível reverter esta ação!",
            icon: 'warning',
            showCancelButton: true,
            cancelButtonColor: '#3085d6',
            confirmButtonColor: '#d33',
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Excluir',
        }).then((result) => {
            if (result.value) {
                $.post('<?= $_SESSION['prod'] ?>/ajax/verifica_tabprod.php', {
                    cod: cod,
                    func: '1'
                }, function(data) {
                    if (data > 0) {
                        Swal.fire({
                            title: "Ooops!",
                            text: "Existem PRODUTOS DE CLIENTES cadastrados!",
                            icon: 'warning',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonText: 'Ok',
                        })
                    } else {
                        $.post('<?= $_SESSION['prod'] ?>/ajax/verifica_tabprod.php', {
                            cod: cod,
                            func: '2'
                        }, function(data) {
                            if (data > 0) {
                                Swal.fire({
                                    title: "Ooops!",
                                    text: "Erro ao excluir esse produto!",
                                    icon: 'error',
                                    showCancelButton: false,
                                    confirmButtonColor: '#3085d6',
                                    cancelButtonText: 'Ok',
                                })
                            } else {
                                Swal.fire({
                                    title: "Sucesso!",
                                    text: "Produto excluído com sucesso!",
                                    icon: 'success',
                                    showCancelButton: false,
                                    confirmButtonColor: '#3085d6',
                                    cancelButtonText: 'Ok',
                                })
                                $(html).parent().parent().remove();
                            }
                        }, 'json');
                    }
                }, 'json');
            }
        });
    });
</script>