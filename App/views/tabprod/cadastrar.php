<?php
echo mensagens($data['mensagem']);
$CatPai = $this->model('CatPaisModel');
$CatFilho = $this->model('CatFilhoModel');
?>
<section class="content-header">
    <div class="container">
    </div>
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6 heading" style="margin-bottom: -1%;">
                <h4>Cadastro Tabela de Produto</h4>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a style="color: green" href="/tabprod/index">Index</a></li>
                    <li class="breadcrumb-item active">Cadastro</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<div class="container-fluid">
    <form action="/tabprod/cadastrar" method="post">
        <div class="col-md-6">
            <div class="card card-default">
                <div class="card-header">
                    <a href="/tabprod/index" class="btn btn-default btn-md float-left" style="margin-left:5px;"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</a>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Nome Produto *</label>
                                <input class="form-control col-md-12" name="nome_prod" id="nome_cat_p" type="text" maxlength="20" required="" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Código Produto</label>
                                <input class="form-control col-md-12" name="cod_prod" id="cod_cat_p" value="<?php echo $data['registros']['last_cod'] + 1; ?>" readonly type="text" required="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Categoria Pai *</label>
                                <select class="form-control select2bs4" style="width: 100%;" name="cod_cat_p" required="">
                                    <?php foreach ($CatPai->getAll() as $value)
                                        echo '<option value="' . $value['cod_cat_p'] . '">' . $value['nome_cat_p'] . '</option>';
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Categoria Filho *</label>
                                <select class="form-control select2bs4" style="width: 100%;" name="cod_cat_f" required="">
                                    <?php foreach ($CatFilho->getAll() as $value)
                                        echo '<option value="' . $value['cod_cat_f'] . '">' . $value['nome_cat_f'] . '</option>';
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group" style="margin-top: 17.5%;">
                                <input type="checkbox" id="ind_ativo" name="ind_ativo" checked data-bootstrap-switch>
                                <label for="ind_ativo" >Produto Ativo</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <input type="submit" name="Cadastrar" class="btn btn-purple float-right" value="Cadastrar">
                    <a href="/tabprod/index" class="btn btn-orange btn-md float-right" style="margin-right:5px;">Cancelar</a>
                </div>
            </div>
        </div>
    </form>
</div>