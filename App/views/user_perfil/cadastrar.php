<!-- Content Header (Page header)  -->
<section class="content-header">
    <div class="container" style="background: lavender;">
        <?php
        if (!empty($data['mensagem'])) :
            foreach ($data['mensagem'] as $m) :
                echo $m . "<br>";
            endforeach;
        endif;
        $matriz_cone = $this->model('Empresa');
        $userperfil = $this->model('UserPerfil');
        $matriz = $userperfil->getultuser($_SESSION['matriz']);
        $cod =  $matriz[0];
        $cod_user = $cod['max_cod'] + 1;
        ?> 

    </div>
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h4>Cadastro de Usuários</h4>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a style="color: indigo" href="#">Index</a></li>
                    <li class="breadcrumb-item active">Cadastro</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-6">
            <div class="card card-default">
                <div class="card-header">
                    <h3 class="card-title">Acesso </h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <form action="/user_perfil/cadastrar" method="post" role="form">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Nome de usuário</label>
                                    <input name="nome_user" id="nome_user" class="form-control" type="text" required="required" name="text" />
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Código de usuário</label>
                                    <input name="cod_user" class="form-control" type="text" readonly value="<?php echo $cod_user; ?>" required="required" name="number" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>CNPJ Matriz</label>
                                    <input class="form-control col-md-12" name="empresa_matriz" id="empresa_matriz" list="list" autocomplete="off" required="">
                                    <datalist id="list">
                                        <?php foreach ($matriz_cone->GETMATRIZ($_SESSION['matriz']) as $value) : ?>
                                            <?php
                                            $cnpj_cpf = $value['empresacnpj_estab'];
                                            $cnpj = preg_replace("/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/", "\$1.\$2.\$3/\$4-\$5", $cnpj_cpf);
                                            ?>
                                            <option value="<?php echo $cnpj . ' - ' . $value['empresanome_abrev']; ?>"></option>
                                        <?php endforeach; ?>
                                    </datalist>
                                    </input>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Validade em dias</label>
                                    <input name="dias_validade" id="dias_validade" type="number" class="form-control" required="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>E-mail </label>
                                    <input name="email" id="email" class="form-control" type="email" required="" class="input-text" />
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Senha</label>
                                    <input name="senha" id="senha" type="password" class="form-control" required="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input" id="user_perfilind_presidentec" name="user_perfilind_presidentec">
                                        <label class="custom-control-label" for="user_perfilind_presidentec" onchange="ind_presidente()">Presidente</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input" id="user_perfilind_diretorc" name="user_perfilind_diretorc">
                                        <label class="custom-control-label" for="user_perfilind_diretorc">Diretor</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input" id="user_perfilind_gerentec" name="user_perfilind_gerentec">
                                        <label class="custom-control-label" for="user_perfilind_gerentec">Gerente</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input" id="user_perfilind_coordenadorc" name="user_perfilind_coordenadorc">
                                        <label class="custom-control-label" for="user_perfilind_coordenadorc">Coordenador</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Código Presidente </label>
                                    <input name="user_perfilcod_presidente" id="user_perfilcod_presidente" class="form-control" list="listpre" class="form-control" autocomplete="off" />
                                    <datalist id="listpre">
                                    </datalist>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Código Diretor</label>
                                    <input name="user_perfilcod_diretor" list="listdire" class="form-control" id="user_perfilcod_diretor" autocomplete="off">
                                    <datalist id="listdire">
                                    </datalist>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Código Gerente </label>
                                    <input name="user_perfilcod_gerente" id="user_perfilcod_gerente" class="form-control" list="listgere" autocomplete="off" />
                                    <datalist id="listgere">
                                    </datalist>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Código Coordenador </label>
                                    <input name="user_perfilcod_coordenador" id="user_perfilcod_coordenador" class="form-control" list="listcoo" autocomplete="off"/>
                                    <datalist id="listcoo">
                                    </datalist>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input name="matriz" type="hidden" value="<?php echo $_SESSION['matriz'] ?>" id="matriz" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Foto </label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="exampleInputFile">
                                    <label class="custom-file-label">Escolher arquivo</label>
                                </div>
                                <div class="input-group-append">
                                    <button name="AtualizarFoto" class="btn btn-default pull-left">Enviar</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input"checked id="ind_ativo" name="ind_ativo">
                                        <label class="custom-control-label" for="ind_ativo"  style="margin-top:25%;">Usuário ativo</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input" id="adm" name="adm">
                                        <label class="custom-control-label" for="adm" style="margin-top:25%;">Administrador</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
        <!-- meio -->
        <!-- right column -->
        <div class="col-md-6">
            <div class="card card-default">
                <div class="card-header">
                    <h3 class="card-title">Pessoal</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="form-group">
                        <label>Nome Completo </label>
                        <input name="nome_completo" id="nome_completo" type="text" class="form-control" required="">
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>País</label>
                                <input name="pais" id="pais" type="text" class="form-control" required="">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Estado</label>
                                <input name="estado" id="estado" type="text" maxlength="4" class="form-control" require="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Cidade</label>
                                <input name="cidade" id="cidade" type="text" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>CEP</label>
                                <input name="cep" name="cep" onfocus="javascript: retirarFormatacaocep(this);" maxlength="8" onblur="javascript: formatarCampocep(this);" type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Bairro</label>
                                <input name="bairro" id="bairro" type="text" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Rua</label>
                                <input name="rua" id="rua" type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Número</label>
                                <input name="numero" id="numero" type="number" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Complemento</label>
                                <input name="complemento" id="complemento" type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Celular</label>
                                <input name="celular" id="celular" onkeyup="mascara( this, mtel );" maxlength="15" type="text" class="form-control" placeholder="(99) 99999-9999">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Telefone</label>
                                <input name="telefone" onkeyup="mascarat( this, mtelt );" maxlength="9" type="text" class="form-control" placeholder="9999-9999" >
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <input style="margin-left:3px" type="submit" name="Cadastrar" class="btn btn-purple btn-md float-right" value="Salvar">&nbsp
                    <a href="/user_perfil/index" class="btn btn-orange btn-md float-right">Cancelar</a>&nbsp
                </div>
                </form>
            </div>
        </div>
        <!-- MÁSCARA CNPJ -->
        <script>
            function formatarCampo(campoTexto) {
                campoTexto.value = mascaraCnpj(campoTexto.value);
            }

            function retirarFormatacao(campoTexto) {
                campoTexto.value = campoTexto.value.replace(/(\.|\/|\-)/g, "");
            }

            function mascaraCnpj(valor) {
                return valor.replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/g, "\$1.\$2.\$3\/\$4\-\$5");
            }
        </script>
        <!-- MÁSCARA CEP -->
        <script>
            function formatarCampocep(campoTexto) {
                campoTexto.value = mascaracep(campoTexto.value);
            }

            function retirarFormatacaocep(campoTexto) {
                campoTexto.value = campoTexto.value.replace(/(\.|\/|\-)/g, "");
            }

            function mascaracep(valor) {
                return valor.replace(/(\d{2})(\d{3})(\d{3})/g, "\$1.\$2-\$3");
            }
        </script>
        <!-- MÁSCARA CELULAR -->
        <script>
            /* Máscaras ER */
            function mascara(o, f) {
                v_obj = o
                v_fun = f
                setTimeout("execmascara()", 1)
            }

            function execmascara() {
                v_obj.value = v_fun(v_obj.value)
            }

            function mtel(v) {
                v = v.replace(/\D/g, ""); //Remove tudo o que não é dígito
                v = v.replace(/^(\d{2})(\d)/g, "($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos
                v = v.replace(/(\d)(\d{4})$/, "$1-$2"); //Coloca hífen entre o quarto e o quinto dígitos
                return v;
            }

            $("#user_perfilind_presidentec").click(function(event) {

                $("#user_perfilcod_presidente").val("");
                $("#user_perfilcod_diretor").val("");
                $("#user_perfilcod_gerente").val("");
                $("#user_perfilcod_coordenador").val("");

                var dire = $("#user_perfilind_diretorc");
                dire.prop("disabled", !dire.prop("disabled"));

                var gere = $("#user_perfilind_gerentec");
                gere.prop("disabled", !gere.prop("disabled"));

                var coo = $("#user_perfilind_coordenadorc");
                coo.prop("disabled", !coo.prop("disabled"));

                var cooi = $("#user_perfilcod_coordenador");
                cooi.prop("readonly", !cooi.prop("readonly"));

                var gerei = $("#user_perfilcod_gerente");
                gerei.prop("readonly", !gerei.prop("readonly"));

                var direi = $("#user_perfilcod_diretor");
                direi.prop("readonly", !direi.prop("readonly"));

                var prei = $("#user_perfilcod_presidente");
                prei.prop("readonly", !prei.prop("readonly"));
            });

            $("#user_perfilind_diretorc").click(function(event) {

                $("#user_perfilcod_presidente").val("");
                $("#user_perfilcod_diretor").val("");
                $("#user_perfilcod_gerente").val("");
                $("#user_perfilcod_coordenador").val("");

                var pre = $("#user_perfilind_presidentec");
                pre.prop("disabled", !pre.prop("disabled"));

                var gere = $("#user_perfilind_gerentec");
                gere.prop("disabled", !gere.prop("disabled"));

                var coo = $("#user_perfilind_coordenadorc");
                coo.prop("disabled", !coo.prop("disabled"));

                var cooi = $("#user_perfilcod_coordenador");
                cooi.prop("readonly", !cooi.prop("readonly"));

                var gerei = $("#user_perfilcod_gerente");
                gerei.prop("readonly", !gerei.prop("readonly"));

                var direi = $("#user_perfilcod_diretor");
                direi.prop("readonly", !direi.prop("readonly"));

            });

            $("#user_perfilind_coordenadorc").click(function(event) {

                $("#user_perfilcod_presidente").val("");
                $("#user_perfilcod_diretor").val("");
                $("#user_perfilcod_gerente").val("");

                $("#user_perfilcod_coordenador").val("");
                var cooi = $("#user_perfilcod_coordenador");
                cooi.prop("readonly", !cooi.prop("readonly"));

                var pre = $("#user_perfilind_presidentec");
                pre.prop("disabled", !pre.prop("disabled"));

                var gere = $("#user_perfilind_gerentec");
                gere.prop("disabled", !gere.prop("disabled"));    

                var dire = $("#user_perfilind_diretorc");
                dire.prop("disabled", !dire.prop("disabled"));
            });

            $("#user_perfilind_gerentec").click(function(event) {

                $("#user_perfilcod_presidente").val("");
                $("#user_perfilcod_diretor").val("");
                $("#user_perfilcod_gerente").val("");
                $("#user_perfilcod_coordenador").val("");

                var pre = $("#user_perfilind_presidentec");
                pre.prop("disabled", !pre.prop("disabled"));

                var cooi = $("#user_perfilcod_coordenador");
                cooi.prop("readonly", !cooi.prop("readonly"));

                var gerei = $("#user_perfilcod_gerente");
                gerei.prop("readonly", !gerei.prop("readonly"));

                var pre = $("#user_perfilind_presidentec");
                pre.prop("disabled", !pre.prop("disabled"));

                var coo = $("#user_perfilind_coordenadorc");
                coo.prop("disabled", !coo.prop("disabled"));

                var dire = $("#user_perfilind_diretorc");
                dire.prop("disabled", !dire.prop("disabled"));
            });
        </script>
        <!-- MÁSCARA TELEFONE -->
        <script>
            /* Máscaras ER */
            function mascarat(o, f) {
                v_obj = o
                v_fun = f
                setTimeout("execmascara()", 1)
            }

            function execmascarat() {
                v_obj.value = v_fun(v_obj.value)
            }

            function mtelt(v) {
                v = v.replace(/\D/g, ""); //Remove tudo o que não é dígito
                v = v.replace(/(\d)(\d{4})$/, "$1-$2"); //Coloca hífen entre o quarto e o quinto dígitos
                return v;
            }


            $(document).ready(function() {

                //cnpj matriz
                var matriz = $("#matriz").val();

                //código do presidente
                $("#user_perfilcod_presidente").val("");
                $.post('/ajax/call_autocomplete.php', {
                    cnpj_estab: matriz,
                    func: '9'
                }, function(data) {
                    console.log(data);
                    var cmb = '<option value=""></option>';
                    $.each(data, function(index, value) {
                        cmb = cmb + '<option value="' + value.user_perfilcod_user + ' - ' + value.user_perfilnome_completo + '"></option>';
                    });
                    $('#listpre').html(cmb);
                }, 'json');

                //código do diretor
                $("#user_perfilcod_presidente").val("");
                $.post('/ajax/call_autocomplete.php', {
                    cnpj_estab: matriz,
                    func: '10'
                }, function(data) {
                    console.log(data);
                    var cmb = '<option value=""></option>';
                    $.each(data, function(index, value) {
                        cmb = cmb + '<option value="' + value.user_perfilcod_user + ' - ' + value.user_perfilnome_completo + '"></option>';
                    });
                    $('#listdire').html(cmb);
                }, 'json');

                //código do gerente
                $("#user_perfilcod_presidente").val("");
                $.post('/ajax/call_autocomplete.php', {
                    cnpj_estab: matriz,
                    func: '11'
                }, function(data) {
                    console.log(data);
                    var cmb = '<option value=""></option>';
                    $.each(data, function(index, value) {
                        cmb = cmb + '<option value="' + value.user_perfilcod_user + ' - ' + value.user_perfilnome_completo + '"></option>';
                    });
                    $('#listgere').html(cmb);
                }, 'json');
            });
        </script>
    </div>
    <!-- /.row -->
</section>