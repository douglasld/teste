<div class="x_panel">
    <div class="x_title">
        <h2>Empresas</h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <?php
        if (!empty($data['mensagem'])):

            foreach ($data['mensagem'] as $m):
                echo $m . "<br>";
            endforeach;

        endif;
        ?>

        <form action="/user_perfil/editar/<?php echo $data['registros']['user_perfilid']; ?>" method="post" class="form-horizontal form-label-left">

            <div class="box-body">

                <div class="form-group row">
                    <label class="control-label col-md-3" for="cidade">CNPJ Matriz:</label>
                    <div class="col-md-7">
                        <?php
                        if(strlen($data['registros']['user_perfilcnpj_matriz'] < 11)){
                            $cnpj_matriz = $data['registros']['user_perfilcnpj_matriz'];
                           } else{
                            $cnpj_matriz = Mask('##.###.###/####-##',$data['registros']['user_perfilcnpj_matriz']);
                           }
                        ?>
                        <input type="text" name="cnpj_matriz" data-inputmask="'mask' : '99.999.999/9999-99'" value="<?php echo $cnpj_matriz ?>"
                               class="form-control col-md-7" readonly>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="control-label col-md-3" for="cidade">Código Usuário:</label>
                    <div class="col-md-7">
                        <input type="text" name="cod_user"
                               value="<?php echo $data['registros']['user_perfilcod_user']; ?>" class="form-control col-md-7">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="control-label col-md-3" for="cidade">Nome usuário:</label>
                    <div class="col-md-7">
                        <input type="text" name="nome_user"
                               value="<?php echo $data['registros']['user_perfilnome_user']; ?>" class="form-control col-md-7">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="control-label col-md-3" for="cidade">Nome Completo:</label>
                    <div class="col-md-7">
                        <input type="text" name="nome_completo"
                               value="<?php echo $data['registros']['user_perfilnome_completo']; ?>"
                               class="form-control col-md-7">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="control-label col-md-3" for="cidade">Dias validade:</label>
                    <div class="col-md-7">
                        <input type="number" step="1" name="dias_validade"
                               value="<?php echo $data['registros']['user_perfildias_validade']; ?>"
                               class="form-control col-md-7">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="control-label col-md-3" for="cidade">e-mail:</label>
                    <div class="col-md-7">
                        <input type="email" name="e_mail" value="<?php echo $data['registros']['user_perfile_mail']; ?>"
                               class="form-control col-md-7">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="control-label col-md-3" for="cidade">Rua:</label>
                    <div class="col-md-7">
                        <input type="text" name="rua" value="<?php echo $data['registros']['user_perfilrua']; ?>"
                               class="form-control col-md-7">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="control-label col-md-3" for="cidade">Número:</label>
                    <div class="col-md-7">
                        <input type="text" name="numero" value="<?php echo $data['registros']['user_perfilnumero']; ?>"
                               class="form-control col-md-7">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="control-label col-md-3" for="cidade">Complemento:</label>
                    <div class="col-md-7">
                        <input type="text" name="complemento"
                               value="<?php echo $data['registros']['user_perfilcomplemento']; ?>"
                               class="form-control col-md-7">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="control-label col-md-3" for="cidade">Bairro:</label>
                    <div class="col-md-7">
                        <input type="text" name="bairro" value="<?php echo $data['registros']['user_perfilbairro']; ?>"
                               class="form-control col-md-7">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="control-label col-md-3" for="cidade">Cidade:</label>
                    <div class="col-md-7">
                        <input type="text" name="cidade" value="<?php echo $data['registros']['user_perfilcidade']; ?>"
                               class="form-control col-md-7">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="control-label col-md-3" for="cidade">Estado:</label>
                    <div class="col-md-7">
                        <input type="text" name="estado" maxlength="2" value="<?php echo $data['registros']['user_perfilestado']; ?>"
                               class="form-control col-md-7">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="control-label col-md-3" for="cidade">Pais:</label>
                    <div class="col-md-7">
                        <input type="text" name="pais" value="<?php echo $data['registros']['user_perfilpais']; ?>"
                               class="form-control col-md-7">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="control-label col-md-3" for="cidade">CEP:</label>
                    <div class="col-md-7">
                        <input type="text" name="cep" data-inputmask="'mask' : '99.999-999'" value="<?php echo $data['registros']['user_perfilcep']; ?>"
                               class="form-control col-md-7">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="control-label col-md-3" for="cidade">Telefone:</label>
                    <div class="col-md-7">
                        <input type="text" name="telefone" data-inputmask="'mask' : '(99)9999-9999'" value="<?php echo $data['registros']['user_perfiltelefone']; ?>"
                               class="form-control col-md-7">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="control-label col-md-3" for="cidade">Celular:</label>
                    <div class="col-md-7">
                        <input type="text" name="celular" data-inputmask="'mask' : '(99)99999-9999'" value="<?php echo $data['registros']['user_perfilcelular']; ?>"
                               class="form-control col-md-7">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="control-label col-md-3" for="cidade">ADM:</label>
                    <div class="col-md-7">
                        <select name="adm" id="adm" class="form-control col-md-7">
                            <option value="<?php echo $data['registros']['user_perfiladm']; ?>">
                                <?php echo $data['registros']['user_perfiladm'] == 0 ? "Sim" : "Não"; ?>
                            </option>
                            <option value="0">Sim</option>
                            <option value="1">Não</option>
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="control-label col-md-3" for="cidade">Gestor:</label>
                    <div class="col-md-7">
                        <select name="gestor" id="gestor" class="form-control col-md-7">
                            <option value="<?php echo $data['registros']['user_perfilgestor']; ?>">
                                <?php echo $data['registros']['user_perfilgestor'] == 0 ? "Sim" : "Não"; ?>
                            </option>
                            <option value="0">Sim</option>
                            <option value="1">Não</option>
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="control-label col-md-3" for="cidade">Ativo:</label>
                    <div class="col-md-7">
                        <select name="ind_ativo" id="ind_ativo" class="form-control col-md-7">
                            <option value="<?php echo $data['registros']['user_perfilind_ativo']; ?>">
                                <?php echo $data['registros']['user_perfilind_ativo'] == 0 ? "Sim" : "Não"; ?>
                            </option>
                            <option value="0">Sim</option>
                            <option value="1">Não</option>
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="control-label col-md-3" for="cidade">Origem:</label>
                    <div class="col-md-7">
                        <select name="ind_origem" id="ind_origem" class="form-control col-md-7">
                            <option value="<?php echo $data['registros']['user_perfilind_origem']; ?>">
                                <?php echo $data['registros']['user_perfilind_origem'] == 0 ? "Sim" : "Não"; ?>
                            </option>
                            <option value="0">Sim</option>
                            <option value="1">Não</option>
                        </select>
                    </div>
                </div>
            </div>

            <!-- /.box-body -->
            <div class="box-footer">
                <input type="submit" name="Atualizar" class="btn btn-success pull-left" value="Atualizar">
                <a href="/user_perfil/index" class="btn btn-danger pull-right">Fechar</a>
            </div>

        </form>

    </div>
</div>
