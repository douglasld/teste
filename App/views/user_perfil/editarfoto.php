<div class="x_panel">
    <div class="x_title">
        <h2>Empresas</h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <?php
        if (!empty($data['mensagem'])):

            foreach ($data['mensagem'] as $m):
                echo $m . "<br>";
            endforeach;

        endif;
        ?>

        <form action="/user_perfil/editarfoto/<?php echo $data['registros']['user_perfilid']; ?>" method="post"
              enctype="multipart/form-data" class="form-horizontal form-label-left">

            <div class="box-body">

                <div class="form-group row">
                    <label class="control-label col-md-3" for="cidade">Foto Atual</label>
                    <div class="col-md-7">
                        <?php if (strlen($data['registros']['user_perfilfoto'] >= 5)) { ?>
                            <img src="<?php echo URL_BASE ?>/images/<?php echo $data['registros']['user_perfilfoto'] ?>"
                                 class="thumbnail">
                        <?php } ?>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="control-label col-md-3" for="cidade">Nova Foto </label>
                    <div class="col-md-7">
                        <input type="file" name="foo" id="foo" value="" class="form-control col-md-7">
                    </div>
                </div>

            </div>

            <!-- /.box-body -->
            <div class="box-footer">
                <button name="AtualizarFoto" class="btn btn-success pull-left">Atualizar Foto</button>
                <a href="/user_perfil/index" class="btn btn-danger pull-right">Cancelar</a>
            </div>
        </form>
    </div>
</div>