<style>
    .nav-tabs .nav-item .nav-link {
        color: #6039A8 !important;
    }
</style>

<!-- Content Header (Page header)  -->
<section class="content-header">

    <div class="container" style="background: pink;">
        <?php
        if (!empty($data['mensagem'])) :
            foreach ($data['mensagem'] as $m) :
                echo $m . "<br>";
            endforeach;
        endif;
        $matriz_cone = $this->model('Empresa');
        $userPerfil = $this->model('UserPerfil');

        //user_presidente
        $nome_user = $userPerfil->getultusercod($data['perfil']['user_perfilcod_presidente']);
        $nome_user = $nome_user[0];

        //user_diretor
        $nome_userr = $userPerfil->getultusercod($data['perfil']['user_perfilcod_diretor']);
        $nome_userr = $nome_userr[0];

        //user_diretor
        $nome_userrr = $userPerfil->getultusercod($data['perfil']['user_perfilcod_gerente']);
        $nome_userrr = $nome_userrr[0];

        ?>
    </div>
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h4>Detalhes Usuário </h4>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a style="color:indigo">Index</a></li>
                    <li class="breadcrumb-item active">Detalhamento</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="row">
        <!-- /.col -->
        <div class="col-md-9">
            <div class="card card-purple card-outline card-outline-tabs">
                <div class="card-header p-0 pt-1 border-bottom-0">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item"><a class="nav-link active" href="#editar" data-toggle="tab">Editar</a></li>
                        <!-- <li class="nav-item"><a class="nav-link " href="#resumo" data-toggle="tab">Resumo</a></li> -->
                        <li class="nav-item"><a class="nav-link" href="#logs" data-toggle="tab">Logs</a></li>
                        <li class="nav-item"><a class="nav-link" href="#estabelecimentos" data-toggle="tab">Estabelecimentos</a></li>
                        <li class="nav-item"><a class="nav-link" href="#exportar'" data-toggle="tab">Exportar</a></li>
                    </ul>
                </div><!-- /.card-header -->
                <div class="card-body">
                    <div class="tab-content">
                        <div class="tab-pane" id="resumo">
                            <div class="invoice p-3 mb-3">
                                <!-- title row -->
                                <div class="row">
                                    <div class="col-12">
                                        <h4>
                                            <i class="fas fa-globe"></i> Posto Maochi
                                            <small class="float-right">Data: <?php echo date("d/m/Y"); ?></small>
                                        </h4>
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- info row -->
                                <div class="row invoice-info">
                                    <div class="col-sm-4 invoice-col">
                                        <address>
                                            <strong>Ipiranga, Inc.</strong><br>
                                            <p style="font-size: 13px">
                                                <?php echo $data['perfil']['user_perfilcidade']; ?>, <?php echo $data['perfil']['user_perfilestado']; ?><br>
                                                <?php echo $data['perfil']['user_perfilpais']; ?><br>
                                                <?php echo $data['perfil']['user_perfilrua']; ?>, N° <?php echo $data['perfil']['user_perfilnumero']; ?><br>
                                                CEP <?php echo Mask('#####-###', $data['perfil']['user_perfilcep']); ?></p>
                                            <div></div>
                                        </address>
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- /.row -->
                                <div class="row">
                                    <div class="col-sm-4 invoice-col">
                                        <h5><b>Nome: </b><?php echo $data['perfil']['user_perfilnome_completo']; ?></h5>
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- Table row -->
                                <div class="row">
                                    <div class="col-12 table-responsive">
                                        <table class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Qty</th>
                                                    <th>Product</th>
                                                    <th>Serial #</th>
                                                    <th>Description</th>
                                                    <th>Subtotal</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>1</td>
                                                    <td>Call of Duty</td>
                                                    <td>455-981-221</td>
                                                    <td>El snort testosterone trophy driving gloves handsome</td>
                                                    <td>$64.50</td>
                                                </tr>
                                                <tr>
                                                    <td>1</td>
                                                    <td>Need for Speed IV</td>
                                                    <td>247-925-726</td>
                                                    <td>Wes Anderson umami biodiesel</td>
                                                    <td>$50.00</td>
                                                </tr>
                                                <tr>
                                                    <td>1</td>
                                                    <td>Monsters DVD</td>
                                                    <td>735-845-642</td>
                                                    <td>Terry Richardson helvetica tousled street art master</td>
                                                    <td>$10.70</td>
                                                </tr>
                                                <tr>
                                                    <td>1</td>
                                                    <td>Grown Ups Blue Ray</td>
                                                    <td>422-568-642</td>
                                                    <td>Tousled lomo letterpress</td>
                                                    <td>$25.99</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- /.row -->

                                <br>
                                <div class="row">
                                    <div class="col-sm-4 invoice-col">
                                        <address>
                                            <strong>Resumo</strong><br>
                                            <p style="font-size: 13px">
                                                Nome: <?php echo $data['perfil']['user_perfilnome_completo']; ?><br>
                                                Usuário: <?php echo $data['perfil']['user_perfilnome_user']; ?><br>
                                                Código: <?php echo $data['perfil']['user_perfilcod_user']; ?><br>
                                                CNPJ Matriz: <?php echo $data['perfil']['user_perfilcnpj_matriz']; ?><br>
                                                <br>
                                                <b>Ativo: </b><?php echo $data['perfil']['user_perfilind_ativo'] == 0 ? "Sim" : "Não"; ?><br>
                                                <b>Origem: </b><?php echo $data['perfil']['user_perfilind_origem'] == 0 ? "Portal" : "Integrador"; ?><br>
                                                <b>Adm: </b><?php echo $data['perfil']['user_perfiladm'] == 0 ? "Sim" : "Não"; ?><br>
                                                <b>Gestor: </b><?php echo $data['perfil']['user_perfilgestor'] == 0 ? "Sim" : "Não"; ?><br>
                                                <b>Validade: </b><?php echo $data['perfil']['user_perfildias_validade']; ?> dias <br>
                                            </p>
                                            <div></div>
                                            <br>
                                        </address>
                                    </div>

                                    <div class="col-sm-4 invoice-col">
                                        <address>
                                            <strong>Endereço</strong><br>
                                            <p style="font-size: 13px">
                                                <?php echo $data['perfil']['user_perfilcidade']; ?>, <?php echo $data['perfil']['user_perfilestado']; ?><br>
                                                <?php echo $data['perfil']['user_perfilpais']; ?><br>
                                                <?php echo $data['perfil']['user_perfilrua']; ?>, N° <?php echo $data['perfil']['user_perfilnumero']; ?><br>
                                                CEP <?php echo Mask('#####-###', $data['perfil']['user_perfilcep']); ?>

                                            </p>
                                            <div></div>
                                        </address>
                                        <address>
                                            <strong>Contato</strong><br>
                                            <p style="font-size: 13px">
                                                <?php echo $data['perfil']['user_perfiltelefone']; ?><br>
                                                <?php echo $data['perfil']['user_perfilcelular']; ?><br>
                                                <?php echo $data['perfil']['user_perfile_mail']; ?>
                                            </p>
                                            <div></div>
                                            <br>
                                        </address>
                                    </div>
                                </div>
                                <!-- this row will not appear when printing -->
                                <div class="row no-print">
                                    <div class="col-12">
                                        <a href="invoice-print.html" target="_blank" class="btn btn-default"><i class="fas fa-print"></i> Imprimir</a>
                                        <button type="button" class="btn btn-primary float-right" style="margin-right: 5px;">
                                            <i class="fas fa-download"></i> Gerar PDF
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- /.tab-pane // fecha resumooo-->
                        <div class="active tab-pane" id="editar">
                            <section class="content">
                                <div class="row">
                                    <!-- left column -->
                                    <div class="col-sm-12">
                                        <div class="invoice p-3 mb-3">
                                            <div class="card-header">
                                                <h3 class="card-title">Acesso </h3>
                                            </div>
                                            <!-- /.card-header -->
                                            <?php var_dump($data['perfil']) ?>
                                            <div class="card-body">
                                                <form action="/user_perfil/editar/<?php echo $data['perfil']['user_perfilid']; ?>" method="post" role="form">
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <label>CNPJ Matriz</label>
                                                                <?php $nome = $matriz_cone->GETMATRIZ($data['perfil']['user_perfilcnpj_matriz']);
                                                                $nome = $nome[0];
                                                                ?>
                                                                <input name="cnpj_matriz" id="cnpj_matriz" readonly value="<?php echo $data['perfil']['user_perfilcnpj_matriz'] . ' - ' . $nome['empresanome_abrev']  ?>" type="text" class="form-control" required="required">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <label>Código de usuário</label>
                                                                <input name="cod_user" readonly value="<?php echo $data['perfil']['user_perfilcod_user']; ?>" class="form-control" placeholder="123..." type="text" required="required" name="numbers" pattern="[0-9]+$" />
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <label>Nome de usuário</label>
                                                                <input name="nome_user" readonly value="<?php echo $data['perfil']['user_perfilnome_user']; ?>" class="form-control" type="text" required="required" name="text" />
                                                            </div>
                                                        </div>



                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <label for="exampleInputEmail1">Nome Completo </label>
                                                                <input name="nome_completo" type="text" value="<?php echo $data['perfil']['user_perfilnome_completo']; ?>" class="form-control" id="exampleInputEmail1" placeholder="Enter email" required="required">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <label for="exampleInputEmail1">E-mail </label>
                                                                <input name="e_mail" value="<?php echo $data['perfil']['user_perfile_mail']; ?>" class="form-control" placeholder="exemplo@resolute.com.br" type="email" required="required" class="input-text" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" />
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <label for="exampleInputPassword1">Senha</label>
                                                                <input name="senha" type="password" class="form-control" id="senha">
                                                            </div>
                                                        </div>


                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <label>Validade em dias</label>
                                                                <input name="dias_validade" value="<?php echo $data['perfil']['user_perfildias_validade']; ?>" type="text" class="form-control" required="required">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <label for="exampleInputFile">Foto </label>
                                                                <div class="input-group">
                                                                    <div class="custom-file">
                                                                        <input type="file" class="custom-file-input" id="exampleInputFile">
                                                                        <label class="custom-file-label" for="exampleInputFile">Escolher arquivo</label>
                                                                    </div>
                                                                    <div class="input-group-append">
                                                                        <button name="AtualizarFoto" class="btn btn-default pull-left">Enviar</button>
                                                                        <!-- FIXME: arrumar envio de formulario com foto e senha juntos -->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <br>

                                                    <div class="row">
                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <div class="custom-control custom-switch">
                                                                    <input type="checkbox" <?php if ($data['perfil']['user_perfilind_presidente'] == '1') {
                                                                                                echo "checked";
                                                                                            } else {
                                                                                                echo "disabled";
                                                                                            } ?> class="custom-control-input" id="user_perfilind_presidentec" name="user_perfilind_presidentec">
                                                                    <label class="custom-control-label" for="user_perfilind_presidentec">Presidente</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <div class="custom-control custom-switch">
                                                                    <input type="checkbox" <?php if ($data['perfil']['user_perfilind_diretor'] == '1') {
                                                                                                echo "checked";
                                                                                            } else {
                                                                                                echo "disabled";
                                                                                            } ?> class="custom-control-input" id="user_perfilind_diretorc" name="user_perfilind_diretorc">
                                                                    <label class="custom-control-label" for="user_perfilind_diretorc">Diretor</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <div class="custom-control custom-switch">
                                                                    <input type="checkbox" <?php if ($data['perfil']['user_perfilind_gerente'] == '1') {
                                                                                                echo "checked";
                                                                                            } else {
                                                                                                echo "disabled";
                                                                                            } ?> class="custom-control-input" id="user_perfilind_gerentec" name="user_perfilind_gerentec">
                                                                    <label class="custom-control-label" for="user_perfilind_gerentec">Gerente</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <div class="custom-control custom-switch">
                                                                    <input type="checkbox" <?php if ($data['perfil']['user_perfilind_coordenador'] == '1') {
                                                                                                echo "checked";
                                                                                            } else {
                                                                                                echo "disabled";
                                                                                            } ?> class="custom-control-input" id="user_perfilind_coordenadorc" name="user_perfilind_coordenadorc">
                                                                    <label class="custom-control-label" for="user_perfilind_coordenadorc">Coordenador</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label>Código Presidente </label>
                                                                <input name="user_perfilcod_presidente" value="<?php echo $data['perfil']['user_perfilcod_presidente'] . ' - ' . $nome_user['user_perfilnome_user']; ?>" value="<?php echo $data['perfil']['user_perfilcod_presidente']; ?>" <?php if ($data['perfil']['user_perfilind_presidente'] == '1') {
                                                                                                                                                                                                                                                                                                echo "readonly";
                                                                                                                                                                                                                                                                                            } ?> id="user_perfilcod_presidente" class="form-control" list="listpre" class="form-control" autocomplete="off" />
                                                                <datalist id="listpre">
                                                                </datalist>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label>Código Diretor</label>
                                                                <input name="user_perfilcod_diretor" value="<?php echo $data['perfil']['user_perfilcod_diretor'] . ' - ' . $nome_userr['user_perfilnome_user']; ?>" <?php if ($data['perfil']['user_perfilind_diretor'] == '1' && $data['perfil']['user_perfilind_presidente'] == '1') {
                                                                                                                                                                                                                        echo "readonly";
                                                                                                                                                                                                                    } ?> list="listdire" class="form-control" id="user_perfilcod_diretor" autocomplete="off" />
                                                                <datalist id="listdire">
                                                                </datalist>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label>Código Gerente </label>
                                                                <input name="user_perfilcod_gerente" value="<?php echo $data['perfil']['user_perfilcod_gerente'] . ' - ' . $nome_userrr['user_perfilnome_user']; ?>" <?php if ($data['perfil']['user_perfilind_gerente'] == '1' || $data['perfil']['user_perfilind_diretor'] == '1' || $data['perfil']['user_perfilind_presidente'] == '1') {
                                                                                                                                                                                                                        echo "readonly";
                                                                                                                                                                                                                    } ?> id="user_perfilcod_gerente" class="form-control" list="listgere" autocomplete="off" />
                                                                <datalist id="listgere">
                                                                </datalist>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label>Código Coordenador </label>
                                                                <input name="user_perfilcod_coordenador" readonly id="user_perfilcod_coordenador" class="form-control" list="listcoo" />
                                                                <datalist id="listcoo">
                                                                </datalist>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <input name="matriz" type="hidden" value="<?php echo $_SESSION['matriz'] ?>" id="matriz" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card-header">
                                                        <h3 class="card-title">Pessoal</h3>
                                                        <br>
                                                    </div>
                                                    <div class="row">

                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <label>Rua</label>
                                                                <input name="rua" value="<?php echo $data['perfil']['user_perfilrua']; ?>" type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <div class="form-group">
                                                                <label>Número</label>
                                                                <input name="numero" value="<?php echo $data['perfil']['user_perfilnumero']; ?>" type="tel" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <label>Bairro</label>
                                                                <input name="bairro" value="<?php echo $data['perfil']['user_perfilbairro']; ?>" type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <label>Complemento</label>
                                                                <input name="complemento" value="<?php echo $data['perfil']['user_perfilcomplemento']; ?>" type="text" class="form-control">
                                                            </div>
                                                        </div>


                                                    </div>

                                                    <div class="row">

                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <label>Cidade</label>
                                                                <input name="cidade" value="<?php echo $data['perfil']['user_perfilcidade']; ?>" type="text" class="form-control" placeholder="Joinville">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <label>Estado</label>
                                                                <input name="estado" value="<?php echo $data['perfil']['user_perfilestado']; ?>" type="text" class="form-control" placeholder="Santa Catarina">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <label>País</label>
                                                                <input name="pais" value="<?php echo $data['perfil']['user_perfilpais']; ?>" type="text" class="form-control" placeholder="Brasil">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <label>CEP</label>
                                                                <input name="cep" id="cep" value="<?php echo $data['perfil']['user_perfilcep']; ?>" maxlength="10" type="text" class="form-control">
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="row">

                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <label>Telefone</label>
                                                                <input name="telefone" id="telefone" value="<?php echo $data['perfil']['user_perfiltelefone']; ?>" type="tel" class="form-control" placeholder="(47) 3025-0000 ">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <div class="form-group">
                                                                <label>Celular</label>
                                                                <input name="celular" id="celular" value="<?php echo $data['perfil']['user_perfilcelular']; ?>" type="text" class="form-control">
                                                            </div>
                                                        </div>                                                                                                        
                                                            <div class="col-sm-3">
                                                                <div class="form-group">
                                                                    <div class="custom-control custom-switch">
                                                                        <input type="checkbox" <?php if ($data['perfil']['user_perfiladm'] == 0) {
                                                                                                        echo "checked";
                                                                                                    } ?>  class="custom-control-input" checked id="adm" name="adm">
                                                                        <label class="custom-control-label" for="adm" >Administrador</label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <div class="form-group">
                                                                    <div class="custom-control custom-switch">
                                                                        <input type="checkbox" <?php if ($data['perfil']['user_perfilind_ativo'] == 0) {
                                                                                                    echo "checked";
                                                                                                } ?> class="custom-control-input" id="ind_ativo" name="ind_ativo">
                                                                        <label class="custom-control-label" for="ind_ativo" >Ativo</label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        
                                                        
                                                    
                                                    </div>

                                                    <div class="card-footer">
                                                        <input type="submit" name="Atualizar" class="btn btn-purple btn-md float-right" value="Salvar">
                                                        <a style="margin-right:3px" href="/user_perfil/index" class="btn btn-orange btn-md float-right">Cancelar</a>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>


                                    </div>

                                </div>
                                <!-- /.row -->
                            </section>




                        </div> <!-- /.tab-pane // fecha resumooo-->






                        <div class="tab-pane" id="logs">
                            <section class="invoice p-3 mb-3">
                                <table id="table_logs" class="display responsive nowrap" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>CNPJ Matriz</th>
                                            <th>Código</th>
                                            <th>Usuário</th>
                                            <th>Data de Emissão</th>
                                            <th>Usuário da Emissão</th>
                                            <th>Título</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($data['logs'] as $user_perfil_log) : ?>
                                            <tr>
                                                <td><?php echo $user_perfil_log['user_perfil_logcnpj_matriz']; ?> </td>
                                                <td><?php echo $user_perfil_log['user_perfil_logcod_user']; ?></td>
                                                <td><?php echo $user_perfil_log['user_perfil_lognome_user']; ?></td>
                                                <td><?php echo implode("/", array_reverse(explode("-", $user_perfil_log['user_perfil_logdt_emis']))); ?> <?php echo $user_perfil_log['user_perfil_loghr_emis']; ?></td>
                                                <td><?php echo $user_perfil_log['user_perfil_loguser_emis']; ?></td>
                                                <td><?php echo $user_perfil_log['user_perfil_logtitulo']; ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </section>
                        </div> <!-- /.tab-pane -->

                        <div class="tab-pane" id="estabelecimentos">
                            <section class="invoice p-3 mb-3">
                                <table id="table_estabs" class="display responsive nowrap" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>CNPJ Matriz</th>
                                            <th>CNPJ Estabelecimentos</th>
                                            <th>Código</th>
                                            <th>Nome de usuário</th>
                                            <th>Visão Global</th>
                                            <th>Visão Gerencial</th>
                                            <th>Visão Vendas</th>
                                            <th>Ativo</th>
                                            <th>Origem</th>
                                            <th>Recebe metas</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($data['estabelecimentos'] as $user_estab) : ?>
                                            <tr>

                                                <td>
                                                    <?php
                                                    if (strlen($user_estab['user_estabcnpj_matriz'] < 11)) {
                                                        echo $user_estab['user_estabcnpj_matriz'];
                                                    } else {
                                                        echo Mask('##.###.###/####-##', $user_estab['user_estabcnpj_matriz']);
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    if (strlen($user_estab['user_estabcnpj_estab'] < 11)) {
                                                        echo $user_estab['user_estabcnpj_estab'];
                                                    } else {
                                                        echo Mask('##.###.###/####-##', $user_estab['user_estabcnpj_estab']);
                                                    }
                                                    ?>
                                                </td>
                                                <td><?php echo $user_estab['user_estabcod_user']; ?></td>
                                                <td><?php echo $user_estab['user_perfilnome_user']; ?></td>
                                                <td><?php echo $user_estab['user_estabvisao_global'] == 0 ? "Sim" : "Não"; ?></td>
                                                <td><?php echo $user_estab['user_estabvisao_gerencial'] == 0 ? "Sim" : "Não"; ?></td>
                                                <td><?php echo $user_estab['user_estabvisao_vendas'] == 0 ? "Sim" : "Não"; ?></td>
                                                <td><?php echo $user_estab['user_estabind_ativo'] == 0 ? "Sim" : "Não"; ?></td>
                                                <td><?php echo $user_estab['user_estabind_origem'] == 0 ? "Portal" : "Integrador"; ?></td>
                                                <td><?php echo $user_estab['user_estabrecebe_metas'] == 0 ? "Sim" : "Não"; ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </section>
                        </div> <!-- /.tab-pane -->

                    </div> <!-- /.tab-content -->
                </div><!-- /.card-body -->
            </div> <!-- /.nav-tabs-custom -->
        </div> <!-- /.col -->
    </div> <!-- /.row -->
</section>
<!-- MÁSCARA CNPJ -->
<script>
    function formatarCampo(campoTexto) {
        campoTexto.value = mascaraCnpj(campoTexto.value);
    }

    function retirarFormatacao(campoTexto) {
        campoTexto.value = campoTexto.value.replace(/(\.|\/|\-)/g, "");
    }

    function mascaraCnpj(valor) {
        return valor.replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/g, "\$1.\$2.\$3\/\$4\-\$5");
    }
</script>
<script>
    $(document).ready(function() {

        $('#table_logs').DataTable({});

        $('#table_estabs').DataTable({
            scrollY: 300,
            "sScrollX": "100%",
            "bScrollCollapse": true,
            paging: false
        });

    });
</script>

<!-- MÁSCARA -->
<script>
    //CEP
    $(document).ready(function() {
        $('#cep').mask('00.000-000', {
            reverse: true
        });
    });

    //TELEFONE
    $(document).ready(function() {
        $('#telefone').mask('0000-0000', {
            reverse: true
        });
    });

    //TELEFONE
    $(document).ready(function() {
        $('#celular').mask('00 0000-00000', {
            reverse: true
        });
    });


    $("#user_perfilind_presidentec").click(function(event) {

        $("#user_perfilcod_presidente").val("");
        $("#user_perfilcod_diretor").val("");
        $("#user_perfilcod_gerente").val("");
        $("#user_perfilcod_coordenador").val("");

        var dire = $("#user_perfilind_diretorc");
        dire.prop("disabled", !dire.prop("disabled"));

        var gere = $("#user_perfilind_gerentec");
        gere.prop("disabled", !gere.prop("disabled"));

        var coo = $("#user_perfilind_coordenadorc");
        coo.prop("disabled", !coo.prop("disabled"));

        var cooi = $("#user_perfilcod_coordenador");
        cooi.prop("readonly", !cooi.prop("readonly"));

        var gerei = $("#user_perfilcod_gerente");
        gerei.prop("readonly", !gerei.prop("readonly"));

        var direi = $("#user_perfilcod_diretor");
        direi.prop("readonly", !direi.prop("readonly"));

        var prei = $("#user_perfilcod_presidente");
        prei.prop("readonly", !prei.prop("readonly"));
    });

    $("#user_perfilind_diretorc").click(function(event) {

        $("#user_perfilcod_presidente").val("");
        $("#user_perfilcod_diretor").val("");
        $("#user_perfilcod_gerente").val("");
        $("#user_perfilcod_coordenador").val("");

        var pre = $("#user_perfilind_presidentec");
        pre.prop("disabled", !pre.prop("disabled"));

        var gere = $("#user_perfilind_gerentec");
        gere.prop("disabled", !gere.prop("disabled"));

        var coo = $("#user_perfilind_coordenadorc");
        coo.prop("disabled", !coo.prop("disabled"));

        var cooi = $("#user_perfilcod_coordenador");
        cooi.prop("readonly", !cooi.prop("readonly"));

        var gerei = $("#user_perfilcod_gerente");
        gerei.prop("readonly", !gerei.prop("readonly"));

        var direi = $("#user_perfilcod_diretor");
        direi.prop("readonly", !direi.prop("readonly"));

    });

    $("#user_perfilind_coordenadorc").click(function(event) {

        $("#user_perfilcod_presidente").val("");
        $("#user_perfilcod_diretor").val("");
        $("#user_perfilcod_gerente").val("");

        $("#user_perfilcod_coordenador").val("");
        var cooi = $("#user_perfilcod_coordenador");
        cooi.prop("readonly", !cooi.prop("readonly"));

        var pre = $("#user_perfilind_presidentec");
        pre.prop("disabled", !pre.prop("disabled"));

        var gere = $("#user_perfilind_gerentec");
        gere.prop("disabled", !gere.prop("disabled"));

        var dire = $("#user_perfilind_diretorc");
        dire.prop("disabled", !dire.prop("disabled"));
    });

    $("#user_perfilind_gerentec").click(function(event) {

        $("#user_perfilcod_presidente").val("");
        $("#user_perfilcod_diretor").val("");
        $("#user_perfilcod_gerente").val("");
        $("#user_perfilcod_coordenador").val("");

        var pre = $("#user_perfilind_presidentec");
        pre.prop("disabled", !pre.prop("disabled"));

        var cooi = $("#user_perfilcod_coordenador");
        cooi.prop("readonly", !cooi.prop("readonly"));

        var gerei = $("#user_perfilcod_gerente");
        gerei.prop("readonly", !gerei.prop("readonly"));

        var pre = $("#user_perfilind_presidentec");
        pre.prop("disabled", !pre.prop("disabled"));

        var coo = $("#user_perfilind_coordenadorc");
        coo.prop("disabled", !coo.prop("disabled"));

        var dire = $("#user_perfilind_diretorc");
        dire.prop("disabled", !dire.prop("disabled"));
    });

    $(document).ready(function() {

        //cnpj matriz
        var matriz = $("#matriz").val();

        //código do presidente

        $.post('/ajax/call_autocomplete.php', {
            cnpj_estab: matriz,
            func: '9'
        }, function(data) {
            console.log(data);
            var cmb = '<option value=""></option>';
            $.each(data, function(index, value) {
                cmb = cmb + '<option value="' + value.user_perfilcod_user + ' - ' + value.user_perfilnome_completo + '"></option>';
            });
            $('#listpre').html(cmb);
        }, 'json');

        //código do diretor

        $.post('/ajax/call_autocomplete.php', {
            cnpj_estab: matriz,
            func: '10'
        }, function(data) {
            console.log(data);
            var cmb = '<option value=""></option>';
            $.each(data, function(index, value) {
                cmb = cmb + '<option value="' + value.user_perfilcod_user + ' - ' + value.user_perfilnome_completo + '"></option>';
            });
            $('#listdire').html(cmb);
        }, 'json');

        //código do gerente

        $.post('/ajax/call_autocomplete.php', {
            cnpj_estab: matriz,
            func: '11'
        }, function(data) {
            console.log(data);
            var cmb = '<option value=""></option>';
            $.each(data, function(index, value) {
                cmb = cmb + '<option value="' + value.user_perfilcod_user + ' - ' + value.user_perfilnome_completo + '"></option>';
            });
            $('#listgere').html(cmb);
        }, 'json');
    });
</script>