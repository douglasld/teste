<style>
    /* #example_filter input {
  border-radius: 5px;
} */
</style>

<!-- Content Header (Page header)  -->
<section class="content-header">

    <div class="container" style="background: pink;">
        <?php
        if (!empty($data['mensagem'])) :

            foreach ($data['mensagem'] as $m) :
                echo $m . "<br>";
            endforeach;
        endif;

        // var_dump ($_SESSION);


        ?>
    </div>

    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h4>Usuários</h4>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                     <li class="breadcrumb-item"><a style="color: indigo" href="#"></a></li> 
                     <li class="breadcrumb-item active">Index</li> 
                </ol>
            </div>
        </div>
    </div>
</section>

<!-- Main Content -->

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card col-sm-12">
                <div class="card-header">
                    <div class="btn-group">
                        <div> <a href="/user_perfil/cadastrar" type="button" class="btn btn-block btn-purple btn-sm">Adicionar Usuário</a> </div>
                        &nbsp
                        <div> <a href="/user_perfil/exportar" type="button" class="btn btn-block btn-orange btn-sm">Exportar</a> </div>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <table id="table" class="display responsive nowrap" style="width:100%">
                                <thead>
                                    <tr>
                                        <th style="width: 10%" class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">Atalhos</th> <!-- botões -->
                                        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Nome Completo</th>
                                        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Nome de usuário</th>
                                        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">CNPJ Matriz</th>
                                        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">E-mail</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($data['registros'] as $user_perfil) : ?>
                                        <tr>
                                            <td>
                                                <a title="Detalhar" href="/user_perfil/ver/<?php echo $user_perfil['user_perfilid']; ?>" type="button" class="btn btn-purple btn-sm"><i class="fas fa-eye"></i></a> <!-- visualizar -->
                                                <a title="Excluir" onclick="verificadel()" href="/user_perfil/excluir/<?php echo $user_perfil['user_perfilid']; ?>" type="button" class="btn btn-purple btn-sm"><i class="fas fa-trash"></i></a> <!-- excluir -->                                          
                                            </td>
                                            <td><?php echo $user_perfil['user_perfilnome_completo']; ?></td>
                                            <td><?php echo $user_perfil['user_perfilnome_user']; ?></td>
                                            <td> <?php
                                                        if (strlen($user_perfil['user_perfilcnpj_matriz'] <= 10)) {
                                                            echo $user_perfil['user_perfilcnpj_matriz'];
                                                        } else {
                                                            echo Mask('##.###.###/####-##', $user_perfil['user_perfilcnpj_matriz']);
                                                        }
                                                        ?></td>
                                            <td><?php echo $user_perfil['user_perfile_mail']; ?></td>                  
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col 12 -->
    </div>
    <!-- /.row -->
</section>
<script>
    $(document).ready(function() {

        $('#table').DataTable({
            scrollY: 300,
            paging: false
        });

    });

    function verificadel() {
        var x;
        var r = confirm("Deseja excluir a linha?");
        if (r == true) {
        } else {
            event.preventDefault();
        }
    }
</script>