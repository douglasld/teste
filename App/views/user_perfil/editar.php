<!-- Content Header (Page header)  -->
<section class="content-header">

<div class="container" style="background: lavender;">
        <?php
        if (!empty($data['mensagem'])) :

            foreach ($data['mensagem'] as $m) :
                echo $m . "<br>";
            endforeach;
        endif;

        // O debug colocado aqui aparecerá no header da página no container lavender
        // echo json_encode($data);

        ?>
    </div>

    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Editar usuário</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a style="color: indigo" href="#">Usuários</a></li>
                    <li class="breadcrumb-item active">Home</li>
                </ol>
            </div>
        </div>
    </div>

</section>
<section class="content">
        <div class="row">

            <!-- left column -->
            <div class="col-md-6">
                <div class="card card-default">
                    <div class="card-header">
                        <h3 class="card-title">Acesso </h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <form action="/user_perfil/editar/<?php echo $data['registros']['user_perfilid']; ?>" method="post" role="form">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Nome de usuário</label>
                                        <input name="nome_user" value="<?php echo $data['registros']['user_perfilnome_user']; ?>" class="form-control" type="text" required="required" name="text"  />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Código de usuário</label>
                                        <input name="cod_user" value="<?php echo $data['registros']['user_perfilcod_user']; ?>" class="form-control" placeholder="123..." type="text" required="required" name="numbers" pattern="[0-9]+$" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>CNPJ Matriz</label>
                                        <?php
                                        if (strlen($data['registros']['user_perfilcnpj_matriz'] < 11)) {
                                            $cnpj_matriz = $data['registros']['user_perfilcnpj_matriz'];
                                        } else {
                                            $cnpj_matriz = Mask('##.###.###/####-##', $data['registros']['user_perfilcnpj_matriz']);
                                        }
                                        ?>
                                        <input name="cnpj_matriz" data-inputmask="'mask' : '99.999.999/9999-99'" value="<?php echo $cnpj_matriz ?>" type=" text" class="form-control" placeholder="123.456.789/10-1 ..." required="required">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Validade em dias</label>
                                        <input name="dias_validade" value="<?php echo $data['registros']['user_perfildias_validade']; ?>" type="text" class="form-control" placeholder="9999..." required="required">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">E-mail </label>
                                <input name="e_mail" value="<?php echo $data['registros']['user_perfile_mail']; ?>" class="form-control" placeholder="exemplo@resolute.com.br" type="email" required="required" class="input-text" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" />
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Senha</label>
                                <input name="senha" type="password" class="form-control" id="exampleInputPassword1" placeholder="Senha" required="required">
                                <!-- FIXME: arrumar envio de formulario com foto e senha juntos -->
                            </div>
                            <div class="form-group">
                                <label for="exampleInputFile">Foto </label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="exampleInputFile">
                                        <label class="custom-file-label" for="exampleInputFile">Escolher arquivo</label>
                                    </div>
                                    <div class="input-group-append">
                                        <button name="AtualizarFoto" class="btn btn-default pull-left">Enviar</button>
                                        <!-- FIXME: arrumar envio de formulario com foto e senha juntos -->
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                    </br>
                                <div class="container">
                                    <div class="form-check">
                                        <input name="ind_ativo" value="<?php echo $data['registros']['user_perfilind_ativo']; ?>" type="checkbox" class="form-check-input" id="exampleCheck1" checked>
                                        <label class="form-check-label" for="exampleCheck1">Ativo</label>
                                    </div>
                                    <div class="form-check">
                                        <input name="adm" id="adm" value="<?php echo $data['registros']['user_perfiladm']; ?>" type="checkbox" class="form-check-input" id="exampleCheck1">
                                        <label class="form-check-label" for="exampleCheck1">Administrador</label>
                                    </div>

                                    <div class="form-check">
                                        <input name="gestor" value="<?php echo $data['registros']['user_perfilgestor']; ?>" type="checkbox" class="form-check-input" id="exampleCheck1">
                                        <label class="form-check-label" for="exampleCheck1">Gestor</label>
                                    </div>
                                    
                                    <div class="form-check">
                                        <input name="ind_origem" value="<?php echo $data['registros']['user_perfilind_origem']; ?>" type="checkbox" class="form-check-input" id="exampleCheck1">
                                        <label class="form-check-label" for="exampleCheck1">Origem</label>
                                    </div>
                                </div>
                            </div>

                    </div>
                </div>
            </div>
            <!-- meio -->

            <!-- right column -->
            <div class="col-md-6">
                <div class="card card-default">
                    <div class="card-header">
                        <h3 class="card-title">Pessoal</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">

                        <div class="form-group">
                            <label for="exampleInputEmail1">Nome Completo </label>
                            <input name="nome_completo" type="text" value="<?php echo $data['registros']['user_perfilnome_completo']; ?>" class="form-control" id="exampleInputEmail1" placeholder="Enter email" required="required">
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>País</label>
                                    <input name="pais" value="<?php echo $data['registros']['user_perfilpais']; ?>" type="text" class="form-control" placeholder="Brasil">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Estado</label>
                                    <input name="estado" value="<?php echo $data['registros']['user_perfilestado']; ?>" type="text" class="form-control" placeholder="Santa Catarina">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Cidade</label>
                                    <input name="cidade" value="<?php echo $data['registros']['user_perfilcidade']; ?>" type="text" class="form-control" placeholder="Joinville">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>CEP</label>
                                    <input name="cep" data-inputmask="'mask' : '99.999-999'" value="<?php echo $data['registros']['user_perfilcep']; ?>" type="text" class="form-control" placeholder="89211-468">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Bairro</label>
                                    <input name="bairro" value="<?php echo $data['registros']['user_perfilbairro']; ?>" type="text" class="form-control" placeholder="Floresta ...">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Rua</label>
                                    <input name="rua" value="<?php echo $data['registros']['user_perfilrua']; ?>" type="text" class="form-control" placeholder="Floresta ...">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Número</label>
                                    <input name="numero" value="<?php echo $data['registros']['user_perfilnumero']; ?>" type="tel" class="form-control" placeholder="539...">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Complemento</label>
                                    <input name="complemento" value="<?php echo $data['registros']['user_perfilcomplemento']; ?>" type="text" class="form-control" placeholder="Complemento...">
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Telefone</label>
                                    <input name="telefone" data-inputmask="'mask' : '(99)9999-9999'" value="<?php echo $data['registros']['user_perfiltelefone']; ?>" type="tel" class="form-control" placeholder="(47) 3025-0000 ">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Celular</label>
                                    <input name="celular" data-inputmask="'mask' : '(99)9999-9999'" value="<?php echo $data['registros']['user_perfilcelular']; ?>" type="tel" class="form-control" placeholder="(47) 99700-0000 ">
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        </br>
                        <input type="submit" name="Atualizar" class="btn btn-primary float-right" value="Atualizar" style="background-color: indigo; border-color: indigo;">
                        <a href="/user_perfil/index" class="btn btn-default float-right">Cancelar</a>
                    </div>
                    </form>
                </div>
            </div>

        </div>
        <!-- /.row -->
</section>
