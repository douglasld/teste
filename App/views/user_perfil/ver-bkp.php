<div class="x_panel">
    <div class="x_title">
        <h2>Usuários</h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li><a href="/user_perfil/index" class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>

    <div class="x_content">

        <div class="col-md-3 col-sm-3  profile_left">
            <div class="thumbnail">
                <div>
                    <!-- Current avatar -->
                    <img src="<?php echo URL_BASE . "/images/" . $data['user_perfilfoto']; ?>"/>
                </div>
            </div>
            <h3><?php echo $data['user_perfilcod_user']; ?></h3>
            <h2><?php echo $data['user_perfilnome_user']; ?></h2>
            <p><b>Ativo: </b><?php echo $data['user_perfilind_ativo'] == 0 ? "Sim" : "Não"; ?></p>
            <p><b>Origem: </b><?php echo $data['user_perfilind_origem'] == 0 ? "Portal" : "Integrador"; ?></p>
            <p><b>Adm: </b><?php echo $data['user_perfiladm'] == 0 ? "Sim" : "Não"; ?></p>
            <p><b>Gestor: </b><?php echo $data['user_perfilgestor'] == 0 ? "Sim" : "Não"; ?></p>
            <p><b>Dias de validade: </b><?php echo $data['user_perfildias_validade']; ?></p>
            <p><b>CNPJ Matriz</b><?php echo $data['user_perfilcnpj_matriz']; ?></p>
        </div>

        <div class="col-md-9 col-sm-9">

            <div class="profile_title">
                <div class="col-md-6">
                    <h2>Nome: </h2>
                </div>
                <div class="col-md-6">
                    <h2><?php echo $data['user_perfilnome_user']; ?></h2>
                </div>
            </div>

            <div class="profile_title">
                <div class="col-md-6">
                    <h2>Nome: </h2>
                </div>
                <div class="col-md-6">
                    <h2><?php echo $data['user_perfilnome_completo']; ?></h2>
                </div>
            </div>

            <div class="profile_details">

                <div class="col-md-6">
                    <h3>Contatos:</h3>
                    <p><b>Telefone: </b><?php echo $data['user_perfiltelefone']; ?></p>
                    <p><b>Celular: </b><?php echo $data['user_perfilcelular']; ?></p>
                    <p><b>e-mail: </b><?php echo $data['user_perfile_mail']; ?></p>
                </div>

                <div class="col-md-6">
                    <h3>Endereço:</h3>
                    <p><b>Rua: </b><?php echo $data['user_perfilrua']; ?></p>
                    <p><b>Número: </b><?php echo $data['user_perfilnumero']; ?></p>
                    <p><b>Complemento: </b><?php echo $data['user_perfilcomplemento']; ?></p>
                    <p><b>Cidade: </b><?php echo $data['user_perfilcidade']; ?></p>
                    <p><b>Estado: </b><?php echo $data['user_perfilestado']; ?></p>
                    <p><b>Pais: </b><?php echo $data['user_perfilpais']; ?></p>
                    <p><b>CEP: </b><?php echo Mask('#####-###', $data['user_perfilcep']); ?></p>
                </div>

            </div>

            <div class="profile_details">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Data Cadastro</th>
                            <th>Hora Cadastro</th>
                            <th>Data Alteração</th>
                            <th>Hora Alteração</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><?php echo implode("/", array_reverse(explode("-", $data['user_perfildt_criacao']))); ?></td>
                            <td><?php echo $data['user_perfilhr_criacao']; ?></td>
                            <td><?php echo implode("/", array_reverse(explode("-", $data['user_perfildt_altera']))); ?></td>
                            <td><?php echo $data['user_perfilhr_altera']; ?></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

