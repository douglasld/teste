<div class="x_panel">
    <div class="x_title">
        <h2>Produtos [Logs]</h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <?php
        if (!empty($data['mensagem'])):

            foreach ($data['mensagem'] as $m):
                echo $m . "<br>";
            endforeach;

        endif;
        ?>

        <div class="table-responsive">
            <table id="datatable" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>Estabelecimento</th>
                    <th>Produto</th>
                    <th>Data</th>
                    <th>Hora</th>
                    <th>Usuário</th>
                    <th>titulo</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($data['registros'] as $produto_log): ?>
                    <tr>
                        <td>
                            <?php
                            if(strlen($produto_log['produto_logcnpj_estab'] < 11)) {
                                echo $produto_log['produto_logcnpj_estab'];
                            }else{
                                echo Mask('##.###.###/####-##',$produto_log['produto_logcnpj_estab']);
                            }
                            ?>
                        </td>
                        <td><?php echo $produto_log['produto_logcod_produto']; ?></td>
                        <td><?php echo implode("/", array_reverse(explode("-",$produto_log['produto_logdt_emis']))); ?></td>
                        <td><?php echo $produto_log['produto_loghr_emis']; ?></td>
                        <td><?php echo $produto_log['user_perfilnome_user']; ?></td>
                        <td><?php echo $produto_log['produto_logtitulo']; ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <div class="row">
            <a href="/produtos/index" class="btn btn-success">Voltar</a>
        </div>
    </div>
</div>




