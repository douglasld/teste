<div class="x_panel">
    <div class="x_title">
        <h2>Usuários [Estabelecimentos vinculados]</h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <?php
        if (!empty($data['mensagem'])):

            foreach ($data['mensagem'] as $m):
                echo $m . "<br>";
            endforeach;

        endif;
        ?>
        <?php
        $url_atual = "$_SERVER[REQUEST_URI]";
        $urlx = explode("/", $url_atual);
        ?>

        <div class="row">
            <div class="col-md-3">
                <a href="/userestabs/cadastrar/<?php echo $urlx[3]; ?>" class="btn btn-success"><i
                            class="fa fa-plus-square"></i></a>
            </div>
        </div>

        <div class="table-responsive">
            <table id="datatable" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>Editar</th>
                    <th>Excluir</th>
                    <th>Visualizar</th>
                    <th>cnpj_matriz</th>
                    <th>cnpj_estab</th>
                    <th>cod_user</th>
                    <th>nome_user</th>
                    <th>visao_global</th>
                    <th>visao_gerencial</th>
                    <th>visao_vendas</th>
                    <th>ind_ativo</th>
                    <th>ind_origem</th>
                    <th>recebe_metas</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($data['registros'] as $user_estab): ?>
                    <tr>
                        <td><a href="/userestabs/editar/<?php echo $user_estab['user_estabid']; ?>" class="btn btn-primary"><i class="fa fa-edit"></i></a></td>
                        <td><a href="/userestabs/excluir/<?php echo $user_estab['user_estabid']; ?>" class="btn btn-danger"><i class="fa fa-close"></i></a>
                        </td>
                        <td><a href="/userestabs/ver/<?php echo $user_estab['user_estabid']; ?>" class="btn btn-success">
                                <i class="fa fa-search"></i>
                            </a>
                        </td>
                        <td>
                            <?php
                            if(strlen($user_estab['user_estabcnpj_matriz'] < 11)) {
                                echo $user_estab['user_estabcnpj_matriz'];
                            }else{
                                echo Mask('##.###.###/####-##',$user_estab['user_estabcnpj_matriz']);
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if(strlen($user_estab['user_estabcnpj_estab'] < 11)) {
                                echo $user_estab['user_estabcnpj_estab'];
                            }else{
                                echo Mask('##.###.###/####-##',$user_estab['user_estabcnpj_estab']);
                            }
                            ?>
                        </td>
                        <td><?php echo $user_estab['user_estabcod_user']; ?></td>
                        <td><?php echo $user_estab['user_perfilnome_user']; ?></td>
                        <td><?php echo $user_estab['user_estabvisao_global'] == 0 ? "Sim" : "Não"; ?></td>
                        <td><?php echo $user_estab['user_estabvisao_gerencial'] == 0 ? "Sim" : "Não"; ?></td>
                        <td><?php echo $user_estab['user_estabvisao_vendas'] == 0 ? "Sim" : "Não"; ?></td>
                        <td><?php echo $user_estab['user_estabind_ativo'] == 0 ? "Sim" : "Não"; ?></td>
                        <td><?php echo $user_estab['user_estabind_origem'] == 0 ? "Portal" : "Integrador"; ?></td>
                        <td><?php echo $user_estab['user_estabrecebe_metas'] == 0 ? "Sim" : "Não"; ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>




