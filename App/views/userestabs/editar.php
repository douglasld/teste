<div class="x_panel">
    <div class="x_title">
        <h2>Usuários Estabelecimento</h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">

        <?php
        if (!empty($data['mensagem'])):

            foreach ($data['mensagem'] as $m):
                echo $m . "<br>";
            endforeach;

        endif;
        ?>

        <form action="/userestabs/editar/<?php echo $data['registros']['user_estabid']; ?>" method="post"
              class="form-horizontal form-label-left">

            <div class="box-body">

                <div class="form-group row">
                    <label class="control-label col-md-3" for="cidade">visao_global:</label>
                    <div class="col-md-7">
                        <select name="visao_global" id="visao_global" class="form-control col-md-7">
                            <option value="<?php echo $data['registros']['user_estabvisao_global']; ?>">
                                <?php echo $data['registros']['user_estabvisao_global'] == 0 ? "Sim" : "Não"; ?>
                            <option value="1">Não</option>
                            <option value="0">Sim</option>
                            </option>
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="control-label col-md-3" for="cidade">visao_gerencial:</label>
                    <div class="col-md-7">
                        <select name="visao_gerencial" id="visao_gerencial" class="form-control col-md-7">
                            <option value="<?php echo $data['registros']['user_estabvisao_gerencial']; ?>">
                                <?php echo $data['registros']['user_estabvisao_gerencial'] == 0 ? "Sim" : "Não"; ?>
                            </option>
                            <option value="1">Não</option>
                            <option value="0">Sim</option>
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="control-label col-md-3" for="cidade">visao_vendas:</label>
                    <div class="col-md-7">
                        <select name="visao_vendas" id="visao_vendas" class="form-control col-md-7">
                            <option value="<?php echo $data['registros']['user_estabvisao_vendas']; ?>">
                                <?php echo $data['registros']['user_estabvisao_vendas'] == 0 ? "Sim" : "Não"; ?>
                            </option>
                            <option value="1">Não</option>
                            <option value="0">Sim</option>
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="control-label col-md-3" for="cidade">ind_ativo:</label>
                    <div class="col-md-7">
                        <select name="ind_ativo" id="ind_ativo" class="form-control col-md-7">
                            <option value="<?php echo $data['registros']['user_estabind_ativo']; ?>">
                                <?php echo $data['registros']['user_estabind_ativo'] == 0 ? "Sim" : "Não"; ?>
                            </option>
                            <option value="1">Não</option>
                            <option value="0">Sim</option>
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="control-label col-md-3" for="cidade">ind_origem:</label>
                    <div class="col-md-7">
                        <select name="ind_origem" id="ind_origem" class="form-control col-md-7">
                            <option value="<?php echo $data['registros']['user_estabind_origem']; ?>">
                                <?php echo $data['registros']['user_estabind_origem'] == 0 ? "Portal" : "Integrador"; ?>
                            </option>
                            <option value="1">Integrador</option>
                            <option value="0">Portal</option>
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="control-label col-md-3" for="cidade">recebe_metas:</label>
                    <div class="col-md-7">
                        <select name="recebe_metas" id="recebe_metas" class="form-control col-md-7">
                            <option value="<?php echo $data['registros']['user_estabrecebe_metas']; ?>">
                                <?php echo $data['registros']['user_estabrecebe_metas'] == 0 ? "Sim" : "Não"; ?>
                            </option>
                            <option value="1">Não</option>
                            <option value="0">Sim</option>
                        </select>
                    </div>
                </div>

            </div>

            <!-- /.box-body -->
            <div class="box-footer">
                <button name="Atualizar" class="btn btn-success pull-left">Atualizar</button>
                <a href="/userestabs/usuario/<?php echo $data['registros']['user_estabid']; ?>" class="btn btn-danger pull-right">Cancelar</a>
            </div>

        </form>
    </div>
</div>





