<div class="x_panel">
    <div class="x_title">
        <h2>Usuários Estabelecimento</h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <div class="">
            <ul class="to_do">
                <li>
                    <p><b>CNPJ Matriz: </b>
                        <?php
                        if(strlen($data['user_estabcnpj_matriz'] < 11)) {
                            echo $data['user_estabcnpj_matriz'];
                        }else{
                            echo Mask('##.###.###/####-##',data['user_estabcnpj_matriz']);
                        }
                        ?>
                    </p>
                </li>
                <li><p><b>CNPJ Establecimento: </b>
                        <?php
                        if(strlen($data['user_estabcnpj_estab'] < 11)) {
                            echo $data['user_estabcnpj_estab'];
                        }else{
                            echo Mask('##.###.###/####-##',data['user_estabcnpj_estab']);
                        }
                        ?>
                    </p>
                </li>
                <li><p><b>Código Usuário: </b><?php echo $data['user_estabcod_user']; ?></p></li>
                <li><p><b>Nome Usuário: </b><?php echo $data['user_perfilnome_user']; ?></p></li>
                <li><p><b>Visão Global: </b><?php echo $data['user_estabvisao_global'] == 0 ? "Sim" : "Não"; ?></p></li>
                <li><p><b>Visão Gerencial: </b><?php echo $data['user_estabvisao_gerencial'] == 0 ? "Sim" : "Não"; ?></p></li>
                <li><p><b>Visão Vendas: </b><?php echo $data['user_estabvisao_vendas'] == 0 ? "Sim" : "Não"; ?></p></li>
                <li><p><b>Ativo: </b><?php echo $data['user_estabind_ativo'] == 0 ? "Sim" : "Não"; ?></p></li>
                <li><p><b>Origem: </b><?php echo $data['user_estabind_origem'] == 0 ? "Portal" : "Integrador"; ?></p></li>
                <li><p><b>Metas: </b><?php echo $data['user_estabrecebe_metas'] == 0 ? "Sim" : "Não"; ?></p></li>
                <li><p><b>Usuário Criação: </b><?php echo $data['user_estabuser_criacao']; ?></p></li>
                <li><p><b>Data Criação: </b><?php echo implode("/", array_reverse(explode("-",$data['user_estabdt_criacao']))); ?></p></li>
                <li><p><b>Hora Criação: </b><?php echo $data['user_estabhr_criacao']; ?></p></li>
                <li><p><b>Data Alteração: </b><?php echo implode("/", array_reverse(explode("-",$data['user_estabdt_altera']))); ?></p></li>
                <li><p><b>Hora Alteração: </b><?php echo $data['user_estabhr_altera']; ?></p></li>
                <li><p><b>Usuário Alteração: </b><?php echo $data['user_estabuser_altera']; ?></p></li>
            </ul>
        </div>

        <!-- /.box-body -->
        <div class="box-footer">
            <a href="/userestabs/usuario/<?php echo $data['user_estabid']; ?>" class="btn btn-success pull-right">Voltar</a>
        </div>

    </div>
</div>
