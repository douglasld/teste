<div class="x_panel">
    <div class="x_title">
        <h2>Usuários Estabelecimento</h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">

        <?php
        if (!empty($data['mensagem'])):

            foreach ($data['mensagem'] as $m):
                echo $m . "<br>";
            endforeach;

        endif;
        ?>


        <form action="/userestabs/cadastrar/<?php echo $data['registros']['user_perfilcod_user']; ?>" method="post"
              class="form-horizontal form-label-left">
            <div class="box-body">

                <div class="form-group row">
                    <label class="control-label col-md-3" for="cidade">cnpj_matriz:</label>
                    <div class="col-md-7">
                    <input type="text" name="cnpj_matriz"  class="form-control col-md-7" data-inputmask="'mask' : '99.999.999/9999-99'" required value="<?php echo $_SESSION['matriz']?>">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="control-label col-md-3" for="cidade">cnpj_estab:</label>
                    <div class="col-md-7">
                    <select name="cnpj_estab" id="cnpj_estab" class="form-control col-md-7" required>
                        <?php $estab = $this->model('Empresa');?>
                        <option value="">Nenhum</option>
                         <?php foreach ($estab->getPorMatriz($_SESSION['matriz']) as $value):?>
                        <option value="<?php echo $value['empresacnpj_estab']?>">
                            <?php echo $value['empresacnpj_estab']?>
                        </option>
                        <?php endforeach; ?>
                    </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="control-label col-md-3" for="cidade">visao_global:</label>
                    <div class="col-md-7">
                    <select name="visao_global" id="visao_global" class="form-control col-md-7">
                        <option value="1">Não</option>
                        <option value="0">Sim</option>
                    </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="control-label col-md-3" for="cidade">visao_gerencial:</label>
                    <div class="col-md-7">
                    <select name="visao_gerencial" id="visao_gerencial" class="form-control col-md-7">
                        <option value="1">Não</option>
                        <option value="0">Sim</option>
                    </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="control-label col-md-3" for="cidade">visao_vendas:</label>
                    <div class="col-md-7">
                    <select name="visao_vendas" id="visao_vendas" class="form-control col-md-7">
                        <option value="1">Não</option>
                        <option value="0">Sim</option>
                    </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="control-label col-md-3" for="cidade">ind_ativo:</label>
                    <div class="col-md-7">
                    <select name="ind_ativo" id="ind_ativo" class="form-control col-md-7">
                        <option value="1">Não</option>
                        <option value="0">Sim</option>
                    </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="control-label col-md-3" for="cidade">ind_origem:</label>
                    <div class="col-md-7">
                    <select name="ind_origem" id="ind_origem" class="form-control col-md-7">
                        <option value="0">Portal</option>
                        <option value="1">Integrador</option>
                    </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="control-label col-md-3" for="cidade">recebe_metas:</label>
                    <div class="col-md-7">
                    <select name="recebe_metas" id="recebe_metas" class="form-control col-md-7">
                        <option value="1">Não</option>
                        <option value="0">Sim</option>
                    </select>
                    </div>
                </div>

            </div>

            <!-- /.box-body -->
            <div class="box-footer">
                <button name="Cadastrar" class="btn btn-success pull-left">Cadastrar</button>
                <a class="btn btn-danger pull-right" href="/userestabs/index">Cancelar</a>
            </div>

        </form>

    </div>
</div>




