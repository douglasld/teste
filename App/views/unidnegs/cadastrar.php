<section class="content-header">
    <div class="container" style="background: lavender;">
        <?php
        if (!empty($data['mensagem'])) :
            foreach ($data['mensagem'] as $m) :
                echo $m . "<br>";
            endforeach;
        endif;
        $estabelecimentos = $this->model('UserEstab');
        $estab_empresa = $this->model('Empresa');

        ?>
    </div>
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h4>Cadastro de Unidades de Negócios</h4>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a style="color: indigo" href="#">Index</a></li>
                    <li class="breadcrumb-item active">Cadastro</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<div class="countainer-fluid">
    <form action="/unidnegs/cadastrar/" method="post" class="form-horizontal form-label-left">
        <div class="col-md-7">
            <div class="card card-default">
                <div class="card-header">
                    <a href="/unidnegs/index" class="btn btn-default btn-md float-left" style="margin-left:5px;">Listar</a>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="form-group">
                                <label>CNPJ Estabelecimento</label>
                                <input class="form-control col-md-12" name="cnpj_estab" id="cnpj_estab" list="list" autocomplete="off" required="">
                                <datalist id="list">
                                    <?php foreach ($estabelecimentos->get_auto_cnpj($_SESSION['matriz'], $_SESSION['userId']) as $value) : ?>
                                        <?php foreach ($estab_empresa->getPorEstab($value) as $value) : ?>
                                            <option value="<?php echo preg_replace("/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/", "\$1.\$2.\$3/\$4-\$5", $value['empresacnpj_estab']) . ' - ' . $value['empresanome_abrev']; ?>"></option>
                                        <?php endforeach; ?>
                                    <?php endforeach; ?>
                                </datalist>
                                </input>
                            </div>
                        </div>
                        <div class="col-sm-7">
                            <div class="form-group">
                                <label> Descrição</label>
                                <input name="unidnegdescricao" type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="custom-control custom-switch">
                                <input type="checkbox" checked class="custom-control-input" id="ind_ativo" name="ind_ativo">
                                <label class="custom-control-label" for="ind_ativo">Unidade Ativa</label>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    </br>
                    <input type="submit" name="Cadastrar" class="btn btn-primary float-right" value="Salvar" style="background-color: indigo; border-color: indigo; margin:6px;">
                    <a href="/unidnegs/index" class="btn btn-orange btn-md float-right" style="margin:6px;">Cancelar</a>
                </div>
    </form>
</div>
<script>
    function formatarCampo(campoTexto) {
        campoTexto.value = mascaraCnpj(campoTexto.value);
    }

    function retirarFormatacao(campoTexto) {
        campoTexto.value = campoTexto.value.replace(/(\.|\/|\-)/g, "");
    }

    function mascaraCnpj(valor) {
        return valor.replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/g, "\$1.\$2.\$3\/\$4\-\$5");
    }
</script>
</div>
</div>