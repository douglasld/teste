<!-- Content Header (Page header)  -->
<section class="content-header">

    <div class="container" style="background: pink;">
        <?php
        if (!empty($data['mensagem'])) :

            foreach ($data['mensagem'] as $m) :
                echo $m . "<br>";
            endforeach;
        endif;
        // var_dump ($_SESSION);
        ?>
    </div>
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h4>Unidade de Negócio</h4>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a style="color: indigo" href="#">Index</a></li>
                    <li class="breadcrumb-item active"></li>
                </ol>
            </div>
        </div>
    </div>
</section>
<!-- Main Content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="btn-group">
                        <div> <a href="/unidnegs/cadastrar" type="button" class="btn btn-block btn-purple btn-sm">Adicionar Unid. Neg.</a> </div>
                        &nbsp
                        <div> <a href="/unidnegs/exportar" type="button" class="btn btn-block btn-orange btn-sm">Exportar</a> </div>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <table id="table" class="display responsive nowrap" style="width:100%">
                                <thead>
                                    <tr>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">Atalhos</th> <!-- botões -->
                                        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Código Unid Negócio</th>
                                        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Estabelecimento</th>
                                        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Descrição</th>
                                        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Ativo</th>
                                </thead>
                                <!-- tbody -->
                                <tbody>
                                    <?php $res_unid_negEmpresa = $this->model('UnidNeg'); ?>
                                    <?php foreach ($data['registros'] as $empresas) : ?>
                                        <tr>
                                            <td>
                                                <a title="Detalhar" href="/unidnegs/ver/<?php echo $empresas['unid_negid']; ?>" type="button" class="btn btn-purple btn-sm"><i class="fas fa-eye"></i></a> <!-- visualizar -->
                                                <a title="Excluir" onclick="verificadel()" href="/unidnegs/excluir/<?php echo $empresas['unid_negid']; ?>" type="button" class="btn btn-purple btn-sm"><i class="fas fa-trash"></i></a> <!-- excluir -->
                                            </td>
                                            <td><?php echo $empresas['unid_negid']; ?></td>
                                            <td>
                                                <?php
                                                if (strlen($empresas['unid_negcnpj_estab'] <= 10)) {
                                                    echo $empresas['unid_negcnpj_estab'];
                                                } else {
                                                    echo Mask('##.###.###/####-##', $empresas['unid_negcnpj_estab']);
                                                }
                                                ?>
                                            </td>
                                            <td><?php echo $empresas['unid_negdescricao']; ?></td>
                                            <td><?php echo $empresas['unid_negind_ativo'] == 0 ? "Sim" : "Não"; ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
</section>
<script>
    $(document).ready(function() {

        $('#table').DataTable({
            scrollY: 300,
            paging: false
        });

    });
    function verificadel() {
        var x;
        var r = confirm("Deseja excluir a linha?");
        if (r == true) {
        } else {
            event.preventDefault();
        }
    }
</script>