<div class="x_panel">
    <div class="x_title">
        <h2>Grupos [Logs]</h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <?php
        if (!empty($data['mensagem'])):

            foreach ($data['mensagem'] as $m):
                echo $m . "<br>";
            endforeach;

        endif;
        ?>

        <div class="table-responsive">
            <table id="datatable" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>cnpj_estab</th>
                    <th>cod_grupo_prod</th>
                    <th>dt_emis</th>
                    <th>hr_emis</th>
                    <th>user_emis</th>
                    <th>usuário</th>
                    <th>titulo</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($data['registros'] as $grupo_prod_log): ?>
                    <tr>
                        <td><?php echo $grupo_prod_log['grupo_prod_logcnpj_estab']; ?></td>
                        <td><?php echo $grupo_prod_log['grupo_prod_logcod_grupo_prod']; ?></td>
                        <td><?php echo implode("/", array_reverse(explode("-",$grupo_prod_log['grupo_prod_logdt_emis']))); ?></td>
                        <td><?php echo $grupo_prod_log['grupo_prod_loghr_emis']; ?></td>
                        <td><?php echo $grupo_prod_log['grupo_prod_loguser_emis']; ?></td>
                        <td><?php echo $grupo_prod_log['user_perfilnome_user']; ?></td>
                        <td><?php echo $grupo_prod_log['grupo_prod_logtitulo']; ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <div class="row">
            <a href="/gruposprodutos/" class="btn btn-success">Voltar</a>
        </div>
    </div>
</div>



