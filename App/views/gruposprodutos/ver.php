<style>
    .nav-tabs .nav-item .nav-link {
        color: #6039A8 !important;
    }
</style>
<!-- Content Header (Page header)  -->
<section class="content-header">
    <div class="container" style="background: pink;">
        <?php
        if (!empty($data['mensagem'])) :

            foreach ($data['mensagem'] as $m) :
                echo $m . "<br>";
            endforeach;
        endif;
        ?>
    </div>
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h4>Detalhes Grupo produtos</h4>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="/empresas/index" style="color:indigo">Index</a></li>
                    <li class="breadcrumb-item active">Detalhamento</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="row">
        <div class="col-md-8">
            <div class="card card-purple card-outline card-outline-tabs">
                <div class="card-header p-0 pt-1 border-bottom-0">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item"><a class="nav-link active" href="#editar" data-toggle="tab">Editar</a></li>
                        <!-- <li class="nav-item"><a class="nav-link " href="#resumo" data-toggle="tab">Resumo</a></li> -->
                    </ul>
                </div><!-- /.card-header -->
                <div class="card-body">
                    <div class="tab-content">
                        <div class="active tab-pane" id="editar">
                            <section class="content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="invoice p-3 mb-3">
                                            <div class="card-body">
                                                <form action="/gruposprodutos/editar/<?php echo $data['grupo_prodid']; ?>" method="post" class="form-horizontal form-label-left">
                                                    <div class="row">

                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <label>CNPJ Estabelecimento</label>
                                                                <input name="cnpj_estab" id="cnpj_estab" value="<?php echo $data['grupo_prodcnpj_estab']; ?>" onfocus="javascript: retirarFormatacao(this);" onblur="javascript: formatarCampo(this);" maxlength="14" type="text" class="form-control" placeholder="00.000.000/000-00" required="required">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <label> Código Produto</label>
                                                                <input name="cod_grupo_prod" id="cod_grupo_prod" value="<?php echo $data['grupo_prodcod_grupo_prod']; ?>" type="number" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <label> Descrição</label>
                                                                <input name="descricao" id="descricao" value="<?php echo $data['grupo_proddescricao']; ?>" type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-6">
                                                        <div class="custom-control custom-switch">
                                                            <div class="custom-control custom-switch">

                                                                <input type="checkbox" name="ind_ativo" <?php if($data['grupo_prodind_ativo'] == 0){echo"checked";}?> class="custom-control-input" id="ind_ativo">
                                                                <label class="custom-control-label"   for="ind_ativo">Grupo Produto Ativo</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                            <div class="card-footer">
                                                <input type="submit" name="Atualizar" class="btn btn-purple btn-md float-right" value="Salvar">
                                                <a href="/empresas/index" class="btn btn-orange btn-md float-right" style="margin-right:4px;">Cancelar</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div> <!-- /.tab-pane // fecha editar-->
                    </div> <!-- /.tab-content -->
                </div><!-- /.card-body -->
            </div> <!-- /.nav-tabs-custom -->
        </div> <!-- /.col -->
    </div> <!-- /.row -->
</section>
<script>
    function formatarCampo(campoTexto) {
        campoTexto.value = mascaraCnpj(campoTexto.value);

    }

    function retirarFormatacao(campoTexto) {
        campoTexto.value = campoTexto.value.replace(/(\.|\/|\-)/g, "");

    }

    function mascaraCnpj(valor) {
        return valor.replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/g, "\$1.\$2.\$3\/\$4\-\$5");
    }
</script>

<script>
    $(document).ready(function() {

        $('#table_logs').DataTable({});

        $('#table_unid_neg').DataTable({
            scrollY: 300,
            "sScrollX": "100%",
            "bScrollCollapse": true,
            paging: false
        });

    });
</script>