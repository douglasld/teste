        <?php
        if (!empty($data['mensagem'])):

            foreach ($data['mensagem'] as $m):
                echo $m . "<br>";
            endforeach;

        endif;
        ?>


        <form action="/gruposprodutos/copiar/<?php echo $data['registros']['grupo_prodid']; ?>" method="post">
            <?php $dadosGrupo = $this->model('GrupoProd');?>

            <h3>Grupos [Copiar]</h3>
            <div class="box-body">

                <div class="form-group row">
                    <label class="control-label col-md-3" for="cidade">CNPJ: *</label>
                    <div class="col-md-7">
                        <input type="text" readonly="readonly" name="cnpj_estab" data-inputmask="'mask' : '99.999.999/9999-99'" value="<?php echo $data['registros']['grupo_prodcnpj_estab']; ?>" class="form-control col-md-7">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="control-label col-md-3" for="cidade">Código Grupo: *</label>
                    <div class="col-md-7">
                        <input type="text" name="cod_grupo_prod"
                         value="<?php echo $dadosGrupo->contaCadastro() + 1; ?>" class="form-control col-md-7" required>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="control-label col-md-3" for="cidade">Descrição:</label>
                    <div class="col-md-7">
                        <input type="text" name="descricao" value="<?php echo $data['registros']['grupo_proddescricao']; ?>" class="form-control col-md-7">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="control-label col-md-3" for="cidade">Ativo:</label>
                    <div class="col-md-7">
                        <select name="ind_ativo" id="ind_ativo" class="form-control col-md-7">
                            <option value="<?php echo $data['registros']['grupo_prodind_ativo']; ?>">
                                <?php echo $data['registros']['grupo_prodind_ativo'] == 0 ? "Sim" : "Não"; ?>
                            </option>
                            <option value="0">Sim</option>
                            <option value="1">Não</option>
                        </select>
                    </div>
                </div>


                <div class="form-group row">
                    <label class="control-label col-md-3" for="cidade">Origem:</label>
                    <div class="col-md-7">
                        <select name="ind_origem" id="ind_origem" class="form-control col-md-7">
                            <option value="<?php echo $data['registros']['grupo_prodind_origem']; ?>">
                                <?php echo $data['registros']['grupo_prodind_origem'] == 0 ? "Portal" : "Integrador"; ?>
                            </option>
                            <option value="0">Sim</option>
                            <option value="1">Não</option>
                        </select>
                    </div>
                </div>

            </div>

            <!-- /.box-body -->
            <div class="box-footer">
                <button name="Cadastrar" class="btn btn-success pull-left">Cadastrar</button>
                <a href="/gruposprodutos/index" class="btn btn-danger pull-right">Cancelar</a>
            </div>

        </form>
    </div>
</div>




