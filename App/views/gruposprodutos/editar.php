<section class="content-header">
    <div class="container" style="background: pink;">
        <?php
        if (!empty($data['mensagem'])) :

            foreach ($data['mensagem'] as $m) :
                echo $m . "<br>";
            endforeach;
        endif;
        ?>
    </div>
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h4>Edita Grupo produtos</h4>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="/empresas/index" style="color:indigo">Index</a></li>
                    <li class="breadcrumb-item active">Edita</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
        <div class="container-fluid">
        <form action="/gruposprodutos/editar/<?php echo $data['registros']['grupo_prodid']; ?>" method="post">
        <div class="col-md-6">
                <div class="card card-default">
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>CNPJ Estabelecimento</label>
                                    <input name="cnpj_estab" id="cnpj_estab"  value="<?php echo $data['registros']['grupo_prodcnpj_estab']; ?>" onfocus="javascript: retirarFormatacao(this);" onblur="javascript: formatarCampo(this);" maxlength="14" type="text" class="form-control" placeholder="00.000.000/000-00" required="required">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label> Código Grupo</label>
                                    <input name="cod_grupo_prod" value="<?php echo $data['registros']['grupo_prodcod_grupo_prod']; ?>" type="numbers" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label> Descrição</label>
                                    <input name="descricao" type="text" value="<?php echo $data['registros']['grupo_proddescricao']; ?>" class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" id="ind_ativo">
                                    <label class="custom-control-label" for="ind_ativo" style="margin-top:25%;">Grupo Ativo</label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" id="ind_origem">
                                    <label class="custom-control-label" for="ind_origem" style="margin-top:25%;">Origem Portal</label>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        </br>
                        <input type="submit" name="Cadastar" class="btn btn-primary float-right" value="Cadastrar" style="background-color: indigo; border-color: indigo; margin:6px;">
                        <a href="/gruposprodutos/index" class="btn btn-default float-right" style="margin:6px;">Cancelar</a>
                    </div>
        </form>
        </div>
        <script>
            function formatarCampo(campoTexto) {
                campoTexto.value = mascaraCnpj(campoTexto.value);
            }

            function retirarFormatacao(campoTexto) {
                campoTexto.value = campoTexto.value.replace(/(\.|\/|\-)/g, "");
            }

            function mascaraCnpj(valor) {
                return valor.replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/g, "\$1.\$2.\$3\/\$4\-\$5");
            }
        </script>
    </div>
</div>

