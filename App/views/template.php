<!DOCTYPE html>

<?php

function mensagens($datas)
{
    if (!empty($datas)) :
        foreach ($datas as $m) :
            //trata mensagem
            $partes = explode(" - ", $m);
            if ($partes[0] == '1') {
                //sucesso
                echo '<script>$.toast({
                    heading: "Sucesso!",
                    text: "' . $partes[1] . '",
                    icon: "success",
                    position: "top-center",
                    stack: true,
                })</script>';
            } else {
                if ($partes[0] == '2') {
                    //alert toast center default
                    echo '<script>$.toast({
                        heading: "Alerta",
                        text: "' . $partes[1] . '",
                        icon: "warning",
                        position: "top-center",
                        stack: true
                    })</script>';
                } else {
                    if ($partes[0] == '3') {
                        //erro toast center default
                        echo '<script>$.toast({
                            heading: "Erro",
                            text: "' . $partes[1] . '",
                            icon: "error",
                            position: "top-center",
                            stack: true
                        })</script>';
                    } else {
                        if ($partes[0] == '4') {
                            //erro ao excluir alert
                            echo '<script>
                            Swal.fire({
                                icon: "error",
                                title: "",
                                showClass: {
                                    popup: "animate__animated animate__bounceInUp"
                                },
                                hideClass: {
                                    popup: "animate__animated animate__bounceOutDown"
                                },
                                text: "' . $partes[1] . '",
                                     })
                            </script>';
                        } else {
                            if ($partes[0] == '5') {
                                //sucesso ao excluir alert
                                echo '<script>
                                Swal.fire({
                                    icon: "success",
                                    title: "",
                                    showClass: {
                                        popup: "animate__animated animate__bounceInUp"
                                    },
                                    hideClass: {
                                        popup: "animate__animated animate__bounceOutDown"
                                    },
                                    text: "' . $partes[1] . '",
                                  })
                               </script>';
                            } else {
                                if ($partes[0] == '6') {
                                    //alert ao excluir alert
                                    echo '<script>
                                    Swal.fire({
                                        icon: "warning",
                                        title: "",
                                        showClass: {
                                            popup: "animate__animated animate__bounceInUp"
                                        },
                                        hideClass: {
                                            popup: "animate__animated animate__bounceOutDown"
                                        },
                                        text: "' . $partes[1] . '",
                                      })
                                   </script>';
                                }
                            }
                        }
                    }
                }
            }
        endforeach;
    endif;
}
?>
<style>
    .btn-green {
        background-color: #4b8046 !important;
        border-color: #4b8046 !important;
        color: #fff !important;

    }

    .btn-green:hover {
        background-color: #4fbfa8 !important;
    }

    .btn-orange {
        background-color: #FA8A0A !important;
        border-color: #FA8A0A !important;
        color: #fff !important;
    }

    .btn-orange:hover {
        color: #fff !important;
        background-color: #FA7A39 !important;
        border-color: #FA7A39 !important;
        transition: 0.3s !important;
    }

    .btn-purple {
        background-color: #6f42c1 !important;
        border-color: #6f42c1 !important;
        color: #fff !important;
    }

    .btn-purple:hover {
        color: #fff !important;
        background-color: #6039A8 !important;
        border-color: #6039A8 !important;
        transition: 0.3s !important;
    }


    .btn-red {
        background-color: #DF0101 !important;
        border-color: #DF0101 !important;
        color: #fff !important;
    }

    .btn-red:hover {
        color: #fff !important;
        background-color: #B40404 !important;
        border-color: #B40404 !important;
        transition: 0.3s !important;
    }
</style>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" href="<?php echo URL_BASE; ?>vendor/css/style.default.css" id="theme-stylesheet">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" href="<?php echo URL_BASE; ?>/images/Logo.png" />
    <title> ArtBrasil</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo URL_BASE; ?>template/plugins/fontawesome-free/css/all.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="<?php echo URL_BASE; ?>template/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo URL_BASE; ?>template/dist/css/adminlte.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <!-- DataTables -->
    <link rel="stylesheet" href="<?php echo URL_BASE; ?>template/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="<?php echo URL_BASE; ?>template/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.dataTables.min.css">
    <!-- jQuery -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>

    <!-- Toast -->
    <script language="JavaScript" src='<?php echo URL_BASE; ?>vendor/toast/src/jquery.toast.js'></script>
    <link rel="stylesheet" href="<?php echo URL_BASE; ?>vendor/toast/src/jquery.toast.css">
    <!-- AlertMaster -->

    <!-- Optional: include a polyfill for ES6 Promises for IE11 -->
    <script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css" />
    <link rel="stylesheet" href="<?php echo URL_BASE; ?>template/plugins/select2/css/select2.min.css">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <!-- Select2 -->
    <link rel="stylesheet" href="<?php echo URL_BASE; ?>template/plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="<?php echo URL_BASE; ?>template/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
    <!-- JCROP -->
    <script language="JavaScript" src='<?php echo URL_BASE; ?>vendor/imgLiquid-master/imgLiquid.js'></script>
    <script language="JavaScript" src='<?php echo URL_BASE; ?>vendor/imgLiquid-master/imgLiquid-min.js'></script>

</head>
<?php if (isset($_SESSION['logado'])) : ?>

    <body class="hold-transition sidebar-mini sidebar-collapse pace-success">
        <!-- Site wrapper -->
        <div class="wrapper">
            <!-- Navbar -->
            <nav class="main-header navbar navbar-expand navbar-dark" style="background:#4b8046;">
                <!-- Left navbar links -->
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                    </li>
                    <li class="nav-item d-none d-sm-inline-block">
                        <a href="/home/index" class="nav-link">Ofertas</a>
                    </li>
                    <li class="nav-item d-none d-sm-inline-block">
                        <a href="/home/loja_geral" class="nav-link">Loja Geral</a>
                    </li>
                    <li class="nav-item d-none d-sm-inline-block">
                        <a href="/vendprod/index" class="nav-link">Vender</a>
                    </li>
                    <li class="nav-item d-none d-sm-inline-block">
                        <a href="#" class="nav-link">Chat de suporte ao cliente</a>
                    </li>
                    <li class="nav-item d-none d-sm-inline-block">
                        <a href="#" class="nav-link">Contato</a>
                    </li>
                </ul>

                <!-- SEARCH FORM -->
                <form class="form-inline ml-2">
                    <div class="input-group input-group-sm">
                        <input class="form-control form-control-navbar" type="search" placeholder="Pesquisar" aria-label="Search">
                        <div class="input-group-append">
                            <button class="btn btn-navbar" type="submit">
                                <i class="fas fa-search"></i>
                            </button>
                        </div>
                    </div>
                </form>

                <!-- Right navbar links -->
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item d-none d-sm-inline-block">
                        <a href="/dashboard/index" class="nav-link" title="Carrinho de Compras"><i class="fas fa-shopping-cart"></i></a>
                    </li>
                    <li class="nav-item d-none d-sm-inline-block">
                        <a href="/dashboard/index" class="nav-link" title="Lista de Desejos"><i class="fas fa-heart"></i></a>
                    </li>
                    <!-- Notifications Dropdown Menu -->
                    <li class="nav-item dropdown">
                        <a class="nav-link" data-toggle="dropdown" href="#">
                            <i class="far fa-bell"></i>
                            <span class="badge badge-warning navbar-badge">15</span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                            <span class="dropdown-item dropdown-header">15 Notifications</span>
                            <div class="dropdown-divider"></div>
                            <a href="#" class="dropdown-item">
                                <i class="fas fa-envelope mr-2"></i> 4 new messages
                                <span class="float-right text-muted text-sm">3 mins</span>
                            </a>
                            <div class="dropdown-divider"></div>
                            <a href="#" class="dropdown-item">
                                <i class="fas fa-users mr-2"></i> 8 friend requests
                                <span class="float-right text-muted text-sm">12 hours</span>
                            </a>
                            <div class="dropdown-divider"></div>
                            <a href="#" class="dropdown-item">
                                <i class="fas fa-file mr-2"></i> 3 new reports
                                <span class="float-right text-muted text-sm">2 days</span>
                            </a>
                            <div class="dropdown-divider"></div>
                            <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
                        </div>
                    </li>


                    <li class="nav-item dropdown">
                        <a class="nav-link" data-toggle="dropdown" href="#">
                            <i class="fas fa-cog"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                            <span class="dropdown-item dropdown-header">15 Notifications</span>
                            <div class="dropdown-divider"></div>
                            <a href="#" class="dropdown-item">
                                <i class="fas fa-envelope mr-2"></i> 4 new messages
                                <span class="float-right text-muted text-sm">3 mins</span>
                            </a>
                            <div class="dropdown-divider"></div>
                            <a href="#" class="dropdown-item">
                                <i class="fas fa-users mr-2"></i> 8 friend requests
                                <span class="float-right text-muted text-sm">12 hours</span>
                            </a>
                            <div class="dropdown-divider"></div>
                            <a href="#" class="dropdown-item">
                                <i class="fas fa-file mr-2"></i> 3 new reports
                                <span class="float-right text-muted text-sm">2 days</span>
                            </a>
                            <div class="dropdown-divider"></div>
                            <a href="/home/logout" class="dropdown-item dropdown-footer">Editar configurações</a>
                            <div class="dropdown-divider"></div>
                            <a href="/home/logout" class="dropdown-item dropdown-footer">Sair</a>
                        </div>
                    </li>

                </ul>
            </nav>
            <!-- Main Sidebar Container -->
            <aside class="main-sidebar elevation-4 sidebar-light-purple">
                <!-- Brand Logo -->
                <a href="/home/loja_geral" class="brand-link " style="background:#4b8046;">
                    <img src="<?php echo URL_BASE; ?>vendor/img/logo.png" alt="AdminLTE Logo" class="brand-image img-circle" style="opacity: .9">
                    <span class="brand-text font-weight-light" style="color: white; font-weight: bold;">ArtBrasil</span>
                </a>

                <!-- Sidebar -->
                <div class="sidebar">
                    <!-- Sidebar user (optional) -->
                    <div class="user-panel mt-2 pb-3 mb-2 d-flex">
                        <div class="image">
                            <img src="<?php echo URL_BASE; ?>images/avatar2.png" class="img-circle elevation-2" alt="User Image">
                        </div>
                        <div class="info">
                            <a href="#" class="d-block"><?php echo $_SESSION['userNome']; ?></a> <!-- FIXME: buscar e colocar Nome Completo em vez do username -->
                        </div>
                    </div>
                    <!-- Sidebar Menu -->
                    <nav class="mt-2">
                        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="true">
                            <li class="nav-item has-treeview">
                            <li class="nav-item">
                                <a href="/home/avisos" class="nav-link">
                                    <i class="nav-icon fas fa-heart"></i>
                                    <p> Lista de Desejos </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="/home/avisos" class="nav-link">
                                    <i class="nav-icon fas fa-shopping-cart"></i>
                                    <p> Carrinho de compras </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="/home/metas" class="nav-link">
                                    <i class="nav-icon fas fa-box"></i>
                                    <p> Meus Pedidos </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="/vendprod/index" class="nav-link">
                                    <i class="nav-icon fas fa-box-open"></i>
                                    <p> Vender Produto </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="/home/dashboard_personalizado" class="nav-link">
                                    <i class="nav-icon fas fa-comment-alt"></i>
                                    <p> Central de Mensagens </p>
                                </a>
                            </li>
                            </li>
                            <li class="nav-item has-treeview">
                                <a href="#" class="nav-link">
                                    <i class="nav-icon fas fa-money-bill"></i>
                                    <p>
                                        Minha Loja
                                        <i class="fas fa-angle-left right"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                        <a href="/meusprod/index" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Meus Produtos</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="/resestabs/" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Vendas em Andamento</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="/resunidnegs/" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Vendas Finalizadas</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-header">CONFIGURAÇÔES</li>
                            <li class="nav-item has-treeview">
                                <a href="#" class="nav-link">
                                    <i class="nav-icon fas fa-book"></i>
                                    <p>
                                        Cadastros
                                        <i class="fas fa-angle-left right"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                        <a href="/catpai/index" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Categoria Pai</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="/catfilho/index" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Categoria Filho</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="/tabprod/index" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Tabela de Produtos</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="/turnos/" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Turnos</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="/feriados/" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Feriados</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item has-treeview">
                                <a href="#" class="nav-link">
                                    <i class="nav-icon fas fa-briefcase"></i>
                                    <p>
                                        Tarefas
                                        <i class="fas fa-angle-left right"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                        <a href="/optimagem/optimagem" onclick="optimg()" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Optimizar Imagens Produtos</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="/vendas/" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Vendas</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="/vendas/" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Mensagens</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item has-treeview">
                                <a href="#" class="nav-link">
                                    <i class="nav-icon fas fa-coffee"></i>
                                    <p>
                                        Produtos
                                        <i class="fas fa-angle-left right"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                        <a href="/gruposprodutos/" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Grupos</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="/produtos/" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Produtos</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
            </aside>
            <div class="content-wrapper">
            <?php endif; ?>

            <?php require_once '../App/views/' . $view . '.php'; ?>

            </div>
            <!-- DataTables -->
            <script src="<?php echo URL_BASE; ?>template/plugins/datatables/jquery.dataTables.min.js"></script>
            <script src="<?php echo URL_BASE; ?>template/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
            <script src="<?php echo URL_BASE; ?>template/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
            <script src="<?php echo URL_BASE; ?>template/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
            <script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
            <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
            <!-- Máscaras -->
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.12/jquery.mask.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.12/jquery.mask.min.js"></script>
            <!-- Jcrop -->
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-jcrop/2.0.4/js/Jcrop.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-jcrop/2.0.4/js/Jcrop.min.js"></script>
            <!-- Bootstrap 4 -->
            <script src="https://unpkg.com/@popperjs/core@2"></script>
            <!-- Bootstrap 4 -->
            <script src="<?php echo URL_BASE; ?>template/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
            <!-- Select2 -->
            <script src="<?php echo URL_BASE; ?>template/plugins/select2/js/select2.full.min.js"></script>
            <!-- Bootstrap4 Duallistbox -->
            <script src="<?php echo URL_BASE; ?>template/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
            <!-- InputMask -->
            <script src="<?php echo URL_BASE; ?>template/plugins/moment/moment.min.js"></script>
            <script src="<?php echo URL_BASE; ?>template/plugins/inputmask/min/jquery.inputmask.bundle.min.js"></script>
            <!-- date-range-picker -->
            <script src="<?php echo URL_BASE; ?>template/plugins/daterangepicker/daterangepicker.js"></script>
            <!-- bootstrap color picker -->
            <script src="<?php echo URL_BASE; ?>template/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
            <!-- Tempusdominus Bootstrap 4 -->
            <script src="<?php echo URL_BASE; ?>template/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
            <!-- Bootstrap Switch -->
            <script src="<?php echo URL_BASE; ?>template/plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
            <!-- AdminLTE App -->
            <script src="<?php echo URL_BASE; ?>template/dist/js/adminlte.min.js"></script>
            <!-- AdminLTE for demo purposes -->
            <script src="<?php echo URL_BASE; ?>template/dist/js/demo.js"></script>
            <!-- Func personaliz -->
            <script language="JavaScript" src='<?php echo URL_BASE; ?>vendor/functions/functions_js.js'></script>
            <!-- pace-progress -->
            <script src="<?php echo URL_BASE; ?>template/plugins/pace-progress/pace.min.js"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>


</html>
<script>
    $(document).ready(function() {
        var table = $('#table').DataTable({
            paging: true,
            responsive: true,
            autoWidth: false,
            lengthChange: true,
            keys: true,
            dom: 'Bfrtip',
            buttons: [
                'csv', 'pdf'
            ],
            language: {
                "sEmptyTable": "Nenhum registro encontrado",
                "sInfo": "Mostrando ( _START_ ... _END_ ) de _TOTAL_ registros encontrados",
                "sInfoEmpty": "Mostrando 0 até 0 de 0 registros encontrados",
                "sInfoFiltered": "(Filtrados de _MAX_ registros encontrados)",
                "sInfoPostFix": "",
                "sInfoThousands": ".",
                "sLengthMenu": "_MENU_ resultados por página",
                "sLoadingRecords": "Carregando...",
                "sProcessing": "Processando...",
                "sZeroRecords": "Nenhum registro encontrado",
                "sSearch": "Pesquisar",
                "oPaginate": {
                    "sNext": "Próximo",
                    "sPrevious": "Anterior",
                    "sFirst": "Primeiro",
                    "sLast": "Último"
                },
                "oAria": {
                    "sSortAscending": ": Ordenar colunas de forma ascendente",
                    "sSortDescending": ": Ordenar colunas de forma descendente"
                }
            },
        });
    });
    $(function() {
        //Initialize Select2 Elements
        $('.select2').select2()
        //Initialize Select2 Elements
        $('.select2bs4').select2({
            theme: 'bootstrap4'
        })
        $("input[data-bootstrap-switch]").each(function() {
            $(this).bootstrapSwitch('state', $(this).prop('checked'));
        });
    });

    //função para optimizar imagens
    function optimg() {
        let timerInterval
        Swal.fire({
            title: 'Aguarde a Optmização!',
            timer: 999999999,
            timerProgressBar: false,
            showClass: {
                popup: 'animate__animated animate__bounceInUp'
            },
            hideClass: {
                popup: 'animate__animated animate__bounceOutDown'
            },
            onBeforeOpen: () => {
                Swal.showLoading()
                timerInterval = setInterval(() => {
                    const content = Swal.getContent()
                    if (content) {
                        const b = content.querySelector('b')
                        if (b) {
                            b.textContent = Swal.getTimerLeft()
                        }
                    }
                }, 100)
            },
            onClose: () => {
                clearInterval(timerInterval)
            }
        }).then((result) => {
            /* Read more about handling dismissals below */
            if (result.dismiss === Swal.DismissReason.timer) {
                console.log('I was closed by the timer')
            }
        })
    }
</script>