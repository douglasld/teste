<!-- Content Header (Page header)  -->
<section class="content-header">
    <div class="container" style="background: pink;">
        <?php
        if (!empty($data['mensagem'])) :

            foreach ($data['mensagem'] as $m) :
                echo $m . "<br>";
            endforeach;
        endif;

        // echo json_encode($data['estab_real']);

        ?>
    </div>
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h4>Indicadores</h4>
            </div>

            <div class="col-md-6">
                <div style="margin-left:20px" class="float-right">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Indicadores</li>
                    </ol>
                </div>
                <form class="form-inline sm-3 float-right" action="/dashboard/getMetasByEstabelecimento" method="GET" >
                    <div class="input-group input-group-sm" style="background-color:lavender;border-radius: 5px;">
                        <input class="form-control" name="mes" type="date"  placeholder="24/01/2020" aria-label="Pesquisar período">
                        <div class="input-group-append">
                            <button class="btn " name="Pesquisar" id="pesquisar" type="submit">
                                <i class="fas fa-search"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">

            <div class="col-12 col-sm-6 col-md-3">

                <!-- <div class="card collapsed-card">
                    <div class="card-header">
                        <h3 class="card-title">Configurações</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i></button>
                        </div>
                    </div>
                    <div class="card-body" style="display: none;">
                        <form action="/dashboard/getMetasByEstabelecimento" method="GET">
                            <div class="info-box">
                                <div class="info-box-content">
                                    <span class="info-box-text">Selecione o período</span>
                                    <span class="info-box-number">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                            </div>
                                            <input name="mes" type="date" class="form-control" data-inputmask-alias="date" data-inputmask-inputformat="dd/mm/yyyy" data-mask="" im-insert="false">
                                            <button class="btn btn-default btn-md" name="Pesquisar" id="pesquisar" type="submit"><i class="fa fa-search"></i></button>
                                        </div>
                                    </span>
                                </div>
                            </div>
                        </form>
                    </div>
                </div> -->

            </div>

        </div>

        <!-- Gráficos -->
        <!-- <div class="col-md-6" id="estab_Real" style="width:100%; height:400px;"></div> -->
        <!-- <div class="col-md-6" id="estab_comparativo" style="width:100%; height:400px;"></div> -->
        <!-- <div class="col-md-12" id="estab_comparativo_quadri" style="width:100%; height:400px; margin-top: 25px"></div> -->
        <!-- Gráficos -->

        <div class="row">

            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header border-0">
                        <div class="d-flex justify-content-between">
                            <h3 class="card-title"> </h3>
                            <a href="javascript:void(0);">Ver detalhes</a>
                        </div>
                    </div>

                    <div class="card-body">
                        <div id="estab_Real"></div>
                    </div>

                </div> <!-- /.card -->
            </div>

            <!-- /.col-md-6 -->
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header border-0">
                        <div class="d-flex justify-content-between">
                            <h3 class="card-title"> </h3>
                            <a href="javascript:void(0);">Ver detalhes</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div id="estab_comparativo"></div>
                    </div>
                </div>
                <!-- /.card -->
            </div>
            <!-- /.card -->
        </div>


        <div class="row">
            <!-- /.col-md-6 -->
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header border-0">
                        <div class="d-flex justify-content-between">
                            <h3 class="card-title"> </h3>
                            <a href="javascript:void(0);">Ver detalhes</a>
                        </div>
                    </div>
                    <div class="card-body">

                        <div class='content' style="background-color:#fff; width: 766px; height: 400px;"></div>

                    </div>
                </div>
                <!-- /.card -->
            </div>
            <!-- /.card -->
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header border-0">
                        <div class="d-flex justify-content-between">
                            <h3 class="card-title"> </h3>
                            <a href="javascript:void(0);">Ver detalhes</a>
                        </div>
                    </div>
                    <div class="card-body">

                        <div id="estab_comparativo_quadri"></div>

                    </div>
                </div>
                <!-- /.card -->
            </div>

        </div>




    </div>
</section>

<script>
    // set the theme
    Highcharts.setOptions({
        colors: ['#FA2340', '#0AAEFA', '#FAF123', '#FA2382', '#22F54E', '#FA2340', '#FF9655', '#A270FF', '#3B1C75'],
        chart: {
            backgroundColor: '#FFF',
            borderWidth: 0,
            plotBackgroundColor: 'rgba(255, 255, 255, .9)',
            plotShadow: true,
            plotBorderWidth: 1
        },
        title: {
            style: {
                color: '#343a40',
                font: 'bold 16px "Trebuchet MS", Verdana, sans-serif'
            }
        },
        subtitle: {
            style: {
                color: '#666666',
                font: 'bold 12px "Trebuchet MS", Verdana, sans-serif'
            }
        },
        xAxis: {
            gridLineWidth: 1,
            lineColor: '#000',
            tickColor: '#000',
            labels: {
                style: {
                    color: '#000',
                    font: '11px Trebuchet MS, Verdana, sans-serif'
                }
            },
            title: {
                style: {
                    color: '#6f42c1',
                    fontWeight: 'bold',
                    fontSize: '12px',
                    fontFamily: 'Trebuchet MS, Verdana, sans-serif'

                }
            }
        },
        yAxis: {
            alternateGridColor: null,
            minorTickInterval: 'auto',
            lineColor: '#000',
            lineWidth: 1,
            tickWidth: 1,
            tickColor: '#000',
            labels: {
                style: {
                    color: '#000',
                    font: '11px Trebuchet MS, Verdana, sans-serif'
                }
            },
            title: {
                style: {
                    color: '#333',
                    fontWeight: 'bold',
                    fontSize: '12px',
                    fontFamily: 'Trebuchet MS, Verdana, sans-serif'
                }
            }
        },
        legend: {
            itemStyle: {
                font: '9pt Trebuchet MS, Verdana, sans-serif',
                color: 'black'

            },
            itemHoverStyle: {
                color: '#6f42c1'
            },
            itemHiddenStyle: {
                color: 'gray'
            }
        },
        credits: {
            style: {
                right: '10px'
            }
        },
        labels: {
            style: {
                color: '#99b'
            }
        }
    });

    // Build the charts
    Highcharts.chart('estab_Real', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Metas por Área (Mensal)' //#73879C
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        accessibility: {
            point: {
                valueSuffix: '%'
            }
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Alcance da Meta',
            colorByPoint: true,
            data: <?php echo json_encode($data['estab_real']); ?>
        }]
    });

    Highcharts.chart('estab_comparativo', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Comparativo sobre as Metas (Mensal)'
        },
        xAxis: {
            categories: <?php echo json_encode($data['estab_comparativo_name']); ?>
        },
        yAxis: {
            min: 0,
            title: {
                text: 'R$'
            }
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
            shared: true
        },
        plotOptions: {
            column: {
                stacking: 'percent'
            }
        },
        series: [{
            name: 'Meta',
            data: <?php echo json_encode($data['estab_comparativo_meta']); ?>
        }, {
            name: 'Real',
            data: <?php echo json_encode($data['estab_comparativo_real']); ?>,
            color: '#22F54E'
        }]
    });

    Highcharts.chart('estab_comparativo_quadri', {
        chart: {
            type: 'bar'
        },
        credits: {
            enabled: false
        },
        title: {
            text: 'Comparativo por Estabeleciomento (Trimestre)'
        },
        xAxis: {
            categories: <?php echo json_encode($data['estab_comparativo_name']); ?>
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Reais (R$)',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            valueSuffix: ' Reais'
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -40,
            y: 100,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Janeiro',
            data: [2269659.04000, 3295372.79000, 505746.09000, 1791912.10000, 530812.85000, 1515841.511, 200000.00]
        }, {
            name: 'Fevereiro',
            data: [2251962.36000, 3579620.01000, 290830.91000, 1692022.64000, 490005.94000, 4598185.37000, 200000.00]
        }, {
            name: 'Março',
            data: [2159174.33000, 2833255.47000, 228473.89000, 928834.17000, 270994.62000, 899545.54, 200000.00]
        }, {
            name: 'Meta',
            data: [2855000.00, 3515000.00, 834000.00, 2305000.00, 95000.00, 4850168.00, 2000.00]
        }]
    });
</script>