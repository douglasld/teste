<?= mensagens($data['mensagem']); ?>

<body>
    <div id="">
        <link rel="stylesheet" href="<?= URL_BASE ?>vendor/css/custom.css">
        <div class="heading">
            <br>
            <center>
                <h3>Escolha pelo menos 3 fotos de apresentação</h3>
            </center>
        </div>
        <!-- ADICIONA PRODUTRO -->
        <div id="content">
            <div class="container">
                <div class="col-md-12" style="width: 100%;">
                    <center>
                        <?php
                        //faz as verificações das fotos
                        ?>
                    </center>
                    <form method="POST" id="prod_form" action="/vendprod_artesanato/atualizaartezanato/<?= $data['registros']['cod_prod'] ?>" enctype="multipart/form-data" onsubmit="javascript:document.getElementById('blanket').style.display = 'block';document.getElementById('aguarde').style.display = 'block';">
                        <div id="blanket"></div>
                        <div id="aguarde">Aguarde...</div>
                        <input id="cod_prod" type="text" hidden name="cod_prod" value="<?= $data['registros']['cod_prod'] ?>">
                        <input id="cod_loja" type="text" hidden name="cod_loja" value="<?= $data['edita']['cod_loja'] ?>">
                        <div>
                            <section class="content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="card card-outline card-info">
                                            <div class="card-header" data-card-widget="collapse">
                                                <h3 class="card-title">
                                                    Fotos do Produto
                                                    <small data-card-widget="collapse">clique para minimizar</small>
                                                </h3>
                                                <!-- tools box -->
                                                <div class="card-tools">
                                                    <button type="button" class="btn btn-tool btn-sm" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                                                        <i class="fas fa-minus"></i></button>
                                                </div>
                                            </div>
                                            <div class="card-body pad">
                                                <div class="col-md-12">
                                                    <div class="row col-md-12 tc">
                                                        <div class="col-md-2 ft">
                                                            <?php
                                                            $URL_BASE = URL_BASE;
                                                            if ($data['edita']['cod_foto_1'] == 0) {
                                                                echo "<img id='blah1' class='ftc' src='$URL_BASE/fotos_produtos/default_produto.svg'/>";
                                                            } else {
                                                                echo "<img id='blah1' class='ftc' src='$URL_BASE/fotos_produtos/" . $data['edita']['cod_foto_1'] . "' />";
                                                            }
                                                            ?>
                                                            <label class='lb_ft1 ft' for='ft1'><i class='fa fa-image' aria-hidden='true'></i> Foto 1</label>
                                                        </div>
                                                        <div class='col-md-2 ft'>
                                                            <?php
                                                            if ($data['edita']['cod_foto_2'] == 0) {
                                                                echo "<img id='blah2' class='ftc' src='$URL_BASE/fotos_produtos/default_produto.svg' />";
                                                            } else {
                                                                echo "<img id='blah2' class='ftc' src='$URL_BASE/fotos_produtos/" . $data['edita']['cod_foto_2'] . "' />";
                                                            }
                                                            ?>
                                                            <label class='lb_ft2 ft' for='ft2'><i class='fa fa-image' aria-hidden='true'></i> Foto 2</label>
                                                        </div>
                                                        <div class='col-md-2 ft'>
                                                            <?php
                                                            if ($data['edita']['cod_foto_3'] == 0) {
                                                                echo "<img id='blah3' class='ftc' src='$URL_BASE/fotos_produtos/default_produto.svg' />";
                                                            } else {
                                                                echo "<img id='blah3' class='ftc' src='$URL_BASE/fotos_produtos/" . $data['edita']['cod_foto_3'] . "' />";
                                                            }
                                                            ?>
                                                            <label class='lb_ft3 ft' for='ft3'><i class='fa fa-image' aria-hidden='true'></i> Foto 3</label>
                                                        </div>
                                                        <div class='col-md-2 ft'>
                                                            <?php
                                                            if ($data['edita']['cod_foto_4'] == 0) {
                                                                echo "<img id='blah4' class='ftc' src='$URL_BASE/fotos_produtos/default_produto.svg' />";
                                                            } else {
                                                                echo "<img id='blah4' class='ftc' src='$URL_BASE/fotos_produtos/" . $data['edita']['cod_foto_4'] . "' />";
                                                            }
                                                            ?>
                                                            <label class='lb_ft4 ft' for='ft4'><i class='fa fa-image' aria-hidden='true'></i> Foto 4</label>
                                                        </div>
                                                        <div class='col-md-2 ft'>
                                                            <?php
                                                            if ($data['edita']['cod_foto_5'] == 0) {
                                                                echo "<img id='blah5' class='ftc' src='$URL_BASE/fotos_produtos/default_produto.svg' />";
                                                            } else {
                                                                echo "<img id='blah5' class='ftc' src='$URL_BASE/fotos_produtos/" . $data['edita']['cod_foto_5'] . "' />";
                                                            }
                                                            ?>
                                                            <label class='lb_ft5 ft' for='ft5'><i class='fa fa-image' aria-hidden='true'></i> Foto 5</label>
                                                        </div>
                                                    </div>
                                                    <input hidden type='file' onchange="readURL(this);" id="ft1" name="ft1" />
                                                    <input hidden type='file' onchange="readURLL(this);" id="ft2" name="ft2" />
                                                    <input hidden type='file' onchange="readURLLL(this);" id="ft3" name="ft3" />
                                                    <input hidden type='file' onchange="readURLLLL(this);" id="ft4" name="ft4" />
                                                    <input hidden type='file' onchange="readURLLLLL(this);" id="ft5" name="ft5" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section>
                                <center>
                                    <h2 class="text-uppercase">Boas descrições geram boas vendas!</h2>
                                    <h5 class="text-uppercase">Campos com * são de preenchimento obrigatório</h5>
                                </center>
                            </section>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="box" style="margin-top:2%;">
                                        <h3>Informações do produto</h3>
                                        <div class="row">
                                            <div class="form-group col-md-12">
                                                <label for="nome_produto">Nome do produto *</label>
                                                <input id="nome_produto_vendedor" type="text" class="form-control" required="" maxlength="30" name="nome_produto_vendedor" value="<?= $data['edita']['nome_prod_v'] ?>" maxlength="50" placeholder="Colar de Quartzo">
                                            </div>
                                            <div class="col-md-12">
                                                <section class="content">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="card card-outline card-info">
                                                                <div class="card-header" data-card-widget="collapse" data-toggle="tooltip">
                                                                    <h3 class="card-title" data-card-widget="collapse" data-toggle="tooltip">
                                                                        Descrição do produto
                                                                        <small data-card-widget="collapse" data-toggle="tooltip">Clique para minimizar</small>
                                                                    </h3>
                                                                    <!-- tools box -->
                                                                    <div class="card-tools">
                                                                        <button type="button" class="btn btn-tool btn-sm" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                                                                            <i class="fas fa-minus"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="card-body pad">
                                                                    <div class="mb-3">
                                                                        <textarea class="textarea" name="desc_produto" placeholder="" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
                                                                        <?= $data['edita']['desc_prod'] ?>
                                                            </textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-5">
                                                <center><label for="valor_produto">Valor a receber *</label></center>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <div class="input-group-text">R$</div>
                                                    </div>
                                                    <input type="text" class="form-control" value="<?= $data['edita']['val_prod'] ?>" id="valor_produto" name="valor_produto">
                                                </div>
                                            </div>
                                            <div class="form-group col-md-2">
                                                <center>
                                                    <label for="valor_servicos">Serviços</label>
                                                    <input id="valor_servicos" type="text" class="form-control" required="" name="valor_servicos" value="30%" readonly="">
                                                </center>
                                            </div>
                                            <div class="form-group col-md-5">
                                                <div class="col-auto">
                                                    <center> <label for="valor_final">Valor Final</label></center>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <div class="input-group-text">R$</div>
                                                        </div>
                                                        <input type="text" class="form-control" id="valor_final" name="valor_final" readonly="" value="<?=number_format(((($data['edita']['val_prod'] / 100) * 30) +  $data['edita']['val_prod']),2)?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <label for="cores" style="color: #4F4F4F;font-size:14px;">Cores e Quantidades *</label>
                                        <div class="row">
                                            <div class="form-group col-sm-3">
                                                <center> <label for="qtd_cor_azul">Azul</label></center>
                                                <div class="input-group">
                                                    <div class="input-group-text" style="background-color: #00BFFF;"> </div>
                                                    <input type="number" class="form-control" id="qtd_cor_azul" name="qtd_cor_azul" value="<?= $data['edita']['qtd_cor_azul'] ?>">
                                                </div>
                                            </div>
                                            <div class="form-group col-sm-3">
                                                <center> <label for="qtd_cor_verde">Verde</label></center>
                                                <div class="input-group">
                                                    <div class="input-group-text" style="background-color:#3CB371;"> </div>
                                                    <input type="number" class="form-control" id="qtd_cor_verde" name="qtd_cor_verde" value="<?= $data['edita']['qtd_cor_verde'] ?>">
                                                </div>
                                            </div>
                                            <div class="form-group col-sm-3">
                                                <center> <label for="qtd_cor_vermelho">Vermelho</label></center>
                                                <div class="input-group">
                                                    <div class="input-group-text" style="background-color:#FF0000;"> </div>
                                                    <input type="number" class="form-control" id="qtd_cor_vermelho" name="qtd_cor_vermelho" value="<?= $data['edita']['qtd_cor_vermelho'] ?>">
                                                </div>
                                            </div>
                                            <div class="form-group col-sm-3">
                                                <center> <label for="qtd_cor_preto">Preto</label></center>
                                                <div class="input-group">
                                                    <div class="input-group-text" style="background-color:black;"> </div>
                                                    <input type="number" class="form-control" id="qtd_cor_preto" name="qtd_cor_preto" value="<?= $data['edita']['qtd_cor_preto'] ?>">
                                                </div>
                                            </div>
                                            <div class="form-group col-sm-3">
                                                <center> <label for="qtd_cor_branco">Branco</label></center>
                                                <div class="input-group">
                                                    <div class="input-group-text" style="background-color:white;"> </div>
                                                    <input type="number" class="form-control" id="qtd_cor_branco" name="qtd_cor_branco" value="<?= $data['edita']['qtd_cor_branco'] ?>">
                                                </div>
                                            </div>
                                            <div class="form-group col-sm-3">
                                                <center> <label for="qtd_cor_marrom">Marrom</label></center>
                                                <div class="input-group">
                                                    <div class="input-group-text" style="background-color:#964B00;"> </div>
                                                    <input type="number" class="form-control" id="qtd_cor_marrom" name="qtd_cor_marrom" value="<?= $data['edita']['qtd_cor_marrom'] ?>">
                                                </div>
                                            </div>
                                            <div class="form-group col-sm-3">
                                                <center> <label for="qtd_cor_cinza">Cinza</label></center>
                                                <div class="input-group">
                                                    <div class="input-group-text" style="background-color:#a9a9a9;"> </div>
                                                    <input type="number" class="form-control" id="qtd_cor_cinza" name="qtd_cor_cinza" value="<?= $data['edita']['qtd_cor_cinza'] ?>">
                                                </div>
                                            </div>
                                            <div class="form-group col-sm-3">
                                                <center> <label for="qtd_cor_rosa">Rosa</label></center>
                                                <div class="input-group">
                                                    <div class="input-group-text" style="background-color:pink;"> </div>
                                                    <input type="number" class="form-control" id="qtd_cor_rosa" name="qtd_cor_rosa" value="<?= $data['edita']['qtd_cor_rosa'] ?>">
                                                </div>
                                            </div>
                                            <div class="form-group col-sm-3">
                                                <center> <label for="qtd_cor_amarelo">Amarelo</label></center>
                                                <div class="input-group">
                                                    <div class="input-group-text" style="background-color:yellow;"> </div>
                                                    <input type="number" class="form-control" id="qtd_cor_amarelo" name="qtd_cor_amarelo" value="<?= $data['edita']['qtd_cor_amarelo'] ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="box" style="margin-top:2%;">
                                        <div class="row">
                                            <h3>Informações para Envio</h3>
                                            <div class="form-group col-md-6">
                                                <center><label for="peso">Peso do objeto *</label></center>
                                                <input id="peso" type="text" class="form-control" required="" name="peso" min=1 oninput="validity.valid||(value='');" value="<?= $data['edita']['peso'] ?>">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <center><label for="unidade_peso">Unidade *</label></center>
                                                <select class="form-control select2bs4" id="unidade_peso" name="unidade_peso">
                                                    <?php
                                                    if ($data['edita']['unidade_peso'] == 1) {
                                                        echo '<option value="1" selected="selected">Grama</option>';
                                                        echo '<option value="2">Kilograma</option>';
                                                    } else {
                                                        echo '<option value="1">Grama</option>';
                                                        echo '<option value="2"  selected="selected">Kilograma</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-4">
                                                <center> <label for="altura">Altura *</label></center>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="altura" name="altura" onblur="calcularcm3()" required="" min=1 oninput="validity.valid||(value='');" value="<?= $data['edita']['altura'] ?>">
                                                    <div class="input-group-text">CM</div>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <center> <label for="largura">Largura *</label></center>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="largura" name="largura" onblur="calcularcm3()" required="" min=1 oninput="validity.valid||(value='');" value="<?= $data['edita']['largura'] ?>">
                                                    <div class="input-group-text">CM</div>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <center> <label for="comprimento">Comprimento *</label></center>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="comprimento" name="comprimento" onblur="calcularcm3()" required="" min=1 oninput="validity.valid||(value='');" value="<?= $data['edita']['comprimento'] ?>">
                                                    <div class="input-group-text">CM</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label for="fragilidade">Escolha a fragilidade do produto *</label>
                                                <select name="fragilidade" id="fragilidade" class="form-control select2bs4" required="">
                                                    <?php
                                                    if ($data['edita']['fragilidade'] == 1) {
                                                        echo '<option value="1" selected="selected">Produto Frágil</option>';
                                                        echo '<option value="2">Produto Resistente</option>';
                                                    } else {
                                                        echo '<option value="1">Produto Frágil</option>';
                                                        echo '<option value="2" selected="selected">Produto Resistente</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="retirar_mao">Aceita retirada em mãos? *</label>
                                                <select name="retirar_mao" id="retirar_mao" class="form-control select2bs4" required="">
                                                    <?php
                                                    if ($data['edita']['retirada_mao'] == 1) {
                                                        echo '<option value="1" selected="selected">Sim</option>';
                                                        echo '<option value="2">Não</option>';
                                                    } else {
                                                        echo '<option value="1">Sim</option>';
                                                        echo '<option value="2" selected="selected">Não</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-4">
                                                <label for="garantia_opcao">Garantia</label>
                                                <select name="garantia_opcao" id="garantia_opcao" class="form-control select2bs4" onchange="verifica(this.value)">
                                                    <?php
                                                    if ($data['edita']['garantia_opcao'] == 1) {
                                                        echo '<option value="1" selected="selected">Sim</option>';
                                                        echo '<option value="2">Não</option>';
                                                    } else {
                                                        echo '<option value="1">Sim</option>';
                                                        echo '<option value="2" selected="selected">Não</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label for="garantia_meses">Meses de Garantia</label>
                                                <input type="number" <?php if ($data['edita']['garantia_opcao'] == 2) {
                                                                            echo "disabled";
                                                                        } ?> name="garantia_meses" id="garantia_meses" value="<?= $data['edita']['garantia_meses'] ?>" class="form-control" required="" max="48" min="1">
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label for="estado_produto">Estado do produto *</label>
                                                <select name="estado_produto" id="estado_produto" class="form-control select2bs4" required="">
                                                    <?php
                                                    if ($data['edita']['estado_prod'] == 1) {
                                                        echo '<option value="1" selected="selected">Novo</option>';
                                                        echo '<option value="2">Usado</option>';
                                                    } else {
                                                        echo '<option value="1">Novo</option>';
                                                        echo '<option value="2" selected="selected">Usado</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-12">
                                                <input type="submit" name="enviar" class="btn btn-green" value="Cadastrar Produto">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                    </form>
                </div>
            </div>
        </div>
</body>
<link rel="stylesheet" href="<?= URL_BASE ?>template/plugins/summernote/summernote-bs4.css">
<script src="<?= URL_BASE ?>template/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?= URL_BASE ?>template/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="<?= URL_BASE ?>template/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?= URL_BASE ?>template/dist/js/demo.js"></script>
<!-- Summernote -->
<script src="<?= URL_BASE ?>template/plugins/summernote/summernote-bs4.min.js"></script>
<script language="JavaScript" src='<?= URL_BASE ?>vendor/functions/functions_artesanato.js'></script>

</html>
<script>
    //calcula o valor final do produto
    $('#valor_produto').keyup(delay(function(e) {
        var valor1 = document.getElementById("valor_produto").value;
        valor1 = valor1.replace(/,/g, ".");
        valor1 = parseFloat(valor1);
        valor1 = (valor1 / 100) * 30 + valor1;
        valor1 = String(valor1.toFixed(2));
        valor1 = valor1.replace(".", ",");

        document.getElementById("valor_final").value = valor1;

        console.log("sd");
    }, 500));

    $('#nome_produto_vendedor').keyup(delay(function(e) {
        //recebe nome do produto
        $.post('<?= $_SESSION['prod'] ?>/ajax/verifica_produtos.php', {
                nome_prod: this.value,
                cod_loja: $("#cod_loja").val(),
                func: 1
            }, function(data) {
                if (data > 0) {
                    $("#nome_produto_vendedor")
                        .removeClass("is-valid")
                        .addClass("is-invalid");
                    $("#nome_produto_vendedor").val("");
                    Swal.fire({
                        icon: "warning",
                        title: "",
                        showClass: {
                            popup: "animate__animated animate__bounceInUp",
                        },
                        hideClass: {
                            popup: "animate__animated animate__bounceOutDown",
                        },
                        text: "Você já cadastrou este produto!",
                    });
                } else {
                    $("#nome_produto_vendedor")
                        .removeClass("is-invalid")
                        .addClass("is-valid");
                }
            },
            "json"
        );
    }, 500));

    $(document).ready(function() {
        $('#valor_produto').mask('000,00', {
            reverse: true
        });
    });
    $(document).ready(function() {
        $('#valor_final').mask('000,00', {
            reverse: true
        });
    });
    $(document).ready(function() {
        $('#altura').mask('000,00', {
            reverse: true
        });
    });
    $(document).ready(function() {
        $('#largura').mask('000,00', {
            reverse: true
        });
    });
    $(document).ready(function() {
        $('#comprimento').mask('000,00', {
            reverse: true
        });
    });
</script>