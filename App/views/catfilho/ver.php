<?php
echo mensagens($data['mensagem']);
$data = $data['registros'];
$data = $data[0];
?>
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6 heading" style="margin-bottom: -1%;">
                <h4>Detalhes da Categoria Filho</h4>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="/catfilho/index" style="color:green">Index</a></li>
                    <li class="breadcrumb-item active">Detalhamento</li>
                </ol>
            </div>
        </div>
    </div>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="card card-purple card-outline card-outline-tabs">
                <div class="card-header p-0 pt-1 border-bottom-0">
                    <ul class="nav nav-tabs" role="tablist">
                        <a href="/catfilho/index" class="nav-link"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</a>
                        <li class="nav-item"><a class="nav-link active" href="#editar" data-toggle="tab">Editar</a></li>
                    </ul>
                </div>
                <div class="card-body">
                    <div class="tab-content">
                        <div class="active tab-pane" id="editar">
                            <section class="content">
                                <div class="row">
                                    <div class="card-body">
                                        <form action="/catfilho/editar/<?php echo $data['cod_cat_f']; ?>" method="post">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Nome Categoria Filho *</label>
                                                        <input type="text" class="form-control" name="nome_cat_f" id="nome_cat_f" value="<?php echo $data['nome_cat_f']; ?>" required="" autocomplete="off">
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label> Código Categoria Filho</label>
                                                        <input type="text" class="form-control" name="cod_cat_f" id="cod_cat_f" value="<?php echo $data['cod_cat_f']; ?>" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <div class="custom-control custom-switch">
                                                            <input type="checkbox" class="custom-control-input" <?php if ($data['ind_ativo'] == 0) echo "checked" ?> id="ind_ativo" name="ind_ativo">
                                                            <label class="custom-control-label" for="ind_ativo" style="margin-top:25%;">Categoria Ativa</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-footer">
                                                <input type="submit" name="Atualizar" class="btn btn-purple btn-md float-right" value="Salvar">
                                                <a href="/catfilho/index" class="btn btn-orange btn-md float-right" style="margin-right:4px;">Cancelar</a>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>