<?php
echo mensagens($data['mensagem']);
?>
<section class="content-header">
    <div class="container">
    </div>
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6 heading" style="margin-bottom: -1%;">
                <h4>Cadastro de Categoria Filho</h4>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a style="color: green" href="/catfilho/index">Index</a></li>
                    <li class="breadcrumb-item active">Cadastro</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<div class="container-fluid">
    <form action="/catfilho/cadastrar" method="post">
        <div class="col-md-6">
            <div class="card card-default">
                <div class="card-header">
                    <a href="/catfilho/index" class="btn btn-default btn-md float-left" style="margin-left:5px;"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</a>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Nome Categoria Filho *</label>
                                <input class="form-control col-md-12" name="nome_cat_f" id="nome_cat_f" type="text" maxlength="20" required="" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Código Categoria Pai</label>
                                <input class="form-control col-md-12" name="cod_cat_f" id="cod_cat_f" value="<?php echo $data['registros']['last_cod'] + 1; ?>" readonly type="text" required="">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" checked id="ind_ativo" name="ind_ativo">
                                    <label class="custom-control-label" for="ind_ativo" style="margin-top:25%;">Categoria Ativa</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <input type="submit" name="Cadastrar" class="btn btn-purple float-right" value="Cadastrar">
                    <a href="/catfilho/index" class="btn btn-orange btn-md float-right" style="margin-right:5px;">Cancelar</a>
                </div>
            </div>
        </div>
    </form>
</div>