<!-- Content Header (Page header)  -->
<section class="content-header">
    <div class="container" style="background: pink;">
        <?php
        if (!empty($data['mensagem'])) :
            foreach ($data['mensagem'] as $m) :
                echo $m . "<br>";
            endforeach;
        endif;
        ?>
    </div>
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h4>Resumo Estabelecimento</h4>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a style="color: indigo" href="#">Index</a></li>
                    <li class="breadcrumb-item active"></li>
                </ol>
            </div>
        </div>
    </div>
</section>
<!-- Main Content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="btn-group">
                        <div> <a href="/resestabs/cadastrar" type="button" class="btn btn-block btn-purple btn-sm">Adicionar Estabelecimento</a> </div>
                        &nbsp
                        <div> <a href="/resestabs/exportar" type="button" class="btn btn-block btn-orange btn-sm">Exportar</a> </div>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <table id="table" class="display responsive nowrap" style="width:100%">
                                <thead>
                                    <tr>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">Atalhos</th> <!-- botões -->
                                        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">CNPJ Estabelecimento</th>
                                        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Data Movimento</th>
                                        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Valor Real</th>
                                    </tr>
                                </thead>
                                <!-- tbody -->
                                <tbody>
                                    <?php $res_estabEmpresa = $this->model('ResEstab'); ?>
                                    <?php foreach ($data['registros'] as $value) : ?>
                                        <?php foreach ($res_estabEmpresa->getAllEstabelecimento($value['empresacnpj_estab']) as $value) : ?>
                                            <tr>
                                                <td>
                                                    <a title="Detalhar" href="/resestabs/ver/<?php echo $value['res_estabid']; ?>" type="button" class="btn btn-purple btn-sm"><i class="fas fa-eye"></i></a> <!-- visualizar -->
                                                    <a title="Excluir" onclick="verificadel()" href="/resestabs/excluir/<?php echo $value['res_estabid']; ?>" type="button" class="btn btn-purple btn-sm"><i class="fas fa-trash"></i></a> <!-- excluir -->
                                                </td>
                                                <td>
                                                    <?php
                                                    if (strlen($value['res_estabcnpj_estab'] <= 10)) {
                                                        echo $value['res_estabcnpj_estab'];
                                                    } else {
                                                        echo Mask('##.###.###/####-##', $value['res_estabcnpj_estab']);
                                                    }
                                                    ?>
                                                </td>
                                                <td><?php echo implode("/", array_reverse(explode("-", $value['res_estabdt_movto']))); ?></td>
                                                <td><?php echo number_format($value['res_estabvl_real'], 2, ',', '.'); ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
</section>
<script>
    $(document).ready(function() {
        $('#table').DataTable({
            scrollY: 300,
            paging: false
        });
    });
    function verificadel() {
        var x;
        var r = confirm("Deseja excluir a linha?");
        if (r == true) {} else {
            event.preventDefault();
        }
    }
</script>