<div class="x_panel">
    <div class="x_title">
        <h2>Unidades Medidas [Logs]</h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <?php
        if (!empty($data['mensagem'])):

            foreach ($data['mensagem'] as $m):
                echo $m . "<br>";
            endforeach;

        endif;
        ?>

        <div class="table-responsive">
            <table id="datatable" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>Estabelecimento</th>
                    <th>Unid. Med.</th>
                    <th>Data</th>
                    <th>Hora</th>
                    <th>Usuário</th>
                    <th>titulo</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($data['registros'] as $unid_med_log): ?>
                    <tr>
                        <td>
                            <?php
                            if(strlen($unid_med_log['cnpj_estab'] < 11)) {
                                echo $unid_med_log['cnpj_estab'];
                            }else{
                                echo Mask('##.###.###/####-##',$unid_med_log['cnpj_estab']);
                            }
                            ?>
                        </td>
                        <td><?php echo $unid_med_log['unidcod_unid_med']; ?></td>
                        <td><?php echo implode("/", array_reverse(explode("-",$unid_med_log['dt_emis']))); ?></td>
                        <td><?php echo $unid_med_log['hr_emis']; ?></td>
                        <td><?php echo $unid_med_log['usernome_user']; ?></td>
                        <td><?php echo $unid_med_log['titulo']; ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <div class="row">
            <a href="/unidmeds/index" class="btn btn-success">Voltar</a>
        </div>
    </div>
</div>





