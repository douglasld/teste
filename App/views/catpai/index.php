<!-- Content Header (Page header)  -->
<section class="content-header">
    <div class="container" style="background: pink;">
        <?php
        echo mensagens($data['mensagem']);
        ?>
    </div>
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6 heading" style="margin-bottom: -1%;">
                <h4>Categoria Pai</h4>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a style="color: green" href="catpai/index">Index</a></li>
                    <li class="breadcrumb-item active"></li>
                </ol>
            </div>
        </div>
    </div>
</section>
<!-- Main Content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="btn-group">
                        <div> <a href="/catpai/cadastrar" type="button" class="btn btn-block btn-green btn-sm"><i class="fas fa-plus"></i> Categoria Pai</a> </div>
                    </div>
                </div>
                <div class="card-body">
                    <table id="table" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Atalhos</th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Código</th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Nome</th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Data Criação</th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Ativo</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($data['registros'] as $value) : ?>
                                <tr>
                                    <td>
                                        <a title="Detalhar" href="/catpai/ver/<?php echo $value['cod_cat_p']; ?>" type="button" class="btn btn-green btn-sm"><i class="fas fa-eye"></i></a> <!-- visualizar -->
                                        <button title="Excluir" value="<?=$value['cod_cat_p'];?>" href="#"type="button" class="btn btn-danger btn-sm excluir"><i class="fas fa-trash"></i></button> <!-- excluir -->
                                    </td>
                                    <td><?php echo  $value['cod_cat_p']; ?></td>
                                    <td><?php echo  $value['nome_cat_p']; ?></td>
                                    <td><?php echo implode("/", array_reverse(explode("-", $value['dt_criacao']))); ?></td>
                                    <td><?php echo $value['ind_ativo'] == 0 ? "Ativo" : "Desativado"; ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
</section>
<script language="JavaScript" src='<?= URL_BASE ?>vendor/functions/functions_catpai.js'></script>
<script>
    $('#table').on('click', '.excluir', function() {
        //seta variaveis
        var html = this;
        var cod = this.value;
        Swal.fire({
            title: "Deseja Excluir?",
            text: "Não será possível reverter esta ação!",
            icon: 'warning',
            showCancelButton: true,
            cancelButtonColor: '#3085d6',
            confirmButtonColor: '#d33',
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Excluir',
        }).then((result) => {
            if (result.value) {
                $.post('<?= $_SESSION['prod'] ?>/ajax/verifica_catpai.php', {
                    cod: cod,
                    func: '1'
                }, function(data) {
                    if (data > 0) {
                        Swal.fire({
                            title: "Ooops!",
                            text: "Existem produtos cadastrados para essa categoria!",
                            icon: 'warning',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonText: 'Ok',
                        })
                    } else {
                        $.post('<?= $_SESSION['prod'] ?>/ajax/verifica_catpai.php', {
                            cod: cod,
                            func: '2'
                        }, function(data) {
                            if (data > 0) {
                                Swal.fire({
                                    title: "Ooops!",
                                    text: "Erro ao excluir essa categoria!",
                                    icon: 'error',
                                    showCancelButton: false,
                                    confirmButtonColor: '#3085d6',
                                    cancelButtonText: 'Ok',
                                })
                            } else {
                                Swal.fire({
                                    title: "Sucesso!",
                                    text: "Categoria Excluída com sucesso!",
                                    icon: 'success',
                                    showCancelButton: false,
                                    confirmButtonColor: '#3085d6',
                                    cancelButtonText: 'Ok',
                                })
                                $(html).parent().parent().remove();
                            }
                        }, 'json');
                    }
                }, 'json');
            }
        });
    });
</script>