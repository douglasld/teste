<!-- Content Header (Page header)  -->
<section class="content-header">
    <div class="container" style="background: lavender;">
        <?php
        if (!empty($data['mensagem'])) :
            foreach ($data['mensagem'] as $m) :
                echo $m . "<br>";
            endforeach;
        endif;
        $estabelecimentos = $this->model('Empresa');
        ?>
    </div>
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h4>Cadastro de Estabelecimentos</h4>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a style="color: indigo" href="#">Index</a></li>
                    <li class="breadcrumb-item active">Cadastro</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="card card-default">
                <form action="/empresas/cadastrar" method="post">
                    <div class="box-body">
                        <div class="box-body">
                            <div class="card-header">
                                <a href="/empresas/index" class="btn btn-default btn-md float-left" style="margin:5px;">Listar</a>
                                <h3 class="card-title" style="margin-top:15px;">Dados Fiscais </h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>CNPJ Matriz</label>
                                            <input name="cnpj_matriz" id="cnpj_matriz" value="<?php echo preg_replace("/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/", "\$1.\$2.\$3/\$4-\$5", $_SESSION['matriz']); ?>" maxlength="14" type="text" class="form-control" required="required">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>CNPJ Estabelecimento</label>
                                            <input class="form-control col-md-12" name="cnpj_estab" id="cnpj_estab" type="text" required="">
                                            </input>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label> Nome Abreviado</label>
                                            <input name="nome_abrev" id="nome_abrev" type="text" maxlength="12" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label> Razão Social</label>
                                            <input name="razao_social" id="razao_social" type="text" maxlength="150" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label> Ramo Atividade</label>
                                            <input name="ramo_atividade" id="ramo_atividade" type="text" maxlength="45" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label> URL</label>
                                            <input name="url" id="url" type="text" class="form-control" maxlength="60">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Celular</label>
                                            <input name="celular" id="celular" onkeyup="mascara( this, mtel );" maxlength="15" type="text" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Telefone</label>
                                            <input name="telefone" id="celular" maxlength="14" type="text" onkeyup="mascara( this, mtel );" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label> E-mail</label>
                                            <input name="e_mail" id="e_mail" type="email" class="form-control" maxlength="60">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label> E-mail-NFE</label>
                                            <input name="e_mail_nfe" id="e_mail_nfe" type="text" class="form-control" maxlength="60">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card card-default">
                <div class="card-header">
                    <h3 class="card-title">Endereço </h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label> Rua</label>
                                <input name="rua" id="rua" type="text" class="form-control" maxlength="60">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label> Número</label>
                                <input name="numero" id="numero" type="number" class="form-control" maxlength="6">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label> Bairro</label>
                                <input name="bairro" id="bairro" type="text" class="form-control" maxlength="45">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label> Complemento</label>
                                <input name="complemento" id="complemento" type="text" class="form-control" maxlength="60">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label> Cidade</label>
                                <input name="cidade" id="cidade" type="text" class="form-control" maxlength="60">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label> Estado</label>
                                <input name="estado" id="estado" type="text" class="form-control" maxlength="4">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label> País<s></s></label>
                                <input name="pais" id="pais" type="text" class="form-control" maxlength="60">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>CEP</label>
                                <input name="cep" type="text" id="cep" onfocus="javascript: retirarFormatacaocep(this);"  onblur="javascript: formatarCampocep(this);" maxlength="8" type="text" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="exampleInputFile">Logo </label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" name="foo" class="custom-file-input" id="exampleInputFile">
                                        <label class="custom-file-label" for="exampleInputFile">Escolher arquivo</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="custom-control custom-switch">
                                <input type="checkbox" name="ind_ativo" checked class="custom-control-input" id="ind_ativo">
                                <label class="custom-control-label"  for="ind_ativo">Empresa Ativa</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <input type="submit" name="Cadastrar" class="btn btn-purple btn-md float-right" value="Salvar">
                    <a href="/empresas/index" class="btn btn-orange btn-md float-right" style="margin-right:3px;">Cancelar</a>
                </div>
                </form>
            </div>
        </div>
    </div>
</section>
</div>
<script>
    function formatarCampo(campoTexto) {
        campoTexto.value = mascaraCnpj(campoTexto.value);
    }

    function retirarFormatacao(campoTexto) {
        campoTexto.value = campoTexto.value.replace(/(\.|\/|\-)/g, "");
    }

    function mascaraCnpj(valor) {
        return valor.replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/g, "\$1.\$2.\$3\/\$4\-\$5");
    }
</script>
<!-- MÁSCARA CEP -->
<script>
    function formatarCampocep(campoTexto) {
        campoTexto.value = mascaracep(campoTexto.value);
    }

    function retirarFormatacaocep(campoTexto) {
        campoTexto.value = campoTexto.value.replace(/(\.|\/|\-)/g, "");
    }

    function mascaracep(valor) {
        return valor.replace(/(\d{2})(\d{3})(\d{3})/g, "\$1.\$2-\$3");
    }
</script>
<script>
    /* Máscaras ER */
    function mascara(o, f) {
        v_obj = o
        v_fun = f
        setTimeout("execmascara()", 1)
    }

    function execmascara() {
        v_obj.value = v_fun(v_obj.value)
    }

    function mtel(v) {
        v = v.replace(/\D/g, ""); //Remove tudo o que não é dígito
        v = v.replace(/^(\d{2})(\d)/g, "($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos
        v = v.replace(/(\d)(\d{4})$/, "$1-$2"); //Coloca hífen entre o quarto e o quinto dígitos
        return v;
    }
</script>
<!-- MÁSCARA TELEFONE -->
<script>
    /* Máscaras ER */
    function mascarat(o, f) {
        v_obj = o
        v_fun = f
        setTimeout("execmascara()", 1)
    }

    function execmascarat() {
        v_obj.value = v_fun(v_obj.value)
    }

    function mtelt(v) {
        v = v.replace(/\D/g, ""); //Remove tudo o que não é dígito
        v = v.replace(/(\d)(\d{4})$/, "$1-$2"); //Coloca hífen entre o quarto e o quinto dígitos
        return v;
    }

    $(document).ready(function() {
        $('#cnpj_estab').mask('00.000.000/0000-00', {
            reverse: true
        });
    });

    
    $(document).ready(function() {
        $('#cnpj_matriz').mask('00.000.000/0000-00', {
            reverse: true
        });
    });
</script>