<style>
    .nav-tabs .nav-item .nav-link {
        color: #6039A8 !important;
    }
</style>
<!-- Content Header (Page header)  -->
<section class="content-header">
    <div class="container" style="background: pink;">
        <?php
        if (!empty($data['mensagem'])) :

            foreach ($data['mensagem'] as $m) :
                echo $m . "<br>";
            endforeach;
        endif;
        $estabelecimentos = $this->model('Empresa');
        ?>
    </div>
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h4>Detalhes Estabelecimento</h4>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="/empresas/index" style="color:indigo">Index</a></li>
                    <li class="breadcrumb-item active">Detalhamento</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="row">
        <div class="col-md-8">
            <div class="card card-purple card-outline card-outline-tabs">
                <div class="card-header p-0 pt-1 border-bottom-0">
                    <ul class="nav nav-tabs" role="tablist">
                        <a href="/empresas/index" class="nav-link" style="margin-left:5px;">Listar</a>
                        <li class="nav-item"><a class="nav-link active" href="#editar" data-toggle="tab">Editar</a></li>
                        <!-- <li class="nav-item"><a class="nav-link " href="#resumo" data-toggle="tab">Resumo</a></li> -->
                        <li class="nav-item"><a class="nav-link" href="#logs" data-toggle="tab">Logs</a></li>
                        <li class="nav-item"><a class="nav-link" href="#unid_neg" data-toggle="tab">Unidade de Negócios</a></li>
                        <li class="nav-item"><a class="nav-link" href="#exportar'" data-toggle="tab">Exportar</a></li>                    
                    </ul>
                </div><!-- /.card-header -->
                <div class="card-body">
                    <div class="tab-content">
                        <div class="active tab-pane" id="editar">
                            <section class="content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="invoice p-3 mb-3">
                                            <div class="card-body">
                                                <form action="/empresas/editar/<?php echo $data['empresa']['empresaid']; ?>" method="post" class="form-horizontal form-label-left">
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <label>CNPJ Mdatriz</label>
                                                                <input name="cnpj_matriz" readonly id="cnpj_matriz" value="<?php echo preg_replace("/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/", "\$1.\$2.\$3/\$4-\$5", $data['empresa']['empresacnpj_matriz']);?>" maxlength="14" type="text" class="form-control" placeholder="00.000.000/000-00" required="required">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                        <div class="form-group">
                                                        <label>CNPJ Estabelecimento</label>
                                                        <?php foreach ($estabelecimentos->getPorMatriz($_SESSION['matriz']) as $value) : ?>
                                                            <?php
                                                            if ($value['empresacnpj_estab'] == $data['empresa']['empresacnpj_estab']) {
                                                                $empresa_abrev_value = $value['empresanome_abrev'];
                                                            break;                                                           
                                                            };
                                                            ?>
                                                        <?php endforeach; ?>
                                                        <?php
                                                        $cnpj_value = $data['empresa']['empresacnpj_estab'];
                                                        $cnpj_value = preg_replace("/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/", "\$1.\$2.\$3/\$4-\$5", $cnpj_value);
                                                        ?>
                                                        <input class="form-control col-md-12" readonly value="<?php echo $cnpj_value . ' - ' . $empresa_abrev_value; ?>" name="cnpj_estab" id="cnpj_estab" list="list" autocomplete="off" required="">
                                                        <datalist id="list">
                                                            <?php foreach ($estabelecimentos->getPorMatriz($_SESSION['matriz']) as $value) : ?>
                                                                <?php
                                                                $cnpj_cpf = $value['empresacnpj_estab'];
                                                                $cnpj = preg_replace("/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/", "\$1.\$2.\$3/\$4-\$5", $cnpj_cpf);
                                                                ?>
                                                                <option value="<?php echo $cnpj . ' - ' . $value['empresanome_abrev']; ?>"></option>
                                                            <?php endforeach; ?>
                                                        </datalist>
                                                        </input>
                                                    </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <label> Nome Abreviado</label>
                                                                <input name="nome_abrev" readonly id="nome_abrev" value="<?php echo $data['empresa']['empresanome_abrev']; ?>" type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label> Razão Social</label>
                                                                <input name="razao_social" id="razao_social" maxlength="12" value="<?php echo $data['empresa']['empresarazao_social']; ?>" type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label> Ramo Atividade</label>
                                                                <input name="ramo_atividade" id="ramo_atividade" value="<?php echo $data['empresa']['empresaramo_atividade']; ?>" type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <label> Rua</label>
                                                                <input name="rua" id="rua" value="<?php echo $data['empresa']['empresarua']; ?>" type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <label> Número</label>
                                                                <input name="numero" id="numero" value="<?php echo $data['empresa']['empresanumero']; ?>" type="number" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <label> Bairro</label>
                                                                <input name="bairro" id="bairro" value="<?php echo $data['empresa']['empresabairro']; ?>" type="text" class="form-control">
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <label> Complemento</label>
                                                                <input name="complemento" id="complemento" value="<?php echo $data['empresa']['empresacomplemento']; ?>" type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <label> Cidade</label>
                                                                <input name="cidade" id="cidade" value="<?php echo $data['empresa']['empresacidade']; ?>" type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <label> Estado</label>
                                                                <input name="estado" id="estado" maxlength="2" value="<?php echo $data['empresa']['empresaestado']; ?>" type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <label> País<s></s></label>
                                                                <input name="pais" id="pais" value="<?php echo $data['empresa']['empresapais']; ?>" type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <label>CEP</label>
                                                                <input name="cep" type="text" id="cep" value="<?php echo $data['empresa']['empresacep']; ?>" onblur="pesquisacep(this.value);" onfocus="javascript: retirarFormatacaocep(this);" onblur="javascript: formatarCampocep(this);" maxlength="8" type="text" class="form-control" placeholder="89211-000">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label> E-mail</label>
                                                                <input name="e_mail" id="e_mail" value="<?php echo $data['empresa']['empresae_mail']; ?>" type="email" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label> E-mail-NFE</label>
                                                                <input name="e_mail_nfe" id="e_mail_nfe" value="<?php echo $data['empresa']['empresae_mail_nfe']; ?>" type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <label>Celular</label>
                                                                <input name="celular" value="<?php echo $data['empresa']['empresacelular']; ?>" id="celular" onkeyup="mascara( this, mtel );" maxlength="15" type="text" class="form-control" placeholder="(99) 99999-9999 ">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <label>Telefone</label>
                                                                <input name="telefone" value="<?php echo $data['empresa']['empresatelefone']; ?>" onkeyup="mascarat( this, mtelt );" id="telefone" maxlength="9" type="text" class="form-control" placeholder="9999-9999 ">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label> URL</label>
                                                                <input name="url" id="url" value="<?php echo $data['empresa']['empresaurl']; ?>" type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="exampleInputFile">Logo </label>
                                                                <div class="input-group">
                                                                    <div class="custom-file">
                                                                        <input type="file" name="foo" class="custom-file-input" id="exampleInputFile">
                                                                        <label class="custom-file-label" for="exampleInputFile">Escolher arquivo</label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="row">
                                                        <?php
                                                        if ($data['empresa']['empresaind_ativo'] == 0) {
                                                            $data['empresa']['empresaind_ativo'] = "checked";
                                                        } else {
                                                            $data['empresa']['empresaind_ativo'] = "";
                                                        }
                                                        ?>
                                                        <div class="col-sm-6">
                                                            <div class="custom-control custom-switch">
                                                                <div class="custom-control custom-switch">

                                                                    <input type="checkbox" name="ind_ativo" <?php echo $data['empresa']['empresaind_ativo'] ?> class="custom-control-input" id="ind_ativo">
                                                                    <label class="custom-control-label" for="ind_ativo">Empresa Ativa</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card-footer">
                                                        <input type="submit" name="Atualizar" class="btn btn-purple btn-md float-right" value="Salvar">
                                                        <a href="/empresas/index" class="btn btn-orange btn-md float-right" style="margin-right:4px;">Cancelar</a>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                            </section>
                        </div> <!-- /.tab-pane // fecha editar-->
                        <div class="tab-pane" id="logs">
                            <section class="invoice p-3 mb-3">
                                <table id="table_logs" class="display responsive nowrap" style="width:100%">
                                    <thead>
                                        <th>Usuário Cadastro</th>
                                        <th>Data Cadastro</th>
                                        <th>Hora Cadastro</th>
                                        <th>Usuário Alteração</th>
                                        <th>Data Alteração</th>
                                        <th>Hora Alteração</th>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($data['logs'] as $data['logs']) : ?>
                                            <tr>
                                                <td><?php echo $data['logs']['user_perfilnome_user'];  ?></td>
                                                <td><?php echo implode("/", array_reverse(explode("-", $data['logs']['empresadt_criacao']))); ?></td>
                                                <td><?php echo $data['logs']['empresahr_criacao']; ?></td>
                                                <td><?php echo $data['logs']['user_perfilnome_user']; ?> </td>
                                                <td><?php echo implode("/", array_reverse(explode("-", $data['logs']['empresadt_altera']))); ?></td>
                                                <td><?php echo $data['logs']['empresahr_altera']; ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </section>
                        </div> <!-- /.tab-pane -->

                        <div class="tab-pane" id="unid_neg">
                            <section class="invoice p-3 mb-3">
                                <table id="table_unid_neg" class="display responsive nowrap" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Unidade de Negócio</th>
                                            <th>CNPJ Estabelecimentos</th>
                                            <th>Descrição</th>
                                            <th>Ativo</th>
                                            <th>Origem</th>
                                            <th>Data de Criação</th>
                                            <th>User Criação</th>
                                            <th>Data Alteração</th>
                                            <th>User Alteração</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($data['unid_neg'] as $unig_neg) : ?>
                                            <tr>
                                                <td><?php echo $unid_neg['unid_negid']; ?></td>
                                                <td>
                                                    <?php
                                                        if (strlen($unig_neg['unid_negnpj_estab'] < 11)) {
                                                            echo $unig_neg['unid_negnpj_estab'];
                                                        } else {
                                                            echo Mask('##.###.###/####-##', $unig_neg['unid_negnpj_estab']);
                                                        }
                                                        ?>
                                                </td>
                                                <td><?php echo $unig_neg['unid_negdescricao']; ?></td>
                                                <td><?php echo $unig_neg['unid_negind_ativo'] == 0 ? "Sim" : "Não"; ?></td>
                                                <td><?php echo $unig_neg['unid_negind_origem'] == 0 ? "Portal" : "Integração"; ?></td>
                                                <td><?php echo $unig_neg['unid_negdt_criacao']; ?> <?php echo $unig_neg['unid_neghr_criacao']; ?></td>
                                                <td><?php echo $unig_neg['unid_negusercriacao']; ?></td>
                                                <td><?php echo $unig_neg['unid_negdt_altera']; ?> <?php echo $unig_neg['unid_neghr_altera']; ?></td>
                                                <td><?php echo $unig_neg['unid_neguser_altera']; ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </section>
                        </div> <!-- /.tab-pane -->
                    </div> <!-- /.tab-content -->
                </div><!-- /.card-body -->
            </div> <!-- /.nav-tabs-custom -->
        </div> <!-- /.col -->
    </div> <!-- /.row -->
</section>
<script>
    function formatarCampo(campoTexto) {
        campoTexto.value = mascaraCnpj(campoTexto.value);
    }
    function retirarFormatacao(campoTexto) {
        campoTexto.value = campoTexto.value.replace(/(\.|\/|\-)/g, "");
    }
    function mascaraCnpj(valor) {
        return valor.replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/g, "\$1.\$2.\$3\/\$4\-\$5");
    }
</script>

<script>
    $(document).ready(function() {
        $('#table_logs').DataTable({});
        $('#table_unid_neg').DataTable({
            scrollY: 300,
            "sScrollX": "100%",
            "bScrollCollapse": true,
            paging: false
        });
    });

    $(document).ready(function() {
        $('#celular').mask(' (00)0000-0000', {
            reverse: true
        });
    });
    
    $(document).ready(function() {
        $('#telefone').mask('(00)0000-0000', {
            reverse: true
        });
    });
</script><style>
    .nav-tabs .nav-item .nav-link {
        color: #6039A8 !important;
    }
</style>
<!-- Content Header (Page header)  -->
<section class="content-header">
    <div class="container" style="background: pink;">
        <?php
        if (!empty($data['mensagem'])) :

            foreach ($data['mensagem'] as $m) :
                echo $m . "<br>";
            endforeach;
        endif;
        $estabelecimentos = $this->model('Empresa');
        ?>
    </div>
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h4>Detalhes Estabelecimento</h4>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="/empresas/index" style="color:indigo">Index</a></li>
                    <li class="breadcrumb-item active">Detalhamento</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="row">
        <div class="col-md-8">
            <div class="card card-purple card-outline card-outline-tabs">
                <div class="card-header p-0 pt-1 border-bottom-0">
                    <ul class="nav nav-tabs" role="tablist">
                        <a href="/empresas/index" class="nav-link" style="margin-left:5px;">Listar</a>
                        <li class="nav-item"><a class="nav-link active" href="#editar" data-toggle="tab">Editar</a></li>
                        <!-- <li class="nav-item"><a class="nav-link " href="#resumo" data-toggle="tab">Resumo</a></li> -->
                        <li class="nav-item"><a class="nav-link" href="#logs" data-toggle="tab">Logs</a></li>
                        <li class="nav-item"><a class="nav-link" href="#unid_neg" data-toggle="tab">Unidade de Negócios</a></li>
                        <li class="nav-item"><a class="nav-link" href="#exportar'" data-toggle="tab">Exportar</a></li>                    
                    </ul>
                </div><!-- /.card-header -->
                <div class="card-body">
                    <div class="tab-content">
                        <div class="active tab-pane" id="editar">
                            <section class="content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="invoice p-3 mb-3">
                                            <div class="card-body">
                                                <form action="/empresas/editar/<?php echo $data['empresa']['empresaid']; ?>" method="post" class="form-horizontal form-label-left">
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <label>CNPJ Mdatriz</label>
                                                                <input name="cnpj_matriz" readonly id="cnpj_matriz" value="<?php echo preg_replace("/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/", "\$1.\$2.\$3/\$4-\$5", $data['empresa']['empresacnpj_matriz']);?>" maxlength="14" type="text" class="form-control" placeholder="00.000.000/000-00" required="required">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                        <div class="form-group">
                                                        <label>CNPJ Estabelecimento</label>
                                                        <?php foreach ($estabelecimentos->getPorMatriz($_SESSION['matriz']) as $value) : ?>
                                                            <?php
                                                            if ($value['empresacnpj_estab'] == $data['empresa']['empresacnpj_estab']) {
                                                                $empresa_abrev_value = $value['empresanome_abrev'];
                                                            break;                                                           
                                                            };
                                                            ?>
                                                        <?php endforeach; ?>
                                                        <?php
                                                        $cnpj_value = $data['empresa']['empresacnpj_estab'];
                                                        $cnpj_value = preg_replace("/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/", "\$1.\$2.\$3/\$4-\$5", $cnpj_value);
                                                        ?>
                                                        <input class="form-control col-md-12" readonly value="<?php echo $cnpj_value . ' - ' . $empresa_abrev_value; ?>" name="cnpj_estab" id="cnpj_estab" list="list" autocomplete="off" required="">
                                                        <datalist id="list">
                                                            <?php foreach ($estabelecimentos->getPorMatriz($_SESSION['matriz']) as $value) : ?>
                                                                <?php
                                                                $cnpj_cpf = $value['empresacnpj_estab'];
                                                                $cnpj = preg_replace("/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/", "\$1.\$2.\$3/\$4-\$5", $cnpj_cpf);
                                                                ?>
                                                                <option value="<?php echo $cnpj . ' - ' . $value['empresanome_abrev']; ?>"></option>
                                                            <?php endforeach; ?>
                                                        </datalist>
                                                        </input>
                                                    </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <label> Nome Abreviado</label>
                                                                <input name="nome_abrev" readonly id="nome_abrev" value="<?php echo $data['empresa']['empresanome_abrev']; ?>" type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label> Razão Social</label>
                                                                <input name="razao_social" id="razao_social" maxlength="12" value="<?php echo $data['empresa']['empresarazao_social']; ?>" type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label> Ramo Atividade</label>
                                                                <input name="ramo_atividade" id="ramo_atividade" value="<?php echo $data['empresa']['empresaramo_atividade']; ?>" type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <label> Rua</label>
                                                                <input name="rua" id="rua" value="<?php echo $data['empresa']['empresarua']; ?>" type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <label> Número</label>
                                                                <input name="numero" id="numero" value="<?php echo $data['empresa']['empresanumero']; ?>" type="number" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <label> Bairro</label>
                                                                <input name="bairro" id="bairro" value="<?php echo $data['empresa']['empresabairro']; ?>" type="text" class="form-control">
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <label> Complemento</label>
                                                                <input name="complemento" id="complemento" value="<?php echo $data['empresa']['empresacomplemento']; ?>" type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <label> Cidade</label>
                                                                <input name="cidade" id="cidade" value="<?php echo $data['empresa']['empresacidade']; ?>" type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <label> Estado</label>
                                                                <input name="estado" id="estado" maxlength="2" value="<?php echo $data['empresa']['empresaestado']; ?>" type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <label> País<s></s></label>
                                                                <input name="pais" id="pais" value="<?php echo $data['empresa']['empresapais']; ?>" type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <label>CEP</label>
                                                                <input name="cep" type="text" id="cep" value="<?php echo $data['empresa']['empresacep']; ?>" onblur="pesquisacep(this.value);" onfocus="javascript: retirarFormatacaocep(this);" onblur="javascript: formatarCampocep(this);" maxlength="8" type="text" class="form-control" placeholder="89211-000">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label> E-mail</label>
                                                                <input name="e_mail" id="e_mail" value="<?php echo $data['empresa']['empresae_mail']; ?>" type="email" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label> E-mail-NFE</label>
                                                                <input name="e_mail_nfe" id="e_mail_nfe" value="<?php echo $data['empresa']['empresae_mail_nfe']; ?>" type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <label>Celular</label>
                                                                <input name="celular" value="<?php echo $data['empresa']['empresacelular']; ?>" id="celular" onkeyup="mascara( this, mtel );" maxlength="15" type="text" class="form-control" placeholder="(99) 99999-9999 ">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <label>Telefone</label>
                                                                <input name="telefone" value="<?php echo $data['empresa']['empresatelefone']; ?>" onkeyup="mascarat( this, mtelt );" id="telefone" maxlength="9" type="text" class="form-control" placeholder="9999-9999 ">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label> URL</label>
                                                                <input name="url" id="url" value="<?php echo $data['empresa']['empresaurl']; ?>" type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="exampleInputFile">Logo </label>
                                                                <div class="input-group">
                                                                    <div class="custom-file">
                                                                        <input type="file" name="foo" class="custom-file-input" id="exampleInputFile">
                                                                        <label class="custom-file-label" for="exampleInputFile">Escolher arquivo</label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="row">
                                                        <?php
                                                        if ($data['empresa']['empresaind_ativo'] == 0) {
                                                            $data['empresa']['empresaind_ativo'] = "checked";
                                                        } else {
                                                            $data['empresa']['empresaind_ativo'] = "";
                                                        }
                                                        ?>
                                                        <div class="col-sm-6">
                                                            <div class="custom-control custom-switch">
                                                                <div class="custom-control custom-switch">

                                                                    <input type="checkbox" name="ind_ativo" <?php echo $data['empresa']['empresaind_ativo'] ?> class="custom-control-input" id="ind_ativo">
                                                                    <label class="custom-control-label" for="ind_ativo">Empresa Ativa</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card-footer">
                                                        <input type="submit" name="Atualizar" class="btn btn-purple btn-md float-right" value="Salvar">
                                                        <a href="/empresas/index" class="btn btn-orange btn-md float-right" style="margin-right:4px;">Cancelar</a>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                            </section>
                        </div> <!-- /.tab-pane // fecha editar-->
                        <div class="tab-pane" id="logs">
                            <section class="invoice p-3 mb-3">
                                <table id="table_logs" class="display responsive nowrap" style="width:100%">
                                    <thead>
                                        <th>Usuário Cadastro</th>
                                        <th>Data Cadastro</th>
                                        <th>Hora Cadastro</th>
                                        <th>Usuário Alteração</th>
                                        <th>Data Alteração</th>
                                        <th>Hora Alteração</th>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($data['logs'] as $data['logs']) : ?>
                                            <tr>
                                                <td><?php echo $data['logs']['user_perfilnome_user'];  ?></td>
                                                <td><?php echo implode("/", array_reverse(explode("-", $data['logs']['empresadt_criacao']))); ?></td>
                                                <td><?php echo $data['logs']['empresahr_criacao']; ?></td>
                                                <td><?php echo $data['logs']['user_perfilnome_user']; ?> </td>
                                                <td><?php echo implode("/", array_reverse(explode("-", $data['logs']['empresadt_altera']))); ?></td>
                                                <td><?php echo $data['logs']['empresahr_altera']; ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </section>
                        </div> <!-- /.tab-pane -->

                        <div class="tab-pane" id="unid_neg">
                            <section class="invoice p-3 mb-3">
                                <table id="table_unid_neg" class="display responsive nowrap" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Unidade de Negócio</th>
                                            <th>CNPJ Estabelecimentos</th>
                                            <th>Descrição</th>
                                            <th>Ativo</th>
                                            <th>Origem</th>
                                            <th>Data de Criação</th>
                                            <th>User Criação</th>
                                            <th>Data Alteração</th>
                                            <th>User Alteração</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($data['unid_neg'] as $unig_neg) : ?>
                                            <tr>
                                                <td><?php echo $unid_neg['unid_negid']; ?></td>
                                                <td>
                                                    <?php
                                                        if (strlen($unig_neg['unid_negnpj_estab'] < 11)) {
                                                            echo $unig_neg['unid_negnpj_estab'];
                                                        } else {
                                                            echo Mask('##.###.###/####-##', $unig_neg['unid_negnpj_estab']);
                                                        }
                                                        ?>
                                                </td>
                                                <td><?php echo $unig_neg['unid_negdescricao']; ?></td>
                                                <td><?php echo $unig_neg['unid_negind_ativo'] == 0 ? "Sim" : "Não"; ?></td>
                                                <td><?php echo $unig_neg['unid_negind_origem'] == 0 ? "Portal" : "Integração"; ?></td>
                                                <td><?php echo $unig_neg['unid_negdt_criacao']; ?> <?php echo $unig_neg['unid_neghr_criacao']; ?></td>
                                                <td><?php echo $unig_neg['unid_negusercriacao']; ?></td>
                                                <td><?php echo $unig_neg['unid_negdt_altera']; ?> <?php echo $unig_neg['unid_neghr_altera']; ?></td>
                                                <td><?php echo $unig_neg['unid_neguser_altera']; ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </section>
                        </div> <!-- /.tab-pane -->
                    </div> <!-- /.tab-content -->
                </div><!-- /.card-body -->
            </div> <!-- /.nav-tabs-custom -->
        </div> <!-- /.col -->
    </div> <!-- /.row -->
</section>
<script>
    function formatarCampo(campoTexto) {
        campoTexto.value = mascaraCnpj(campoTexto.value);
    }
    function retirarFormatacao(campoTexto) {
        campoTexto.value = campoTexto.value.replace(/(\.|\/|\-)/g, "");
    }
    function mascaraCnpj(valor) {
        return valor.replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/g, "\$1.\$2.\$3\/\$4\-\$5");
    }
</script>

<script>
    $(document).ready(function() {
        $('#table_logs').DataTable({});
        $('#table_unid_neg').DataTable({
            scrollY: 300,
            "sScrollX": "100%",
            "bScrollCollapse": true,
            paging: false
        });
    });

    $(document).ready(function() {
        $('#celular').mask(' (00)0000-0000', {
            reverse: true
        });
    });
    
    $(document).ready(function() {
        $('#telefone').mask('(00)0000-0000', {
            reverse: true
        });
    });
</script><style>
    .nav-tabs .nav-item .nav-link {
        color: #6039A8 !important;
    }
</style>
<!-- Content Header (Page header)  -->
<section class="content-header">
    <div class="container" style="background: pink;">
        <?php
        if (!empty($data['mensagem'])) :

            foreach ($data['mensagem'] as $m) :
                echo $m . "<br>";
            endforeach;
        endif;
        $estabelecimentos = $this->model('Empresa');
        ?>
    </div>
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h4>Detalhes Estabelecimento</h4>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="/empresas/index" style="color:indigo">Index</a></li>
                    <li class="breadcrumb-item active">Detalhamento</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="row">
        <div class="col-md-8">
            <div class="card card-purple card-outline card-outline-tabs">
                <div class="card-header p-0 pt-1 border-bottom-0">
                    <ul class="nav nav-tabs" role="tablist">
                        <a href="/empresas/index" class="nav-link" style="margin-left:5px;">Listar</a>
                        <li class="nav-item"><a class="nav-link active" href="#editar" data-toggle="tab">Editar</a></li>
                        <!-- <li class="nav-item"><a class="nav-link " href="#resumo" data-toggle="tab">Resumo</a></li> -->
                        <li class="nav-item"><a class="nav-link" href="#logs" data-toggle="tab">Logs</a></li>
                        <li class="nav-item"><a class="nav-link" href="#unid_neg" data-toggle="tab">Unidade de Negócios</a></li>
                        <li class="nav-item"><a class="nav-link" href="#exportar'" data-toggle="tab">Exportar</a></li>                    
                    </ul>
                </div><!-- /.card-header -->
                <div class="card-body">
                    <div class="tab-content">
                        <div class="active tab-pane" id="editar">
                            <section class="content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="invoice p-3 mb-3">
                                            <div class="card-body">
                                                <form action="/empresas/editar/<?php echo $data['empresa']['empresaid']; ?>" method="post" class="form-horizontal form-label-left">
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <label>CNPJ Mdatriz</label>
                                                                <input name="cnpj_matriz" readonly id="cnpj_matriz" value="<?php echo preg_replace("/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/", "\$1.\$2.\$3/\$4-\$5", $data['empresa']['empresacnpj_matriz']);?>" maxlength="14" type="text" class="form-control" placeholder="00.000.000/000-00" required="required">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                        <div class="form-group">
                                                        <label>CNPJ Estabelecimento</label>
                                                        <?php foreach ($estabelecimentos->getPorMatriz($_SESSION['matriz']) as $value) : ?>
                                                            <?php
                                                            if ($value['empresacnpj_estab'] == $data['empresa']['empresacnpj_estab']) {
                                                                $empresa_abrev_value = $value['empresanome_abrev'];
                                                            break;                                                           
                                                            };
                                                            ?>
                                                        <?php endforeach; ?>
                                                        <?php
                                                        $cnpj_value = $data['empresa']['empresacnpj_estab'];
                                                        $cnpj_value = preg_replace("/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/", "\$1.\$2.\$3/\$4-\$5", $cnpj_value);
                                                        ?>
                                                        <input class="form-control col-md-12" readonly value="<?php echo $cnpj_value . ' - ' . $empresa_abrev_value; ?>" name="cnpj_estab" id="cnpj_estab" list="list" autocomplete="off" required="">
                                                        <datalist id="list">
                                                            <?php foreach ($estabelecimentos->getPorMatriz($_SESSION['matriz']) as $value) : ?>
                                                                <?php
                                                                $cnpj_cpf = $value['empresacnpj_estab'];
                                                                $cnpj = preg_replace("/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/", "\$1.\$2.\$3/\$4-\$5", $cnpj_cpf);
                                                                ?>
                                                                <option value="<?php echo $cnpj . ' - ' . $value['empresanome_abrev']; ?>"></option>
                                                            <?php endforeach; ?>
                                                        </datalist>
                                                        </input>
                                                    </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <label> Nome Abreviado</label>
                                                                <input name="nome_abrev" readonly id="nome_abrev" value="<?php echo $data['empresa']['empresanome_abrev']; ?>" type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label> Razão Social</label>
                                                                <input name="razao_social" id="razao_social" maxlength="12" value="<?php echo $data['empresa']['empresarazao_social']; ?>" type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label> Ramo Atividade</label>
                                                                <input name="ramo_atividade" id="ramo_atividade" value="<?php echo $data['empresa']['empresaramo_atividade']; ?>" type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <label> Rua</label>
                                                                <input name="rua" id="rua" value="<?php echo $data['empresa']['empresarua']; ?>" type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <label> Número</label>
                                                                <input name="numero" id="numero" value="<?php echo $data['empresa']['empresanumero']; ?>" type="number" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <label> Bairro</label>
                                                                <input name="bairro" id="bairro" value="<?php echo $data['empresa']['empresabairro']; ?>" type="text" class="form-control">
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <label> Complemento</label>
                                                                <input name="complemento" id="complemento" value="<?php echo $data['empresa']['empresacomplemento']; ?>" type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <label> Cidade</label>
                                                                <input name="cidade" id="cidade" value="<?php echo $data['empresa']['empresacidade']; ?>" type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <label> Estado</label>
                                                                <input name="estado" id="estado" maxlength="2" value="<?php echo $data['empresa']['empresaestado']; ?>" type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <label> País<s></s></label>
                                                                <input name="pais" id="pais" value="<?php echo $data['empresa']['empresapais']; ?>" type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <label>CEP</label>
                                                                <input name="cep" type="text" id="cep" value="<?php echo $data['empresa']['empresacep']; ?>" onblur="pesquisacep(this.value);" onfocus="javascript: retirarFormatacaocep(this);" onblur="javascript: formatarCampocep(this);" maxlength="8" type="text" class="form-control" placeholder="89211-000">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label> E-mail</label>
                                                                <input name="e_mail" id="e_mail" value="<?php echo $data['empresa']['empresae_mail']; ?>" type="email" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label> E-mail-NFE</label>
                                                                <input name="e_mail_nfe" id="e_mail_nfe" value="<?php echo $data['empresa']['empresae_mail_nfe']; ?>" type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <label>Celular</label>
                                                                <input name="celular" value="<?php echo $data['empresa']['empresacelular']; ?>" id="celular" onkeyup="mascara( this, mtel );" maxlength="15" type="text" class="form-control" placeholder="(99) 99999-9999 ">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <label>Telefone</label>
                                                                <input name="telefone" value="<?php echo $data['empresa']['empresatelefone']; ?>" onkeyup="mascarat( this, mtelt );" id="telefone" maxlength="9" type="text" class="form-control" placeholder="9999-9999 ">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label> URL</label>
                                                                <input name="url" id="url" value="<?php echo $data['empresa']['empresaurl']; ?>" type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="exampleInputFile">Logo </label>
                                                                <div class="input-group">
                                                                    <div class="custom-file">
                                                                        <input type="file" name="foo" class="custom-file-input" id="exampleInputFile">
                                                                        <label class="custom-file-label" for="exampleInputFile">Escolher arquivo</label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="row">
                                                        <?php
                                                        if ($data['empresa']['empresaind_ativo'] == 0) {
                                                            $data['empresa']['empresaind_ativo'] = "checked";
                                                        } else {
                                                            $data['empresa']['empresaind_ativo'] = "";
                                                        }
                                                        ?>
                                                        <div class="col-sm-6">
                                                            <div class="custom-control custom-switch">
                                                                <div class="custom-control custom-switch">

                                                                    <input type="checkbox" name="ind_ativo" <?php echo $data['empresa']['empresaind_ativo'] ?> class="custom-control-input" id="ind_ativo">
                                                                    <label class="custom-control-label" for="ind_ativo">Empresa Ativa</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card-footer">
                                                        <input type="submit" name="Atualizar" class="btn btn-purple btn-md float-right" value="Salvar">
                                                        <a href="/empresas/index" class="btn btn-orange btn-md float-right" style="margin-right:4px;">Cancelar</a>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                            </section>
                        </div> <!-- /.tab-pane // fecha editar-->
                        <div class="tab-pane" id="logs">
                            <section class="invoice p-3 mb-3">
                                <table id="table_logs" class="display responsive nowrap" style="width:100%">
                                    <thead>
                                        <th>Usuário Cadastro</th>
                                        <th>Data Cadastro</th>
                                        <th>Hora Cadastro</th>
                                        <th>Usuário Alteração</th>
                                        <th>Data Alteração</th>
                                        <th>Hora Alteração</th>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($data['logs'] as $data['logs']) : ?>
                                            <tr>
                                                <td><?php echo $data['logs']['user_perfilnome_user'];  ?></td>
                                                <td><?php echo implode("/", array_reverse(explode("-", $data['logs']['empresadt_criacao']))); ?></td>
                                                <td><?php echo $data['logs']['empresahr_criacao']; ?></td>
                                                <td><?php echo $data['logs']['user_perfilnome_user']; ?> </td>
                                                <td><?php echo implode("/", array_reverse(explode("-", $data['logs']['empresadt_altera']))); ?></td>
                                                <td><?php echo $data['logs']['empresahr_altera']; ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </section>
                        </div> <!-- /.tab-pane -->

                        <div class="tab-pane" id="unid_neg">
                            <section class="invoice p-3 mb-3">
                                <table id="table_unid_neg" class="display responsive nowrap" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Unidade de Negócio</th>
                                            <th>CNPJ Estabelecimentos</th>
                                            <th>Descrição</th>
                                            <th>Ativo</th>
                                            <th>Origem</th>
                                            <th>Data de Criação</th>
                                            <th>User Criação</th>
                                            <th>Data Alteração</th>
                                            <th>User Alteração</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($data['unid_neg'] as $unig_neg) : ?>
                                            <tr>
                                                <td><?php echo $unid_neg['unid_negid']; ?></td>
                                                <td>
                                                    <?php
                                                        if (strlen($unig_neg['unid_negnpj_estab'] < 11)) {
                                                            echo $unig_neg['unid_negnpj_estab'];
                                                        } else {
                                                            echo Mask('##.###.###/####-##', $unig_neg['unid_negnpj_estab']);
                                                        }
                                                        ?>
                                                </td>
                                                <td><?php echo $unig_neg['unid_negdescricao']; ?></td>
                                                <td><?php echo $unig_neg['unid_negind_ativo'] == 0 ? "Sim" : "Não"; ?></td>
                                                <td><?php echo $unig_neg['unid_negind_origem'] == 0 ? "Portal" : "Integração"; ?></td>
                                                <td><?php echo $unig_neg['unid_negdt_criacao']; ?> <?php echo $unig_neg['unid_neghr_criacao']; ?></td>
                                                <td><?php echo $unig_neg['unid_negusercriacao']; ?></td>
                                                <td><?php echo $unig_neg['unid_negdt_altera']; ?> <?php echo $unig_neg['unid_neghr_altera']; ?></td>
                                                <td><?php echo $unig_neg['unid_neguser_altera']; ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </section>
                        </div> <!-- /.tab-pane -->
                    </div> <!-- /.tab-content -->
                </div><!-- /.card-body -->
            </div> <!-- /.nav-tabs-custom -->
        </div> <!-- /.col -->
    </div> <!-- /.row -->
</section>
<script>
    function formatarCampo(campoTexto) {
        campoTexto.value = mascaraCnpj(campoTexto.value);
    }
    function retirarFormatacao(campoTexto) {
        campoTexto.value = campoTexto.value.replace(/(\.|\/|\-)/g, "");
    }
    function mascaraCnpj(valor) {
        return valor.replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/g, "\$1.\$2.\$3\/\$4\-\$5");
    }
</script>

<script>
    $(document).ready(function() {
        $('#table_logs').DataTable({});
        $('#table_unid_neg').DataTable({
            scrollY: 300,
            "sScrollX": "100%",
            "bScrollCollapse": true,
            paging: false
        });
    });

    $(document).ready(function() {
        $('#celular').mask(' (00)0000-0000', {
            reverse: true
        });
    });
    
    $(document).ready(function() {
        $('#telefone').mask('(00)0000-0000', {
            reverse: true
        });
    });
</script>