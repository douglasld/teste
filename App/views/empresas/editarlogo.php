<div class="x_panel">
    <div class="x_title">
        <h2>Empresas</h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <?php
        if (!empty($data['mensagem'])):

            foreach ($data['mensagem'] as $m):
                echo $m . "<br>";
            endforeach;

        endif;
        ?>

        <form action="/empresas/editarlogo/<?php echo $data['registros']['empresaid']; ?>" method="post"
              enctype="multipart/form-data" class="form-horizontal form-label-left">

            <div class="box-body">

                <div class="form-group row">
                    <div class="col-md-7">
                        <?php if(strlen($data['registros']['empresalogo_marca'] > 5)){ ?>
                            <img src="<?php echo URL_BASE ?>/images/<?php echo $data['registros']['empresalogo_marca'] ?>" class="thumbnail">
                        <?php }?>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="control-label col-md-3" for="cidade">Logo: </label>
                    <div class="col-md-7">
                        <input type="file" name="foo" id="foo" value="" class="form-control col-md-7">
                    </div>
                </div>

            </div>

            <!-- /.box-body -->
            <div class="box-footer">
                <button name="AtualizarLogo" class="btn btn-success pull-left">Atualizar Logo</button>
                <a href="/empresas/index" class="btn btn-danger pull-right">Cancelar</a>
            </div>
        </form>
    </div>
</div>
