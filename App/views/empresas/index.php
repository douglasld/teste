<!-- Content Header (Page header)  -->
<section class="content-header">
    <div class="container" style="background: pink;">
        <?php


        if (!empty($data['mensagem'])) {
            foreach ($data['mensagem'] as $o) {
                echo $o . "<br>";
            }
        }

        if (!empty($data['erro_empresa'])) {
            foreach ($data['erro_empresa'] as $m) {
                foreach($m as $val){
                    $cnpj_value = $val['0']['empresacnpj_estab'];
                    $cnpj_value = preg_replace("/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/", "\$1.\$2.\$3/\$4-\$5", $cnpj_value);
                    echo "Existem outros estabelecimentos vinculados a empresa:" . $cnpj_value . '-' . $val['empresanome_abrev'] . "<br>";
                }              
            }
            
        }

        if (!empty($data['erro_estab'])) {
            foreach ($data['erro_estab'] as $error) {
                $cod =  $error['user_estabcod_user'];
                $nome = $error['user_estabnome_user'];
                echo "Existem usuários vinculados a empresa:" . $cod . '-' . $nome . "<br>";
            }
        }
        


      



        ?>
    </div>
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h4>Estabelecimentos</h4>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a style="color: indigo" href="#">Index</a></li>
                    <li class="breadcrumb-item active"></li>
                </ol>
            </div>
        </div>
    </div>
</section>
<!-- Main Content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="btn-group">
                        <div> <a href="/empresas/cadastrar" type="button" class="btn btn-block btn-purple btn-sm">Adicionar Estabelecimento</a> </div>
                        &nbsp
                        <div> <a href="/empresas/exportar" type="button" class="btn btn-block btn-orange btn-sm">Exportar</a> </div>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <table id="table" class="display responsive nowrap" style="width:100%">
                                <thead>
                                    <tr>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">Atalhos</th> <!-- botões -->
                                        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">CNPJ</th>
                                        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">CNPJ Estabelecimento</th>
                                        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Nome</th>
                                        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Cidade</th>
                                        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Estado</th>
                                        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Telefone</th>
                                        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Celular</th>
                                    </tr>
                                </thead>
                                <!-- tbody -->
                                <tbody>
                                    <?php
                                    foreach ($data['registros'] as $empresas) : ?>
                                        <tr>
                                            <td>
                                                <a title="Detalhar" href="/empresas/ver/<?php echo $empresas['empresaid']; ?>" type="button" class="btn btn-purple btn-sm"><i class="fas fa-eye"></i></a> <!-- visualizar -->
                                                <a title="Excluir" onclick="verificadel()" href="/empresas/excluir/<?php echo $empresas['empresaid']; ?>" type="button" class="btn btn-purple btn-sm"><i class="fas fa-trash"></i></a> <!-- excluir -->
                                            </td>
                                            <td>
                                                <?php
                                                if (strlen($empresas['empresacnpj_matriz'] <= 10)) {
                                                    echo $empresas['empresacnpj_matriz'];
                                                } else {
                                                    echo Mask('##.###.###/####-##', $empresas['empresacnpj_matriz']);
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                if (strlen($empresas['empresacnpj_estab'] < 11)) {
                                                    echo $empresas['empresacnpj_estab'];
                                                } else {
                                                    echo Mask('##.###.###/####-##', $empresas['empresacnpj_estab']);
                                                }
                                                ?>
                                            </td>
                                            <td><?php echo $empresas['empresanome_abrev']; ?></td>
                                            <td><?php echo $empresas['empresacidade']; ?></td>
                                            <td><?php echo $empresas['empresaestado']; ?></td>
                                            <td>
                                                <?php
                                                if (strlen($empresas['empresatelefone'] < 11)) {
                                                    echo $empresas['empresatelefone'];
                                                } else {
                                                    echo Mask('####-####', $empresas['empresatelefone']);
                                                };
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                if (strlen($empresas['empresacelular'] < 11)) {
                                                    echo $empresas['empresacelular'];
                                                } else {
                                                    echo Mask('(##)#####-####', $empresas['empresacelular']);
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
</section>
<script>
    $(document).ready(function() {
        $('#table').DataTable({
            scrollY: 300,
            paging: false
        });
    });

    function verificadel() {
        var x;
        var r = confirm("Deseja excluir a linha?");
        if (r == true) {} else {
            event.preventDefault();
        }
    }
</script>