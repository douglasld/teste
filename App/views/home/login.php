<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="<?php echo URL_BASE; ?>vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="<?php echo URL_BASE; ?>vendor/font-awesome/css/font-awesome.min.css">
    <!-- Google fonts - Roboto-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,700">
    <!-- Bootstrap Select-->
    <link rel="stylesheet" href="<?php echo URL_BASE; ?>vendor/bootstrap-select/css/bootstrap-select.min.css">
    <!-- owl carousel-->
    <link rel="stylesheet" href="<?php echo URL_BASE; ?>vendor/owl.carousel/assets/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo URL_BASE; ?>vendor/owl.carousel/assets/owl.theme.green.css">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="<?php echo URL_BASE; ?>vendor/css/style.green.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="<?php echo URL_BASE; ?>vendor/css/custom.css">
    <!-- Favicon and apple touch icons-->
</head>
</head>

<body>
    <div id="all">
        <!-- Top bar-->
        <div class="top-bar">
            <div class="container">
                <div class="row d-flex align-items-center">
                    <div class="col-md-6 d-md-block d-none">
                        <p>contato@artbrasil.net</p>
                    </div>
                    <div class="col-md-6">
                        <div class="d-flex justify-content-md-end justify-content-between">
                            <ul class="list-inline contact-info d-block d-md-none">
                                <li class="list-inline-item"><a href="#"><i class="fa fa-phone"></i></a></li>
                                <li class="list-inline-item"><a href="#"><i class="fa fa-envelope"></i></a></li>
                            </ul>
                            <div class="login"><a href="#" data-toggle="modal" data-target="#login-modal" class="login-btn"><i class="fa fa-sign-in"></i><span class="d-none d-md-inline-block">Entrar</span></a><a href="/home/cadastrar" class="signup-btn"><i class="fa fa-user"></i><span class="d-none d-md-inline-block">Novo cadastro</span></a></div>
                            <ul class="social-custom list-inline">
                                <li class="list-inline-item"><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li class="list-inline-item"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                <li class="list-inline-item"><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li class="list-inline-item"><a href="#"><i class="fa fa-envelope"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Top bar end-->
        <!-- Login Modal-->
        <div id="login-modal" tabindex="-1" role="dialog" aria-labelledby="login-modalLabel" aria-hidden="true" class="modal fade">
            <div role="document" class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 id="login-modalLabel" class="modal-title">Entrar</h4>
                        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <form action="/home/login" method="post">
                            <div class="form-group">
                                <input id="email_modal" type="text" placeholder="E-mail" class="form-control" name="email" required="">
                            </div>
                            <div class="form-group">
                                <input id="password_modal" type="password" placeholder="Senha" class="form-control" name="senha" required="">
                                <input type="hidden" name="entrar">
                            </div>
                            <p class="text-center">
                                <button class="btn btn-template-outlined"><i class="fa fa-sign-in"></i> Entrar</button>
                            </p>
                        </form>
                        <p class="text-center text-muted">Ainda não possui uma conta?</p>
                        <p class="text-center text-muted"><a href="/home/cadastrar"><strong>Clique aqui</strong></a> para criar uma conta, é rápido e fácil!</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- Login modal end-->
        <!-- Navbar Start-->
        <header class="nav-holder make-sticky">
            <div id="navbar" role="navigation" class="navbar navbar-expand-lg">
                <div class="container"><a href="/home/login" class="navbar-brand home"><img src="<?=URL_BASE?>images/Logo.png" alt="Universal logo" class="d-none d-md-inline-block"><img src="<?=URL_BASE?>images/Logo.png" alt="Universal logo" class="d-inline-block d-md-none"><span class="sr-only">Universal - go to homepage</span></a>
                    <button type="button" data-toggle="collapse" data-target="#navigation" class="navbar-toggler btn-template-outlined"><span class="sr-only">Toggle navigation</span><i class="fa fa-align-justify"></i></button>
                    <div id="navigation" class="navbar-collapse collapse">
                        <ul class="nav navbar-nav ml-auto">
                            <li class="nav-item dropdown active"><a href="javascript: void(0)" data-toggle="dropdown" class="dropdown-toggle">Início<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li class="dropdown-item"><a href="#" class="nav-link">Option 1: Default Page</a></li>
                                    <li class="dropdown-item"><a href="#" class="nav-link">Option 2: Application</a></li>
                                    <li class="dropdown-item"><a href="#" class="nav-link">Option 3: Startup</a></li>
                                    <li class="dropdown-item"><a href="#" class="nav-link">Option 4: Agency</a></li>
                                    <li class="dropdown-item dropdown-submenu"><a id="navbarDropdownMenuLink2" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">Dropdown link</a>
                                        <ul aria-labelledby="navbarDropdownMenuLink2" class="dropdown-menu">
                                            <li class="dropdown-item"><a href="#" class="nav-link">Action</a></li>
                                            <li class="dropdown-item"><a href="#" class="nav-link">Another action</a></li>
                                            <li class="dropdown-item"><a href="#" class="nav-link">Something else here</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item dropdown menu-large"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Produtos<b class="caret"></b></a>
                                <ul class="dropdown-menu megamenu">
                                    <li>
                                        <div class="row">

                                            <div class="col-lg-3 col-md-6">
                                                <h5>Cosméticos Artesanais</h5>
                                                <ul class="list-unstyled mb-3">
                                                    <li class="nav-item"><a href="#" class="nav-link">Cremes</a></li>
                                                    <li class="nav-item"><a href="#" class="nav-link">Cremes esfoliantes</a></li>
                                                    <li class="nav-item"><a href="#" class="nav-link">Perfumes</a></li>
                                                    <li class="nav-item"><a href="#" class="nav-link">Sabonetes</a></li>
                                                    <li class="nav-item"><a href="#" class="nav-link">Creme Dental</a></li>

                                                </ul>
                                            </div>
                                            <div class="col-lg-3 col-md-6">
                                                <h5>Artesanatos</h5>
                                                <ul class="list-unstyled mb-3">
                                                    <li class="nav-item"><a href="#" class="nav-link">Mandalas</a></li>
                                                    <li class="nav-item"><a href="#" class="nav-link">Colares</a></li>
                                                    <li class="nav-item"><a href="#" class="nav-link">Pulseiras</a></li>
                                                    <li class="nav-item"><a href="#" class="nav-link">Anéis</a></li>
                                                    <li class="nav-item"><a href="#" class="nav-link">Filtros dos sonhos</a></li>
                                                </ul>
                                            </div>
                                            <div class="col-lg-3 col-md-6">
                                                <h5>Esculturas</h5>
                                                <ul class="list-unstyled mb-3">
                                                    <li class="nav-item"><a href="#" class="nav-link">Madeira</a></li>
                                                    <li class="nav-item"><a href="#" class="nav-link">Argila</a></li>
                                                    <li class="nav-item"><a href="#" class="nav-link">Biscuit</a></li>
                                                    <li class="nav-item"><a href="#" class="nav-link">Cerâmica</a></li>
                                                    <li class="nav-item"><a href="#" class="nav-link">Vidro</a></li>
                                                </ul>
                                            </div>

                                            <div class="col-lg-3"><img src="<?=URL_BASE?>images/menu_categoria_imagem.png" alt="" class="img-fluid d-none d-lg-block"></div>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <!-- ========== Contact dropdown ==================-->
                            <li class="nav-item dropdown"><a href="javascript: void(0)" data-toggle="dropdown" class="dropdown-toggle">Contato <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li class="dropdown-item"><a href="#" class="nav-link">Contato</a></li>
                                </ul>
                            </li>
                            <!-- ========== Contact dropdown end ==================-->
                        </ul>
                    </div>
                    <div id="search" class="collapse clearfix">
                        <form role="search" class="navbar-form">
                            <div class="input-group">
                                <input type="text" placeholder="Search" class="form-control"><span class="input-group-btn">
                                    <button type="submit" class="btn btn-template-main"><i class="fa fa-search"></i></button></span>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </header>
        <!-- Navbar End-->

        <section style="background: url('<?=URL_BASE?>images/photogrid.jpg') center center repeat; background-size: cover;" class="relative-positioned">
            <!-- Carousel Start-->
            <div class="home-carousel">
                <div class="dark-mask mask-primary"></div>
                <div class="container">
                    <div class="homepage owl-carousel">
                        <div class="item">
                            <div class="row">
                                <div class="col-md-5 text-right">
                                    <p><img src="<?=URL_BASE?>images/logo-carousel-index.png" alt="" class="ml-auto"></p>
                                    <h1>Ligando pessoas à arte puramente brasileira!</h1>
                                    <p>Traga para nosso site a arte que represente o lugar onde vive, passoas, biomas!</p>
                                </div>
                                <div class="col-md-7"><img src="<?=URL_BASE?>images/menu_carousel_artbrasil.png" alt="" class="img-fluid"></div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-md-7 text-center"><img src="<?=URL_BASE?>images/menu_carousel_ligacao.png" alt="" class="img-fluid"></div>
                                <div class="col-md-5">
                                    <h2>Aqui você liga sua marca a novos clientes.</h2>
                                    <ul class="list-unstyled">
                                        <li>Apresente seus produtos com boas fotos, isso impulsionará suas vendas.</li>
                                        <li>Uma boa descrição do produto deixará seus compradores mais confiantes.</li>
                                        <li>Avalie as pessoas com quem compra, isso ajudará a tornar a plataforma mais segura.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
        </section>
        <section class="bar background-white no-mb">
            <div class="container">
                <div class="col-md-12">
                    <div class="heading text-center">
                        <h2>O que vendemos?</h2>
                    </div>
                    <p class="lead">Nós da ArtBrasil, apoiamos a venda de qualquer produto que seja feito com amor, desde o brinco ou colar que expresse sua arte, até o cachecol da vovó que aquece quem o usa!</p>
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="home-blog-post">
                                <div class="image"><img src="<?=URL_BASE?>images/portfolio-4.jpg" alt="..." class="img-fluid">
                                    <div class="overlay d-flex align-items-center justify-content-center"><a href="#" class="btn btn-template-outlined-white"><i class="fa fa-chain"> </i> Ler Mais</a></div>
                                </div>
                                <div class="text">
                                    <h4><a href="#">Mãos que criam arte!</a></h4>
                                    <p class="intro">Nessa sessão você encontra diversos produtos feito por pessoas que gostam de materializar sua arte em objetos mais duradouros, um exemplo é a madeira, natural e de aspecto aconchegante.</p><a href="#" class="btn btn-template-outlined">Ver Sessão</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="home-blog-post">
                                <div class="image"><img src="<?=URL_BASE?>images/portfolio-3.png" alt="..." class="img-fluid">
                                    <div class="overlay d-flex align-items-center justify-content-center"><a href="#" class="btn btn-template-outlined-white"><i class="fa fa-chain"> </i> Ler Mais</a></div>
                                </div>
                                <div class="text">
                                    <h4><a href="#">Cosméticos Naturais</a></h4>
                                    <p class="intro">Temos também uma vasta linha de cosméticos naturais como sabonetes com sementes esfoliantes, cremes com cheiro natural de ervas e flores para os mais variados gostos.</p><a href="#" class="btn btn-template-outlined">Ver Sessão</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="home-blog-post">
                                <div class="image"><img src="<?=URL_BASE?>images/portfolio-5.jpg" alt="..." class="img-fluid">
                                    <div class="overlay d-flex align-items-center justify-content-center"><a href="#" class="btn btn-template-outlined-white"><i class="fa fa-chain"> </i> Ler Mais</a></div>
                                </div>
                                <div class="text">
                                    <h4><a href="#">Ambiente</a></h4>
                                    <p class="intro">Produtos como incensos, aromatizadores de ambientes, luminarias e enfeites você pode encontrar nessa sessão destinada a produtos para ambiente!</p><a href="#" class="btn btn-template-outlined">Ver Sessão</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="home-blog-post">
                                <div class="image"><img src="<?=URL_BASE?>images/portfolio-6.jpg" alt="..." class="img-fluid">
                                    <div class="overlay d-flex align-items-center justify-content-center"><a href="#" class="btn btn-template-outlined-white"><i class="fa fa-chain"> </i> Ler Mais</a></div>
                                </div>
                                <div class="text">
                                    <h4><a href="#">Blog ArtBrasil</a></h4>
                                    <p class="intro">Está com duvidas sobre produtos para usar em suas artes? aqui você encontra diversas dicas e ainda pode falar com artistas que podem te auxiliar no caminho a seguir para inicar suas vendas na plataforma.</p><a href="#" class="btn btn-template-outlined">Ver Sessão</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="bar background-pentagon no-mb" style="background: url('<?=URL_BASE?>images/texture-bw.png') center center repeat;">
            <div class="container">
                <div class="row showcase text-center">
                    <div class="col-md-4 col-sm-6">
                        <div class="item">
                            <div class="icon-outlined icon-sm icon-thin"><i class="fa fa-align-justify"></i></div>
                            <h4><span class="h1 counter">580</span><br> Vendedores</h4>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="item">
                            <div class="icon-outlined icon-sm icon-thin"><i class="fa fa-users"></i></div>
                            <h4><span class="h1 counter">100</span><br>Vendas</h4>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="item">
                            <div class="icon-outlined icon-sm icon-thin"><i class="fa fa-copy"></i></div>
                            <h4><span class="h1 counter">320</span><br>Produtos</h4>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- FOOTER -->
        <!-- FOOTER -->
        <footer style=" padding: 20px 0;padding-bottom: 0;background: #555;color: #fff;">
            <div class="copyrights">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-4 text-center-md">
                            <p>&copy; 2019. Todos os direitos reservados à ArtBrasil</p>
                        </div>
                        <div class="col-lg-8 text-right text-center-md">
                            <p>Desenvolvido por ArtBrasil</p>
                            <!-- Please do not remove the backlink to us unless you purchase the Attribution-free License at https://bootstrapious.com/donate. Thank you. -->
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    </div>
    <!-- Javascript files-->
    <script language="JavaScript" src='<?php echo URL_BASE; ?>vendor/functions/functions_js.js'></script>
    <script src="<?php echo URL_BASE; ?>vendor/jquery/jquery.min.js"></script>
    <script src="<?php echo URL_BASE; ?>vendor/popper.js/umd/popper.min.js"> </script>
    <script src="<?php echo URL_BASE; ?>vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="<?php echo URL_BASE; ?>vendor/waypoints/lib/jquery.waypoints.min.js"> </script>
    <script src="<?php echo URL_BASE; ?>vendor/jquery.counterup/jquery.counterup.min.js"> </script>
    <script src="<?php echo URL_BASE; ?>vendor/owl.carousel/owl.carousel.min.js"></script>
    <script src="<?php echo URL_BASE; ?>vendor/owl.carousel2.thumbs/owl.carousel2.thumbs.min.js"></script>
    <script src="<?php echo URL_BASE; ?>vendor/js/jquery.parallax-1.1.3.js"></script>
    <script src="<?php echo URL_BASE; ?>vendor/bootstrap-select/js/bootstrap-select.min.js"></script>
    <script src="<?php echo URL_BASE; ?>vendor/jquery.scrollto/jquery.scrollTo.js"></script>
    <script src="<?php echo URL_BASE; ?>vendor/jquery.scrollto/jquery.scrollTo.min.js"></script>
    <script src="<?php echo URL_BASE; ?>vendor/js/front.js"></script>
    <script src="<?php echo URL_BASE; ?>vendor/js/bootstrap-hover-dropdown"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>

</html>