<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="<?=URL_BASE?>vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="<?=URL_BASE?>vendor/font-awesome/css/font-awesome.min.css">
    <!-- Google fonts - Roboto-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,700">
    <!-- Bootstrap Select-->
    <link rel="stylesheet" href="<?=URL_BASE?>vendor/bootstrap-select/css/bootstrap-select.min.css">
    <!-- owl carousel-->
    <link rel="stylesheet" href="<?=URL_BASE?>vendor/owl.carousel/assets/owl.carousel.css">
    <link rel="stylesheet" href="<?=URL_BASE?>vendor/owl.carousel/assets/owl.theme.green.css">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="<?=URL_BASE?>vendor/css/style.green.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="<?=URL_BASE?>vendor/css/custom.css">
    <!-- Favicon and apple touch icons-->
</head>

<body>
    <div id="all">
        <!-- START MODAL PARA RECUPERAR EMAIL-->
        <div id="login-modal" tabindex="-1" role="dialog" aria-labelledby="login-modalLabel" aria-hidden="true" class="modal fade">
            <div role="document" class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 id="login-modalLabel" class="modal-title">Recuperar informações de login</h4>
                        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <form id="recupera_email" method="" action="">
                            <div class="form-group">
                                <label for="recuperaemail">Digite seu E-mail</label>
                                <input id="recuperaemail" type="text" placeholder="E-mail" class="form-control" id="recupera_email" name="recupera_email" required="">
                            </div>
                            <p class="text-center">
                                <button class="btn btn-template-outlined" type="submit" onclick="recupera_senha()"><i class="fa fa-sign-in"></i> Recuperar</button>
                            </p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- START MODAL PARA RECUPERAR EMAIL-->
        <p style="color: red;" id="sucesso_modal"></p>
        <!-- Navbar Start-->
        <header class="nav-holder make-sticky">
            <div id="navbar" role="navigation" class="navbar navbar-expand-lg">
                <div class="container"><a href="index.php" class="navbar-brand home"><img src="<?=URL_BASE?>vendor/img/logo.png" alt="Universal logo" class="d-none d-md-inline-block"><img src="../view/img/logo-small.png" alt="Universal logo" class="d-inline-block d-md-none"><span class="sr-only">Universal - go to homepage</span></a> </div>
            </div>
        </header>
        <!-- Navbar End-->
        <!-- INCLUI MENSAGENS VINDAS VIA GET END-->
        <?php
        if (!empty($data['mensagem'])) :
            foreach ($data['mensagem'] as $m) :
                //trata mensagem
                $partes = explode(" - ", $m);
                if ($partes[0] == '1') {
                    //sucesso
                    echo '<br><div>
                    <center>
                      <div role="alert" class="alert alert-success alert-dismissible">
                            <button type="button" data-dismiss="alert" class="close"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>' . $partes[1] . '
                          </div>
                    </center>
                  </div>';
                } else {
                    if ($partes[0] == '2') {
                        //alert
                        echo '<br><div>
                        <center>
                          <div role="alert" class="alert alert-warning alert-dismissible">
                                <button type="button" data-dismiss="alert" class="close"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>' . $partes[1] . '
                              </div>
                        </center>
                      </div>';
                    } else {
                        if ($partes[0] == '3') {
                            //erro
                            echo '<br><div>
                            <center>
                              <div role="alert" class="alert alert-danger alert-dismissible">
                                    <button type="button" data-dismiss="alert" class="close"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>' . $partes[1] . '
                                  </div>
                            </center>
                          </div>';
                        }
                    }
                }
            endforeach;
        endif;
        ?>
        <div id="content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="box">
                            <h2 class="text-uppercase">Criar Nova Conta</h2>
                            <p class="lead">Registre-se rapidamente para começar a comprar!</p>
                            <hr>
                            <form id="formdados" action="/home/cadastrar" method="POST">
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="name-login">Nome</label>
                                        <input type="text" class="form-control" required="" name="nome">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="name-login">Sobrenome</label>
                                        <input type="text" class="form-control" required="" name="sobrenome">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="name-login">Data de Nascimento</label>
                                        <input type="date" class="form-control" required="" name="datanasc">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="inputState">Gênero</label>
                                        <select class="form-control" required="" name="genero">
                                            <option value="1">Masculino</option>
                                            <option value="0">Feminino</option>
                                        </select>
                                    </div>
                                </div>
                                <center>
                                    <p style="color: red;" id="erro_email"></p>
                                </center>
                                <div class="form-group">
                                    <label for="email">E-mail</label>
                                    <input type="text" class="form-control" required="" id="email" name="email" onchange="verificaemail()">
                                </div>
                                <div class="form-group">
                                    <label for="confemail">Verificação de E-mail</label>
                                    <input type="text" class="form-control" required="" id="confemail" name="confemail" onchange="verificaemail()">
                                </div>
                                <center>
                                    <p style="color: red;" id="erro_senha"></p>
                                </center>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="password-login">Senha</label>
                                        <input type="password" class="form-control" required="" id="senha" name="senha" onchange="verificasenha()">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="password-login">Confirmação de Senha</label>
                                        <input type="password" class="form-control" required="" id="confsenha" name="confsenha" onchange="verificasenha()">
                                    </div>
                                </div>
                                <div class="text-center">
                                    <input type="submit" name="cadastrar" value="Cadastrar" class="btn btn-template-outlined">
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="box">
                            <h2 class="text-uppercase">Entrar</h2>
                            <p class="lead">Já possui um cadastro? então entre e aproveite!</p>
                            <hr>
                            <form action="/home/login" method="POST">
                                <div class="form-group">
                                    <label for="email">E-mail</label>
                                    <input id="email" type="text" class="form-control" required="" name="email">
                                </div>
                                <div class="form-group">
                                    <label for="password">Senha</label>
                                    <input id="password" type="password" class="form-control" required="" name="senha">
                                </div>
                                <div class="text-center">
                                    <input type="submit" name="entrar" value="Entrar" class="btn btn-template-outlined"></i> </div>
                                <br>
                                <center>
                                    <p class="lead">Esqueceu a senha ou e-mail? <a href="#" data-toggle="modal" data-target="#login-modal"><strong>Clique aqui</strong></a> para recuperar.</p>
                                </center>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <!-- FOOTER -->
        <footer style=" padding: 10px 0;background: #555;color: #fff;">
            <div class="copyrights">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-4 text-center-md">
                            <p>&copy; 2019. Todos os direitos reservados à ArtBrasil</p>
                        </div>
                        <div class="col-lg-8 text-right text-center-md">
                            <p>Desenvolvido por Douglas Dutra</p>
                            <!-- Please do not remove the backlink to us unless you purchase the Attribution-free License at https://bootstrapious.com/donate. Thank you. -->
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    </div>
</body>

</html>