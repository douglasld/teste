<style>
    .nav-tabs .nav-item .nav-link {
        color: #6039A8 !important;
    }
</style>
<!-- Content Header (Page header)  -->
<section class="content-header">
    <div class="container" style="background: pink;">
        <?php
        if (!empty($data['mensagem'])) :
            foreach ($data['mensagem'] as $m) :
                echo $m . "<br>";
            endforeach;
        endif;
        ?>
    </div>
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h4>Detalhes Vendas</h4>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="/empresas/index" style="color:indigo">Index</a></li>
                    <li class="breadcrumb-item active">Detalhamento</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="row">
        <div class="col-md-8">
            <div class="card card-purple card-outline card-outline-tabs">
                <div class="card-header p-0 pt-1 border-bottom-0">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item"><a class="nav-link active" href="#editar" data-toggle="tab">Editar</a></li>
                        <!-- <li class="nav-item"><a class="nav-link " href="#resumo" data-toggle="tab">Resumo</a></li> -->
                    </ul>
                </div><!-- /.card-header -->
                <div class="card-body">
                    <form action="/vendas/editar/<?php echo $data['registros']['vendasid']; ?>" method="post">
                        <div class="tab-content">
                            <div class="active tab-pane" id="editar">
                                <section class="content">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label> CNPJ</label>
                                                <input name="cnpj_estab" id="cnpj_estab" value="<?php echo $data['registros']['vendascnpj_estab']; ?>" onfocus="javascript: retirarFormatacao(this);" onblur="javascript: formatarCampo(this);" maxlength="14" type="text" class="form-control" placeholder="00.000.000/000-00" required="required">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label> Código Meta </label>
                                                <input name="vendascod_meta" id="vendascod_meta" value="<?php echo $data['registros']['vendascod_meta']; ?>" type="number" class="form-control" required="required">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label> Descrição</label>
                                                <input name="vendasdescricao" id="vendasdescricao" value="<?php echo $data['registros']['vendasdescricao']; ?>" type="text" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label> Série Documento</label>
                                                <input name="vendasserie_docto" id="vendasserie_docto" value="<?php echo $data['registros']['vendasserie_docto']; ?>" type="text" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label> Número Documento</label>
                                                <input name="vendasnr_docto" value="<?php echo $data['registros']['vendasnr_docto']; ?>" id="vendasnr_docto" type="number" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label> Data Documento</label>
                                                <input name="vendasdt_docto" id="vendasdt_docto" value="<?php echo $data['registros']['vendasdt_docto']; ?>" type="date" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label> Código Produto</label>
                                                <select name="vendascod_produto" id="vendascod_produto" class="form-control col-md-12">
                                                    <?php
                                                    $produtos = $this->model('Produto');
                                                    foreach ($produtos->getAll() as $value) :
                                                    ?>
                                                        <?php
                                                        if ($value['produtoid'] == $data['registros']['vendascod_produto']) {
                                                            echo '<option  value="' . $value['produtoid'] . '"selected >';
                                                            echo $value['produtodescricao'];
                                                            echo "</option>";
                                                        } else {
                                                            echo '<option  value="' . $value['produtoid'] . '">';
                                                            echo $value['produtodescricao'];
                                                            echo "</option>";
                                                        }
                                                        ?>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label> Quantidade Vendida</label>
                                                <input name="vendasqt_vendida" id="vendasqt_vendida" value="<?php echo $data['registros']['vendasqt_vendida']; ?>" type="number" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label> Código Unidade</label>
                                                <select name="vendascod_unid_med" id="vendascod_unid_med" class="form-control col-md-12">
                                                    <?php
                                                    $unidades = $this->model('UnidMed');
                                                    foreach ($unidades->getAll() as $value) :
                                                    ?>
                                                        <?php
                                                        if ($value['unid_medid'] == $data['registros']['vendascod_unid_med']) {
                                                            echo '<option  value="' . $value['unid_medid'] . '"selected >';
                                                            echo $value['unid_meddescricao'];
                                                            echo "</option>";
                                                        } else {
                                                            echo '<option  value="' . $value['unid_medid'] . '">';
                                                            echo $value['unid_meddescricao'];
                                                            echo "</option>";
                                                        }
                                                        ?>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label> Gerente</label>
                                                <select name="vendasgerente" id="vendasgerente" class="form-control col-md-12">
                                                    <?php
                                                    if ($data['registros']['vendasgerente'] == 1) {
                                                        echo '<option value="1" selected >Ger. Matriz</option>';
                                                    } else {
                                                        echo '<option value="1" selected >Ger. Matriz</option>';
                                                    }
                                                    if ($data['registros']['vendasgerente'] == 2) {
                                                        echo '<option value="2" selected >Ger. Pista</option>';
                                                    } else {
                                                        echo '<option value="2">Ger. Pista</option>';
                                                    }
                                                    if ($data['registros']['vendasgerente'] == 3) {
                                                        echo '<option value="3" selected >Ger. Conveniência</option>';
                                                    } else {
                                                        echo '<option value="3">Ger. Conveniência</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label> Valor Unitário<s></s></label>
                                                <input name="vendasvl_unit" id="vendasvl_unit" value="<?php echo $data['registros']['vendasvl_unit']; ?>" type="number" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label> Valor Total</label>
                                                <input name="vendasvl_total" type="number" value="<?php echo $data['registros']['vendasvl_total']; ?>" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label> Nome Vendedor</label>
                                                <select name="vendasnome_vendedor" id="vendasnome_vendedor" class="form-control col-md-12">
                                                    <?php
                                                    if ($data['registros']['vendasnome_vendedor'] == "1") {
                                                        echo '<option value="1" selected >Vend. Matriz A</option>';
                                                    } else {
                                                        echo '<option value="1" >Vend. Matriz A</option>';
                                                    }
                                                    if ($data['registros']['vendasnome_vendedor'] == "2") {
                                                        echo '<option value="2" selected >Vend. Pista A</option>';
                                                    } else {
                                                        echo '<option value="2">Vend. Pista A</option>';
                                                    }
                                                    if ($data['registros']['vendasnome_vendedor'] == "3") {
                                                        echo '<option value="3" selected >Vend. Conveniência A</option>';
                                                    } else {
                                                        echo '<option value="3">Vend. Conveniência A</option>';
                                                    }
                                                    if ($data['registros']['vendasnome_vendedor'] == "4") {
                                                        echo '<option value="4" selected >Vend. Matriz B </option>';
                                                    } else {
                                                        echo '<option value="4" selected >Vend. Matriz B </option>';
                                                    }
                                                    if ($data['registros']['vendasnome_vendedor'] == "5") {
                                                        echo '<option value="5" selected >Vend. Pista B</option>';
                                                    } else {
                                                        echo '<option value="5">Vend. Pista B</option>';
                                                    }
                                                    if ($data['registros']['vendasnome_vendedor'] == "6") {
                                                        echo ' <option value="6" selected >Vend. Conveniência B</option>';
                                                    } else {
                                                        echo ' <option value="6">Vend. Conveniência B</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label> Unidade de Negócio</label>
                                                <select name="vendascod_unid_neg" id="vendascod_unid_neg" class="form-control col-md-12">
                                                    <?php $unid_neg = $this->model('UnidNeg') ?>
                                                    <?php foreach ($unid_neg->getAll() as $value) : ?>
                                                        <?php
                                                        if ($value['unid_negid'] == $data['registros']['vendascod_unid_neg']) {
                                                            echo '<option  value="' . $value['unid_negid'] . '"selected >';
                                                            echo $value['unid_negdescricao'];
                                                            echo "</option>";
                                                        } else {
                                                            echo '<option  value="' . $value['unid_negid'] . '">';
                                                            echo $value['unid_negdescricao'];
                                                            echo "</option>";
                                                        }
                                                        ?>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="custom-control custom-switch">
                                            <div class="custom-control custom-switch">
                                                <input type="checkbox" name="vendasind_ativo" id="vendasind_ativo" <?php if ($data['registros']['vendasind_ativo'] == "0") {
                                                                                            echo "checked";
                                                                                        } ?> class="custom-control-input" id="vendasind_ativo">
                                                <label class="custom-control-label" for="vendasind_ativo">Grupo Produto Ativo</label>
                                            </div>
                                            <br>
                                        </div>
                                    </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <input type="submit" name="Atualizar" class="btn btn-purple btn-md float-right" value="Salvar">
                            <a href="/vendas/index" class="btn btn-orange btn-md float-right" style="margin-right:4px;">Cancelar</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
</section>
</div> <!-- /.tab-pane // fecha editar-->
</div> <!-- /.tab-content -->
</div><!-- /.card-body -->
</div> <!-- /.nav-tabs-custom -->
</div> <!-- /.col -->
</div> <!-- /.row -->
</section>
<script>
    function formatarCampo(campoTexto) {
        campoTexto.value = mascaraCnpj(campoTexto.value);
    }

    function retirarFormatacao(campoTexto) {
        campoTexto.value = campoTexto.value.replace(/(\.|\/|\-)/g, "");
    }

    function mascaraCnpj(valor) {
        return valor.replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/g, "\$1.\$2.\$3\/\$4\-\$5");
    }
</script>
<script>
    $(document).ready(function() {
        $('#table_logs').DataTable({});
        $('#table_unid_neg').DataTable({
            scrollY: 300,
            "sScrollX": "100%",
            "bScrollCollapse": true,
            paging: false
        });
    });
</script>