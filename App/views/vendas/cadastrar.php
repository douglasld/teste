<section class="content-header">
    <div class="container" style="background: lavender;">
        <?php
        if (!empty($data['mensagem'])) :
            foreach ($data['mensagem'] as $m) :
                echo $m . "<br>";
            endforeach;
        endif;
        // O debug colocado aqui aparecerá no header da página no container lavender
        // echo json_encode($data);
        ?>
    </div>
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h4>Cadastro de Vendas</h4>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a style="color: indigo" href="#">Index</a></li>
                    <li class="breadcrumb-item active">Cadastro</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <div class="card card-default">
                <form action="/vendas/cadastrar" method="post">
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label> CNPJ</label>
                                    <input name="cnpj_estab" id="cnpj_estab" onfocus="javascript: retirarFormatacao(this);" onblur="javascript: formatarCampo(this);" maxlength="14" type="text" class="form-control" placeholder="00.000.000/000-00" required="required">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label> Código Meta </label>
                                    <input name="vendascod_meta" id="vendascod_meta" type="number" class="form-control" required="required">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label> Descrição</label>
                                    <input name="vendasdescricao" id="vendasdescricao" type="text" class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label> Série Documento</label>
                                    <input name="vendasserie_docto" id="vendasserie_docto" type="text" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label> Número Documento</label>
                                    <input name="vendasnr_docto" id="vendasnr_docto" type="number" class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label> Data Documento</label>
                                    <input name="vendasdt_docto" id="vendasdt_docto" type="date" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label> Código Produto</label>
                                    <select name="vendascod_produto" id="vendascod_produto" class="form-control col-md-12">
                                        <?php
                                        $produtos = $this->model('Produto');
                                        foreach ($produtos->getAll() as $value) :
                                        ?>
                                            <option value="<?php echo $value['produtoid']; ?>">
                                                <?php echo $value['produtodescricao']; ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label> Quantidade Vendida</label>
                                    <input name="vendasqtvendida" id="vendasqtvendida" type="number" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label> Código Unidade</label>
                                    <select name="vendascod_unid_med" id="vendascod_unid_med" class="form-control col-md-12">
                                        <?php
                                        $unidades = $this->model('UnidMed');
                                        foreach ($unidades->getAll() as $value) :
                                        ?>
                                            <option value="<?php echo $value['unid_medid']; ?>">
                                                <?php echo $value['unid_meddescricao']; ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label> Gerente</label>
                                    <select name="vendasgerente" id="vendasgerente" class="form-control col-md-12">
                                        <option value="1">Ger. Matriz</option>
                                        <option value="2">Ger. Pista</option>
                                        <option value="3">Ger. Conveniência</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card card-default">
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label> Valor Unitário<s></s></label>
                                <input name="vendasvl_unit" id="vendasvl_unit" type="number" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label> Valor Total</label>
                                <input name="vendasvl_total" type="number" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label> Nome Vendedor</label>
                                <select name="vendasnome_vendedor" id="vendasnome_vendedor" class="form-control col-md-12">
                                    <option value="1">Vend. Matriz A</option>
                                    <option value="2">Vend. Pista A</option>
                                    <option value="3">Vend. Conveniência A</option>
                                    <option value="4">Vend. Matriz B </option>
                                    <option value="5">Vend. Pista B</option>
                                    <option value="6">Vend. Conveniência B</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label> Unidade de Negócio</label>
                                <select name="vendascod_unid_neg" id="vendascod_unid_neg" class="form-control col-md-12">
                                    <?php $unid_neg = $this->model('UnidNeg') ?>
                                    <?php foreach ($unid_neg->getAll() as $value) : ?>
                                        <option value="<?php echo $value['unid_negid']; ?>">
                                            <?php echo $value['unid_negdescricao']; ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="custom-control custom-switch">
                                <input type="checkbox" class="custom-control-input" id="ind_ativo" name="ind_ativo">
                                <label class="custom-control-label" for="ind_ativo">Venda Ativa</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <input type="submit" name="Cadastrar" class="btn btn-primary float-right" value="Cadastrar" style="background-color: indigo; border-color: indigo; margin:6px;">
                    <a href="/vendas/index" class="btn btn-orange btn-md float-right" style="margin:6px;">Cancelar</a>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    function formatarCampo(campoTexto) {
        campoTexto.value = mascaraCnpj(campoTexto.value);
    }
    function retirarFormatacao(campoTexto) {
        campoTexto.value = campoTexto.value.replace(/(\.|\/|\-)/g, "");
    }
    function mascaraCnpj(valor) {
        return valor.replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/g, "\$1.\$2.\$3\/\$4\-\$5");
    }
</script>



<!-- MÁSCARA TELEFONE -->

</div>