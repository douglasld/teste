<section class="content-header">

    <div class="container" style="background: pink;">
        <?php
        if (!empty($data['mensagem'])) :

            foreach ($data['mensagem'] as $m) :
                echo $m . "<br>";
            endforeach;
        endif;
        // var_dump ($_SESSION);
        ?>
    </div>
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h4>Editar Produtos</h4>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a style="color: indigo" href="#">Index</a></li>
                    <li class="breadcrumb-item active">Editar</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<div class="container-fluid">
    <form action="/produtos/editar/<?php echo $data['registros']['produtoid']; ?>" method="post">
        <div class="col-md-6">
            <div class="card card-default">
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>CNPJ Estabelecimento</label>
                                <input name="cnpj_estab" id="cnpj_estab" value="<?php echo $data['registros']['produtocnpj_estab']; ?>" onfocus="javascript: retirarFormatacao(this);" onblur="javascript: formatarCampo(this);" maxlength="14" type="text" class="form-control" placeholder="00.000.000/000-00" required="required">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label> Código Produto</label>
                                <input name="cod_produto" type="number" value="<?php echo $data['registros']['produtocod_produto']; ?>" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label> Descrição</label>
                                <input name="descricao" type="text" value="<?php echo $data['registros']['produtodescricao']; ?>" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label> Código Unidade Medida</label>
                                <select name="cod_unid_med" id="cod_unid_med" class="form-control col-md-12">
                                    <?php
                                    $unidades = $this->model('UnidMed');
                                    

                                    foreach ($unidades->getAll() as $key => $value) {
                                        if ($data[registros][unid_medcod_unid_med] == $value['unid_medcod_unid_med']) {
                                            $status = "selected";
                                        } else {
                                            $status = "";
                                        }

                                        ?>

                                        <option <?php echo $status; ?> value="<?php echo $value['unid_medid']; ?>"><?php echo $value['unid_medcod_unid_med']; ?></option>
                                    <?php } ?>
                                </select>

                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="custom-control custom-switch">
                                <input type="checkbox" name="ind_ativo" <?php if ($data['registros']['produtoind_ativo'] == 0) {
                                                                                                                echo "checked";
                                                                                                            } ?> class="custom-control-input" id="ind_ativo">
                                <label class="custom-control-label" for="ind_ativo" style="margin-top:25%;">Produto Ativo</label>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    </br>
                    <input type="submit" name="Cadastar" class="btn btn-primary float-right" value="Cadastrar" style="background-color: indigo; border-color: indigo; margin:6px;">
                    <a href="/produtos/index" class="btn btn-default float-right" style="margin:6px;">Cancelar</a>
                </div>
    </form>
</div>
<script>
    function formatarCampo(campoTexto) {
        campoTexto.value = mascaraCnpj(campoTexto.value);
    }

    function retirarFormatacao(campoTexto) {
        campoTexto.value = campoTexto.value.replace(/(\.|\/|\-)/g, "");
    }

    function mascaraCnpj(valor) {
        return valor.replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/g, "\$1.\$2.\$3\/\$4\-\$5");
    }
</script>
</div>
</div>