<style>
    .nav-tabs .nav-item .nav-link {
        color: #6039A8 !important;
    }
</style>
<!-- Content Header (Page header)  -->
<section class="content-header">
    <div class="container" style="background: pink;">
        <?php
        if (!empty($data['mensagem'])) :
            foreach ($data['mensagem'] as $m) :
                echo $m . "<br>";
            endforeach;
        endif;
        ?>
    </div>
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h4>Detalhes Produtos</h4>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="/empresas/index" style="color:indigo">Index</a></li>
                    <li class="breadcrumb-item active">Detalhamento</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="row">
        <div class="col-md-8">
            <div class="card card-purple card-outline card-outline-tabs">
                <div class="card-header p-0 pt-1 border-bottom-0">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item"><a class="nav-link active" href="#editar" data-toggle="tab">Editar</a></li>
                        <!-- <li class="nav-item"><a class="nav-link " href="#resumo" data-toggle="tab">Resumo</a></li> -->
                    </ul>
                </div><!-- /.card-header -->
                <div class="card-body">
                    <div class="tab-content">
                        <div class="active tab-pane" id="editar">
                            <section class="content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="invoice p-3 mb-3">
                                            <div class="card-body">
                                                <form action="/produtos/editar/<?php echo $data['produtoid']; ?>" method="post" class="form-horizontal form-label-left">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label>CNPJ Estabelecimento</label>
                                                                <input name="cnpj_estab" id="cnpj_estab" value="<?php echo $data['produtocnpj_estab']; ?>" onfocus="javascript: retirarFormatacao(this);" onblur="javascript: formatarCampo(this);" maxlength="14" type="text" class="form-control" placeholder="00.000.000/000-00" required="required">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label> Código Produto</label>
                                                                <input name="cod_produto" id="cod_produto" value="<?php echo $data['produtocod_produto']; ?>" type="number" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label> Descrição</label>
                                                                <input name="descricao" id="descricao" value="<?php echo $data['produtodescricao']; ?>" type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label> Código Unidade Medida</label>
                                                                <select name="cod_unid_med" id="cod_unid_med" class="form-control col-md-12">
                                                                    <?php
                                                                    $unidades = $this->model('UnidMed');
                                                                    foreach ($unidades->getAll() as $key => $value) {
                                                                        if ($data[unid_medcod_unid_med] == $value['unid_medcod_unid_med']) {
                                                                            $status = "selected";
                                                                        } else {
                                                                            $status = "";
                                                                        }
                                                                        ?>
                                                                        <option <?php echo $status; ?> value="<?php echo $value['unid_medid']; ?>"><?php echo $value['unid_medcod_unid_med']; ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-6">
                                                            <div class="custom-control custom-switch">
                                                                <div class="custom-control custom-switch" style="margin-top:37px;">
                                                                    <input type="checkbox" name="vendasind_ativo" <?php if ($data['produtoind_ativo'] == 0) {
                                                                                                                echo "checked";
                                                                                                            } ?> class="custom-control-input" id="vendasind_ativo">
                                                                    <label class="custom-control-label" for="vendasind_ativo">Grupo Produto Ativo</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>                             
                                            </div>
                                            <div class="card-footer">
                                                <input type="submit" name="Atualizar" class="btn btn-purple btn-md float-right" value="Salvar">
                                                <a href="/produtos/index" class="btn btn-orange btn-md float-right" style="margin-right:4px;">Cancelar</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div> <!-- /.tab-pane // fecha editar-->
                    </div> <!-- /.tab-content -->
                </div><!-- /.card-body -->
            </div> <!-- /.nav-tabs-custom -->
        </div> <!-- /.col -->
    </div> <!-- /.row -->
</section>
<script>
    function formatarCampo(campoTexto) {
        campoTexto.value = mascaraCnpj(campoTexto.value);
    }
    function retirarFormatacao(campoTexto) {
        campoTexto.value = campoTexto.value.replace(/(\.|\/|\-)/g, "");
    }
    function mascaraCnpj(valor) {
        return valor.replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/g, "\$1.\$2.\$3\/\$4\-\$5");
    }
</script>
<script>
    $(document).ready(function() {
        $('#table_logs').DataTable({});
        $('#table_unid_neg').DataTable({
            scrollY: 300,
            "sScrollX": "100%",
            "bScrollCollapse": true,
            paging: false
        });
    });
</script>