<!-- Content Header (Page header)  -->
<section class="content-header">
    <div class="container" style="background: pink;">
        <?php
        echo mensagens($data['mensagem']);
        ?>
    </div>
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h4>Meus Produtos</h4>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a style="color: indigo" href="#">Index</a></li>
                    <li class="breadcrumb-item active"></li>
                </ol>
            </div>
        </div>
    </div>
</section>
<div id="content">
    <div class="container">
        <div class="row bar">
            <div class="col-md-12">
                <div class="products-big">
                    <div class="row products">
                        <?php foreach ($data['user_prods'] as $value) : ?>
                            <div class="col-lg-4">
                                <div class="product" style="margin:1.5%;">
                                    <div class="imgLiquidNoFill image zoom" id=""><a href="shop-detail.html">
                                            <img src='<?=URL_BASE?>fotos_produtos/<?= $value['cod_foto_1'] ?>' class='img-fluid'>
                                        </a></div>
                                    <div class="text" style="margin-top:-7%;">
                                        <h5><a href="shop-detail.html"><?= $value['nome_prod_v'] ?></a></h3>
                                            <p class="price">
                                                <!-- <del>$280</del> --> <?= 'R$: ' . number_format($value['val_prod'], 2, ',', '') ?>
                                            </p>
                                    </div>
                                    <div class="ribbon-holder">
                                        <?php
                                        $unidades = $value['unidades'];
                                        if ($value['status_prod'] == 1) {
                                            echo "<div class='ribbon sale'>À venda</div>
                                            <div class='ribbon quantidade'>Unidades:$unidades</div>";
                                        } else {
                                            if ($value['status_prod'] == 2) {
                                                echo "<div class='ribbon new'>Oculto</div>";
                                                echo "<div class='ribbon quantidade' style='width:150px;'>Unidades:$unidades</div>";
                                            } else {
                                                if ($value['status_prod'] == 3) {
                                                    echo "<div class='ribbon sold' style='width:150px;'>Indisponível</div>";
                                                    echo "<div class='ribbon sold' style='width:150px;'>Unidades:$unidades</div>";
                                                } else {
                                                    if ($value['status_prod'] == 4) {
                                                        echo "<div class='ribbon indis'>Indisponível</div>";
                                                        echo "<div class='ribbon indis'>Bloqueado</div>";
                                                    } else {
                                                        if ($value['status_prod'] == 5) {
                                                            echo "<div class='ribbon quantidade'>Vendido!</div>";
                                                            echo "<div class='ribbon sale' style='width:200px;'>Aguardando envio</div>";
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        ?>
                                        <?php
                                        if ($value['status_prod'] == 2) {
                                            echo "<a class='btn btn-green' href='/meusprod/desocultar/" . $value['cod_uni_prod'] . "'>Desocultar</a>";
                                        } else {
                                            echo "<a class='btn btn-green' href='/meusprod/ocultar/" . $value['cod_uni_prod'] . "'>Ocultar</a>";
                                        }
                                        ?>
                                        <a class="btn btn-green" href="/meusprod/editar/<?php echo $value['cod_uni_prod']; ?>">Editar</a>
                                        <a class="btn btn-red" onclick="verificadel(<?php echo $value['cod_uni_prod']; ?>)">Excluir</a>
                                    </div>
                                    <script>
                                        $(document).ready(function() {
                                            $('#e').hide();
                                        });
                                    </script>
                                    <br>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 banner mb-small text-center"><a href="#"><img src="img/banner2.jpg" alt="" class="img-fluid"></a></div>
                </div>
                <div class="pages">
                    <p class="loadMore text-center"><a href="#" class="btn btn-template-outlined"><i class="fa fa-chevron-down"></i> Ver Mais</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $(".imgLiquidNoFill").imgLiquid({
            fill: false,
            horizontalAlign: "center",
            verticalAlign: "50%"
        });
    });
</script>
<script>
    function verificadel(cod) {
        Swal.fire({
            title: "Deseja Excluir?",
            text: "Não será possível reverter esta ação!",
            icon: 'warning',
            showCancelButton: true,
            cancelButtonColor: '#3085d6',
            confirmButtonColor: '#d33',
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Deletar',
            showClass: {
                popup: 'animate__animated animate__bounceInUp'
            },
            hideClass: {
                popup: 'animate__animated animate__bounceOutDown'
            }
        }).then((result) => {
            if (result.value) {
                window.location.href = "/meusprod/excluir/" + cod;
            }
        })
    };
</script>