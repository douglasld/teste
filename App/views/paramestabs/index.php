<!-- Content Header (Page header)  -->
<section class="content-header">
    <div class="container" style="background: pink;">
        <?php
        if (!empty($data['mensagem'])) :
            foreach ($data['mensagem'] as $m) :
                echo $m . "<br>";
            endforeach;
        endif;
        ?>
    </div>
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h4>Parâmetro de Estabelecimento</h4>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a style="color: indigo" href="#">Index</a></li>
                    <li class="breadcrumb-item active"></li>
                </ol>
            </div>
        </div>
    </div>
</section>
<!-- Main Content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="btn-group">
                        <div> <a href="/paramestabs/cadastrar" type="button" class="btn btn-block btn-purple btn-sm">Adicionar P. Estab.</a> </div>
                        &nbsp
                        <div> <a href="/paramestabs/exportar" type="button" class="btn btn-block btn-orange btn-sm">Exportar</a> </div>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <table id="table" class="display responsive nowrap" style="width:100%">
                                <thead>
                                    <tr>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">Atalhos</th> <!-- botões -->
                                        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">CNPJ Estabelecimento</th>
                                        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">% Rendimento Feriado</th>
                                        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Valor Bônus Extra</th>
                                        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">% Bônus Extra</th>
                                        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Ativo</th>
                                    </tr>
                                </thead>
                                <!-- tbody -->
                                <tbody>
                                    <?php $res_gru_prodEmpresa = $this->model('ParamEstab'); ?>
                                    <?php foreach ($data['registros'] as $empresas) : ?>
                                    <?php endforeach; ?>
                                    <?php foreach ($res_gru_prodEmpresa->getAllEstabelecimento($empresas['param_estabcnpj_estab']) as $value) : ?>
                                        <tr>
                                            <td>
                                                <a title="Detalhar" href="/paramestabs/ver/<?php echo $value['param_estabid']; ?>" type="button" class="btn btn-purple btn-sm"><i class="fas fa-eye"></i></a> <!-- visualizar -->
                                                <a title="Excluir" onclick="verificadel()" href="/paramestabs/excluir/<?php echo $value['param_estabid']; ?>" type="button" class="btn btn-purple btn-sm"><i class="fas fa-trash"></i></a> <!-- excluir -->
                                            </td>
                                            <td>
                                                <?php
                                                if (strlen($value['param_estabcnpj_estab'] <= 10)) {
                                                    echo $value['param_estabcnpj_estab'];
                                                } else {
                                                    echo Mask('##.###.###/####-##', $value['param_estabcnpj_estab']);
                                                }
                                                ?>
                                            </td>                                           
                                            <td><?php echo number_format($value['param_estabperc_red_meta_feriado'], 2, ',', '.'); ?></td>
                                            <td><?php echo number_format($value['param_estabvl_bonus_extra'], 2, ',', '.'); ?></td>
                                            <td><?php echo number_format($value['param_estabperc_bonus_extra'], 2, ',', '.'); ?></td>
                                            <td><?php echo $value['param_estabind_ativo'] ? "Desativado" : "Ativo"; ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
</section>
<script>
    $(document).ready(function() {
        $('#table').DataTable({
            scrollY: 300,
            paging: false
        });
    });

    function verificadel() {
        var x;
        var r = confirm("Deseja excluir a linha?");
        if (r == true) {
        } else {
            event.preventDefault();
        }
    }
</script>