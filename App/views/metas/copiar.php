<style>
    .nav-tabs .nav-item .nav-link {
        color: #6039A8 !important;
    }
</style>
<!-- Content Header (Page header)  -->
<section class="content-header">
    <div class="container" style="background: pink;">
        <?php
        if (!empty($data['mensagem'])) :
            foreach ($data['mensagem'] as $m) :
                echo $m . "<br>";
            endforeach;
        endif;
        $estabelecimentos = $this->model('Empresa');
        $unidades = $this->model('UnidNeg');
        $user_estab = $this->model('UserEstab');
        $meta = $this->model('Meta');
        foreach ($data['registros'] as $value_meta) {
            $estab = $value_meta['metacnpj_estab'];
        }
        $gerente = $user_estab->FindCodUser($value_meta['metacod_gerente']);
        $vendedor = $user_estab->FindCodUser($value_meta['metacod_vendedor']);
        $ultima_meta = $meta->GetUltimaMeta($estab);
        ?>
    </div>
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h4>Detalhes Turnos</h4>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="/empresas/index" style="color:indigo">Index</a></li>
                    <li class="breadcrumb-item active">Detalhamento</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="row">
        <div class="col-md-8">
            <div class="card card-purple card-outline card-outline-tabs">
                <div class="card-header p-0 pt-1 border-bottom-0">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item"><a class="nav-link active" href="#editar" data-toggle="tab">Editar</a></li>
                        <a href="/metas/index" class="nav-link" style="margin-left:5px;">Listar</a>
                        <!-- <li class="nav-item"><a class="nav-link " href="#resumo" data-toggle="tab">Resumo</a></li> -->
                    </ul>
                </div><!-- /.card-header -->
                <div class="card-body">
                    <div class="tab-content">
                        <div class="active tab-pane" id="editar">
                            <section class="content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="invoice p-3 mb-3">
                                            <div class="card-body">
                                                <form action="/metas/copiarmeta/<?php echo $data['registros']['empresaid']; ?>" method="post" class="form-horizontal form-label-left">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label>CNPJ Estabelecimento</label>
                                                                <?php foreach ($estabelecimentos->getPorMatriz($_SESSION['matriz']) as $value) : ?>
                                                                    <?php
                                                                    if ($value_meta['metacnpj_estab'] == $value['empresacnpj_estab']) {
                                                                        $nome = $value['empresanome_abrev'];
                                                                        $cnpj = $value['empresacnpj_estab'];
                                                                    }
                                                                    ?>
                                                                <?php endforeach; ?>
                                                                <input class="form-control col-md-12" value="<?php echo $cnpj . ' - ' . $nome; ?>" name="empresacnpj_estab" id="empresacnpj_estab" list="list" autocomplete="off" required="">
                                                                <datalist id="list">
                                                                    <?php foreach ($estabelecimentos->getPorMatriz($_SESSION['matriz']) as $value) : ?>
                                                                        <?php
                                                                        $cnpj_cpf = $value['empresacnpj_estab'];
                                                                        $cnpj = preg_replace("/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/", "\$1.\$2.\$3/\$4-\$5", $cnpj_cpf);
                                                                        ?>
                                                                        <option value="<?php echo $cnpj . ' - ' . $value['empresanome_abrev']; ?>"></option>
                                                                    <?php endforeach; ?>
                                                                </datalist>
                                                                </input>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label>Unidade de Negócio</label>
                                                                <?php foreach ($unidades->getAllEstabelecimento($estab) as $value) : ?>
                                                                    <?php
                                                                    if ($value_meta['metacod_unid_neg'] == $value['unid_negid']) {
                                                                        $id =  $value['unid_negid'];
                                                                        $desc =  $value['unid_negdescricao'];
                                                                    }
                                                                    ?>
                                                                <?php endforeach; ?>
                                                                <input class="form-control col-md-12" value="<?php echo $id . ' - ' . $desc ?>" name="cod_unid_neg" id="cod_unid_neg" list="lista" autocomplete="off" required="">
                                                                <datalist id="lista">
                                                                </datalist>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label> Código do Gerente</label>
                                                                <input name="cod_gerente" id="cod_gerente" value="<?php echo $value_meta['metacod_gerente'] . ' - ' . $gerente['user_estabnome_user']; ?>" list="listgerente" class="form-control" autocomplete="off" required="">
                                                                <datalist id="listgerente">
                                                                </datalist>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label> Código do Vendedor</label>
                                                                <input name="cod_vendedor" id="cod_vendedor" value="<?php echo $value_meta['metacod_vendedor'] . ' - ' . $vendedor['user_estabnome_user']; ?>" list="litvendedor" class="form-control" autocomplete="off" required="">
                                                                <datalist id="litvendedor">
                                                                </datalist>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label> Grupo de Produto</label>
                                                                <?php
                                                                $grupos  = $this->model('GrupoProd');
                                                                foreach ($grupos->getAllEstabelecimento($estab) as $key => $value) { ?>
                                                                    <?php
                                                                    if ($value_meta['metacod_grupo_prod'] == $value['grupo_prodcod_grupo_prod']) {
                                                                        $cod_g = $value['grupo_prodcod_grupo_prod'];
                                                                        $desc_g = $value['grupo_proddescricao'];
                                                                    }
                                                                    ?>
                                                                <?php } ?>
                                                                <input name="cod_grupo_prod" id="cod_grupo_prod" value="<?php echo $cod_g . ' - ' . $desc_g; ?> " class="select2_single form-control col-md-12" list="gprod" autocomplete="off">
                                                                <datalist id="gprod">
                                                                </datalist>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label> Código do Produto</label>
                                                                <?php
                                                                $produtos = $this->model('Produto');
                                                                foreach ($produtos->getPorEstabelecimento($estab) as $key => $value) { ?>
                                                                    <?php
                                                                    if ($value_meta['metacod_produto'] == $value['produtocod_produto']) {
                                                                        $cod_p = $value['produtocod_produto'];
                                                                        $nome_p = $value['produtodescricao'];
                                                                    }
                                                                    ?>
                                                                <?php } ?>
                                                                <input name="cod_produto" value="<?php echo $cod_p . ' - ' . $nome_p; ?>" id="cod_produto" class="select2_single form-control col-md-12" list="prod" autocomplete="off">
                                                                <datalist id="prod">
                                                                </datalist>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label> Código Meta</label>
                                                                <input name="cod_meta" readonly id="cod_meta" value="<?php echo $ultima_meta; ?>" type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label> Descricão</label>
                                                                <input name="descricao" id="descricao" value="<?php echo $value_meta['metadescricao']; ?>" type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label> Código do Produto Pai</label>
                                                                <?php
                                                                $grupos  = $this->model('GrupoProd');
                                                                foreach ($grupos->getAllEstabelecimento($estab) as $key => $value) { ?>
                                                                    <?php
                                                                    if ($value_meta['metacod_grupo_prod_pai'] == $value['grupo_prodcod_grupo_prod']) {
                                                                        $cod_p = $value_meta['metacod_grupo_prod_pai'];
                                                                        $desc_p = $value['grupo_proddescricao'];
                                                                    }
                                                                    ?>
                                                                <?php }
                                                                ?>
                                                                <input name="cod_grupo_prod_pai" value="<?php echo $cod_p . ' - ' . $desc_p; ?>" id="cod_grupo_prod_pai" class="select2_single form-control col-md-12" list="prodp" autocomplete="off">
                                                                <datalist id="prodp">
                                                                </datalist>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label> Unidade de Cálculo</label>
                                                                <?php
                                                                $unidades = $this->model('UnidMed');
                                                                foreach ($unidades->getAllEstabelecimento($estab) as $key => $value) {
                                                                ?>
                                                                    <?php
                                                                    if ($value_meta['metaunid_calc'] == $value['unid_medcod_unid_med']) {
                                                                        $cod_u = $value['unid_medcod_unid_med'];
                                                                        $desc_u = $value['unid_meddescricao'];
                                                                    }
                                                                    ?>
                                                                <?php } ?>
                                                                <input name="unid_calc" id="unid_calc" value="<?php echo $cod_u . ' - ' . $desc_u; ?>" class="select2_single form-control col-md-12" list="listunidc" autocomplete="off" required="">
                                                                <datalist id="listunidc">
                                                                </datalist>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label> Valor Meta</label>
                                                                <input name="vl_meta" id="vl_meta" value="<?php echo $value_meta['metavl_meta']; ?>" type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label> Valor Mínimo</label>
                                                                <input name="vl_minimo" id="vl_minimo" value="<?php echo $value_meta['metavl_minimo']; ?>" type="text" class="form-control" placeholder="10">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label> Valor Variável</label>
                                                                <input name="vl_variavel" id="vl_variavel" value="<?php echo $value_meta['metavl_variavel']; ?>" type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label> Valor Adicional</label>
                                                                <input name="vl_adic" id="vl_adic" value="<?php echo $value_meta['metavl_adic']; ?>" type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label> Valor Bônus</label>
                                                                <input name="vl_bonus" id="vl_bonus" value="<?php echo $value_meta['metavl_bonus']; ?>" type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label> Código Meta Base</label>
                                                                <input name="cod_meta_base" id="cod_meta_base" value="<?php echo $value_meta['metacod_meta_base']; ?>" type="text" class="form-control" list="listmtb" autocomplete="off">
                                                                <datalist id="listmtb">
                                                                </datalist>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label> Data Meta</label>
                                                                <?php
                                                                $partes = explode("-", $value_meta['metadt_meta']);
                                                                $value_meta['metadt_meta'] = $partes[0] . '-' . $partes[1];
                                                                ?>
                                                                <input name="dt_meta" id="dt_meta" value="<?php echo $value_meta['metadt_meta']; ?>" type="month" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label> Data Início Valor</label>
                                                                <input name="dt_ini_val" id="dt_ini_val" value="<?php echo $value_meta['metadt_ini_val']; ?>" type="date" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label> Data Fim Valor</label>
                                                                <input name="dt_fim_val" id="dt_fim_val" value="<?php echo $value_meta['metadt_fim_val']; ?>" type="date" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <div class="custom-control custom-switch">
                                                                    <input type="checkbox" class="custom-control-input" id="ind_ativo" name="ind_ativo" <?php
                                                                                                                                                        if ($value_meta['metaind_ativo'] == "0") {
                                                                                                                                                            echo "checked";
                                                                                                                                                        } ?>>
                                                                    <label class="custom-control-label" for="ind_ativo" style="margin-top:9%;">Meta Ativa</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                            <!-- /.card-body -->
                                            <div class="card-footer">
                                                <input type="submit" name="Cadastrar" class="btn btn-primary float-right" id="Atualizar" value="Salvar" style="background-color: indigo; border-color: indigo; margin:6px;">
                                                <a href="/metas/index" class="btn btn-orange btn-md float-right" style="margin:6px;">Cancelar</a>
                                            </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div> <!-- /.tab-pane // fecha editar-->
                    </div> <!-- /.tab-content -->
                </div><!-- /.card-body -->
            </div> <!-- /.nav-tabs-custom -->
        </div> <!-- /.col -->
    </div> <!-- /.row -->
</section>
<script>
    function formatarCampo(campoTexto) {
        campoTexto.value = mascaraCnpj(campoTexto.value);
    }

    function retirarFormatacao(campoTexto) {
        campoTexto.value = campoTexto.value.replace(/(\.|\/|\-)/g, "");
    }

    function mascaraCnpj(valor) {
        return valor.replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/g, "\$1.\$2.\$3\/\$4\-\$5");
    }
</script>
<script>
    $(document).ready(function() {
        $('#table_logs').DataTable({});
        $('#table_unid_neg').DataTable({
            scrollY: 300,
            "sScrollX": "100%",
            "bScrollCollapse": true,
            paging: false
        });
    });
</script>
<script type="text/javascript">
    $("#Atualizar").click(function(event) {
        var vl_meta = document.getElementById('vl_meta').value;
        var vl_minimo = document.getElementById('vl_minimo').value;
        var dt_meta = document.getElementById('dt_meta').value;
        var dt_ini_val = document.getElementById('dt_ini_val').value;
        var dt_fim_val = document.getElementById('dt_fim_val').value;
        dt_meta = dt_meta.concat('-01');

        //formata data_meta
        var partesData = dt_meta.split("-");
        var data_formatada = new Date(partesData[0], partesData[1] - 1, partesData[2]);
        var data_calcula = new Date(data_formatada);

        //obtem primeiro e ultimo dia
        var primeiroDia = new Date(data_calcula.getFullYear(), data_calcula.getMonth(), 1);
        var ultimoDia = new Date(data_calcula.getFullYear(), data_calcula.getMonth() + 1, 0);

        //trata data_ini_val
        var dt_ini_val_c = new Date(dt_ini_val);

        //trata data_fim_val
        var dt_fim_val_c = new Date(dt_fim_val);


        //trata val_meta
        vl_meta = vl_meta.replace(/[.]/gi, '');
        vl_meta = vl_meta.replace(/[^a-z0-9]/gi, '.');

        //trata_val_minimo
        vl_minimo = vl_minimo.replace(/[.]/gi, '');
        vl_minimo = vl_minimo.replace(/[^a-z0-9]/gi, '.');

        //efetua as verificações necessárias
        if (dt_ini_val_c > ultimoDia) {
            alert("Data início meta não deve ser maior que a data da Meta");
            event.preventDefault();
        }

        if (dt_ini_val_c < primeiroDia) {
            alert("Data início meta não deve ser menor que a data da Meta");
            event.preventDefault();
        }

        if (dt_fim_val_c > ultimoDia) {
            alert("Data fim meta não deve ser maior que a data da Meta");
            event.preventDefault();
        }

        if (dt_fim_val_c < primeiroDia) {
            alert("Data fim meta não deve ser menor que a data da Meta");
            event.preventDefault();
        }

        if (vl_meta < vl_minimo) {
            alert("Valor mínimo não deve ser maior do que o valor Meta");
            event.preventDefault();
        }
    });
</script>

<script type="text/javascript">
    //VALOR META
    $(document).ready(function() {
        $('#vl_meta').mask('000.000.000.000.000,00', {
            reverse: true
        });
    });
    $("#vl_meta").focusout(function() {
        if ($(this).val().length <= 2) {
            if ($(this).val().length != '') {
                temp = $(this).val()
                var newNum = temp + ",00"
                $(this).val(newNum)
            }
        }
    });

    //VALOR VARIAVEL
    $(document).ready(function() {
        $('#vl_variavel').mask('000.000.000.000.000,00', {
            reverse: true
        });
    });
    $("#vl_variavel").focusout(function() {
        if ($(this).val().length <= 2) {
            if ($(this).val().length != '') {
                temp = $(this).val()
                var newNum = temp + ",00"
                $(this).val(newNum)
            }
        }
    });

    //VALOR MINIMO
    $(document).ready(function() {
        $('#vl_minimo').mask('000.000.000.000.000,00', {
            reverse: true
        });
    });
    $("#vl_minimo").focusout(function() {
        if ($(this).val().length <= 2) {
            if ($(this).val().length != '') {
                temp = $(this).val()
                var newNum = temp + ",00"
                $(this).val(newNum)
            }
        }
    });

    //VALOR ADICIONAL
    $(document).ready(function() {
        $('#vl_adic').mask('000.000.000.000.000,00', {
            reverse: true
        });
    });
    $("#vl_adic").focusout(function() {
        if ($(this).val().length <= 2) {
            if ($(this).val().length != '') {
                temp = $(this).val()
                var newNum = temp + ",00"
                $(this).val(newNum)
            }
        }
    });

    //VALOR BÔNUS
    $(document).ready(function() {
        $('#vl_bonus').mask('000.000.000.000.000,00', {
            reverse: true
        });
    });
    $("#vl_bonus").focusout(function() {
        if ($(this).val().length <= 2) {
            if ($(this).val().length != '') {
                temp = $(this).val()
                var newNum = temp + ",00"
                $(this).val(newNum)
            }
        }
    });
</script>

<script type="text/javascript">
    $(document).ready(function() {

        //unid_neg
        var id = $($("#empresacnpj_estab")).val();
        //formata o cnpj
        var resultado = id.replace(/[^a-z0-9]/gi, '');
        cnpj_estab_formatado = resultado.slice(0, 14);
        $.post('/ajax/call_autocomplete.php', {
            cnpj_estab: cnpj_estab_formatado,
            func: '1'
        }, function(data) {
            console.log(data);
            var cmb = '<option value=""></option>';
            $.each(data, function(index, value) {
                cmb = cmb + '<option value="' + value.unid_negid + ' - ' + value.unid_negdescricao + '"></option>';
            });
            $('#lista').html(cmb);
        }, 'json');



        //cod_gerente
        //formata o cnpj
        var resultado = id.replace(/[^a-z0-9]/gi, '');
        cnpj_estab_formatado = resultado.slice(0, 14);
        $.post('/ajax/call_autocomplete.php', {
            cnpj_estab: cnpj_estab_formatado,
            func: '2'
        }, function(data) {
            var cmb = '<option value=""></option>';
            $.each(data, function(index, value) {
                cmb = cmb + '<option value="' + value.user_estabcod_user + ' - ' + value.user_estabnome_user + '"></option>';
            });
            $('#listgerente').html(cmb);
        }, 'json');


        //cod_vendedor

        //formata o cnpj
        var resultado = id.replace(/[^a-z0-9]/gi, '');
        cnpj_estab_formatado = resultado.slice(0, 14);
        $.post('/ajax/call_autocomplete.php', {
            cnpj_estab: cnpj_estab_formatado,
            func: '3'
        }, function(data) {
            var cmb = '<option value=""></option>';
            $.each(data, function(index, value) {
                cmb = cmb + '<option value="' + value.user_estabcod_user + ' - ' + value.user_estabnome_user + '"></option>';
            });
            $('#litvendedor').html(cmb);
        }, 'json');


        //cod_groprod
        //formata o cnpj
        var resultado = id.replace(/[^a-z0-9]/gi, '');
        cnpj_estab_formatado = resultado.slice(0, 14);
        $.post('/ajax/call_autocomplete.php', {
            cnpj_estab: cnpj_estab_formatado,
            func: '4'
        }, function(data) {
            var cmb = '<option value=""></option>';
            $.each(data, function(index, value) {
                cmb = cmb + '<option value="' + value.grupo_prodcod_grupo_prod + ' - ' + value.grupo_proddescricao + '"></option>';
            });
            $('#gprod').html(cmb);
        }, 'json');

        //produto
        //formata o cnpj
        var resultado = id.replace(/[^a-z0-9]/gi, '');
        cnpj_estab_formatado = resultado.slice(0, 14);
        var cod_gprod = $("#cod_grupo_prod").val();
        cod_gprod = cod_gprod.split(" - ");
        var res = cod_gprod[0];
        $.post('/ajax/call_autocomplete.php', {
            cnpj_estab: cnpj_estab_formatado,
            func: '5',
            cod_gprod: res
        }, function(data) {
            var cmb = '<option value=""></option>';
            $.each(data, function(index, value) {
                cmb = cmb + '<option value="' + value.produtocod_produto + ' - ' + value.produtodescricao + '"></option>';
            });
            $('#prod').html(cmb);
        }, 'json');

        $('#cod_grupo_prod').change(function(e) {
            $("#cod_produto").val("");
            //formata o cnpj
            var resultado = id.replace(/[^a-z0-9]/gi, '');
            cnpj_estab_formatado = resultado.slice(0, 14);
            var cod_gprod = $("#cod_grupo_prod").val();
            cod_gprod = cod_gprod.split(" - ");
            var res = cod_gprod[0];
            $.post('/ajax/call_autocomplete.php', {
                cnpj_estab: cnpj_estab_formatado,
                func: '5',
                cod_gprod: res
            }, function(data) {
                var cmb = '<option value=""></option>';
                $.each(data, function(index, value) {
                    cmb = cmb + '<option value="' + value.produtocod_produto + ' - ' + value.produtodescricao + '"></option>';
                });
                $('#prod').html(cmb);
            }, 'json');
        });

        //grupo_pai
        //formata o cnpj
        var resultado = id.replace(/[^a-z0-9]/gi, '');
        cnpj_estab_formatado = resultado.slice(0, 14);
        $.post('/ajax/call_autocomplete.php', {
            cnpj_estab: cnpj_estab_formatado,
            func: '6'
        }, function(data) {
            console.log(data);
            var cmb = '<option value=""></option>';
            $.each(data, function(index, value) {
                cmb = cmb + '<option value="' + value.grupo_prodcod_grupo_prod + ' - ' + value.grupo_proddescricao + '"></option>';
            });
            $('#prodp').html(cmb);
        }, 'json');

        //unid_med
        //formata o cnpj
        var resultado = id.replace(/[^a-z0-9]/gi, '');
        cnpj_estab_formatado = resultado.slice(0, 14);
        $.post('/ajax/call_autocomplete.php', {
            cnpj_estab: cnpj_estab_formatado,
            func: '7'
        }, function(data) {
            console.log(data);
            var cmb = '<option value=""></option>';
            $.each(data, function(index, value) {
                cmb = cmb + '<option value="' + value.unid_medcod_unid_med + ' - ' + value.unid_meddescricao + '"></option>';
            });
            $('#listunidc').html(cmb);
        }, 'json');

        $('#cod_meta').change(function(e) {
            //cod_meta_base
            $("#cod_meta_base").val("");
            var val_cod_meta = $("#cod_meta").val();
            //formata o cnpj
            var resultado = id.replace(/[^a-z0-9]/gi, '');
            cnpj_estab_formatado = resultado.slice(0, 14);
            $.post('/ajax/call_autocomplete.php', {
                cnpj_estab: cnpj_estab_formatado,
                func: '8',
                cod_meta: val_cod_meta
            }, function(data) {
                console.log(data);
                var cmb = '<option value=""></option>';
                $.each(data, function(index, value) {
                    cmb = cmb + '<option value="' + value.metacod_meta + ' - ' + value.metadescricao + '"></option>';
                });
                $('#listmtb').html(cmb);
            }, 'json');
        });


        /////////

        $('#empresacnpj_estab').change(function(e) {
            var id = $(this).val();
            //unid_neg
            $("#cod_unid_neg").val("");
            //formata o cnpj
            var resultado = id.replace(/[^a-z0-9]/gi, '');
            cnpj_estab_formatado = resultado.slice(0, 14);
            $.post('/ajax/call_autocomplete.php', {
                cnpj_estab: cnpj_estab_formatado,
                func: '1'
            }, function(data) {
                console.log(data);
                var cmb = '<option value=""></option>';
                $.each(data, function(index, value) {
                    cmb = cmb + '<option value="' + value.unid_negid + ' - ' + value.unid_negdescricao + '"></option>';
                });
                $('#lista').html(cmb);
            }, 'json');

            //cod_gerente
            $("#cod_gerente").val("");
            //formata o cnpj
            var resultado = id.replace(/[^a-z0-9]/gi, '');
            cnpj_estab_formatado = resultado.slice(0, 14);
            $.post('/ajax/call_autocomplete.php', {
                cnpj_estab: cnpj_estab_formatado,
                func: '2'
            }, function(data) {
                var cmb = '<option value=""></option>';
                $.each(data, function(index, value) {
                    cmb = cmb + '<option value="' + value.user_estabcod_user + ' - ' + value.user_estabnome_user + '"></option>';
                });
                $('#listgerente').html(cmb);
            }, 'json');


            //cod_vendedor
            $("#cod_vendedor").val("");
            //formata o cnpj
            var resultado = id.replace(/[^a-z0-9]/gi, '');
            cnpj_estab_formatado = resultado.slice(0, 14);
            $.post('/ajax/call_autocomplete.php', {
                cnpj_estab: cnpj_estab_formatado,
                func: '3'
            }, function(data) {
                var cmb = '<option value=""></option>';
                $.each(data, function(index, value) {
                    cmb = cmb + '<option value="' + value.user_estabcod_user + ' - ' + value.user_estabnome_user + '"></option>';
                });
                $('#litvendedor').html(cmb);
            }, 'json');

            //cod_groprod
            $("#cod_grupo_prod").val("");
            //formata o cnpj
            var resultado = id.replace(/[^a-z0-9]/gi, '');
            cnpj_estab_formatado = resultado.slice(0, 14);
            $.post('/ajax/call_autocomplete.php', {
                cnpj_estab: cnpj_estab_formatado,
                func: '4'
            }, function(data) {
                var cmb = '<option value=""></option>';
                $.each(data, function(index, value) {
                    cmb = cmb + '<option value="' + value.grupo_prodcod_grupo_prod + ' - ' + value.grupo_proddescricao + '"></option>';
                });
                $('#gprod').html(cmb);
            }, 'json');


            $('#cod_grupo_prod').change(function(e) {
                //produto
                $("#cod_produto").val("");
                //formata o cnpj
                var resultado = id.replace(/[^a-z0-9]/gi, '');
                cnpj_estab_formatado = resultado.slice(0, 14);
                var cod_gprod = $("#cod_grupo_prod").val();
                cod_gprod = cod_gprod.split(" - ");
                var res = cod_gprod[0];
                $.post('/ajax/call_autocomplete.php', {
                    cnpj_estab: cnpj_estab_formatado,
                    func: '5',
                    cod_gprod: res
                }, function(data) {
                    var cmb = '<option value=""></option>';
                    $.each(data, function(index, value) {
                        cmb = cmb + '<option value="' + value.produtocod_produto + ' - ' + value.produtodescricao + '"></option>';
                    });
                    $('#prod').html(cmb);
                }, 'json');

            });

            //grupo_pai
            $("#cod_grupo_prod_pai").val("");
            //formata o cnpj
            var resultado = id.replace(/[^a-z0-9]/gi, '');
            cnpj_estab_formatado = resultado.slice(0, 14);
            $.post('/ajax/call_autocomplete.php', {
                cnpj_estab: cnpj_estab_formatado,
                func: '6'
            }, function(data) {
                console.log(data);
                var cmb = '<option value=""></option>';
                $.each(data, function(index, value) {
                    cmb = cmb + '<option value="' + value.grupo_prodcod_grupo_prod + ' - ' + value.grupo_proddescricao + '"></option>';
                });
                $('#prodp').html(cmb);
            }, 'json');

            //unid_med
            $("#unid_calc").val("");
            //formata o cnpj
            var resultado = id.replace(/[^a-z0-9]/gi, '');
            cnpj_estab_formatado = resultado.slice(0, 14);
            $.post('/ajax/call_autocomplete.php', {
                cnpj_estab: cnpj_estab_formatado,
                func: '7'
            }, function(data) {
                console.log(data);
                var cmb = '<option value=""></option>';
                $.each(data, function(index, value) {
                    cmb = cmb + '<option value="' + value.unid_medcod_unid_med + ' - ' + value.unid_meddescricao + '"></option>';
                });
                $('#listunidc').html(cmb);
            }, 'json');

            $('#cod_meta').change(function(e) {
                //cod_meta_base
                $("#cod_meta_base").val("");
                var val_cod_meta = $("#cod_meta").val();
                //formata o cnpj
                var resultado = id.replace(/[^a-z0-9]/gi, '');
                cnpj_estab_formatado = resultado.slice(0, 14);
                $.post('/ajax/call_autocomplete.php', {
                    cnpj_estab: cnpj_estab_formatado,
                    func: '8',
                    cod_meta: val_cod_meta
                }, function(data) {
                    console.log(data);
                    var cmb = '<option value=""></option>';
                    $.each(data, function(index, value) {
                        cmb = cmb + '<option value="' + value.metacod_meta + ' - ' + value.metadescricao + '"></option>';
                    });
                    $('#listmtb').html(cmb);
                }, 'json');
            });
        });
    });
</script>