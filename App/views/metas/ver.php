<style>
    .nav-tabs .nav-item .nav-link {
        color: #6039A8 !important;
    }
</style>
<!-- Content Header (Page header)  -->
<section class="content-header">
    <div class="container" style="background: pink;">
        <?php
        if (!empty($data['mensagem'])) :
            foreach ($data['mensagem'] as $m) :
                echo $m . "<br>";
            endforeach;
        endif;
        $estabelecimentos = $this->model('Empresa');
        $unidades = $this->model('UnidNeg');
        $user_estab = $this->model('UserEstab');
        $user_estab = $this->model('UserEstab');
        foreach ($data['registros'] as $value_meta) {
            $estab = $value_meta['metacnpj_estab'];
        }
        $gerente = $user_estab->FindCodUser($value_meta['metacod_gerente']);
        $vendedor = $user_estab->FindCodUser($value_meta['metacod_vendedor']);
        ?>
    </div>
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h4>Detalhes Turnos</h4>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="/empresas/index" style="color:indigo">Index</a></li>
                    <li class="breadcrumb-item active">Detalhamento</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="row">
        <div class="col-md-8">
            <div class="card card-purple card-outline card-outline-tabs">
                <div class="card-header p-0 pt-1 border-bottom-0">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item"><a class="nav-link active" href="#editar" data-toggle="tab">Editar</a></li>
                        <a href="/metas/index" class="nav-link" style="margin-left:5px;">Listar</a>
                        <!-- <li class="nav-item"><a class="nav-link " href="#resumo" data-toggle="tab">Resumo</a></li> -->
                    </ul>
                </div><!-- /.card-header -->
                <div class="card-body">
                    <div class="tab-content">
                        <div class="active tab-pane" id="editar">
                            <section class="content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="invoice p-3 mb-3">
                                            <div class="card-body">
                                                <form action="/metas/editar/<?php echo $value_meta['metaid']; ?>" method="post" class="form-horizontal form-label-left">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-12" for="cidade">CNPJ Estabelecimento</label>
                                                                <?php foreach ($estabelecimentos->getPorMatriz($_SESSION['matriz']) as $value) : ?>
                                                                    <?php
                                                                    if ($value_meta['metacnpj_estab'] == $value['empresacnpj_estab']) {
                                                                        echo '<input id="empresacnpj_estab" name="empresacnpj_estab" readonly type="text" class="form-control" value="' . $value['empresacnpj_estab'] . ' - ' . $value['empresanome_abrev'] . '">';
                                                                    } else {
                                                                    }
                                                                    ?>
                                                                <?php endforeach; ?>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label> Código Meta</label>
                                                                <input name="cod_meta" id="cod_meta" value="<?php echo $value_meta['metacod_meta']; ?>" readonly type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label> Código do Gerente</label>
                                                                <input name="cod_gerente" id="cod_gerente" readonly value="<?php echo $value_meta['metacod_gerente'] . ' - ' . $gerente['user_estabnome_user']; ?>" type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label> Código do Vendedor</label>
                                                                <input name="cod_vendedor" id="cod_vendedor" readonly value="<?php echo $value_meta['metacod_vendedor'] . ' - ' . $vendedor['user_estabnome_user']; ?>" type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label> Grupo de Produto</label>
                                                                <?php
                                                                $grupos  = $this->model('GrupoProd');
                                                                foreach ($grupos->getAllEstabelecimento($estab) as $key => $value) { ?><?php
                                                                                                                                        if ($value_meta['metacod_grupo_prod'] == $value['grupo_prodcod_grupo_prod']) {
                                                                                                                                            echo '<input type="text" readonly name="cod_grupo_prod" id="cod_grupo_prod"
                                                                 class="form-control" value="' . $value['grupo_prodcod_grupo_prod'] . ' - ' . $value['grupo_proddescricao'] . '">';
                                                                                                                                        }
                                                                                                                                        ?>
                                                            <?php } ?>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label> Código do Produto</label>
                                                                <?php
                                                                $produtos = $this->model('Produto');
                                                                foreach ($produtos->getPorEstabelecimento($estab) as $key => $value) { ?>
                                                                    <?php
                                                                    if ($value_meta['metacod_produto'] == $value['produtocod_produto']) {
                                                                        echo '<input class="form-control" readonly type="text" name="cod_produto" id="cod_produto"  type="text" class="form-control"
                                                                              value="' . $value['produtocod_produto'] . ' - ' . $value['produtodescricao'] . '">';
                                                                    }
                                                                    ?>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label> Unidade de Negócio</label>
                                                                <?php foreach ($unidades->getAllEstabelecimento($estab) as $value) : ?>
                                                                    <?php
                                                                    if ($value_meta['metacod_unid_neg'] == $value['unid_negid']) {
                                                                        echo '<input readonly type="text" class="form-control" name="cod_unid_neg" id="cod_unid_neg" value="' . $value['unid_negid'] . ' - ' . $value['unid_negdescricao'] . ' "selected >';
                                                                    }
                                                                    ?>
                                                                <?php endforeach; ?>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label> Descricão</label>
                                                                <input name="descricao" id="descricao" value="<?php echo $value_meta['metadescricao']; ?>" type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label> Código Grupo Produto Pai</label>
                                                                <?php
                                                                $grupos  = $this->model('GrupoProd');
                                                                foreach ($grupos->getAllEstabelecimento($estab) as $key => $value) { ?><?php
                                                                                                                                        if ($value_meta['metacod_grupo_prod_pai'] == $value['grupo_prodcod_grupo_prod']) {
                                                                                                                                            echo '<input type="text" readonly name="cod_grupo_prod_pai" id="cod_grupo_prod_pai" type="text" class="form-control"
                                                                 class="form-control" value="' . $value_meta['metacod_grupo_prod_pai'] . ' - ' . $value['grupo_proddescricao'] . '">';
                                                                                                                                        }
                                                                                                                                        ?>
                                                            <?php } ?>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label> Unidade de Cálculo</label>
                                                                <?php
                                                                $unidades = $this->model('UnidMed');
                                                                foreach ($unidades->getAllEstabelecimento($estab) as $key => $value) {
                                                                ?>
                                                                    <?php
                                                                    if ($value_meta['metaunid_calc'] == $value['unid_medcod_unid_med']) {
                                                                        echo '<input type="text" name="unid_calc" readonly id="unid_calc" class="form-control"
                                                                              value="' . $value['unid_medcod_unid_med'] . ' - ' . $value['unid_meddescricao'] . '">';
                                                                    }
                                                                    ?>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label> Valor Meta</label>
                                                                <input name="vl_meta" id="vl_meta" value="<?php echo $value_meta['metavl_meta']; ?>" type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label> Valor Mínimo</label>
                                                                <input name="vl_minimo" id="vl_minimo" value="<?php echo $value_meta['metavl_minimo']; ?>" type="text" class="form-control" placeholder="10">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label> Valor Variável</label>
                                                                <input name="vl_variavel" id="vl_variavel" value="<?php echo $value_meta['metavl_variavel']; ?>" type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label> Valor Adicional</label>
                                                                <input name="vl_adic" id="vl_adic" value="<?php echo $value_meta['metavl_adic']; ?>" type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label> Valor Bônus</label>
                                                                <input name="vl_bonus" id="vl_bonus" value="<?php echo $value_meta['metavl_bonus']; ?>" type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label> Código Meta Base</label>
                                                                <input name="cod_meta_base" id="cod_meta_base" value="<?php echo $value_meta['metacod_meta_base']; ?>" type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label> Data Meta</label>
                                                                <?php
                                                                $partes = explode("-", $value_meta['metadt_meta']);
                                                                $value_meta['metadt_meta'] = $partes[0] . '-' . $partes[1];
                                                                ?>
                                                                <input name="dt_meta" id="dt_meta" value="<?php echo $value_meta['metadt_meta']; ?>" type="month" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label> Data Início Valor</label>
                                                                <input name="dt_ini_val" id="dt_ini_val" value="<?php echo $value_meta['metadt_ini_val']; ?>" type="date" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label> Data Fim Valor</label>
                                                                <input name="dt_fim_val" id="dt_fim_val" value="<?php echo $value_meta['metadt_fim_val']; ?>" type="date" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <div class="custom-control custom-switch">
                                                                    <input type="checkbox" class="custom-control-input" id="ind_ativo" name="ind_ativo" <?php
                                                                                                                                                        if ($value_meta['metaind_ativo'] == "0") {
                                                                                                                                                            echo "checked";
                                                                                                                                                        } ?>>
                                                                    <label class="custom-control-label" for="ind_ativo" style="margin-top:9%;">Meta Ativa</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                            <!-- /.card-body -->
                                            <div class="card-footer">
                                                <input type="submit" name="Atualizar" class="btn btn-primary float-right" id="Atualizar" value="Salvar" style="background-color: indigo; border-color: indigo; margin:6px;">
                                                <a href="/metas/index" class="btn btn-orange btn-md float-right" style="margin:6px;">Cancelar</a>
                                            </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div> <!-- /.tab-pane // fecha editar-->
                    </div> <!-- /.tab-content -->
                </div><!-- /.card-body -->
            </div> <!-- /.nav-tabs-custom -->
        </div> <!-- /.col -->
    </div> <!-- /.row -->
</section>
<script>
    function formatarCampo(campoTexto) {
        campoTexto.value = mascaraCnpj(campoTexto.value);
    }

    function retirarFormatacao(campoTexto) {
        campoTexto.value = campoTexto.value.replace(/(\.|\/|\-)/g, "");
    }

    function mascaraCnpj(valor) {
        return valor.replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/g, "\$1.\$2.\$3\/\$4\-\$5");
    }
</script>
<script>
    $(document).ready(function() {
        $('#table_logs').DataTable({});
        $('#table_unid_neg').DataTable({
            scrollY: 300,
            "sScrollX": "100%",
            "bScrollCollapse": true,
            paging: false
        });
    });
</script>
<script type="text/javascript">
    $("#Atualizar").click(function(event) {
        var vl_meta = document.getElementById('vl_meta').value;
        var vl_minimo = document.getElementById('vl_minimo').value;
        var dt_meta = document.getElementById('dt_meta').value;
        var dt_ini_val = document.getElementById('dt_ini_val').value;
        var dt_fim_val = document.getElementById('dt_fim_val').value;
        dt_meta = dt_meta.concat('-01');

        //formata data_meta
        var partesData = dt_meta.split("-");
        var data_formatada = new Date(partesData[0], partesData[1] - 1, partesData[2]);
        var data_calcula = new Date(data_formatada);

        //obtem primeiro e ultimo dia
        var primeiroDia = new Date(data_calcula.getFullYear(), data_calcula.getMonth(), 1);
        var ultimoDia = new Date(data_calcula.getFullYear(), data_calcula.getMonth() + 1, 0);

        //trata data_ini_val
        var dt_ini_val_c = new Date(dt_ini_val);

        //trata data_fim_val
        var dt_fim_val_c = new Date(dt_fim_val);


        //trata val_meta
        vl_meta = vl_meta.replace(/[.]/gi, '');
        vl_meta = vl_meta.replace(/[^a-z0-9]/gi, '.');

        //trata_val_minimo
        vl_minimo = vl_minimo.replace(/[.]/gi, '');
        vl_minimo = vl_minimo.replace(/[^a-z0-9]/gi, '.');

        //efetua as verificações necessárias
        if (dt_ini_val_c > ultimoDia) {
            alert("Data início meta não deve ser maior que a data da Meta");
            event.preventDefault();
        }

        if (dt_ini_val_c < primeiroDia) {
            alert("Data início meta não deve ser menor que a data da Meta");
            event.preventDefault();
        }

        if (dt_fim_val_c > ultimoDia) {
            alert("Data fim meta não deve ser maior que a data da Meta");
            event.preventDefault();
        }

        if (dt_fim_val_c < primeiroDia) {
            alert("Data fim meta não deve ser menor que a data da Meta");
            event.preventDefault();
        }

        if (vl_meta < vl_minimo) {
            alert("Valor mínimo não deve ser maior do que o valor Meta");
            event.preventDefault();
        }
    });
</script>

<script type="text/javascript">
    //VALOR META
    $(document).ready(function() {
        $('#vl_meta').mask('000.000.000.000.000,00', {
            reverse: true
        });
    });
    $("#vl_meta").focusout(function() {
        if ($(this).val().length <= 2) {
            if ($(this).val().length != '') {
                temp = $(this).val()
                var newNum = temp + ",00"
                $(this).val(newNum)
            }
        }
    });

    //VALOR VARIAVEL
    $(document).ready(function() {
        $('#vl_variavel').mask('000.000.000.000.000,00', {
            reverse: true
        });
    });
    $("#vl_variavel").focusout(function() {
        if ($(this).val().length <= 2) {
            if ($(this).val().length != '') {
                temp = $(this).val()
                var newNum = temp + ",00"
                $(this).val(newNum)
            }
        }
    });

    //VALOR MINIMO
    $(document).ready(function() {
        $('#vl_minimo').mask('000.000.000.000.000,00', {
            reverse: true
        });
    });
    $("#vl_minimo").focusout(function() {
        if ($(this).val().length <= 2) {
            if ($(this).val().length != '') {
                temp = $(this).val()
                var newNum = temp + ",00"
                $(this).val(newNum)
            }
        }
    });

    //VALOR ADICIONAL
    $(document).ready(function() {
        $('#vl_adic').mask('000.000.000.000.000,00', {
            reverse: true
        });
    });
    $("#vl_adic").focusout(function() {
        if ($(this).val().length <= 2) {
            if ($(this).val().length != '') {
                temp = $(this).val()
                var newNum = temp + ",00"
                $(this).val(newNum)
            }
        }
    });

    //VALOR BÔNUS
    $(document).ready(function() {
        $('#vl_bonus').mask('000.000.000.000.000,00', {
            reverse: true
        });
    });
    $("#vl_bonus").focusout(function() {
        if ($(this).val().length <= 2) {
            if ($(this).val().length != '') {
                temp = $(this).val()
                var newNum = temp + ",00"
                $(this).val(newNum)
            }
        }
    });
</script>