<!-- Content Header (Page header)  -->
<section class="content-header">
    <div class="container" style="background: pink;">
        <?php
        if (!empty($data['mensagem'])) :
            foreach ($data['mensagem'] as $m) :
                echo $m . "<br>";
            endforeach;
        endif;
        // var_dump ($_SESSION);
        ?>
    </div>
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h4>Resumo Metas</h4>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a style="color: indigo" href="#">Index</a></li>
                    <li class="breadcrumb-item active"></li>
                </ol>
            </div>
        </div>
    </div>
</section>
<!-- Main Content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
            <div class="card-header">
                    <div class="btn-group">
                        <div> <a href="/metas/cadastrar" type="button" class="btn btn-block btn-purple btn-sm">Adicionar Meta</a> </div>
                        &nbsp
                        <div> <a href="/metas/exportar" type="button" class="btn btn-block btn-orange btn-sm">Exportar</a> </div>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <table id="table" class="display responsive nowrap" style="width:100%">
                                <thead>
                                    <tr>
                                        <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">Atalhos</th> <!-- botões -->
                                        <th style="width: 10%" class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Cod Meta</th>
                                        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">CNPJ Estab</th>
                                        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Unidade Negócio</th>
                                        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Cod Gerente</th>
                                        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Cod Vendedor</th>
                                        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Gru Produto</th>
                                        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Cod Produto</th>
                                </thead>
                                <tbody>
                                    <?php
                                    $metas = $this->model('Meta');
                                    foreach ($data['registros'] as $item) : ?>
                                        <?php $cnpj_estab = $item['empresacnpj_estab'];
                                        foreach ($metas->getAllEstabelecimento($cnpj_estab) as $value) : ?>
                                            <tr>
                                                <td>
                                                    <a title="Detalhar" href="/metas/ver/<?php echo $value['metaid']; ?>" type="button" class="btn btn-purple btn-sm"><i class="fas fa-eye"></i></a> <!-- visualizar -->
                                                    <a title="Excluir" href="/metas/excluir/<?php echo $value['metaid']; ?>" type="button" class="btn btn-purple btn-sm"><i class="fas fa-trash"></i></a>
                                                    <a title="Cópia" href="/metas/copiar/<?php echo $value['metaid']; ?>" type="button" class="btn btn-purple btn-sm"><i class="fas fa-copy"></i></a>
                                                    <!-- excluir -->
                                                </td>
                                                <td><?php echo $value['metacod_meta']; ?></td>
                                                <td><?php echo $value['metacnpj_estab']; ?></td>
                                                <td><?php echo $value['metacod_unid_neg']; ?></td>
                                                <td><?php echo $value['metacod_gerente']; ?></td>
                                                <td><?php echo $value['metacod_vendedor']; ?></td>
                                                <td><?php echo $value['metacod_grupo_prod']; ?></td>
                                                <td><?php echo $value['metacod_produto']; ?></td>                    
                                            </tr>
                                        <?php endforeach; ?>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
</section>
<script>
    $(document).ready(function() {
        $('#table').DataTable({
            scrollY: 300,
            paging: false
        });
    });
</script>