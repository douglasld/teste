<!-- Content Header (Page header)  -->
<section class="content-header">

    <div class="container" style="background: pink;">
        <?php
        if (!empty($data['mensagem'])) :

            foreach ($data['mensagem'] as $m) :
                echo $m . "<br>";
            endforeach;
        endif;

        // var_dump ($_SESSION);


        ?>
    </div>

    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Resumo Parâmetro Meta</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a style="color: indigo" href="#">Home</a></li>
                    <li class="breadcrumb-item active">Cadastro</li>
                </ol>
            </div>
        </div>
    </div>
</section>

<!-- Main Content -->

<section class="content">
    <div class="row">
        <div class="col-12">

            <div class="card">
                <div class="card-header">
                    <div class="row form-inline">

                        <div style="margin-left: 10px"> <a href="/parammetas/cadastrar" type="button" class="btn btn-block btn-default btn-sm">Adicionar Parm. Meta</a> </div>
                        <div class="col-md-10 float-right">
                            <div style="float:right; padding-left: 6px;" id="example1_filter" class="dataTables_filter"><label><input type="search" class="form-control form-control-sm" placeholder="Pesquisar" aria-controls="example1"></label></div>
                            <div style="float:right;" class="dataTables_length" id="example1_length"><label> Mostrar <select name="example1_length" aria-controls="example1" class="custom-select custom-select-sm form-control form-control-sm">
                                        <option value="10">10</option>
                                        <option value="25">25</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select> </label></div>
                        </div>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                        <div class="row">

                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="example1" class="table table-bordered table-striped dataTable dtr-inline" role="grid" aria-describedby="example1_info">
                                    <thead>
                                        <tr role="row">
                                            <th style="width: 10%" class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">Atalhos</th> <!-- botões -->
                                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Data Criação</th>
                                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Usuário Criação</th>
                                        </tr>
                                    </thead>
                                    <!-- tbody -->
                                    <tbody>
                                        <?php foreach ($data['registros'] as $param_meta) : ?>
                                            <tr>
                                                <td>
                                                    <a title="editar" href="/parammetas/editar/<?php echo $user_perfil['user_perfilid']; ?>" type="button" class="btn btn-default btn-sm"><i class="fas fa-edit"></i></a> <!-- visualizar -->
                                                    <a title="excluir" href="/parammetas/excluir/<?php echo $user_perfil['user_perfilid']; ?>" type="button" class="btn btn-default btn-sm"><i class="fas fa-trash"></i></a> <!-- editar -->

                                                </td>
                                                <td>
                                                    <a href="/parammetas/ver/<?php echo $param_meta['param_metaid']; ?>">
                                                        <?php echo implode("/", array_reverse(explode("-", $param_meta['param_metadt_criacao']))); ?>
                                                    </a>
                                                </td>
                                                <td><?php echo $param_meta['user_perfilnome_user']; ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                        <?php foreach ($data['registros'] as $user_perfil) : ?>
                                            <tr>
                                                <td><?php echo $user_perfil['user_perfilnome_completo']; ?></td>
                                                <td><?php echo $user_perfil['user_perfilnome_user']; ?></td>
                                                <td><?php echo $user_perfil['user_perfilcnpj_matriz']; ?></td>
                                                <td><?php echo $user_perfil['user_perfile_mail']; ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                    <!-- tbody -->
                                    <!-- <tfoot>
                                        nada por enquanto
                                    </tfoot> -->
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-5">
                                <div class="dataTables_info" id="example1_info" role="status" aria-live="polite">Mostrando 1 to 10 of 57 registros</div>
                            </div>
                            <div class="col-sm-12 col-md-7">
                                <div class="dataTables_paginate paging_simple_numbers float-right" id="example1_paginate">
                                    <ul class="pagination">
                                        <li class="paginate_button page-item previous disabled" id="example1_previous"><a style="color: indigo; background-color: white;" href="#" aria-controls="example1" data-dt-idx="0" tabindex="0" class="page-link">Anterior</a></li>
                                        <li class="paginate_button page-item active"><a style="color: white; background-color: indigo; border-color:indigo" href="#" aria-controls="example1" data-dt-idx="1" tabindex="0" class="page-link">1</a></li>
                                        <li class="paginate_button page-item "><a style="color: indigo; background-color: white;" href="#" aria-controls="example1" data-dt-idx="2" tabindex="0" class="page-link">2</a></li>
                                        <li class="paginate_button page-item "><a style="color: indigo; background-color: white;" href="#" aria-controls="example1" data-dt-idx="3" tabindex="0" class="page-link">3</a></li>
                                        <li class="paginate_button page-item "><a style="color: indigo; background-color: white;" href="#" aria-controls="example1" data-dt-idx="4" tabindex="0" class="page-link">4</a></li>
                                        <li class="paginate_button page-item "><a style="color: indigo; background-color: white;" href="#" aria-controls="example1" data-dt-idx="5" tabindex="0" class="page-link">5</a></li>
                                        <li class="paginate_button page-item "><a style="color: indigo; background-color: white;" href="#" aria-controls="example1" data-dt-idx="6" tabindex="0" class="page-link">6</a></li>
                                        <li class="paginate_button page-item next" id="example1_next"><a style="color: indigo; background-color: white;" href="#" aria-controls="example1" data-dt-idx="7" tabindex="0" class="page-link">Próximo</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col 12 -->
    </div>
    <!-- /.row -->
</section>