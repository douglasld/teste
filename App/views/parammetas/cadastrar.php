<div class="x_panel">
    <div class="x_title">
    <br>
        <h3 style="margin-left:10px;">Cadastrar Parametros Metas</h3>
        <ul class="nav navbar-right panel_toolbox">
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <?php
        if (!empty($data['mensagem'])) :

            foreach ($data['mensagem'] as $m) :
                echo $m . "<br>";
            endforeach;

        endif;
        ?>
        <div class="container-fluid">
            <div class="col-md-6">
                <div class="card card-default">
                    <!-- /.card-header -->
                    <div class="card-header">
                        <h3 class="card-title">Criação</h3>
                    </div>
                    <form action="/parammetas/cadastrar" method="post" class="form-horizontal form-label-left">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label> Usuário Criação</label>
                                        <input name="user_criacao" id="user_criacao" type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label> Data Criação</label>
                                        <input name="dt_criacao" id="dt_criacao" type="date" class="form-control" required="required">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label> Hora Criação</label>
                                        <input name="hr_criacao" id="hr_criacao" type="time" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="card-header">
                                <h3 class="card-title">Alteração</h3>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label> User Alteração</label>
                                        <input name="user_altera" id="user_altera" type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label> Data Alteração</label>
                                        <input name="dt_altera" id="dt_altera" type="date" class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label> Hora Alteração</label>
                                        <input name="hr_altera" id="hr_altera" type="time" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <input type="submit" name="Cadastar" class="btn btn-primary float-right" value="Cadastrar" style="background-color: indigo; border-color: indigo; margin:6px;">
                            <a href="/parammetas/index" class="btn btn-default float-right" style="margin:6px;">Cancelar</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>