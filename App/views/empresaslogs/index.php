<div class="x_panel">
    <div class="x_title">
        <h2>Empresas [Logs]</h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <?php
        if (!empty($data['mensagem'])):

            foreach ($data['mensagem'] as $m):
                echo $m . "<br>";
            endforeach;

        endif;
        ?>

        <div class="table-responsive">
            <table  id="datatable" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>cnpj_matriz</th>
                    <th>cnpj_estab</th>
                    <th>Nome_abrev</th>
                    <th>dt_emis</th>
                    <th>hr_emis</th>
                    <th>user_emis</th>
                    <th>usuário</th>
                    <th>titulo</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($data['registros'] as $empresa_log): ?>
                    <tr>
                        <td><?php echo $empresa_log['cnpj_matriz']; ?></td>
                        <td><?php echo $empresa_log['cnpj_estab']; ?></td>
                        <td><?php echo $empresa_log['nome_abrev']; ?></td>
                        <td><?php echo implode("/", array_reverse(explode("-",$empresa_log['dt_emis']))); ?></td>
                        <td><?php echo $empresa_log['hr_emis']; ?></td>
                        <td><?php echo $empresa_log['user_emis']; ?></td>
                        <td><?php echo $empresa_log['usernome_user']; ?></td>
                        <td><?php echo $empresa_log['titulo']; ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

