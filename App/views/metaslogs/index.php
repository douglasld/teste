<div class="x_panel">
    <div class="x_title">
        <h2>Metas [Logs]</h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <?php
        if (!empty($data['mensagem'])):

            foreach ($data['mensagem'] as $m):
                echo $m . "<br>";
            endforeach;

        endif;
        ?>

        <div class="table-responsive">
            <table  id="datatable" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>Estabelecimento</th>
                    <th>Data</th>
                    <th>Hora</th>
                    <th>Usuário</th>
                    <th>titulo</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($data['registros'] as $meta_log): ?>
                    <tr>
                        <td><?php echo $meta_log['meta_logcnpj_estab']; ?></td>
                        <td><?php echo $meta_log['meta_logdt_emis']; ?></td>
                        <td><?php echo $meta_log['meta_loghr_emis']; ?></td>
                        <td><?php echo $meta_log['meta_loguser_emis']; ?></td>
                        <td><?php echo $meta_log['meta_logtitulo']; ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>


