<section class="content-header">
    <div class="container" style="background: lavender;">
        <?php
        if (!empty($data['mensagem'])) :
            foreach ($data['mensagem'] as $m) :
                echo $m . "<br>";
            endforeach;
        endif;
        $estabelecimentos = $this->model('Empresa');
        $estabelecimentouse = $this->model('UserEstab');
        $estab_empresa = $this->model('Empresa');
        ?>
    </div>
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h4>Cadastro de Feriados</h4>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a style="color: indigo" href="#">Index</a></li>
                    <li class="breadcrumb-item active">Cadastro</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<div class="container-fluid">
    <form action="/feriados/cadastrar" method="post">
        <div class="col-md-6">
            <div class="card card-default">
                <div class="card-header">
                    <a href="/feriados/index" class="btn btn-default btn-md float-left" style="margin-left:5px;">Listar</a>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>CNPJ Estabelecimento</label>
                                <input class="form-control col-md-12" name="cnpj_estab" id="cnpj_estab" list="list" autocomplete="off" required="">
                                <datalist id="list">
                                <?php foreach ($estabelecimentouse->get_auto_cnpj($_SESSION['matriz'], $_SESSION['userId']) as $value) : ?>
                                        <?php foreach ($estab_empresa->getPorEstab($value) as $value) : ?>
                                            <option value="<?php echo preg_replace("/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/", "\$1.\$2.\$3/\$4-\$5", $value['empresacnpj_estab']) . ' - ' . $value['empresanome_abrev']; ?>"></option>
                                        <?php endforeach; ?>
                                    <?php endforeach; ?>
                                </datalist>
                                </input>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label> Data Feriado</label>
                                <input name="dt_feriado" type="date" class="form-control" required="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label> Descrição</label>
                                <input name="descricao" type="text" class="form-control" required="">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="custom-control custom-switch">
                                <input type="checkbox" checked class="custom-control-input" id="ind_ativo" name="ind_ativo" required="">
                                <label class="custom-control-label" for="ind_ativo" style="margin-top:25%;">Feriado Ativo</label>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <input type="submit" name="Cadastrar" class="btn btn-primary float-right" value="Salvar" style="background-color: indigo; border-color: indigo; margin:6px;">
                    <a href="/feriados/index" class="btn btn-orange btn-md float-right" style="margin:6px;">Cancelar</a>
                </div>
    </form>
</div>
<script>
    function formatarCampo(campoTexto) {
        campoTexto.value = mascaraCnpj(campoTexto.value);
    }

    function retirarFormatacao(campoTexto) {
        campoTexto.value = campoTexto.value.replace(/(\.|\/|\-)/g, "");
    }

    function mascaraCnpj(valor) {
        return valor.replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/g, "\$1.\$2.\$3\/\$4\-\$5");
    }
</script>
</div>
</div>