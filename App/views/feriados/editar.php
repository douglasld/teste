<style>
    .nav-tabs .nav-item .nav-link {
        color: #6039A8 !important;
    }
</style>
<!-- Content Header (Page header)  -->
<section class="content-header">
    <div class="container" style="background: pink;">
        <?php
        if (!empty($data['mensagem'])) :
            foreach ($data['mensagem'] as $m) :
                echo $m . "<br>";
            endforeach;
        endif;
        $estabelecimentos = $this->model('Empresa');

        ?>
    </div>
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h4>Detalhes Feriados</h4>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="/empresas/index" style="color:indigo">Index</a></li>
                    <li class="breadcrumb-item active">Detalhamento</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="row">
        <div class="col-md-8">
            <div class="card card-purple card-outline card-outline-tabs">
                <div class="card-header p-0 pt-1 border-bottom-0">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item"><a class="nav-link active" href="#editar" data-toggle="tab">Editar</a></li>
                        <a href="/feriados/index" class="nav-link" style="margin-left:5px;">Listar</a>
                        <!-- <li class="nav-item"><a class="nav-link " href="#resumo" data-toggle="tab">Resumo</a></li> -->
                    </ul>
                </div><!-- /.card-header -->
                <div class="card-body">
                    <div class="tab-content">
                        <div class="active tab-pane" id="editar">
                            <section class="content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="invoice p-3 mb-3">
                                            <div class="card-body">
                                                <form action="/feriados/editar/<?php echo $data['registros']['feriadoid']; ?>" method="post" class="form-horizontal form-label-left">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                        <div class="form-group">
                                                                <label>CNPJ Estabelecimento</label>
                                                                <?php foreach ($estabelecimentos->getall($_SESSION['matriz']) as $value) : ?>
                                                                    <?php
                                                                    if ($value['empresacnpj_estab'] == $data['registros']['feriadocnpj_estab']) {
                                                                        $empresa_abrev_value = $value['empresanome_abrev'];
                                                                        break;
                                                                    };
                                                                    ?>
                                                                <?php endforeach; ?>
                                                                <?php
                                                                $cnpj_value = $data['registros']['feriadocnpj_estab'];
                                                                $cnpj_value = preg_replace("/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/", "\$1.\$2.\$3/\$4-\$5", $cnpj_value);
                                                                ?>
                                                                <input class="form-control col-md-12" value="<?php echo $cnpj_value . ' - ' . $empresa_abrev_value; ?>" name="cnpj_estab" id="cnpj_estab" list="list" autocomplete="off" required="">
                                                                <datalist id="list">
                                                                    <?php foreach ($estabelecimentos->getall($_SESSION['matriz']) as $value) : ?>
                                                                        <?php
                                                                        $cnpj_cpf = $value['empresacnpj_estab'];
                                                                        $cnpj = preg_replace("/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/", "\$1.\$2.\$3/\$4-\$5", $cnpj_cpf);
                                                                        ?>
                                                                        <option value="<?php echo $cnpj . ' - ' . $value['empresanome_abrev']; ?>"></option>
                                                                    <?php endforeach; ?>
                                                                </datalist>
                                                                </input>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label> Data Feriado</label>
                                                                <input name="dt_feriado" type="date" value="<?php echo $data['registros']['feriadodt_feriado']; ?>" class="form-control" required=""> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label> Descrição</label>
                                                                <input name="descricao" type="text" value="<?php echo $data['registros']['feriadodescricao']; ?>" class="form-control" required="">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <div class="custom-control custom-switch">
                                                                <input type="checkbox" class="custom-control-input" id="ind_ativo" name="ind_ativo" <?php if ($data['registros']['feriadoind_ativo'] == "0") {
                                                                                                                                                        echo "checked";
                                                                                                                                                    } ?> required="">
                                                                <label class="custom-control-label" for="ind_ativo" style="margin-top:20%;">Feriado Ativo</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                        </div>
                                        <div class="card-footer">
                                            <input type="submit" name="Atualizar" class="btn btn-purple btn-md float-right" value="Salvar">
                                            <a href="/produtos/index" class="btn btn-orange btn-md float-right" style="margin-right:4px;">Cancelar</a>
                                        </div>
                                    </div>
                                </div>
                        </div>
</section>
</div> <!-- /.tab-pane // fecha editar-->
</div> <!-- /.tab-content -->
</div><!-- /.card-body -->
</div> <!-- /.nav-tabs-custom -->
</div> <!-- /.col -->
</div> <!-- /.row -->
</section>
<script>
    function formatarCampo(campoTexto) {
        campoTexto.value = mascaraCnpj(campoTexto.value);
    }

    function retirarFormatacao(campoTexto) {
        campoTexto.value = campoTexto.value.replace(/(\.|\/|\-)/g, "");
    }

    function mascaraCnpj(valor) {
        return valor.replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/g, "\$1.\$2.\$3\/\$4\-\$5");
    }
</script>

<script>
    $(document).ready(function() {
        $('#table_logs').DataTable({});
        $('#table_unid_neg').DataTable({
            scrollY: 300,
            "sScrollX": "100%",
            "bScrollCollapse": true,
            paging: false
        });
    });
</script>