<div class="x_panel">
    <div class="x_title">
        <br>
        <h3 style="margin-left:10px;">Editar Matriz</h3>
        <ul class="nav navbar-right panel_toolbox">
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <?php
        if (!empty($data['mensagem'])) :

            foreach ($data['mensagem'] as $m) :
                echo $m . "<br>";
            endforeach;

        endif;
        ?>
        <div class="container-fluid">
        <form action="/resmatrizs/editar/<?php echo $data['registros']['res_matrizid']; ?>" method="post">
                <div class="col-md-6">
                    <div class="card card-default">
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>CNPJ Estabelecimento</label>
                                        <input name="cnpj_matriz" id="cnpj_matriz" value="<?php echo $data['registros']['res_matrizcnpj_matriz']; ?>" onfocus="javascript: retirarFormatacao(this);" onblur="javascript: formatarCampo(this);" maxlength="14" type="text" class="form-control" placeholder="00.000.000/000-00" required="required">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label> Valor Meta</label>
                                        <input name="vl_meta"value="<?php echo $data['registros']['res_matrizvl_meta']; ?>" type="number" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label> Valor Variável</label>
                                        <input name="vl_variavel" value="<?php echo $data['registros']['res_matrizvl_variavel']; ?>" type="number" class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label> Valor Mínimo</label>
                                        <input name="vl_minimo" value="<?php echo $data['registros']['res_matrizvl_minimo']; ?>" type="number" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label> Valor Adicional</label>
                                        <input name="vl_adic" value="<?php echo $data['registros']['res_matrizvl_adic']; ?>" type="number" class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label> Valor Bônus</label>
                                        <input name="vl_bonus" value="<?php echo $data['registros']['res_matrizvl_bonus']; ?>" type="number" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label> Data Mvto.</label>
                                        <input name="dt_mvto" value="<?php echo $data['registros']['res_matrizdt_movto']; ?>" type="date" class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label> Valor real</label>
                                        <input name="vl_real"  value="<?php echo $data['registros']['res_matrizvl_real']; ?>" type="number" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <input type="submit" name="Cadastar" class="btn btn-primary float-right" value="Cadastrar" style="background-color: indigo; border-color: indigo; margin:6px;">
                            <a href="/resmatriz/index" class="btn btn-default float-right" style="margin:6px;">Cancelar</a>
                        </div>
            </form>
        </div>
        <script>
            function formatarCampo(campoTexto) {
                campoTexto.value = mascaraCnpj(campoTexto.value);
            }

            function retirarFormatacao(campoTexto) {
                campoTexto.value = campoTexto.value.replace(/(\.|\/|\-)/g, "");
            }

            function mascaraCnpj(valor) {
                return valor.replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/g, "\$1.\$2.\$3\/\$4\-\$5");
            }
        </script>
    </div>
</div>