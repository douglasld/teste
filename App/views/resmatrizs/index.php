<!-- Content Header (Page header)  -->
<section class="content-header">
    <div class="container" style="background: pink;">
        <?php
        if (!empty($data['mensagem'])) :

            foreach ($data['mensagem'] as $m) :
                echo $m . "<br>";
            endforeach;
        endif;

        // var_dump ($_SESSION);
        ?>
    </div>
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Resumo Matriz</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a style="color: indigo" href="#">Home</a></li>
                    <li class="breadcrumb-item active">Cadastro</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<!-- Main Content -->
<section class="content">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <div class="row form-inline">
                        <div style="margin-left: 10px"> <a href="/resmatrizs/cadastrar" type="button" class="btn btn-block btn-default btn-sm">Adicionar Matriz</a> </div>
                        <div class="col-md-10 float-right">
                            <div style="float:right; padding-left: 6px;" id="example1_filter" class="dataTables_filter"><label><input type="search" class="form-control form-control-sm" placeholder="Pesquisar" aria-controls="example1"></label></div>
                            <div style="float:right;" class="dataTables_length" id="example1_length"><label> Mostrar <select name="example1_length" aria-controls="example1" class="custom-select custom-select-sm form-control form-control-sm">
                                        <option value="10">10</option>
                                        <option value="25">25</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select> </label></div>
                        </div>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                        <div class="row">
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="example1" class="table table-bordered table-striped dataTable dtr-inline" role="grid" aria-describedby="example1_info">
                                    <thead>
                                        <tr role="row">
                                            <th style="width: 10%" class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">Atalhos</th> <!-- botões -->
                                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Matriz</th>
                                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Valor Meta</th>
                                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Valor Mínimo</th>
                                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Valor Variável</th>
                                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Valor Adicional</th>
                                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Valor Bônus</th>
                                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Dta. Movto</th>
                                            <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Valor Real</th>
                                        </tr>
                                    </thead>
                                    <!-- tbody -->
                                    <tbody>
                                        <?php foreach ($data['registros'] as $value) : ?>
                                            <tr>
                                                <td>
                                                    <a title="editar" href="/resmatrizs/editar/<?php echo $value['res_matrizid']; ?>" type="button" class="btn btn-default btn-sm"><i class="fas fa-edit"></i></a> <!-- editar -->
                                                    <a title="excluir" href="/resmatrizs/excluir/<?php echo $value['res_matrizid']; ?>" type="button" class="btn btn-default btn-sm"><i class="fas fa-trash"></i></a> <!-- excluir -->
                                                </td>

                                                <td>
                                                    <?php
                                                        if (strlen($value['res_matrizcnpj_matriz'] <= 10)) {
                                                            echo $value['res_matrizcnpj_matriz'];
                                                        } else {
                                                            echo Mask('##.###.###/####-##', $value['res_matrizcnpj_matriz']);
                                                        }
                                                        ?>
                                                </td>
                                                <td><?php echo number_format($value['res_matrizvl_meta'], 2, ',', '.'); ?></td>
                                                <td><?php echo number_format($value['res_matrizvl_minimo'], 2, ',', '.'); ?></td>
                                                <td><?php echo number_format($value['res_matrizvl_variavel'], 2, ',', '.'); ?></td>
                                                <td><?php echo number_format($value['res_matrizvl_adic'], 2, ',', '.'); ?></td>
                                                <td><?php echo number_format($value['res_matrizvl_bonus'], 2, ',', '.'); ?></td>
                                                <td><?php echo implode("/", array_reverse(explode("-", $value['res_matrizdt_movto']))); ?></td>
                                                <td><?php echo number_format($value['res_matrizvl_real'], 2, ',', '.'); ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                    <!-- tbody -->
                                    <!-- <tfoot>
                                        nada por enquanto
                                    </tfoot> -->
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-5">
                                <div class="dataTables_info" id="example1_info" role="status" aria-live="polite">Mostrando 1 to 10 of 57 registros</div>
                            </div>
                            <div class="col-sm-12 col-md-7">
                                <div class="dataTables_paginate paging_simple_numbers float-right" id="example1_paginate">
                                    <ul class="pagination">
                                        <li class="paginate_button page-item previous disabled" id="example1_previous"><a style="color: indigo; background-color: white;" href="#" aria-controls="example1" data-dt-idx="0" tabindex="0" class="page-link">Anterior</a></li>
                                        <li class="paginate_button page-item active"><a style="color: white; background-color: indigo; border-color:indigo" href="#" aria-controls="example1" data-dt-idx="1" tabindex="0" class="page-link">1</a></li>
                                        <li class="paginate_button page-item "><a style="color: indigo; background-color: white;" href="#" aria-controls="example1" data-dt-idx="2" tabindex="0" class="page-link">2</a></li>
                                        <li class="paginate_button page-item "><a style="color: indigo; background-color: white;" href="#" aria-controls="example1" data-dt-idx="3" tabindex="0" class="page-link">3</a></li>
                                        <li class="paginate_button page-item "><a style="color: indigo; background-color: white;" href="#" aria-controls="example1" data-dt-idx="4" tabindex="0" class="page-link">4</a></li>
                                        <li class="paginate_button page-item "><a style="color: indigo; background-color: white;" href="#" aria-controls="example1" data-dt-idx="5" tabindex="0" class="page-link">5</a></li>
                                        <li class="paginate_button page-item "><a style="color: indigo; background-color: white;" href="#" aria-controls="example1" data-dt-idx="6" tabindex="0" class="page-link">6</a></li>
                                        <li class="paginate_button page-item next" id="example1_next"><a style="color: indigo; background-color: white;" href="#" aria-controls="example1" data-dt-idx="7" tabindex="0" class="page-link">Próximo</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col 12 -->
    </div>
    <!-- /.row -->
</section>s