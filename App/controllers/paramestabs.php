<?php

use \App\Core\Controller;
use App\Auth;

class paramestabs extends Controller
{

    public function index()
    {
        Auth::checkLogin();

        $param_estab = $this->model('ParamEstab');
        $dados = $param_estab->getAll();

        $this->view('paramestabs/index', $dados = ['registros' => $dados]);
    }


    public function ver($id = '')
    {
        Auth::checkLogin();

        $param_estab = $this->model('ParamEstab');
        $dados = $param_estab->findId($id);
        $this->view('paramestabs/ver', $dados = ['registros' => $dados]);
    }

    public function cadastrar()
    {
        Auth::checkLogin();

        $mensagem = array();

        if (isset($_POST['Cadastrar'])) {

            $param_estab = $this->model('ParamEstab');

            $cnpj_estab = str_replace(array('.', '/', '-', '_'), '', $_POST['cnpj_estab']);
            $param_estab->param_estabcnpj_estab = substr($cnpj_estab, 0, 14);

            $param_estab->param_estabdt_criacao = date("Y-m-d");
            $param_estab->param_estabhr_criacao = date("H:i:s");
            $param_estab->param_estabuser_criacao = $_SESSION['userId'];
            $param_estab->param_estabdt_altera = date("Y-m-d");
            $param_estab->param_estabhr_altera = date("H:i:s");
            $param_estab->param_estabuser_altera = date("H:i:s");
            $param_estab->param_estabperc_red_meta_feriado = str_replace(array('.', ','), array('', '.'), $_POST['perc_red_meta_feriado']);
            $param_estab->param_estabvl_bonus_extra = str_replace(array('.', ','), array('', '.'), $_POST['vl_bonus_extra']);
            $param_estab->param_estabperc_bonus_extra = str_replace(array('.', ','), array('', '.'), $_POST['perc_bonus_extra']);
            $param_estab->param_estabind_ativo = $_POST['ind_ativo'];
            if ($param_estab->param_estabind_ativo == "on") {
                $param_estab->param_estabind_ativo = "0";
            } else {
                $param_estab->param_estabind_ativo = "1";
            }

            $mensagem[] = $param_estab->save();

          

            $param_estab_logs = $this->model('ParamEstabLog');

            $param_estab_logs->param_estab_logdt_emis = date("Y-m-d");
            $param_estab_logs->param_estab_loghr_emis = date("H:i:s");
            $param_estab_logs->param_estab_loguser_emis = $_SESSION['userId'];
            $param_estab_logs->param_estab_logtitulo = "Inclusão";

            $mensagem[] = $param_estab_logs->save();
        }

        $this->view('paramestabs/cadastrar', $dados = ['mensagem' => $mensagem]);
    }

    public function editar($id)
    {

        Auth::checkLogin();

        $mensagem = array();
        $param_estab = $this->model('ParamEstab');


        if (isset($_POST['Atualizar'])) {

            $cnpj_estab = str_replace(array('.', '/', '-', '_'), '', $_POST['cnpj_estab']);
            $param_estab->param_estabcnpj_estab = substr($cnpj_estab, 0, 14);

            $param_estab->param_estabdt_altera = date("Y-m-d");
            $param_estab->param_estabhr_altera = date("H:i:s");
            $param_estab->param_estabuser_altera = date("H:i:s");
            $param_estab->param_estabperc_red_meta_feriado = str_replace(array('.', ','), array('', '.'), $_POST['perc_red_meta_feriado']);
            $param_estab->param_estabvl_bonus_extra = str_replace(array('.', ','), array('', '.'), $_POST['vl_bonus_extra']);
            $param_estab->param_estabperc_bonus_extra = str_replace(array('.', ','), array('', '.'), $_POST['perc_bonus_extra']);
            $param_estab->param_estabind_ativo = $_POST['ind_ativo'];
            if ($param_estab->param_estabind_ativo == "on") {
                $param_estab->param_estabind_ativo = "0";
            } else {
                $param_estab->param_estabind_ativo = "1";
            }

            $mensagem[] = $param_estab->update($id);

            $param_estab_logs = $this->model('ParamEstabLog');
            $param_estab_logs->param_estab_logdt_emis = date("Y-m-d");
            $param_estab_logs->param_estab_loghr_emis = date("H:i:s");
            $param_estab_logs->param_estab_loguser_emis = $_SESSION['userId'];
            $param_estab_logs->param_estab_logtitulo = "Alteração";

            $mensagem[] = $param_estab_logs->save();
        }

        $dados = $param_estab->findId($id);
        $this->view('paramestabs/editar', $dados = ['mensagem' => $mensagem, 'registros' => $dados]);
    }


    public function excluir($id = '')
    {

        Auth::checkLogin();

        $mensagem = array();
        $param_estab = $this->model('ParamEstab');

        $mensagem[] = $param_estab->delete($id);
        
        $dados = $param_estab->getAll();

        $this->view('paramestabs/index', $dados = ['registros' => $dados]);
    }
}
