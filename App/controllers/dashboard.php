<?php

use \App\Core\Controller;
use App\Auth;

class dashboard extends Controller
{

    public function index()
    {
        Auth::checkLogin();

        $res_matriz = $this->model('ResMatriz');
        $dados = $res_matriz->getAll(null);
        $dados = [1,2,3];
        $a = $this->getMetasByEstabelecimento(date("Y-m-d"));

        $this->view('dashboard/index', $dados = ['registros' => $dados]);
    }


    public function ver($id = '')
    {
        Auth::checkLogin();

        $metas = $this->model('Meta');
        $dados = $metas->findId($id);

        $this->view('dashboard/ver', $dados);
    }


    public function teste($id = '')
    {
        Auth::checkLogin();

        $userperfil = $this->model('UserPerfil');
        $dados['user'] = $userperfil->getAll($id);

        $this->view('dashboard/teste', $dados);
    }

    public function getMetasByEstabelecimento($mes = '')
    {
        Auth::checkLogin(); // $_SESSION['userNome'];

        $mensagem = array();
        $res_matriz = $this->model('ResMatriz');

        if (!isset($_GET['Pesquisar']) || isset($_GET['Pesquisar'])) {

            $dados['estab_real'] = $res_matriz->getSeriesEstab($_GET['mes']);
            $dados['estab_comparativo_name'] = $res_matriz->getComparativoEstabName($_GET['mes']);
            $dados['estab_comparativo_meta'] = $res_matriz->getComparativoEstabMeta($_GET['mes']);
            $dados['estab_comparativo_real'] = $res_matriz->getComparativoEstabReal($_GET['mes']);
            // $dados['estab'] = $res_matriz->estabData($_GET['mes']);
            // $dados['grupo_produto'] = $res_matriz->grupo_produtoData($_GET['mes']);
            // $dados['unid_neg'] = $res_matriz->unid_negData($_GET['mes']);
            // $dados['gerentes'] = $res_matriz->gerentesData($_GET['mes']);
            // $dados['vendedores'] = $res_matriz->vendedoresData($_GET['mes']);
            // $dados['grupo_produto_vendedor'] = $res_matriz->grupo_produto_vendedoresData($_GET['mes']);
            // $dados['produtos'] = $res_matriz->produtosData($_GET['mes']);
        }

        $this->view('dashboard/index', $dados);
    }
}
