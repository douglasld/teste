<?php

use \App\Core\Controller;
use App\Auth;

class user_perfil extends Controller
{

    public function index()
    {
        Auth::checkLogin();

        $userPerfil = $this->model('UserPerfil');
        $dados = $userPerfil->getAllMatriz($_SESSION['matriz']);

        $this->view('user_perfil/index', $dados = ['registros' => $dados]);
    }

    public function ver($id = '')
    {
        Auth::checkLogin();

        $userPerfil = $this->model('UserPerfil');
        $dados['perfil'] = $userPerfil->findId($id);
        $dados['logs'] = $userPerfil->getAllLogs($id);
        $dados['estabelecimentos'] = $userPerfil->getAllEstabelecimentos($id);

        $this->view('user_perfil/ver', $dados);
    }

    public function cadastrar()
    {
        Auth::checkLogin();

        $mensagem = array();

        if (isset($_POST['Cadastrar'])) {


            if (empty($_POST['empresa_matriz']) or empty($_POST['nome_user']) or empty($_POST['email']) or empty($_POST['senha'])) {
                $mensagem[] = "Os campos: CNPJ, nome, e-mail e senha são obrigatórios";
            } else {

                //upload
                if (!empty($_POST['foo'])) {
                    $storage = new \Upload\Storage\FileSystem('images');
                    $file = new \Upload\File('foo', $storage);

                    // Optionally you can rename the file on upload
                    $new_filename = uniqid();
                    $file->setName($new_filename);

                    // Validate file upload
                    // MimeType List => http://www.iana.org/assignments/media-types/media-types.xhtml
                    $file->addValidations(array(
                        // Ensure file is of type "image/png"
                        new \Upload\Validation\Mimetype(array('image/png', 'image/gif', 'image/jpeg')),

                        //You can also add multi mimetype validation
                        //new \Upload\Validation\Mimetype(array('image/png', 'image/gif'))

                        // Ensure file is no larger than 5M (use "B", "K", M", or "G")
                        new \Upload\Validation\Size('5M')
                    ));

                    // Access data about the file that has been uploaded
                    $data = array(
                        'name' => $file->getNameWithExtension(),
                        'extension' => $file->getExtension(),
                        'mime' => $file->getMimetype(),
                        'size' => $file->getSize(),
                        'md5' => $file->getMd5(),
                        'dimensions' => $file->getDimensions()
                    );

                    //Para gravar no banco de dados
                    $nome_arquivo = "images/" . $data['name'];

                    // Try to upload file
                    try {
                        // Success!
                        $file->upload();
                        $mensagem[] = "Upload feito com sucesso!";
                    } catch (\Exception $e) {
                        // Fail!
                        $errors = $file->getErrors();
                        $mensagem[] = implode("<br>", $errors);
                    }
                }

                $userPerfil = $this->model('UserPerfil');

                //trata cnpj
                $cnpj_estab = str_replace(array('.', '/', '-', '_'), '', $_POST['empresa_matriz']);
                $userPerfil->user_perfilcnpj_matriz = substr($cnpj_estab, 0, 14);

                $userPerfil->user_perfilcod_user = $_POST['cod_user'];
                $userPerfil->user_perfilnome_user = $_POST['nome_user'];
                $userPerfil->user_perfilnome_completo = $_POST['nome_completo'];
                $userPerfil->user_perfildias_validade = $_POST['dias_validade'];
                $userPerfil->user_perfilsenha = password_hash($_POST['senha'], PASSWORD_DEFAULT);
                $userPerfil->user_perfile_mail = $_POST['email'];
                $userPerfil->user_perfilrua = $_POST['rua'];
                $userPerfil->user_perfilnumero = $_POST['numero'];
                $userPerfil->user_perfilcomplemento = $_POST['complemento'];
                $userPerfil->user_perfilbairro = $_POST['bairro'];
                $userPerfil->user_perfilcidade = $_POST['cidade'];
                $userPerfil->user_perfilestado = $_POST['estado'];
                $userPerfil->user_perfilpais = $_POST['pais'];
                $userPerfil->user_perfilcep = str_replace(array('.', '/', '-', '_'), '', $_POST['cep']);
                $userPerfil->user_perfiltelefone = str_replace(array('.', '/', '-', ' '), '', $_POST['telefone']);
                $userPerfil->user_perfilcelular = str_replace(array('(', ')', '-', ' '), '', $_POST['celular']);
                $userPerfil->user_perfiladm = $_POST['adm'];
                $userPerfil->user_perfilgestor = $_POST['gestor'];
                $userPerfil->user_perfilind_ativo = $_POST['ind_ativo'];
                $userPerfil->user_perfilind_origem = "0";
                $userPerfil->user_perfildt_altera = date("Y-m-d");
                $userPerfil->user_perfilhr_altera = date("H:i:s");
                $userPerfil->user_perfildt_criacao = date("Y-m-d");
                $userPerfil->user_perfilhr_criacao = date("H:i:s");

                $userPerfil->user_perfilind_presidente = $_POST['user_perfilind_presidentec'];
                $userPerfil->user_perfilind_diretor = $_POST['user_perfilind_diretorc'];
                $userPerfil->user_perfilind_gerente = $_POST['user_perfilind_gerentec'];
                $userPerfil->user_perfilind_coordenador = $_POST['user_perfilind_coordenadorc'];


                //trata ind_check de PRESIDENTE
                if ($userPerfil->user_perfilind_presidente == 'on') {
                    $userPerfil->user_perfilind_presidente = "1";
                } else {
                    $userPerfil->user_perfilind_presidente = "0";
                }
                if ($userPerfil->user_perfilind_presidente == '1') {
                    $userPerfil->user_perfilind_diretor = "0";
                    $userPerfil->user_perfilind_gerente = "0";
                    $userPerfil->user_perfilind_coordenador = "0";

                    $userPerfil->user_perfilcod_presidente = "0";
                    $userPerfil->user_perfilcod_diretor = "0";
                    $userPerfil->user_perfilcod_gerente = "0";
                    $userPerfil->user_perfilcod_coordenador = "0";
                }

                //trata ind_check de diretor
                if ($userPerfil->user_perfilind_diretor == 'on') {
                    $userPerfil->user_perfilind_diretor = "1";
                } else {
                    $userPerfil->user_perfilind_diretor = "0";
                }
                if ($userPerfil->user_perfilind_diretor == '1') {

                    $userPerfil->user_perfilind_presidente = "0";
                    $userPerfil->user_perfilind_gerente = "0";
                    $userPerfil->user_perfilind_coordenador = "0";

                    $userPerfil->user_perfilcod_gerente = "0";
                    $userPerfil->user_perfilind_coordenador = "0";

                    $partes = explode(" -", $_POST['user_perfilcod_presidente']);
                    $userPerfil->user_perfilcod_presidente = $partes[0];
                }

                //trata ind_check de gerente
                if ($userPerfil->user_perfilind_gerente == 'on') {
                    $userPerfil->user_perfilind_gerente = "1";
                } else {
                    $userPerfil->user_perfilind_gerente = "0";
                }
                if ($userPerfil->user_perfilind_gerente == '1') {

                    $userPerfil->user_perfilcod_gerente = '0';
                    $userPerfil->user_perfilcod_coordenador = '0';

                    $partes = explode(" -", $_POST['user_perfilcod_presidente']);
                    $userPerfil->user_perfilcod_diretor = $partes[0];

                    $partes = explode(" -", $_POST['user_perfilcod_diretor']);
                    $userPerfil->user_perfilcod_diretor = $partes[0];
                }


                //trata ind_check de cooredenador
                if ($userPerfil->user_perfilind_coordenador == 'on') {
                    $userPerfil->user_perfilind_coordenador = "1";
                } else {
                    $userPerfil->user_perfilind_coordenador = "0";
                }
                if ($userPerfil->user_perfilind_coordenador == '1') {
                    $userPerfil->user_perfilcod_coordenador = '0';

                    $partes = explode(" -", $_POST['user_perfilcod_presidente']);
                    $userPerfil->user_perfilcod_presidente = $partes[0];

                    $partes = explode(" -", $_POST['user_perfilcod_diretor']);
                    $userPerfil->user_perfilcod_diretor = $partes[0];
                    
                    $partes = explode(" -", $_POST['user_perfilcod_gerente']);
                    $userPerfil->user_perfilcod_gerente = $partes[0];
                }

                if (!empty($data['name'])) {
                    $userPerfil->user_perfilfoto = $nome_arquivo;
                } else {
                    $userPerfil->user_perfilfoto = "NULL";
                }

                if ($userPerfil->user_perfilgestor == "on") {
                    $userPerfil->user_perfilgestor = "0";
                } else {
                    $userPerfil->user_perfilgestor = "1";
                }

                if ($userPerfil->user_perfilind_ativo == "on") {
                    $userPerfil->user_perfilind_ativo = "0";
                } else {
                    $userPerfil->user_perfilind_ativo = "1";
                }

                if ($userPerfil->user_perfiladm == "on") {
                    $userPerfil->user_perfiladm = "0";
                } else {
                    $userPerfil->user_perfiladm = "1";
                }



                $mensagem[] = $userPerfil->save();

                var_dump($userPerfil);
                die;

                //grava o log
                $dadosUltimo = $userPerfil->ultimocadastro($_POST['email'], $userPerfil->user_perfilcnpj_matriz);

                $userPerfilLog = $this->model('UserPerfilLog');

                $userPerfilLog->user_perfil_logcnpj_matriz = $dadosUltimo['empresacnpj_estab'];
                $userPerfilLog->user_perfil_logcod_user = $dadosUltimo['user_perfilcod_user'];
                $userPerfilLog->user_perfil_logdt_emis = date("Y-m-d");
                $userPerfilLog->user_perfil_loghr_emis = date("H:i:s");
                $userPerfilLog->user_perfil_loguser_emis = $_SESSION['userId'];
                $userPerfilLog->user_perfil_logtitulo = "Inclusão";
                $userPerfilLog->user_perfil_lognome_user = $_POST['nome_user'];

                $mensagem[] = $userPerfilLog->save();
            }
        }

        $this->view('user_perfil/cadastrar', $dados = ['mensagem' => $mensagem]);
    }

    public function editar($id)
    {


        Auth::checkLogin();

        $mensagem = array();

        if (isset($_POST['Atualizar'])) {


            $userPerfil = $this->model('UserPerfil');

            //trata cnpj
            $cnpj_estab = str_replace(array('.', '/', '-', '_'), '', $_POST['empresa_matriz']);
            $userPerfil->user_perfilcnpj_matriz = substr($cnpj_estab, 0, 14);

            $userPerfil->user_perfilcod_user = $_POST['cod_user'];
            $userPerfil->user_perfilnome_user = $_POST['nome_user'];
            $userPerfil->user_perfilnome_completo = $_POST['nome_completo'];
            $userPerfil->user_perfildias_validade = $_POST['dias_validade'];
            $userPerfil->user_perfilsenha = password_hash($_POST['senha'], PASSWORD_DEFAULT);
            $userPerfil->user_perfile_mail = $_POST['email'];
            $userPerfil->user_perfilrua = $_POST['rua'];
            $userPerfil->user_perfilnumero = $_POST['numero'];
            $userPerfil->user_perfilcomplemento = $_POST['complemento'];
            $userPerfil->user_perfilbairro = $_POST['bairro'];
            $userPerfil->user_perfilcidade = $_POST['cidade'];
            $userPerfil->user_perfilestado = $_POST['estado'];
            $userPerfil->user_perfilpais = $_POST['pais'];
            $userPerfil->user_perfilcep = str_replace(array('.', '/', '-', '_'), '', $_POST['cep']);
            $userPerfil->user_perfiltelefone = str_replace(array('.', '/', '-', ' '), '', $_POST['telefone']);
            $userPerfil->user_perfilcelular = str_replace(array('(', ')', '-', ' '), '', $_POST['celular']);
            $userPerfil->user_perfiladm = $_POST['adm'];
            $userPerfil->user_perfilgestor = $_POST['gestor'];
            $userPerfil->user_perfilind_ativo = $_POST['ind_ativo'];
            $userPerfil->user_perfilind_origem = "0";
            $userPerfil->user_perfildt_altera = date("Y-m-d");
            $userPerfil->user_perfilhr_altera = date("H:i:s");
            $userPerfil->user_perfildt_criacao = date("Y-m-d");
            $userPerfil->user_perfilhr_criacao = date("H:i:s");

            $userPerfil->user_perfilind_presidente = $_POST['user_perfilind_presidentec'];
            $userPerfil->user_perfilind_diretor = $_POST['user_perfilind_diretorc'];
            $userPerfil->user_perfilind_gerente = $_POST['user_perfilind_gerentec'];
            $userPerfil->user_perfilind_coordenador = $_POST['user_perfilind_coordenadorc'];


            //trata ind_check de PRESIDENTE
            if ($userPerfil->user_perfilind_presidente == 'on') {
                $userPerfil->user_perfilind_presidente = "1";
            } else {
                $userPerfil->user_perfilind_presidente = "0";
            }
            if ($userPerfil->user_perfilind_presidente == '1') {
                $userPerfil->user_perfilind_diretor = "0";
                $userPerfil->user_perfilind_gerente = "0";
                $userPerfil->user_perfilind_coordenador = "0";

                $userPerfil->user_perfilcod_presidente = "0";
                $userPerfil->user_perfilcod_diretor = "0";
                $userPerfil->user_perfilcod_gerente = "0";
                $userPerfil->user_perfilcod_coordenador = "0";
            }

            //trata ind_check de diretor
            if ($userPerfil->user_perfilind_diretor == 'on') {
                $userPerfil->user_perfilind_diretor = "1";
            } else {
                $userPerfil->user_perfilind_diretor = "0";
            }
            if ($userPerfil->user_perfilind_diretor == '1') {

                $userPerfil->user_perfilind_presidente = "0";
                $userPerfil->user_perfilind_gerente = "0";
                $userPerfil->user_perfilind_coordenador = "0";

                $userPerfil->user_perfilcod_gerente = "0";
                $userPerfil->user_perfilind_coordenador = "0";

                $partes = explode(" -", $_POST['user_perfilcod_presidente']);
                $userPerfil->user_perfilcod_presidente = $partes[0];
            }

            //trata ind_check de gerente
            if ($userPerfil->user_perfilind_gerente == 'on') {
                $userPerfil->user_perfilind_gerente = "1";
            } else {
                $userPerfil->user_perfilind_gerente = "0";
            }
            if ($userPerfil->user_perfilind_gerente == '1') {

                $userPerfil->user_perfilcod_gerente = '0';
                $userPerfil->user_perfilcod_coordenador = '0';

                $partes = explode(" -", $_POST['user_perfilcod_presidente']);
                $userPerfil->user_perfilcod_diretor = $partes[0];

                $partes = explode(" -", $_POST['user_perfilcod_diretor']);
                $userPerfil->user_perfilcod_diretor = $partes[0];
            }


            //trata ind_check de cooredenador
            if ($userPerfil->user_perfilind_coordenador == 'on') {
                $userPerfil->user_perfilind_coordenador = "1";
            } else {
                $userPerfil->user_perfilind_coordenador = "0";
            }
            if ($userPerfil->user_perfilind_coordenador == '1') {
                $userPerfil->user_perfilcod_coordenador = '0';

                $partes = explode(" -", $_POST['user_perfilcod_presidente']);
                $userPerfil->user_perfilcod_presidente = $partes[0];

                $partes = explode(" -", $_POST['user_perfilcod_diretor']);
                $userPerfil->user_perfilcod_diretor = $partes[0];
                
                $partes = explode(" -", $_POST['user_perfilcod_gerente']);
                $userPerfil->user_perfilcod_gerente = $partes[0];
            }

            if ($userPerfil->user_perfilgestor == "on") {
                $userPerfil->user_perfilgestor = "0";
            } else {
                $userPerfil->user_perfilgestor = "1";
            }

            if ($userPerfil->user_perfilind_ativo == "on") {
                $userPerfil->user_perfilind_ativo = "0";
            } else {
                $userPerfil->user_perfilind_ativo = "1";
            }

            if ($userPerfil->user_perfiladm == "on") {
                $userPerfil->user_perfiladm = "0";
            } else {
                $userPerfil->user_perfiladm = "1";
            }

            $mensagem[] = $userPerfil->update($id);

            //grava o log
            $userPerfilLog = $this->model('UserPerfilLog');

            $userPerfilLog->user_perfil_logcnpj_matriz = str_replace(array('.', '/', '-', '_'), '', $_POST['cnpj_matriz']);
            $userPerfilLog->user_perfil_logcod_user = $id;
            $userPerfilLog->user_perfil_logdt_emis = date("Y-m-d");
            $userPerfilLog->user_perfil_loghr_emis = date("H:i:s");
            $userPerfilLog->user_perfil_loguser_emis = $_SESSION['userId'];
            $userPerfilLog->user_perfil_logtitulo = "Alteração";
            $userPerfilLog->user_perfil_lognome_user = $_POST['nome_user'];


            $mensagem[] = $userPerfilLog->save();
        }

        $dados = $userPerfil->findId($id);

        $this->view('user_perfil/ver', $dados = ['mensagem' => $mensagem, 'perfil' => $dados]);
    }


    public function excluir($id = '')
    {
        Auth::checkLogin();


        $mensagem = array();
        $userPerfil = $this->model('UserPerfil');

        $mensagem[] = $userPerfil->delete($id);

        $dados = $userPerfil->getAll();

        $this->view('home/index', $dados = ['registros' => $dados, 'mensagem' => $mensagem]);
    }

    public function editarsenha($id)
    {
        Auth::checkLogin();

        $mensagem = array();
        $userPerfil = $this->model('UserPerfil');

        if (isset($_POST['AtualizarSenha'])) {
            if (empty($_POST['senha']) or empty($_POST['senhaconfirmar'])) {
                $mensagem[] = "Os campos: Senha e confirmar Senha não podem ser vazios";
            } elseif ($_POST['senha'] <> $_POST['senhaconfirmar']) {
                $mensagem[] = "Os campos: Senha e confirmar Senha precisam ser iguais";
            } else {
                $userPerfil->user_perfilsenha = password_hash($_POST['senha'], PASSWORD_DEFAULT);
                $mensagem[] = $userPerfil->updateSenha($id);
            }
        }

        $dados = $userPerfil->findId($id);

        //grava o log
        $userPerfilLog = $this->model('UserPerfilLog');
        $userPerfilLog->user_perfil_logcnpj_matriz = str_replace(array('.', '/', '-', '_'), '', $dados['user_perfilcnpj_matriz']);
        $userPerfilLog->user_perfil_logcod_user = $id;
        $userPerfilLog->user_perfil_logdt_emis = date("Y-m-d");
        $userPerfilLog->user_perfil_loghr_emis = date("H:i:s");
        $userPerfilLog->user_perfil_loguser_emis = $_SESSION['userId'];
        $userPerfilLog->user_perfil_logtitulo = "Alteração";
        $userPerfilLog->user_perfil_lognome_user = $dados['user_perfilnome_user'];

        $mensagem[] = $userPerfilLog->save();

        $this->view('user_perfil/editarsenha', $dados = ['mensagem' => $mensagem, 'registros' => $dados]);
    }

    public function editarfoto($id)
    {

        Auth::checkLogin();

        $mensagem = array();
        $userPerfil = $this->model('UserPerfil');
        $dados = $userPerfil->findId($id);

        if (isset($_POST['AtualizarFoto'])) {
            //upload
            if (empty($_POST['foo'])) {
                $storage = new \Upload\Storage\FileSystem('images');
                $file = new \Upload\File('foo', $storage);

                // Optionally you can rename the file on upload
                $new_filename = uniqid();
                $file->setName($new_filename);

                // Validate file upload
                // MimeType List => http://www.iana.org/assignments/media-types/media-types.xhtml
                $file->addValidations(array(
                    // Ensure file is of type "image/png"
                    new \Upload\Validation\Mimetype(array('image/png', 'image/gif', 'image/jpeg')),

                    //You can also add multi mimetype validation
                    //new \Upload\Validation\Mimetype(array('image/png', 'image/gif'))

                    // Ensure file is no larger than 5M (use "B", "K", M", or "G")
                    new \Upload\Validation\Size('5M')
                ));

                // Access data about the file that has been uploaded
                $data = array(
                    'name' => $file->getNameWithExtension(),
                    'extension' => $file->getExtension(),
                    'mime' => $file->getMimetype(),
                    'size' => $file->getSize(),
                    'md5' => $file->getMd5(),
                    'dimensions' => $file->getDimensions()
                );

                //Para gravar no banco de dados
                //$nome_arquivo = "images/" . $data['name'];

                // Try to upload file
                try {
                    // Success!
                    $file->upload();
                    $mensagem[] = "Upload feito com sucesso!";
                } catch (\Exception $e) {
                    // Fail!
                    $errors = $file->getErrors();
                    $mensagem[] = implode("<br>", $errors);
                }
            }

            if (!empty($data['name'])) {
                $userPerfil->user_perfilfoto = $data['name'];
                $mensagem[] = $userPerfil->updatefoto($id);

                //grava o log
                $userPerfilLog = $this->model('UserPerfilLog');
                $userPerfilLog->user_perfil_logcnpj_matriz = $dados['user_perfilcnpj_matriz'];
                $userPerfilLog->user_perfil_logcod_user = $id;
                $userPerfilLog->user_perfil_logdt_emis = date("Y-m-d");
                $userPerfilLog->user_perfil_loghr_emis = date("H:i:s");
                $userPerfilLog->user_perfil_loguser_emis = $_SESSION['userId'];
                $userPerfilLog->user_perfil_logtitulo = "Alteração";
                $userPerfilLog->user_perfil_lognome_user = $dados['user_perfilnome_user'];

                $mensagem[] = $userPerfilLog->save();
            } else {
                $userPerfil->user_perfilfoto = "NULL";
            }
        }

        $this->view('user_perfil/editarfoto', $dados = ['mensagem' => $mensagem, 'registros' => $dados]);
    }

    public function pesquisar()
    {
        Auth::checkLogin();

        $busca = isset($_POST['texto']) ? $_POST['texto'] : $_SESSION['texto'];
        $_SESSION['texto'] = $busca;

        $user_perfil = $this->model('UserPerfil');
        $dados = $user_perfil->pesquisar($busca);

        $this->view('user_perfil/index', $dados = ['registros' => $dados]);
    }
}
