<?php

use \App\Core\Controller;
use App\Auth;

class catfilho extends Controller
{

    public function index()
    {
        Auth::checkLogin();
        $CatFilho = $this->model('CatFilhoModel');

        $dados = $CatFilho->GetAll();

        $this->view('catfilho/index', $dados = ['registros' => $dados]);
    }

    public function ver($id = '')
    {
        Auth::checkLogin();
        $CatFilho = $this->model('CatFilhoModel');

        $dados = $CatFilho->getId($id);

        $this->view('catfilho/ver', $dados = ['registros' => $dados]);
    }

    public function cadastrar()
    {
        Auth::checkLogin();
        $CatFilho = $this->model('CatFilhoModel');
        $mensagem = array();

        if (isset($_POST['Cadastrar'])) {

            //recebe posts
            $CatFilho->nome_cat_f = $_POST['nome_cat_f'];
            $CatFilho->user_criacao = $_SESSION['userId'];
            $CatFilho->dt_criacao = date("Y-m-d");
            $CatFilho->hr_criacao = date("H:i:s");

            //trata ind Ativo]
            if ($_POST['ind_ativo'] == "on") {
                $CatFilho->ind_ativo = '0';
            } else {
                $CatFilho->ind_ativo = '1';
            }
            $mensagem[] = $CatFilho->save();
        }
        $dados = $CatFilho->GetUltimoCod();

        $this->view('catfilho/cadastrar', $dados = ['registros' => $dados, 'mensagem' => $mensagem]);
    }

    public function editar($id)
    {

        Auth::checkLogin();
        $CatFilho = $this->model('CatFilhoModel');
        $mensagem = array();

        if (isset($_POST['Atualizar'])) {

            $CatFilho->nome_cat_f = $_POST['nome_cat_f'];

            //trata ind Ativo]
            if ($_POST['ind_ativo'] == "on") {
                $CatFilho->ind_ativo = '0';
            } else {
                $CatFilho->ind_ativo = '1';
            }

            $mensagem[] = $CatFilho->update($id);
        }

        $dados = $CatFilho->getId($id);

        $this->view('catfilho/ver', $dados = ['mensagem' => $mensagem, 'registros' => $dados]);
    }


    public function excluir($id = '')
    {
        Auth::checkLogin();

        $mensagem = array();
        $CatFilho = $this->model('CatFilhoModel');

        if ($CatFilho->VerificaExcluir($id)) {
            $mensagem[] = $CatFilho->delete($id);
        }else{
            $mensagem[] = "6 - Existem produtos vinculados a essa categoria!";
        }

        $dados = $CatFilho->GetAll();

        $this->view('catfilho/index', $dados = ['mensagem' => $mensagem, 'registros' => $dados]);
    }
}
