<?php

use \App\Core\Controller;
use App\Auth;

class Home extends Controller
{
    public function index($id = '')
    {
        Auth::checkLogin();

        $empresas = $this->model('Empresa');
        $dados = $empresas->findId($id);

        $this->view('home/loja_geral', $dados = ['mensagem' => '3 - Bem Vindo(a)!']);
    }

    public function login()
    {
        $mensagem = array();

        if (isset($_POST["entrar"])) {
            if ((empty($_POST['email'])) or (empty($_POST['senha']))) {
                $mensagem[] = "Os campos são obrigatórios";
            } else {
                $email = $_POST['email'];
                $senha = $_POST['senha'];
                //criptografa a senha
                $senha = sha1($senha);
                $mensagem[] = Auth::Login($email, $senha);
            }
        }

        $this->view('home/login', $dados = ['mensagem' => $mensagem]);
    }

    public function cadastrar()
    {

        $mensagem = array();

        if (isset($_POST["cadastrar"])) {
            //recebe os valores
            $nome = $_POST['nome'];
            $sobrenome = $_POST['sobrenome'];
            $datanasc = $_POST['datanasc'];
            $genero = $_POST['genero'];
            $email = $_POST['email'];
            $confemail = $_POST['confemail'];
            $senha = $_POST['senha'] . '3';
            $senha = sha1($senha);
            $confsenha = $_POST['confsenha'];
            $confsenha = sha1($confsenha);
            $dt_criacao = date("Y-m-d");
            $hr_criacao = date("H:i:s");

            if ($senha != $confsenha) {
                $mensagem[] = "3 - As senhas não coincidem!";
            } else {
                if ($email != $confemail) {
                    $mensagem[] = "3 - Os e-mails não coincidem!";
                } else {
                    $mensagem[] = Auth::Cadastrar($nome, $sobrenome, $datanasc, $genero, $email, $senha, $confsenha, $dt_criacao, $hr_criacao);
                }
            }
        }

        $this->view('home/cadastrar', $dados = ['mensagem' => $mensagem]);
    }

    public function logout()
    {
        Auth::Logout();
    }
}
