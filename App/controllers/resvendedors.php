<?php

use \App\Core\Controller;
use App\Auth;

class resvendedors extends Controller
{

    public function index()
    {
        Auth::checkLogin();

        $empresas = $this->model('Empresa');
        $dados = $empresas->getPorMatriz($_SESSION['matriz']);

        $this->view('resvendedors/index', $dados = ['registros' => $dados]);
    }

    public function ver($id = '')
    {
        Auth::checkLogin();

        $res_vendedor = $this->model('ResVendedor');
        $dados = $res_vendedor->findId($id);

        $this->view('resvendedors/ver', $dados = ['registros' => $dados]);
    }

    public function cadastrar()
    {
        Auth::checkLogin();

        $mensagem = array();

        if (isset($_POST['Cadastrar'])) {

            $res_vendedor = $this->model('ResVendedor');

            //trata cnpj
            $cnpj_estab = str_replace(array('.', '/', '-', '_'), '', $_POST['cnpj_estab']);
            $res_vendedor->res_vendedorcnpj_estab = substr($cnpj_estab, 0, 14);

            //trata o cod_unid_neg
            $partes = explode(" -", $_POST['cod_unid_neg']);
            $res_vendedor->res_vendedorcod_unid_neg = $partes[0];

            $res_vendedor->res_vendedordt_movto = $_POST['dt_movto'];
            $res_vendedor->res_vendedorcod_gerente = $_POST['cod_gerente'];
            $res_vendedor->res_vendedorcod_vendedor = $_POST['cod_vendedor'];
            $res_vendedor->res_vendedorvl_real = str_replace(array('.', ','), array('', '.'), $_POST['vl_real']);

            $mensagem[] = $res_vendedor->save();
        }

        $this->view('resvendedors/cadastrar', $dados = ['mensagem' => $mensagem]);
    }

    public function editar($id)
    {

        Auth::checkLogin();

        $mensagem = array();
        $res_vendedor = $this->model('ResVendedor');

        if (isset($_POST['Atualizar'])) {
            //trata cnpj
            $cnpj_estab = str_replace(array('.', '/', '-', '_'), '', $_POST['cnpj_estab']);
            $res_vendedor->res_vendedorcnpj_estab = substr($cnpj_estab, 0, 14);

            //trata o cod_unid_neg
            $partes = explode(" -", $_POST['cod_unid_neg']);
            $res_vendedor->res_vendedorcod_unid_neg = $partes[0];
            
            $res_vendedor->res_vendedordt_movto = $_POST['dt_movto'];
            $res_vendedor->res_vendedorcod_gerente = $_POST['cod_gerente'];
            $res_vendedor->res_vendedorcod_vendedor = $_POST['cod_vendedor'];
            $res_vendedor->res_vendedorvl_real = str_replace(array('.', ','), array('', '.'), $_POST['vl_real']);

            $mensagem[] = $res_vendedor->update($id);

        }

        $dados = $res_vendedor->findId($id);

        $this->view('resvendedors/editar', $dados = ['mensagem' => $mensagem, 'registros' => $dados]);
    }


    public function excluir($id = '')
    {
        Auth::checkLogin();

        $mensagem = array();
        $res_vendedor = $this->model('ResVendedor');

        $mensagem[] = $res_vendedor->delete($id);

        $empresas = $this->model('Empresa');
        $dados = $empresas->getPorMatriz($_SESSION['matriz']);

        $this->view('resvendedors/index', $dados = ['registros' => $dados]);
    }
}
