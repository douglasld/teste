<?php

use \App\Core\Controller;
use App\Auth;

class meusprod extends Controller
{

    public function index()
    {
        //verifica permissao
        Auth::checkLogin();

        //faz os requires
        $MeusProd = $this->model('MeuProd');

        //pega todos os produtos do usuário
        $GetAllProds = $MeusProd->GetAllProdsIndex($_SESSION['userId']);

        //verifica se todos os produtos pertencem a ele
        foreach ($GetAllProds as $count) {
            if ($count['cod_loja'] != $_SESSION['userId']) {
                $mensagem[] = "3 - Este produto não pertence a esta conta!";
                $this->view('meusprod/index', $dados = ['mensagem' => $mensagem]);
            }
        }

        $this->view('meusprod/index', $dados = ['user_prods' => $GetAllProds]);
    }



    public function ver($id = '')
    {
        Auth::checkLogin();

        $produto = $this->model('Produto');
        $dados = $produto->findId($id);

        $this->view('meusprod/ver', $dados);
    }

    public function cadastrar()
    {
        Auth::checkLogin();

        $mensagem = array();

        if (isset($_POST['Cadastrar'])) {

            $produto = $this->model('Produto');

            $produto->produtocnpj_estab = str_replace(array('.', '/', '-', '_'), '', $_POST['cnpj_estab']);
            $produto->produtocod_produto = $_POST['cod_produto'];
            $produto->produtodescricao = $_POST['descricao'];
            $produto->produtocod_unid_med = $_POST['cod_unid_med'];
            $produto->produtoind_ativo = $_POST['ind_ativo'];
            $produto->produtoind_origem = "0";

            //altera o valor recebido do checkbox
            if ($produto->produtoind_ativo == "on") {
                $produto->produtoind_ativo = "0";
            } else {
                $produto->produtoind_ativo = "1";
            }

            $produto->produtodt_criacao = date("Y-m-d");
            $produto->produtohr_criacao = date("H:i:s");
            $produto->produtouser_criacao = $_SESSION['userId'];
            $produto->produtodt_altera = date("Y-m-d");
            $produto->produtohr_altera = date("H:i:s");
            $produto->produtouser_altera = $_SESSION['userId'];

            $mensagem[] = $produto->save();

            $estab = str_replace(array('.', '/', '-', '_'), '', $_POST['cnpj_estab']);
            $ultimo = $produto->ultimo($estab, $_SESSION['userId']);

            $produto_log = $this->model('ProdutoLog');

            $produto_log->produto_logcnpj_estab = str_replace(array('.', '/', '-', '_'), '', $_POST['cnpj_estab']);
            $produto_log->produto_logdt_emis = date("Y-m-d");
            $produto_log->produto_loghr_emis = date("H:i:s");
            $produto_log->produto_loguser_emis = $_SESSION['userId'];
            $produto_log->produto_logtitulo = "Inclusão";
            $produto_log->produto_logcod_produto = $ultimo['produtoid'];

            $mensagem[] = $produto_log->save();
        }

        $this->view('meusprod/cadastrar', $dados = ['mensagem' => $mensagem]);
    }

    public function editar($id = '')
    {
        //verifica login
        Auth::checkLogin();
        //cria array mensagem    
        $mensagem = array();

        if ($_SESSION['data']) {
            $id = $_SESSION['data']['cod'];
            $mensagem[] = $_SESSION['data']['mensagem'];
            unset($_SESSION['data']);
        }

        //faz requires
        $MeusProd = $this->model('MeuProd');

        //busca o prod pelo código
        $GetAllProd = $MeusProd->findId($id);

        //verifica caso o produto é realmente seu
        if ($GetAllProd['cod_loja'] == $_SESSION['userId']) {

            //includes
            $prods = $this->model('VendProds');

            //verifica produto
            if ($verifica_prod = $prods->VerificaProdEdita($id)) {
                $verifica_prod = $verifica_prod[0];
                $cod_prod = $verifica_prod['cod_prod'];
                $cod_cat_p = $verifica_prod['cod_cat_p'];

                //verifica a pagina a ser direcionada
                if ($cod_cat_p == 1) {
                    $this->view('editaprod/artesanato', $dados = ['registros' => $verifica_prod, 'edita' => $GetAllProd, 'mensagem' => $mensagem[0]]);
                }
                if ($cod_cat_p == 2) {
                    $this->view('editaprod/tabacaria', $dados = ['registros' => $verifica_prod, 'edita' => $GetAllProd, 'mensagem' => $mensagem[0]]);
                }
                if ($cod_cat_p == 3) {
                    $this->view('editaprod/malha', $dados = ['registros' => $verifica_prod, 'edita' => $GetAllProd, 'mensagem' => $mensagem[0]]);
                }
                if ($cod_cat_p == 4) {
                    $this->view('editaprod/pintura', $dados = ['registros' => $verifica_prod, 'edita' => $GetAllProd, 'mensagem' => $mensagem[0]]);
                }
                if ($cod_cat_p == 5) {
                    $this->view('editaprod/modelagem', $dados = ['registros' => $verifica_prod, 'edita' => $GetAllProd, 'mensagem' => $mensagem[0]]);
                }
                if ($cod_cat_p == 6) {
                    $this->view('editaprod/estetica', $dados = ['registros' => $verifica_prod, 'edita' => $GetAllProd, 'mensagem' => $mensagem[0]]);
                }
                if ($cod_cat_p == 7) {
                    $this->view('editaprod/higiene', $dados = ['registros' => $verifica_prod, 'edita' => $GetAllProd, 'mensagem' => $mensagem[0]]);
                }
            }
        }

        //pega todos os produtos do usuário
        $GetAllProds = $MeusProd->GetAllProdsIndex($_SESSION['userId']);

        //verifica se todos os produtos pertencem a ele
        foreach ($GetAllProds as $count) {
            if ($count['cod_loja'] != $_SESSION['userId']) {
                $mensagem[] = "3 - Este produto não pertence a esta conta!";
                $this->view('meusprod/index', $dados = ['mensagem' => $mensagem]);
            }
        }

        $this->view('meusprod/index', $dados = ['user_prods' => $GetAllProds, 'mensagem' => $mensagem]);
    }


    public function excluir($id = '')
    {
        //verifica login
        Auth::checkLogin();

        //cria array mensagem    
        $mensagem = array();

        //faz requires
        $MeusProd = $this->model('MeuProd');

        //busca o id pelo código
        $GetAllProd = $MeusProd->findId($id);

        //verifica caso o produto é realmente seu
        if ($GetAllProd['cod_loja'] == $_SESSION['userId']) {

            //exclui o produto
            $mensagem[] = $MeusProd->excluir($id);
        } else {
            //exclui o produto
            $mensagem[] = "3 - Este produto não pertence a esta conta!";
        }

        //pega todos os produtos do usuário
        $GetAllProds = $MeusProd->GetAllProdsIndex($_SESSION['userId']);

        //verifica se todos os produtos pertencem a ele
        foreach ($GetAllProds as $count) {
            if ($count['cod_loja'] != $_SESSION['userId']) {
                $mensagem[] = "3 - Este produto não pertence a esta conta!";
                $this->view('meusprod/index', $dados = ['mensagem' => $mensagem]);
            }
        }

        $this->view('meusprod/index', $dados = ['user_prods' => $GetAllProds, 'mensagem' => $mensagem]);
    }


    public function ocultar($id = '')
    {
        //verifica login
        Auth::checkLogin();

        //cria array mensagem    
        $mensagem = array();

        //faz requires
        $MeusProd = $this->model('MeuProd');

        //busca o id pelo código
        $GetAllProd = $MeusProd->findId($id);

        //verifica caso o produto é realmente seu
        if ($GetAllProd['cod_loja'] == $_SESSION['userId']) {

            //oculta o produto
            $mensagem[] = $MeusProd->ocultar($id);
        } else {
            //retorna mensagem
            $mensagem[] = "3 - Este produto não pertence a esta conta!";
        }

        //pega todos os produtos do usuário
        $GetAllProds = $MeusProd->GetAllProdsIndex($_SESSION['userId']);

        $this->view('meusprod/index', $dados = ['user_prods' => $GetAllProds, 'mensagem' => $mensagem]);
    }

    public function desocultar($id = '')
    {
        //verifica login
        Auth::checkLogin();

        //cria array mensagem    
        $mensagem = array();

        //faz requires
        $MeusProd = $this->model('MeuProd');

        //busca o id pelo código
        $GetAllProd = $MeusProd->findId($id);

        //verifica caso o produto é realmente seu
        if ($GetAllProd['cod_loja'] == $_SESSION['userId']) {

            //oculta o produto
            $mensagem[] = $MeusProd->desocultar($id);
        } else {
            //retorna mensagem
            $mensagem[] = "3 - Este produto não pertence a esta conta!";
        }

        //pega todos os produtos do usuário
        $GetAllProds = $MeusProd->GetAllProdsIndex($_SESSION['userId']);

        $this->view('meusprod/index', $dados = ['user_prods' => $GetAllProds, 'mensagem' => $mensagem]);
    }
}
