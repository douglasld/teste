<?php

use \App\Core\Controller;
use App\Auth;

class unidnegs extends Controller{

    public function index()
    {
        Auth::checkLogin();

        $unid_neg = $this->model('UnidNeg');

        $dados = $unid_neg->getindex();

        $this->view('unidnegs/index', $dados = ['registros' => $dados]);
    }


    public function ver($id = '')
    {
        Auth::checkLogin();

        $unid_neg = $this->model('UnidNeg');
        $dados = $unid_neg->findId($id);

        $this->view('unidnegs/ver', $dados = ['registros' => $dados]);
    }

    public function cadastrar()
    {
        Auth::checkLogin();
        $unid_neg = $this->model('UnidNeg');

        $mensagem = array();

        if(isset($_POST['Cadastrar'])){

            $cnpj_estab = str_replace(array('.', '/', '-', '_'), '', $_POST['cnpj_estab']);
            $unid_neg->unid_negcnpj_estab = substr($cnpj_estab,0, 14);

            $unid_neg->unid_negdescricao = $_POST['unidnegdescricao'];
            $unid_neg->unid_negind_ativo = $_POST['ind_ativo'];
            if($unid_neg->unid_negind_ativo == "on"){
                $unid_neg->unid_negind_ativo = "0";
            }else{
                $unid_neg->unid_negind_ativo = "1";
            }
            $unid_neg->unid_negind_origem = "0";
            $unid_neg->unid_negdt_criacao = date("Y-m-d");
            $unid_neg->unid_neghr_criacao = date("h:i:s");
            $unid_neg->unid_neguser_criacao = $_SESSION['userId'];
            $unid_neg->unid_negdt_altera = date("Y-m-d");
            $unid_neg->unid_neghr_altera = date("h:i:s");
            $unid_neg->unid_neguser_altera = $_SESSION['userId'];

            $mensagem[] = $unid_neg->save();

            $unid_neg_log = $this->model('UnidNegLog');
            $cnpj_estab = str_replace(array('.', '/', '-', '_'), '', $_POST['cnpj_estab']);
            $unid_neg_log->unid_neg_logcnpj_estab = substr($cnpj_estab,0, 14);
            $unid_neg_log->unid_neg_logdt_emis = date("Y-m-d");
            $unid_neg_log->unid_neg_loghr_emis = date("h:i:s");
            $unid_neg_log->unid_neg_loguser_emis = $_SESSION['userId'];
            $unid_neg_log->unid_neg_logtitulo = "Inclusão";

            $mensagem[] = $unid_neg_log->save();
        }

        $this->view('unidnegs/cadastrar', $dados = ['mensagem' => $mensagem]);

    }

    public function editar($id){

        Auth::checkLogin();

        $mensagem = array();
        $unid_neg = $this->model('UnidNeg');

        if(isset($_POST['Atualizar'])){

            $cnpj_estab = str_replace(array('.', '/', '-', '_'), '', $_POST['cnpj_estab']);
            $unid_neg->unid_negcnpj_estab = substr($cnpj_estab,0, 14);

            $unid_neg->unid_negdescricao = $_POST['unidnegdescricao'];
            $unid_neg->unid_negind_ativo = $_POST['ind_ativo'];
            if($unid_neg->unid_negind_ativo == "on"){
                $unid_neg->unid_negind_ativo = "0";
            }else{
                $unid_neg->unid_negind_ativo = "1";
            }
            $unid_neg->unid_negind_origem = "0";
            $unid_neg->unid_negdt_criacao = date("Y-m-d");
            $unid_neg->unid_neghr_criacao = date("h:i:s");
            $unid_neg->unid_neguser_criacao = $_SESSION['userId'];
            $unid_neg->unid_negdt_altera = date("Y-m-d");
            $unid_neg->unid_neghr_altera = date("h:i:s");
            $unid_neg->unid_neguser_altera = $_SESSION['userId'];

            $mensagem[] = $unid_neg->update($id);

            $unid_neg_log = $this->model('UnidNegLog');
            $cnpj_estab = str_replace(array('.', '/', '-', '_'), '', $_POST['cnpj_estab']);
            $unid_neg_log->unid_neg_logcnpj_estab = substr($cnpj_estab,0, 14);
            $unid_neg_log->unid_neg_logdt_emis = date("Y-m-d");
            $unid_neg_log->unid_neg_loghr_emis = date("h:i:s");
            $unid_neg_log->unid_neg_loguser_emis = $_SESSION['userId'];
            $unid_neg_log->unid_neg_logtitulo = "Alteração";

            $mensagem[] = $unid_neg_log->save();

        }

        $dados = $unid_neg->findId($id);

        $this->view('unidnegs/editar', $dados = [ 'mensagem' => $mensagem, 'registros' => $dados]);
    }


    public function excluir($id = '')
    {

        Auth::checkLogin();

        $mensagem = array();
        $unid_neg = $this->model('UnidNeg');

        $mensagem[] = $unid_neg->delete($id);

        $dados = $unid_neg->getindex();

        $this->view('unidnegs/index', $dados = ['mensagens' => $mensagem, 'registros' => $dados]);
    }

}




