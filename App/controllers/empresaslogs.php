<?php

use App\Core\Controller;
use App\Auth;

class empresaslogs extends Controller{

    public function index($id = '')
    {
        Auth::checkLogin();

        $empresa_log = $this->model('EmpresaLog');
        $dados = $empresa_log->findCnpjEstab($id);

        $this->view('empresaslogs/index', $dados = ['registros' => $dados]);
    }

    public function ver($id = '')
    {
        Auth::checkLogin();

        $empresa_log = $this->model('EmpresaLog');
        $dados['logs'] = $empresa_log->findId($id);

        $this->view('empresas/ver', $dados);
    }


}
