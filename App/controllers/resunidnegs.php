<?php

use \App\Core\Controller;
use App\Auth;

class resunidnegs extends Controller
{

    public function index()
    {
        Auth::checkLogin();

        $empresas = $this->model('Empresa');
        $dados = $empresas->getPorMatriz($_SESSION['matriz']);

        $this->view('resunidnegs/index', $dados = ['registros' => $dados]);
    }

    public function ver($id = '')
    {
        Auth::checkLogin();

        $res_unid_neg = $this->model('ResUnidNeg');
        $dados = $res_unid_neg->findId($id);

        $this->view('resunidnegs/ver', $dados = ['registros' => $dados]);
    }

    public function cadastrar()
    {
        Auth::checkLogin();

        $mensagem = array();

        if (isset($_POST['Cadastrar'])) {

            $res_unid_neg = $this->model('ResUnidNeg');

            $cnpj_estab = str_replace(array('.', '/', '-', '_'), '', $_POST['cnpj_estab']);
            $res_unid_neg->res_unid_negcnpj_estab = substr($cnpj_estab, 0, 14);
            $res_unid_neg->res_unid_negcod_unid_neg = $_POST['cod_unid_neg'];
            $res_unid_neg->res_unid_negdt_movto = $_POST['dt_movto'];
            $res_unid_neg->res_unid_negvl_real = str_replace(array('.', ','), array('', '.'), $_POST['vl_real']);

            if (strlen($res_unid_neg->res_unid_negcnpj_estab) < 14) {
                $mensagem[] = "CNPJ incompleto!";
                $this->view('resunidnegs/cadastrar', $dados = ['mensagem' => $mensagem]);
            }
            $mensagem[] = $res_unid_neg->save();
        }

        $this->view('resunidnegs/cadastrar', $dados = ['mensagem' => $mensagem]);
    }

    public function editar($id)
    {

        Auth::checkLogin();

        $mensagem = array();
        $res_unid_neg = $this->model('ResUnidNeg');

        if (isset($_POST['Atualizar'])) {

            $cnpj_estab = str_replace(array('.', '/', '-', '_'), '', $_POST['cnpj_estab']);
            $res_unid_neg->res_unid_negcnpj_estab = substr($cnpj_estab, 0, 14);

            $res_unid_neg->res_unid_negdt_movto = $_POST['dt_movto'];
            $res_unid_neg->res_unid_negcod_unid_neg = $_POST['cod_unid_neg'];
            
            $res_unid_neg->res_unid_negvl_real = str_replace(array('.', ','), array('', '.'), $_POST['vl_real']);

            if (strlen($res_unid_neg->res_unid_negcnpj_estab) < 14) {
                $mensagem[] = "CNPJ incompleto!";
                $this->view('resunidnegs/cadastrar', $dados = ['mensagem' => $mensagem]);
            }

            $mensagem[] = $res_unid_neg->update($id);
        }

        $dados = $res_unid_neg->findId($id);
        $this->view('resunidnegs/editar', $dados = ['mensagem' => $mensagem, 'registros' => $dados]);
    }


    public function excluir($id = '')
    {
        Auth::checkLogin();

        $mensagem = array();
        $res_unid_neg = $this->model('ResUnidNeg');

        $mensagem[] = $res_unid_neg->delete($id);

        $empresas = $this->model('Empresa');
        $dados = $empresas->getPorMatriz($_SESSION['matriz']);

        $this->view('resunidnegs/index', $dados = ['registros' => $dados]);
    }
}
