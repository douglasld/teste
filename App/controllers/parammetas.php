<?php

use \App\Core\Controller;
use App\Auth;

class parammetas extends Controller{

    public function index()
    {
        Auth::checkLogin();

        $param_meta = $this->model('ParamMeta');
        $dados = $param_meta->getAll();

        $this->view('parammetas/index', $dados = ['registros' => $dados]);
    }


    public function ver($id = '')
    {
        Auth::checkLogin();

        $param_meta = $this->model('ParamMeta');
        $dados = $param_meta->findId($id);

        $this->view('parammetas/ver', $dados);
    }

    public function cadastrar()
    {
        Auth::checkLogin();

        $mensagem = array();

        if(isset($_POST['Cadastrar'])){

            $param_meta = $this->model('ParamMeta');

            $param_meta->param_metadt_criacao = date("Y-m-d");
            $param_meta->param_metahr_criacao = date("H:i:s");
            $param_meta->param_metauser_criacao = $_SESSION['userId'];
            $param_meta->param_metadt_altera = date("Y-m-d");
            $param_meta->param_metahr_altera = date("H:i:s");
            $param_meta->param_metauser_altera = $_SESSION['userId'];

            $mensagem[] = $param_meta->save();

            $param_meta_log = $this->model('ParamMetaLog');

            $param_meta_log->cnpj_matriz = 0;
            $param_meta_log->cnpj_estab = 0;
            $param_meta_log->dt_emis = date("Y-m-d");
            $param_meta_log->hr_emis = date("H:i:s");
            $param_meta_log->user_emis = $_SESSION['userId'];
            $param_meta_log->titulo = "Inclusão";

            $mensagem[] = $param_meta_log->save();

        }

        $this->view('parammetas/cadastrar', $dados = ['mensagem' => $mensagem]);

    }

    public function editar($id){

        Auth::checkLogin();

        $mensagem = array();
        $param_meta = $this->model('ParamMeta');

        if(isset($_POST['Atualizar'])){

            $param_meta->param_metadt_altera = date("Y-m-d");
            $param_meta->param_metahr_altera = date("H:i:s");
            $param_meta->param_metauser_altera = $_SESSION['userId'];

            $mensagem[] = $param_meta->update($id);

            $param_meta_log = $this->model('ParamMetaLog');

            $param_meta_log->cnpj_matriz = 0;
            $param_meta_log->cnpj_estab = 0;
            $param_meta_log->dt_emis = date("Y-m-d");
            $param_meta_log->hr_emis = date("H:i:s");
            $param_meta_log->user_emis = $_SESSION['userId'];
            $param_meta_log->titulo = "Alteração";

            $mensagem[] = $param_meta_log->save();

        }

        $dados = $param_meta->findId($id);

        $this->view('parammetas/editar', $dados = [ 'mensagem' => $mensagem, 'registros' => $dados]);
    }


    public function excluir($id = '')
    {

        Auth::checkLogin();
        die();

        $mensagem = array();
        $param_meta = $this->model('ParamMeta');

        $mensagem[] = $param_meta->delete($id);

        $dados = $param_meta->getAll();

        $this->view('home/index', $dados = ['registros' => $dados, 'mensagem' => $mensagem]);
    }

}


