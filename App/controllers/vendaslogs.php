<?php

use App\Core\Controller;
use App\Auth;

class vendaslogs extends Controller{

    public function index($estabelecimento =' ',$numeroDocto =' ',$serieDocto =' ',$dataDocto =' ')
    {
        Auth::checkLogin();

        $vendas_log = $this->model('VendaLog');
        $dados = $vendas_log->getAll($estabelecimento,$numeroDocto,$serieDocto,$dataDocto);

        $this->view('vendaslogs/index', $dados = ['registros' => $dados]);
    }

}


