<?php

use \App\Core\Controller;
use App\Auth;

class resprodutos extends Controller
{

    public function index()
    {
        Auth::checkLogin();

        $empresas = $this->model('Empresa');
        $dados = $empresas->getPorMatriz($_SESSION['matriz']);

        $this->view('resprodutos/index', $dados = ['registros' => $dados]);
    }

    public function ver($id = '')
    {
        Auth::checkLogin();

        $res_produto = $this->model('ResProduto');
        $dados = $res_produto->findId($id);

        $this->view('resprodutos/ver',  $dados = ['registros' => $dados]);
    }

    public function cadastrar()
    {
        Auth::checkLogin();

        $mensagem = array();

        if (isset($_POST['Cadastrar'])) {

            $res_produto = $this->model('ResProduto');

            $cnpj_estab = str_replace(array('.', '/', '-', '_'), '', $_POST['cnpj_estab']);
            $res_produto->res_produtocnpj_estab = substr($cnpj_estab,0, 14);   
            $res_produto->res_produtodt_movto = $_POST['dt_movto'];
            $res_produto->res_produtocod_unid_neg = $_POST['cod_unid_neg'];
            $res_produto->res_produtocod_gerente = $_POST['cod_gerente'];
            $res_produto->res_produtocod_vendedor = $_POST['cod_vendedor'];
            $res_produto->res_produtocod_gru_prod = $_POST['cod_gru_prod'];
            $res_produto->res_produtocod_produto = $_POST['cod_produto'];
            $res_produto->res_produtovl_real = str_replace(array('.', ','), array('', '.'), $_POST['vl_real']);


            $mensagem[] = $res_produto->save();

        }
        $this->view('resprodutos/cadastrar', $dados = ['mensagem' => $mensagem]);
    }
    public function editar($id)
    {
        Auth::checkLogin();
        $mensagem = array();
        $res_produto = $this->model('ResProduto');   

        if (isset($_POST['Atualizar'])) {

            $cnpj_estab = str_replace(array('.', '/', '-', '_'), '', $_POST['cnpj_estab']);
            $res_produto->res_produtocnpj_estab = substr($cnpj_estab,0, 14);   
            $res_produto->res_produtodt_movto = $_POST['dt_movto'];
            $res_produto->res_produtocod_unid_neg = $_POST['cod_unid_neg'];
            $res_produto->res_produtocod_gerente = $_POST['cod_gerente'];
            $res_produto->res_produtocod_vendedor = $_POST['cod_vendedor'];
            $res_produto->res_produtocod_gru_prod = $_POST['cod_gru_prod'];
            $res_produto->res_produtocod_produto = $_POST['cod_produto'];

            $res_produto->res_produtovl_real = str_replace(array('.', ','), array('', '.'), $_POST['vl_real']);

            $mensagem[] = $res_produto->update($id);
        
        }

        $dados = $res_produto->findId($id);

        $this->view('resprodutos/editar', $dados = ['mensagem' => $mensagem, 'registros' => $dados]);
    }


    public function excluir($id = '')
    {
        Auth::checkLogin();

        $mensagem = array();
        $res_produto = $this->model('ResProduto');

        $mensagem[] = $res_produto->delete($id);

        $empresas = $this->model('Empresa');
        $dados = $empresas->getPorMatriz($_SESSION['matriz']);

        $this->view('resprodutos/index', $dados = ['registros' => $dados]);
    }
}
