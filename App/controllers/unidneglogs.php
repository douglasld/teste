<?php

use App\Core\Controller;
use App\Auth;

class unidneglogs extends Controller{

    public function index($id = '')
    {
        Auth::checkLogin();

        $unid_neg_log = $this->model('UnidNegLog');
        $dados = $unid_neg_log->findId($id);

        $this->view('unidneglogs/index', $dados = ['registros' => $dados]);
    }

}



