<?php

use \App\Core\Controller;
use App\Auth;

class tabprod extends Controller
{

    public function index()
    {
        Auth::checkLogin();
        $TabProd = $this->model('TabProdModel');

        $dados = $TabProd->GetAll();

        $this->view('tabprod/index', $dados = ['registros' => $dados]);
    }

    public function ver($id = '')
    {
        Auth::checkLogin();
        $TabProd = $this->model('TabProdModel');

        $dados = $TabProd->getId($id);

        $this->view('tabprod/ver', $dados = ['registros' => $dados]);
    }

    public function cadastrar()
    {
        Auth::checkLogin();
        $TabProd = $this->model('TabProdModel');
        $mensagem = array();

        if (isset($_POST['Cadastrar'])) {

            //recebe posts
            $TabProd->nome_prod = $_POST['nome_prod'];
            $TabProd->user_criacao = $_SESSION['userId'];
            $TabProd->dt_criacao = date("Y-m-d");
            $TabProd->hr_criacao = date("H:i:s");

            //trata o produto_pai
            $TabProd->cod_cat_p = $_POST['cod_cat_p'];

            //trata o produto_filho
            $TabProd->cod_cat_f = $_POST['cod_cat_f'];

            //trata ind Ativo]
            if ($_POST['ind_ativo'] == "on") {
                $TabProd->ind_ativo = '0';
            } else {
                $TabProd->ind_ativo = '1';
            }

            $mensagem[] = $TabProd->save();
        }
        $dados = $TabProd->GetUltimoCod();

        $this->view('tabprod/cadastrar', $dados = ['registros' => $dados, 'mensagem' => $mensagem]);
    }

    public function editar($id)
    {

        Auth::checkLogin();
        $TabProd = $this->model('TabProdModel');
        $mensagem = array();

        if (isset($_POST['Atualizar'])) {
            //recebe posts
            $TabProd->nome_prod = $_POST['nome_prod'];

            //trata o produto_pai
            $TabProd->cod_cat_p = $_POST['cod_cat_p'];

            //trata o produto_filho
            $TabProd->cod_cat_f = $_POST['cod_cat_f'];


            //trata ind Ativo]
            if ($_POST['ind_ativo'] == "on") {
                $TabProd->ind_ativo = '0';
            } else {
                $TabProd->ind_ativo = '1';
            }

            $mensagem[] = $TabProd->update($id);
        }

        $dados = $TabProd->getId($id);

        $this->view('tabprod/ver', $dados = ['mensagem' => $mensagem, 'registros' => $dados]);
    }


    public function excluir($id = '')
    {
        Auth::checkLogin();

        $mensagem = array();
        $TabProd = $this->model('TabProdModel');

        $mensagem[] = $TabProd->delete($id);

        $dados = $TabProd->GetAll();

        $this->view('tabprod/index', $dados = ['mensagem' => $mensagem, 'registros' => $dados]);
    }
}
