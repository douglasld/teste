<?php

use Spatie\ImageOptimizer\OptimizerChainFactory;
use App\Core\Controller;
use App\Auth;

class optimagem extends Controller
{

    public function optimagem()
    {
        Auth::checkLogin();
        $optimizerChain = OptimizerChainFactory::create();

        $path = "../public/fotos_produtos/";
        $diretorio = dir($path);
        $iterator = new FileSystemIterator($path);

        $bytestotalAntes = 0;
        $path = realpath($path);
        if ($path !== false) {
            foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path, FilesystemIterator::SKIP_DOTS)) as $object) {
                $bytestotalAntes += $object->getSize();
            }
        }

        foreach ($iterator as $file) {
            $ext = $file->getExtension();
            $name = $file->getFilename();
            if ($ext == 'png' || $ext == 'jpg' || $ext == 'jpeg') {
                $optimizerChain->optimize("../public/fotos_produtos/{$name}");
            }
        }

        $bytestotalDepois = 0;
        $path = realpath($path);
        if ($path !== false) {
            foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path, FilesystemIterator::SKIP_DOTS)) as $object) {
                $bytestotalDepois += $object->getSize();
            }
        }

        if ($bytestotalAntes > $bytestotalDepois) {
            $mb = $bytestotalAntes - $bytestotalDepois;
            $mb = $mb / 2048;
            $mb = $mb / 100;
            $mb = number_format($mb, 2, '.', '');

            $mensagem[] = '5 - Sucesso ' . $mb . ' Mbs Optimizados!';
        } else {
            $mensagem[] = '6 - Não Houve Optimização!';
        }

        $this->view('home/loja_geral', $dados = ['mensagem' => $mensagem]);
    }
}
