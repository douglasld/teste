<?php

use \App\Core\Controller;
use App\Auth;

class resestabs extends Controller
{

    public function index()
    {
        Auth::checkLogin();
        $empresas = $this->model('Empresa');
        $dados = $empresas->getPorMatriz($_SESSION['matriz']);

        $this->view('resestabs/index', $dados = ['registros' => $dados]);
    }

    public function ver($id = '')
    {
        Auth::checkLogin();

        $res_estab = $this->model('ResEstab');
        $dados = $res_estab->findId($id);

        $this->view('resestabs/ver', $dados = ['registros' => $dados]);
    }

    public function cadastrar()
    {
        Auth::checkLogin();

        $mensagem = array();

        if (isset($_POST['Cadastrar'])) {

            $res_estab = $this->model('ResEstab');

            $cnpj_estab = str_replace(array('.', '/', '-', '_'), '', $_POST['cnpj_estab']);
            $res_estab->res_estabcnpj_estab = substr($cnpj_estab,0, 14);   
            $res_estab->res_estabdt_movto = $_POST['dt_movto'];
            $res_estab->res_estabvl_real = str_replace(array('.', ','), array('', '.'), $_POST['vl_real']);
            
            $mensagem[] = $res_estab->save();
        }

        $this->view('resestabs/cadastrar', $dados = ['mensagem' => $mensagem]);
    }


    public function editar($id)
    {

        Auth::checkLogin();

        $mensagem = array();
        $res_estab = $this->model('ResEstab');


        if (isset($_POST['Atualizar'])) {
            
            $cnpj_estab = str_replace(array('.', '/', '-', '_'), '', $_POST['cnpj_estab']);
            $res_estab->res_estabcnpj_estab = substr($cnpj_estab,0, 14);   
            $res_estab->res_estabdt_movto = $_POST['dt_movto'];
            $res_estab->res_estabvl_real = str_replace(array('.', ','), array('', '.'), $_POST['vl_real']);

            $mensagem[] = $res_estab->update($id);
        }

        $dados = $res_estab->findId($id);
        $this->view('resestabs/editar', $dados = ['mensagem' => $mensagem, 'registros' => $dados]);
    }

    public function excluir($id = '')
    {
        Auth::checkLogin();

        $mensagem = array();
        $res_estab = $this->model('ResEstab');

        $mensagem[] = $res_estab->delete($id);

        $empresas = $this->model('Empresa');
        $dados = $empresas->getPorMatriz($_SESSION['matriz']);

        $this->view('resestabs/index', $dados = ['registros' => $dados]);
    }
}
