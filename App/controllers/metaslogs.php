<?php

use App\Core\Controller;
use App\Auth;

class metaslogs extends Controller{

    public function index($id = '')
    {
        Auth::checkLogin();

        $meta_log = $this->model('MetaLog');
        $dados = $meta_log->getAll($id);

        $this->view('metaslogs/index', $dados = ['registros' => $dados]);
    }

}
