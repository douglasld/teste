<?php

use App\Core\Controller;
use App\Auth;

class produtoslogs extends Controller{

    public function index($id = '')
    {
        Auth::checkLogin();

        $produto_log = $this->model('ProdutoLog');
        $dados = $produto_log->findId($id);

        $this->view('produtoslogs/index', $dados = ['registros' => $dados]);
    }

    public function ver($id = '')
    {
        Auth::checkLogin();

        $produto_log  = $this->model('ProdutoLog');
        $dados = $produto_log->findId($id);

        $this->view('produtoslogs/ver', $dados);
    }


}

