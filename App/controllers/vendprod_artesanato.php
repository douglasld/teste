<?php

use \App\Core\Controller;
use App\Auth;

class vendprod_artesanato extends Controller
{

    public function cadartesanato()
    {
        Auth::checkLogin();

        if (isset($_POST['enviar'])) {
            //includes
            $prods = $this->model('VendProds');
            $tabprod = $this->model('TabProdModel');

            //trata post
            $prods->cod_prod = $_POST['cod_prod'];
            $prods->nome_prod_v = $_POST['nome_produto_vendedor'];
            $prods->desc_prod = $_POST['desc_produto'];
            $prods->val_prod = str_replace(',', '.', $_POST['valor_produto']);
            $cores = $_POST['cores'];
            $prods->unidade_peso = $_POST['unidade_peso'];
            $prods->peso = number_format($_POST['peso'], 2, '.', '');
            $prods->altura = number_format($_POST['altura'], 2, '.', '');
            $prods->largura = number_format($_POST['largura'], 2, '.', '');
            $prods->comprimento = number_format($_POST['comprimento'], 2, '.', '');
            $prods->materiais = $_POST['material'];
            $prods->fragilidade = $_POST['fragilidade'];
            $prods->retirada_mao = $_POST['retirar_mao'];
            $prods->garantia_opcao = $_POST['garantia_opcao'];
            $prods->garantia_meses = $_POST['garantia_meses'];
            $prods->estado_prod = $_POST['estado_produto'];
            $prods->dt_criacao = date("Y-m-d");
            $prods->hr_criacao = date("H:i:s");

            if ($_POST['confirma_responsabilidade'] == "on") {
                $prods->confirma_responsabilidade = 1;
            } else {
                $prods->confirma_responsabilidade = 0;
            }

            $qtd_prod = $_POST['qtd_prod'];

            $prods->porc_serv = 30.00;
            $prods->status_prod = 1;
            $prods->cod_loja = $_SESSION['userId'];

            //trata o centimetro cubico
            $prods->cm3 = ($prods->altura * $prods->largura * $prods->comprimento) / 6000;
            $prods->cm3 = number_format($prods->cm3, 2, '.', '');

            //trata valor final

            $prods->val_prod = ($prods->val_prod / 100) *  $prods->porc_serv + $prods->val_prod;
            $prods->val_prod = number_format($prods->val_prod, 2, '.', '');

            //cor azul
            if (array_search("1", $cores)) {
                $cor_azul = array_search("1", $cores);
                $prods->qtd_cor_azul = $qtd_prod[$cor_azul];
                $prods->cor_azul = 1;
            } else {
                $prods->cor_azul = 0;
                $prods->qtd_cor_azul = 0;
            }

            //cor verde
            if (array_search("2", $cores)) {
                $cor_verde = array_search("2", $cores);
                $prods->qtd_cor_verde = $qtd_prod[$cor_verde];
                $prods->qtd_cor_verde = 1;
            } else {
                $prods->qtd_cor_verde = 0;
                $prods->qtd_cor_verde = 0;
            }

            //cor vermelho
            if (array_search("3", $cores)) {
                $cor_vermelho = array_search("3", $cores);
                $prods->qtd_cor_vermelho = $qtd_prod[$cor_vermelho];
                $prods->cor_vermelho = 1;
            } else {
                $prods->cor_vermelho = 0;
                $prods->qtd_cor_vermelho = 0;
            }

            //cor preto
            if (array_search("4", $cores)) {
                $cor_preto = array_search("4", $cores);
                $prods->qtd_cor_preto = $qtd_prod[$cor_preto];
                $prods->cor_preto = 1;
            } else {
                $prods->cor_preto = 0;
                $prods->qtd_cor_preto = 0;
            }

            //cor branco
            if (array_search("5", $cores)) {
                $cor_branco = array_search("5", $cores);
                $prods->qtd_cor_branco = $qtd_prod[$cor_branco];
                $prods->cor_branco = 1;
            } else {
                $prods->cor_branco = 0;
                $prods->qtd_cor_branco = 0;
            }


            //cor marrom
            if (array_search("6", $cores)) {
                $cor_marrom = array_search("6", $cores);
                $prods->qtd_cor_marrom = $qtd_prod[$cor_marrom];
                $prods->cor_marrom = 1;
            } else {
                $prods->cor_marrom = 0;
                $prods->qtd_cor_marrom = 0;
            }

            //cor cinza
            if (array_search("7", $cores)) {
                $cor_cinza = array_search("7", $cores);
                $prods->qtd_cor_cinza = $qtd_prod[$cor_cinza];
                $prods->cor_cinza = 1;
            } else {
                $prods->cor_cinza = 0;
                $prods->qtd_cor_cinza = 0;
            }

            //cor rosa
            if (array_search("8", $cores)) {
                $cor_rosa = array_search("8", $cores);
                $prods->qtd_cor_rosa = $qtd_prod[$cor_rosa];
                $prods->cor_rosa = 1;
            } else {
                $prods->cor_rosa = 0;
                $prods->qtd_cor_rosa = 0;
            }

            //cor amarelo
            if (array_search("9", $cores)) {
                $cor_amarelo = array_search("9", $cores);
                $prods->qtd_cor_amarelo = $qtd_prod[$cor_amarelo];
                $prods->cor_amarelo = 1;
            } else {
                $prods->cor_amarelo = 0;
                $prods->qtd_cor_amarelo = 0;
            }

            //verifica erros
            $mensagem[] = '';

            if ($prods->val_prod < 1) {
                $mensagem[] = "3 - O valor do prod informado não é válido!";
            }

            if ($prods->altura < 1) {
                $mensagem[] = "3 - A altura informada não é válida!";
            }
            if ($prods->largura < 1) {
                $mensagem[] = "3 - A largura informada não é válida!";
            }
            if ($prods->comprimento < 1) {
                $mensagem[] = "3 - O comprimento informado não é válido!";
            }
            if ($prods->garantia_opcao == 1) {
                if ($prods->garantia_meses < 1) {
                    $mensagem[] = "3 - A garantia informada não é válida!";
                }
            }
            if ($prods->garantia_opcao == 1) {
                if ($prods->garantia_meses > 48) {
                    $mensagem[] = "3 - Garantia meses ultrapassada";
                }
            }
            if ($prods->garantia_opcao == 2) {
                $prods->garantia_meses = 0;
            }

            //busca cod_pai/filho
            if (empty($valores = $tabprod->getId($_POST['cod_prod']))) {
                $mensagem[] = "Não foi possivel verificar cod_cat_p/f";
            }

            $prods->cod_cat_p = $valores[0]['cod_cat_p'];
            $prods->cod_cat_f = $valores[0]['cod_cat_f'];

            //diretório de envio das imagens
            $target_dir = "fotos_produtos/";

            //recebe imagem 1
            if (!basename($_FILES["ft1"]["name"])) {
                $mensagem[] = "É necessário a inserção de no mínimo 3 fotos!";
                $novoNome1 = 0;
            } else {
                //recebe a foto
                $target_file1 = $target_dir . basename($_FILES["ft1"]["name"]);

                //recebe extenção da foto 1
                $imageFileType1 = strtolower(pathinfo($target_file1, PATHINFO_EXTENSION));

                //verifica a extenção da foto 1
                if ($imageFileType1 != "jpg" && $imageFileType1 != "png" && $imageFileType1 != "jpeg") {
                    $mensagem[] = "Apenas imagens JPG, JPEG, PNG.(1)";
                }

                // Check file size 1
                if ($_FILES["ft1"]["size"] > 52428800) {
                    $mensagem[] = "Desculpe, foto demasiada grande.(1)";
                }

                //renomeia foto 1
                $extensao1 = strtolower(end(explode(".", $target_file1)));
                $novoNome1 = uniqid(time()) . $_SESSION['userId'] . '.' . $extensao1;
                $target_file1 = $target_dir . $novoNome1;
            }

            //recebe imagem 2
            if (!basename($_FILES["ft2"]["name"])) {
                $mensagem[] = "É necessário a inserção de no mínimo 3 fotos!";
                $novoNome2 = 0;
            } else {
                //recebe a foto 2
                $target_file2 = $target_dir . basename($_FILES["ft2"]["name"]);

                //recebe extenção da foto 2
                $imageFileType2 = strtolower(pathinfo($target_file2, PATHINFO_EXTENSION));

                //verifica a extenção da foto 2
                if ($imageFileType2 != "jpg" && $imageFileType2 != "png" && $imageFileType2 != "jpeg") {
                    $mensagem[] = "Apenas imagens JPG, JPEG, PNG.(2)";
                }

                // Check file size 2
                if ($_FILES["ft2"]["size"] > 52428800) {
                    $mensagem[] = "Desculpe, foto demasiada grande.(2)";
                }

                //renomeia foto 2
                $extensao2 = strtolower(end(explode(".", $target_file2)));
                $novoNome2 = uniqid(time()) . $_SESSION['userId'] . '.' . $extensao2;
                $target_file2 = $target_dir . $novoNome2;
            }

            //recebe imagem 3
            if (!basename($_FILES["ft3"]["name"])) {
                $mensagem[] = "É necessário a inserção de no mínimo 3 fotos!";
                $novoNome3 = 0;
            } else {
                //recebe foto 3
                $target_file3 = $target_dir . basename($_FILES["ft3"]["name"]);

                //recebe extenção da foto 3
                $imageFileType3 = strtolower(pathinfo($target_file3, PATHINFO_EXTENSION));

                //verifica a extenção da foto 3
                if ($imageFileType3 != "jpg" && $imageFileType3 != "png" && $imageFileType3 != "jpeg") {
                    $mensagem[] = "Apenas imagens JPG, JPEG, PNG.(3)";
                }
                // Check file size 3
                if ($_FILES["ft3"]["size"] > 52428800) {
                    $mensagem[] = "Desculpe, foto demasiada grande.(3)";
                }

                //renomeia foto 3
                $extensao3 = strtolower(end(explode(".", $target_file3)));
                $novoNome3 = uniqid(time()) . $_SESSION['userId'] . '.' . $extensao3;
                $target_file3 = $target_dir . $novoNome3;
            }

            //recebe imagem 4
            if (!basename($_FILES["ft4"]["name"])) {
                $novoNome4 = 0;
            } else {
                //recebe foto 4
                $target_file4 = $target_dir . basename($_FILES["ft4"]["name"]);

                //recebe extenção da foto 4
                $imageFileType4 = strtolower(pathinfo($target_file4, PATHINFO_EXTENSION));

                //verifica a extenção da foto 4
                if ($imageFileType4 != "jpg" && $imageFileType4 != "png" && $imageFileType4 != "jpeg") {
                    $mensagem[] = "Apenas imagens JPG, JPEG, PNG.(4)";
                }

                // Check file size 4
                if ($_FILES["ft4"]["size"] > 52428800) {
                    $mensagem[] = "Desculpe, foto demasiada grande.(4)";
                }

                //renomeia foto 4
                $extensao4 = strtolower(end(explode(".", $target_file4)));
                $novoNome4 = uniqid(time()) . $_SESSION['userId'] . '.' . $extensao4;
                $target_file4 = $target_dir . $novoNome4;
            }

            //recebe imagem 5
            if (!basename($_FILES["ft5"]["name"])) {
                $novoNome5 = 0;
            } else {
                //recebe foto 5
                $target_file5 = $target_dir . basename($_FILES["ft5"]["name"]);

                //recebe extenção da foto 5
                $imageFileType5 = strtolower(pathinfo($target_file5, PATHINFO_EXTENSION));

                //verifica a extenção da foto 5
                if ($imageFileType5 != "jpg" && $imageFileType5 != "png" && $imageFileType5 != "jpeg") {
                    $mensagem[] = "Apenas imagens JPG, JPEG, PNG.(5)";
                }

                // Check file size 5
                if ($_FILES["ft5"]["size"] > 52428800) {
                    $mensagem[] = "Desculpe, foto demasiada grande.(5)";
                }

                //renomeia foto 5
                $extensao5 = strtolower(end(explode(".", $target_file5)));
                $novoNome5 = uniqid(time()) . $_SESSION['userId'] . '.' . $extensao5;
                $target_file5 = $target_dir . $novoNome5;
            }

            //verifica se existe algum erro.
            if ($mensagem[0] == '') {
                if ($novoNome1 != 0  && $novoNome2 != 0 && $novoNome3 != 0) {
                    //foto 1
                    if (!move_uploaded_file($_FILES["ft1"]["tmp_name"], $target_file1)) {
                        $mensagem[] = "Erro ao mover foto.(1)";
                    }
                    //foto 2
                    if (!move_uploaded_file($_FILES["ft2"]["tmp_name"], $target_file2)) {
                        $mensagem[] = "Erro ao mover foto.(2)";
                    }
                    //foto 3
                    if ($novoNome3 != 0) {
                        if (!move_uploaded_file($_FILES["ft3"]["tmp_name"], $target_file3)) {
                            $mensagem[] = "Erro ao mover foto.(3)";
                        }
                    }
                    if ($novoNome4 != 0) {
                        //foto 4
                        if (!move_uploaded_file($_FILES["ft4"]["tmp_name"], $target_file4)) {
                            $mensagem[] = "Erro ao mover foto.(4)";
                        }
                    }
                    if ($novoNome5 != 0) {
                        //foto 5
                        if (!move_uploaded_file($_FILES["ft5"]["tmp_name"], $target_file5)) {
                            $mensagem[] = "Erro ao mover foto.(5)";
                        }
                    }
                }

                $prods->cod_foto_1 = $novoNome1;
                $prods->cod_foto_2 = $novoNome2;
                $prods->cod_foto_3 = $novoNome3;
                $prods->cod_foto_4 = $novoNome4;
                $prods->cod_foto_5 = $novoNome5;

                $mensagem[] = $prods->save();
            }

            $this->view('vendprod/artesanato', $dados = ['mensagem' => $mensagem]);
        }
    }


    public function atualizaartezanato($id = '')
    {
        Auth::checkLogin();

        if (isset($_POST['enviar'])) {

            //includes
            $prods = $this->model('VendProds');
            $tabprod = $this->model('TabProdModel');

            $prods->nome_prod_v = $_POST['nome_produto_vendedor'];
            $prods->desc_prod = $_POST['desc_produto'];
            $prods->val_prod = str_replace(',', '.', $_POST['valor_produto']);
            $prods->unidade_peso = $_POST['unidade_peso'];
            $prods->peso = number_format($_POST['peso'], 2, '.', '');
            $prods->altura = number_format($_POST['altura'], 2, '.', '');
            $prods->largura = number_format($_POST['largura'], 2, '.', '');
            $prods->comprimento = number_format($_POST['comprimento'], 2, '.', '');
            $prods->fragilidade = $_POST['fragilidade'];
            $prods->retirada_mao = $_POST['retirar_mao'];
            $prods->garantia_opcao = $_POST['garantia_opcao'];
            $prods->garantia_meses = $_POST['garantia_meses'];
            $prods->estado_prod = $_POST['estado_produto'];


            $prods->porc_serv = 30.00;
            $prods->status_prod = 1;

            //trata o centimetro cubico
            $prods->cm3 = ($prods->altura * $prods->largura * $prods->comprimento) / 6000;
            $prods->cm3 = number_format($prods->cm3, 2, '.', '');

            //trata valor final

            $prods->val_prod = ($prods->val_prod / 100) *  $prods->porc_serv + $prods->val_prod;
            $prods->val_prod = number_format($prods->val_prod, 2, '.', '');

            //cor azul
            if ($_POST['qtd_cor_azul']) {
                $prods->qtd_cor_azul = $_POST['qtd_cor_azul'];
                $prods->cor_azul = 1;
            } else {
                $prods->cor_azul = 0;
                $prods->qtd_cor_azul = 0;
            }

            //cor verde
            if ($_POST['qtd_cor_verde']) {
                $prods->qtd_cor_verde = $_POST['qtd_cor_verde'];
                $prods->cor_verde = 1;
            } else {
                $prods->cor_verde = 0;
                $prods->qtd_cor_verde = 0;
            }

            //cor vermelho
            if ($_POST['qtd_cor_vermelho']) {
                $prods->qtd_cor_vermelho = $_POST['qtd_cor_vermelho'];
                $prods->cor_vermelho = 1;
            } else {
                $prods->cor_vermelho = 0;
                $prods->qtd_cor_vermelho = 0;
            }

            //cor preto
            if ($_POST['qtd_cor_preto']) {
                $prods->qtd_cor_preto = $_POST['qtd_cor_preto'];
                $prods->cor_preto = 1;
            } else {
                $prods->cor_preto = 0;
                $prods->qtd_cor_preto = 0;
            }

            //cor branco
            if ($_POST['qtd_cor_branco']) {
                $prods->qtd_cor_branco = $_POST['qtd_cor_branco'];
                $prods->cor_branco = 1;
            } else {
                $prods->cor_branco = 0;
                $prods->qtd_cor_branco = 0;
            }

            //cor marrom
            if ($_POST['qtd_cor_marrom']) {
                $prods->qtd_cor_marrom = $_POST['qtd_cor_marrom'];
                $prods->cor_marrom = 1;
            } else {
                $prods->cor_marrom = 0;
                $prods->qtd_cor_marrom = 0;
            }

            //cor cinza
            if ($_POST['qtd_cor_cinza']) {
                $prods->qtd_cor_cinza = $_POST['qtd_cor_cinza'];
                $prods->cor_cinza = 1;
            } else {
                $prods->cor_cinza = 0;
                $prods->qtd_cor_cinza = 0;
            }

            //cor rosa
            if ($_POST['qtd_cor_rosa']) {
                $prods->qtd_cor_rosa = $_POST['qtd_cor_rosa'];
                $prods->cor_rosa = 1;
            } else {
                $prods->cor_rosa = 0;
                $prods->qtd_cor_rosa = 0;
            }

            //cor amarelo
            if ($_POST['qtd_cor_amarelo']) {
                $prods->qtd_cor_amarelo = $_POST['qtd_cor_rosa'];
                $prods->cor_amarelo = 1;
            } else {
                $prods->cor_amarelo = 0;
                $prods->qtd_cor_amarelo = 0;
            }

            if ($prods->val_prod < 1) {
                $mensagem[] = "3 - O valor do prod informado não é válido!";
            }

            if ($prods->altura < 1) {
                $mensagem[] = "3 - A altura informada não é válida!";
            }
            if ($prods->largura < 1) {
                $mensagem[] = "3 - A largura informada não é válida!";
            }
            if ($prods->comprimento < 1) {
                $mensagem[] = "3 - O comprimento informado não é válido!";
            }
            if ($prods->garantia_opcao == 1) {
                if ($prods->garantia_meses < 1) {
                    $mensagem[] = "3 - A garantia informada não é válida!";
                }
            }
            if ($prods->garantia_opcao == 1) {
                if ($prods->garantia_meses > 48) {
                    $mensagem[] = "3 - Garantia meses ultrapassada";
                }
            }
            if ($prods->garantia_opcao == 2) {
                $prods->garantia_meses = 0;
            }

            //busca cod_pai/filho
            if (empty($valores = $tabprod->getId($_POST['cod_prod']))) {
                $mensagem[] = "3 - Não foi possivel verificar cod_cat_p/f";
            }

            //verifica erros
            if ($mensagem[0]) {
                $this->route('meusprod/editar', $dados = ['cod' => $id, 'mensagem' => $mensagem]);
                die;
            }

            $prods->cod_cat_p = $valores[0]['cod_cat_p'];
            $prods->cod_cat_f = $valores[0]['cod_cat_f'];

            //diretório de envio das imagens
            $target_dir = "fotos_produtos/";

            //recebe imagem 1
            if (!basename($_FILES["ft1"]["name"])) {
                $novoNome1 = 0;
            } else {
                //recebe a foto
                $target_file1 = $target_dir . basename($_FILES["ft1"]["name"]);

                //recebe extenção da foto 1
                $imageFileType1 = strtolower(pathinfo($target_file1, PATHINFO_EXTENSION));

                //verifica a extenção da foto 1
                if ($imageFileType1 != "jpg" && $imageFileType1 != "png" && $imageFileType1 != "jpeg") {
                    $mensagem[] = "3 - Apenas imagens JPG, JPEG, PNG.(1)";
                }

                // Check file size 1
                if ($_FILES["ft1"]["size"] > 52428800) {
                    $mensagem[] = "3 - Desculpe, foto demasiada grande.(1)";
                }

                //renomeia foto 1
                $extensao1 = strtolower(end(explode(".", $target_file1)));
                $novoNome1 = uniqid(time()) . $_SESSION['userId'] . '.' . $extensao1;
                $target_file1 = $target_dir . $novoNome1;
            }

            //recebe imagem 2
            if (!basename($_FILES["ft2"]["name"])) {
                $novoNome2 = 0;
            } else {
                //recebe a foto 2
                $target_file2 = $target_dir . basename($_FILES["ft2"]["name"]);

                //recebe extenção da foto 2
                $imageFileType2 = strtolower(pathinfo($target_file2, PATHINFO_EXTENSION));

                //verifica a extenção da foto 2
                if ($imageFileType2 != "jpg" && $imageFileType2 != "png" && $imageFileType2 != "jpeg") {
                    $mensagem[] = "3 - Apenas imagens JPG, JPEG, PNG.(2)";
                }

                // Check file size 2
                if ($_FILES["ft2"]["size"] > 52428800) {
                    $mensagem[] = "3 - Desculpe, foto demasiada grande.(2)";
                }

                //renomeia foto 2
                $extensao2 = strtolower(end(explode(".", $target_file2)));
                $novoNome2 = uniqid(time()) . $_SESSION['userId'] . '.' . $extensao2;
                $target_file2 = $target_dir . $novoNome2;
            }

            //recebe imagem 3
            if (!basename($_FILES["ft3"]["name"])) {
                $novoNome3 = 0;
            } else {
                //recebe foto 3
                $target_file3 = $target_dir . basename($_FILES["ft3"]["name"]);

                //recebe extenção da foto 3
                $imageFileType3 = strtolower(pathinfo($target_file3, PATHINFO_EXTENSION));

                //verifica a extenção da foto 3
                if ($imageFileType3 != "jpg" && $imageFileType3 != "png" && $imageFileType3 != "jpeg") {
                    $mensagem[] = "3 - Apenas imagens JPG, JPEG, PNG.(3)";
                }
                // Check file size 3
                if ($_FILES["ft3"]["size"] > 52428800) {
                    $mensagem[] = "3 - Desculpe, foto demasiada grande.(3)";
                }

                //renomeia foto 3
                $extensao3 = strtolower(end(explode(".", $target_file3)));
                $novoNome3 = uniqid(time()) . $_SESSION['userId'] . '.' . $extensao3;
                $target_file3 = $target_dir . $novoNome3;
            }

            //recebe imagem 4
            if (!basename($_FILES["ft4"]["name"])) {
                $novoNome4 = 0;
            } else {
                //recebe foto 4
                $target_file4 = $target_dir . basename($_FILES["ft4"]["name"]);

                //recebe extenção da foto 4
                $imageFileType4 = strtolower(pathinfo($target_file4, PATHINFO_EXTENSION));

                //verifica a extenção da foto 4
                if ($imageFileType4 != "jpg" && $imageFileType4 != "png" && $imageFileType4 != "jpeg") {
                    $mensagem[] = "3 - Apenas imagens JPG, JPEG, PNG.(4)";
                }

                // Check file size 4
                if ($_FILES["ft4"]["size"] > 52428800) {
                    $mensagem[] = "3 - Desculpe, foto demasiada grande.(4)";
                }

                //renomeia foto 4
                $extensao4 = strtolower(end(explode(".", $target_file4)));
                $novoNome4 = uniqid(time()) . $_SESSION['userId'] . '.' . $extensao4;
                $target_file4 = $target_dir . $novoNome4;
            }

            //recebe imagem 5
            if (!basename($_FILES["ft5"]["name"])) {
                $novoNome5 = 0;
            } else {
                //recebe foto 5
                $target_file5 = $target_dir . basename($_FILES["ft5"]["name"]);

                //recebe extenção da foto 5
                $imageFileType5 = strtolower(pathinfo($target_file5, PATHINFO_EXTENSION));

                //verifica a extenção da foto 5
                if ($imageFileType5 != "jpg" && $imageFileType5 != "png" && $imageFileType5 != "jpeg") {
                    $mensagem[] = "3 - Apenas imagens JPG, JPEG, PNG.(5)";
                }

                // Check file size 5
                if ($_FILES["ft5"]["size"] > 52428800) {
                    $mensagem[] = "3 - Desculpe, foto demasiada grande.(5)";
                }

                //renomeia foto 5
                $extensao5 = strtolower(end(explode(".", $target_file5)));
                $novoNome5 = uniqid(time()) . $_SESSION['userId'] . '.' . $extensao5;
                $target_file5 = $target_dir . $novoNome5;
            }

            //verifica erros
            if ($mensagem[0]) {
                $this->route('meusprod/editar', $dados = ['cod' => $id, 'mensagem' => $mensagem]);
                die;
            }

            //verifica se existe algum erro.
            if ($mensagem[0] == '') {

                //foto 1
                if ($novoNome1 != 0) {
                    if (!move_uploaded_file($_FILES["ft1"]["tmp_name"], $target_file1)) {
                        $mensagem[] = "3 - Erro ao mover foto 1";
                    }
                }
                //foto 2
                if ($novoNome2 != 0) {
                    if (!move_uploaded_file($_FILES["ft2"]["tmp_name"], $target_file2)) {
                        $mensagem[] = "3 - Erro ao mover foto 2";
                    }
                }
                //foto 3
                if ($novoNome3 != 0) {
                    if (!move_uploaded_file($_FILES["ft3"]["tmp_name"], $target_file3)) {
                        $mensagem[] = "3 - Erro ao mover foto 3";
                    }
                }
                if ($novoNome4 != 0) {
                    //foto 4
                    if (!move_uploaded_file($_FILES["ft4"]["tmp_name"], $target_file4)) {
                        $mensagem[] = "3 - Erro ao mover foto 4";
                    }
                }
                if ($novoNome5 != 0) {
                    //foto 5
                    if (!move_uploaded_file($_FILES["ft5"]["tmp_name"], $target_file5)) {
                        $mensagem[] = "3 - Erro ao mover foto 5";
                    }
                }


                //verifica erros
                if ($mensagem[0]) {
                    $this->route('meusprod/editar', $dados = ['cod' => $id, 'mensagem' => $mensagem]);
                    die;
                }

                //busca os nomes das fotos para efetuar a alteração ou não
                $fotos = $prods->VerificaFotos($id);

                if ($novoNome1 == 0) {
                    $novoNome1 = $fotos[0]['cod_foto_1'];
                } else {
                    !unlink('.' . $_SESSION['prod'] . '/fotos_produtos/' . $fotos[0]['cod_foto_1'] . '');
                }
                if ($novoNome2 == 0) {
                    $novoNome2 = $fotos[0]['cod_foto_2'];
                } else {
                    !unlink('.' . $_SESSION['prod'] . '/fotos_produtos/' . $fotos[0]['cod_foto_2'] . '');
                }
                if ($novoNome3 == 0) {
                    $novoNome3 = $fotos[0]['cod_foto_3'];
                } else {
                    !unlink('.' . $_SESSION['prod'] . '/fotos_produtos/' . $fotos[0]['cod_foto_3'] . '');
                }
                if ($novoNome4 == 0) {
                    $novoNome4 = $fotos[0]['cod_foto_4'];
                } else {
                    !unlink('.' . $_SESSION['prod'] . '/fotos_produtos/' . $fotos[0]['cod_foto_4'] . '');
                }
                if ($novoNome5 == 0) {
                    $novoNome5 = $fotos[0]['cod_foto_5'];
                } else {
                    !unlink('.' . $_SESSION['prod'] . '/fotos_produtos/' . $fotos[0]['cod_foto_5'] . '');
                }

                //verifica erros
                if ($mensagem[0]) {
                    $this->route('meusprod/editar', $dados = ['cod' => $id, 'mensagem' => $mensagem]);
                    die;
                }

                $prods->cod_foto_1 = $novoNome1;
                $prods->cod_foto_2 = $novoNome2;
                $prods->cod_foto_3 = $novoNome3;
                $prods->cod_foto_4 = $novoNome4;
                $prods->cod_foto_5 = $novoNome5;

                $mensagem[] = $prods->update($id);
            } else {
                $this->route('meusprod/editar', $dados = ['cod' => $id, 'mensagem' => $mensagem]);
                die;
            }

            $this->route('meusprod/editar', $dados = ['cod' => $id, 'mensagem' => $mensagem]);
        }
    }
}
