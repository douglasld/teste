<?php

use App\Core\Controller;
use App\Auth;

class userperfillogs extends Controller{

    public function index($id = '')
    {
        Auth::checkLogin();

        $user_perfil_log = $this->model('UserPerfilLog');
        $dados = $user_perfil_log->findId($id);

        $this->view('userperfi/index', $dados = ['logs' => $dados]);
    }

    public function ver($id = '')
    {
        Auth::checkLogin();

        $user_perfil_log = $this->model('UserPerfilLog');
        $dados = $user_perfil_log->findId($id);

        $this->view('userperfillogs/ver', $dados);
    }


}

