<?php

use \App\Core\Controller;
use App\Auth;

class feriados extends Controller{

    public function index()
    {
        Auth::checkLogin();

        $feriados = $this->model('Feriado');
        $dados = $feriados->getAll();

        $this->view('feriados/index', $dados = ['registros' => $dados]);
    }

    public function ver($id = '')
    {
        Auth::checkLogin();

        $feriados = $this->model('Feriado');
        $dados = $feriados->findId($id);

        $this->view('feriados/ver', $dados = ['registros' => $dados]);
    }

    public function cadastrar()
    {
        Auth::checkLogin();

        $mensagem = array();

        if(isset($_POST['Cadastrar'])){

            $feriados = $this->model('Feriado');

            $cnpj_estab = str_replace(array('.', '/', '-', '_'), '', $_POST['cnpj_estab']);
            $feriados->feriadocnpj_estab = substr($cnpj_estab,0, 14);   
            $feriados->feriadodt_feriado = $_POST['dt_feriado'];
            $feriados->feriadodescricao = $_POST['descricao'];
            $feriados->feriadoind_ativo = $_POST['ind_ativo'];
            if($feriados->feriadoind_ativo == "on"){
                $feriados->feriadoind_ativo = "0";
            }else{
                $feriados->feriadoind_ativo = "1";
            }
            $feriados->feriadoind_origem = "0";
            $feriados->feriadodt_criacao = date("Y-m-d");
            $feriados->feriadohr_criacao = date("h:i:s");
            $feriados->feriadouser_criacao = $_SESSION['userId'];
            $feriados->feriadodt_altera = date("Y-m-d");
            $feriados->feriadohr_altera = date("h:i:s");
            $feriados->feriadouser_altera = $_SESSION['userId'];

            $mensagem[] = $feriados->save();

            $feriado_log = $this->model('FeriadoLog');

            $cnpj_estab = str_replace(array('.', '/', '-', '_'), '', $_POST['cnpj_estab']);
            $feriado_log->feriado_logcnpj_estab = substr($cnpj_estab,0, 14);  
            $feriado_log->feriado_logdt_feriado = $_POST['dt_feriado'];
            $feriado_log->feriado_loguser_emis = $_SESSION['userId'];
            $feriado_log->feriado_logdt_emis = date("Y-m-d");
            $feriado_log->feriado_loghr_emis = date("h:i:s");
            $feriado_log->feriado_logtitulo = "Inclusão";

            $mensagem[] = $feriado_log->save();

        }

        $this->view('feriados/cadastrar', $dados = [ 'mensagem' => $mensagem]);

    }

    public function editar($id){

        Auth::checkLogin();

        $mensagem = array();
        $feriados = $this->model('Feriado');

        if(isset($_POST['Atualizar'])){

            $cnpj_estab = str_replace(array('.', '/', '-', '_'), '', $_POST['cnpj_estab']);
            $feriados->feriadocnpj_estab = substr($cnpj_estab,0, 14);   
            $feriados->feriadodt_feriado = $_POST['dt_feriado'];
            $feriados->feriadodescricao = $_POST['descricao'];
            $feriados->feriadoind_ativo = $_POST['ind_ativo'];
            if($feriados->feriadoind_ativo == "on"){
                $feriados->feriadoind_ativo = "0";
            }else{
                $feriados->feriadoind_ativo = "1";
            }
            $feriados->feriadoind_origem = "0";
            $feriados->feriadodt_altera = date("Y-m-d");
            $feriados->feriadohr_altera = date("h:i:s");
            $feriados->feriadouser_altera = $_SESSION['userId'];

            $mensagem[] = $feriados->update($id);

            $feriado_log = $this->model('FeriadoLog');


            $cnpj_estab = str_replace(array('.', '/', '-', '_'), '', $_POST['cnpj_estab']);
            $feriado_log->feriado_logcnpj_estab = substr($cnpj_estab,0, 14);   
            $feriado_log->feriado_logdt_feriado = $_POST['dt_feriado'];
            $feriado_log->feriado_loguser_emis = $_SESSION['userId'];
            $feriado_log->feriado_logdt_emis = date("Y-m-d");
            $feriado_log->feriado_loghr_emis = date("h:i:s");
            $feriado_log->feriado_logtitulo = "Alteração";

            $mensagem[] = $feriado_log->save();

        }

        $dados = $feriados->findId($id);

        $this->view('feriados/editar', $dados = [ 'mensagem' => $mensagem, 'registros' => $dados]);
    }


    public function excluir($id = '')
    {
        Auth::checkLogin();

        $mensagem = array();
        $feriados = $this->model('Feriado');

        $mensagem[] = $feriados->delete($id);

        $dados = $feriados->getAll();

        $this->view('feriados/index', $dados = ['registros' => $dados, 'mensagem' => $mensagem]);
    }

}




