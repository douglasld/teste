<?php

use App\Core\Controller;
use App\Auth;

class unidmedlogs extends Controller{

    public function index($id = '')
    {
        Auth::checkLogin();

        $unid_med_log = $this->model('UnidMedLog');
        $dados = $unid_med_log->findId($id);

        $this->view('unidmedlogs/index', $dados = ['registros' => $dados]);
    }

    public function ver($id = '')
    {
        Auth::checkLogin();

        $unid_med_log = $this->model('UnidMedLog');
        $dados = $unid_med_log->findId($id);

        $this->view('unidmedlogs/ver', $dados);
    }


}


