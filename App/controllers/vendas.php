<?php

use \App\Core\Controller;
use App\Auth;

class vendas extends Controller{

    public function index()
    {
        Auth::checkLogin();

        $empresas = $this->model('Empresa');
        $dados = $empresas->getPorMatriz($_SESSION['matriz']);

        $this->view('vendas/index', $dados = ['registros' => $dados]);
    }

    public function ver($id = '')
    {
        Auth::checkLogin();

        $vendas = $this->model('Venda');
        $dados = $vendas->findId($id);


        $this->view('vendas/ver', $dados = ['registros' => $dados]);
    }

    public function cadastrar()
    {
        Auth::checkLogin();

        $mensagem = array();

        if(isset($_POST['Cadastrar'])){

            $vendas = $this->model('Venda');
            $produtos = $this->model('Produto');
            $produtosDados = $produtos->findId($_POST['vendascod_produto']);

           
            $vendas->vendascnpj_estab = str_replace(array('.', '/', '-','_'), '', $_POST['cnpj_estab']);
            $vendas->vendascod_meta = $_POST['vendascod_meta'];
            $vendas->vendasdescricao = $_POST['vendasdescricao'];
            $vendas->vendasserie_docto = $_POST['vendasserie_docto'];
            $vendas->vendasnr_docto = $_POST['vendasnr_docto'];
            $vendas->vendasdt_docto = $_POST['vendasdt_docto'];
            $vendas->vendascod_produto = $_POST['vendascod_produto'];
            $vendas->vendasqt_vendida = $_POST['vendasqt_vendida'];
            $vendas->vendascod_unid_med = $_POST['vendascod_unid_med'];
            $vendas->vendasvl_unit = $_POST['vendasvl_unit'];
            $vendas->vendasvl_total = $_POST['vendasvl_total'];
            $vendas->vendasnome_vendedor = $_POST['vendasnome_vendedor'];
            $vendas->vendasind_ativo = $_POST['vendasind_ativo'];
            if( $vendas->vendasind_ativo == "on"){
                $vendas->vendasind_ativo = "0";
            }else{
                $vendas->vendasind_ativo = "1";
            }
            $vendas->vendasind_origem = "0";
            $vendas->vendasuser_criacao = $_SESSION['userId'];
            $vendas->vendasdt_criacao = date("Y-m-d");
            $vendas->vendasuser_altera = $_SESSION['userId'];
            $vendas->vendasdt_altera = date("Y-m-d");
            $vendas->vendascod_unid_neg = $_POST['vendascod_unid_neg'];
            $vendas->vendashr_altera = date("h:i:s");
            $vendas->vendashr_criacao  = date("h:i:s");
            $vendas->vendasgerente = $_POST['vendasgerente'];
            $vendas->vendasgrupoprod = $produtosDados['produtocod_grup_prod'];
            if($vendas->vendasgrupoprod == ""){
                $vendas->vendasgrupoprod = "0";
            }
            $mensagem[] = $vendas->save();

            
            $vendas_log = $this->model('VendaLog');

            $vendas_log->vendas_logcnpj_estab = str_replace(array('.', '/', '-','_'), '', $_POST['cnpj_estab']);
            $vendas_log->vendas_logserie_docto = $_POST['vendasserie_docto'];
            $vendas_log->vendas_logdt_emis = date("Y-m-d");
            $vendas_log->vendas_loghr_emis = date("h:i:s");
            $vendas_log->vendas_loguser_emis = $_SESSION['userId'];
            $vendas_log->vendas_logtitulo = "Inclusão";
            $vendas_log->vendas_lognr_docto = $_POST['vendasnr_docto'];
            $vendas_log->vendas_logdt_docto = $_POST['vendasdt_docto'];
            
            $mensagem[] = $vendas_log->save();

        }

        $this->view('vendas/cadastrar', $dados = [ 'mensagem' => $mensagem]);

    }

    public function editar($id){

        Auth::checkLogin();

        $mensagem = array();
        $vendasup = $this->model('Venda');
        $dados = $vendasup->findId($id);

        if(isset($_POST['Atualizar'])){

            $vendasup->vendascnpj_estab = str_replace(array('.', '/', '-','_'), '', $_POST['cnpj_estab']);
            $vendasup->vendascod_meta = $_POST['vendascod_meta'];
            $vendasup->vendasdescricao = $_POST['vendasdescricao'];
            $vendasup->vendasserie_docto = $_POST['vendasserie_docto'];
            $vendasup->vendasnr_docto = $_POST['vendasnr_docto'];
            $vendasup->vendasdt_docto = $_POST['vendasdt_docto'];
            $vendasup->vendascod_produto = $_POST['vendascod_produto'];
            $vendasup->vendasqt_vendida = $_POST['vendasqt_vendida'];
            $vendasup->vendascod_unid_med = $_POST['vendascod_unid_med'];
            $vendasup->vendasvl_unit = $_POST['vendasvl_unit'];
            $vendasup->vendasvl_total = $_POST['vendasvl_total'];
            $vendasup->vendasnome_vendedor = $_POST['vendasnome_vendedor'];
            $vendasup->vendasind_ativo = $_POST['vendasind_ativo'];
            if($vendasup->vendasind_ativo == "on"){
                $vendasup->vendasind_ativo = "0";
            }else{
                $vendasup->vendasind_ativo = "1";
            }
            $vendasup->vendasind_origem = "0";
            $vendasup->vendasuser_criacao = $_SESSION['userId'];
            $vendasup->vendasdt_criacao = date("Y-m-d");
            $vendasup->vendasuser_altera = $_SESSION['userId'];
            $vendasup->vendasdt_altera = date("Y-m-d");
            $vendasup->vendascod_unid_neg = $_POST['vendascod_unid_neg'];
            $vendasup->vendashr_altera = date("h:i:s");
            $vendasup->vendasgerente = $_POST['vendasgerente'];

    
            $mensagem[] = $vendasup->update($id);

            

            $vendas_log = $this->model('VendaLog');

            $vendas_log->vendas_logcnpj_estab = str_replace(array('.', '/', '-','_'), '', $_POST['cnpj_estab']);
            $vendas_log->vendas_logserie_docto = $_POST['vendasserie_docto'];
            $vendas_log->vendas_logdt_emis = date("Y-m-d");
            $vendas_log->vendas_loghr_emis = date("h:i:s");
            $vendas_log->vendas_loguser_emis = $_SESSION['userId'];
            $vendas_log->vendas_logtitulo = "Alteração";
            $vendas_log->vendas_lognr_docto = $_POST['nr_docto'];
            $vendas_log->vendas_logdt_docto = $_POST['dt_docto'];

            $mensagem[] = $vendas_log->save();

        }

        $this->view('vendas/editar', $dados = [ 'mensagem' => $mensagem, 'registros' => $dados]);
    }


    public function excluir($id = '')
    {
        Auth::checkLogin();

        $mensagem = array();
        $vendas = $this->model('Venda');

        $mensagem[] = $vendas->delete($id);

        $dados = $vendas->getAll();

        $this->view('vendas/index', $dados = ['registros' => $dados, 'mensagem' => $mensagem]);
    }

}






