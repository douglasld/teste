<?php

use \App\Core\Controller;
use App\Auth;

class resmatrizs extends Controller{

    public function index()
    {
        Auth::checkLogin();

        $res_matriz = $this->model('ResMatriz');
        $dados = $res_matriz->getAll($_SESSION['matriz']);

        $this->view('resmatrizs/index', $dados = ['registros' => $dados]);
    }

    public function ver($id = '')
    {
        Auth::checkLogin();

        $res_matriz = $this->model('ResMatriz');
        $dados = $res_matriz->findId($id);

        $this->view('resmatrizs/ver', $dados);
    }

    public function cadastrar()
    {
        Auth::checkLogin();

        $mensagem = array();

        if(isset($_POST['Cadastrar'])){

            $res_matriz = $this->model('ResMatriz');

            $res_matriz->res_matrizcnpj_matriz = str_replace(array('.', '/', '-','_'), '', $_POST['cnpj_matriz']);
            $res_matriz->res_matrizvl_meta = $_POST['vl_meta'];
            $res_matriz->res_matrizvl_minimo = $_POST['vl_minimo'];
            $res_matriz->res_matrizvl_variavel = $_POST['vl_variavel'];
            $res_matriz->res_matrizvl_adic = $_POST['vl_adic'];
            $res_matriz->res_matrizvl_bonus = $_POST['vl_bonus'];
            $res_matriz->res_matrizdt_movto = $_POST['dt_movto'];
            $res_matriz->res_matrizvl_real = $_POST['vl_real'];

            $mensagem[] = $res_matriz->save();
        }

        $this->view('resmatrizs/cadastrar', $dados = [ 'mensagem' => $mensagem]);

    }

    public function editar($id){

        Auth::checkLogin();

        $mensagem = array();
        $res_matriz = $this->model('ResMatriz');
        $dados = $res_matriz->findId($id);


        if(isset($_POST['Atualizar'])){

            $res_matriz->res_matrizcnpj_matriz = str_replace(array('.', '/', '-','_'), '', $_POST['cnpj_matriz']);
            $res_matriz->res_matrizvl_meta = $_POST['vl_meta'];
            $res_matriz->res_matrizvl_minimo = $_POST['vl_minimo'];
            $res_matriz->res_matrizvl_variavel = $_POST['vl_variavel'];
            $res_matriz->res_matrizvl_adic = $_POST['vl_adic'];
            $res_matriz->res_matrizvl_bonus = $_POST['vl_bonus'];
            $res_matriz->res_matrizdt_movto = $_POST['dt_movto'];
            $res_matriz->res_matrizvl_real = $_POST['vl_real'];

            $mensagem[] = $res_matriz->update($id);

        }

        $this->view('resmatrizs/editar', $dados = [ 'mensagem' => $mensagem, 'registros' => $dados]);
    }


    public function excluir($id = '')
    {
        Auth::checkLogin();

        $mensagem = array();
        $res_matriz = $this->model('ResMatriz');

        $mensagem[] = $res_matriz->delete($id);

        $dados = $res_matriz->getAll();

        $this->view('resmatrizs/index', $dados = ['registros' => $dados, 'mensagem' => $mensagem]);
    }

}






