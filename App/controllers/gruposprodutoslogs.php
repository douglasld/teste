<?php

use App\Core\Controller;
use App\Auth;

class gruposprodutoslogs extends Controller{

    public function index($id = '')
    {
        Auth::checkLogin();

        $grupo_prod_log = $this->model('GrupoProdLog');
        $dados = $grupo_prod_log->findId($id);

        $this->view('gruposprodutoslogs/index', $dados = ['registros' => $dados]);
    }

    public function ver($id = '')
    {
        Auth::checkLogin();

        $grupo_prod_log = $this->model('GrupoProdLog');
        $dados = $grupo_prod_log->findId($id);

        $this->view('gruposprodutoslogs/ver', $dados);
    }


}
