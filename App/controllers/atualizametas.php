<?php

use App\Core\Controller;
use App\Auth;

class atualizametas extends Controller
{

    public function index()
    {
        Auth::checkLogin();

        $metas = $this->model('Meta');
        $vendas = $this->model('Venda');
        $res_estab = $this->model('ResEstab');
        $empresas = $this->model('Empresa');
        $produtos = $this->model('Produto');
        $res_produto = $this->model('ResProduto');
        $resUnidNeg = $this->model('ResUnidNeg');
        $res_vendedor = $this->model('ResVendedor');
        $res_gerente = $this->model('ResGerente');
        $res_grupo = $this->model('ResGruProd');
        $res_matriz = $this->model('ResMatriz');

        //Grava resumo RES_ESTAB
        foreach ($empresas->getAll() as $valueE):

            $estab = $valueE['empresacnpj_estab'];
            $vendasDataVenda = $vendas;
            $mes = ltrim(date("m"), 0);
            $ano = date("Y");
            $dias = cal_days_in_month(CAL_GREGORIAN, $mes, $ano);

            $total = $metas->somaValorTotalMeta($estab);
            $metaDiaria = $total['soma'] / $dias;

            $minimo = $metas->somaValorMinimoMeta($estab);
            $metaMinimaDiaria = $minimo['soma'] / $dias;

            $variavel = $metas->somaValorVariavelMeta($estab);
            $metaVariavelDiaria = $variavel['soma'] / $dias;

            $adicional = $metas->somaValorAdicionalMeta($estab);
            $metaAdicionalDiaria = $adicional['soma'] / $dias;

            $bonus = $metas->somaValorBonusMeta($estab);
            $metaBonusDiaria = $bonus['soma'] / $dias;

            $vendasTotal = $vendas->somaVendasEstabelecimento($estab);
            $vendasTotalDiaria = $vendasTotal['soma'] / $dias;

            //Elimina os dados para não duplicar
            $res_estab->deleteEstab($estab);

            foreach ($vendas->getPorEstabelecimento($estab) as $value):

                $linhas = $res_estab->verificaData($estab, $value['vendasdt_docto']);
                $valor = $res_estab->localizaData($estab, $value['vendasdt_docto']);

                if ($linhas > 0) {
                    $res_estab->res_estabvl_real = $valor['res_estabvl_real'] + $value['vendasvl_total'];
                    $res_estab->res_estabvl_meta = $metaDiaria;
                    $res_estab->res_estabvl_minimo = $metaMinimaDiaria;
                    $res_estab->res_estabvl_variavel = $metaVariavelDiaria;
                    $res_estab->res_estabvl_adic = $metaAdicionalDiaria;
                    $res_estab->res_estabvl_bonus = $metaBonusDiaria;

                    $res_estab->atualizaResumo($valor['res_estabid']);
                } else {
                    $res_estab->res_estabcnpj_estab = $estab;
                    $res_estab->res_estabvl_meta = $metaDiaria;
                    $res_estab->res_estabvl_minimo = $metaMinimaDiaria;
                    $res_estab->res_estabvl_variavel = $metaVariavelDiaria;
                    $res_estab->res_estabvl_adic = $metaAdicionalDiaria;
                    $res_estab->res_estabvl_bonus = $metaBonusDiaria;
                    $res_estab->res_estabdt_movto = $value['vendasdt_docto'];
                    $res_estab->res_estabvl_real = $value['vendasvl_total'];

                    $res_estab->save();

                }

            endforeach;
        endforeach;
        //Grava resumo RES_ESTAB

        //Grava resumo RES_produto

        //Elimina os dados para não duplicar
        $res_produto->deleteLimpa();

        foreach ($empresas->getAll() as $value):

            $mes = ltrim(date("m"), 0);
            $ano = date("Y");
            $dias = cal_days_in_month(CAL_GREGORIAN, $mes, $ano);

            //<!-- Vendas por estabelecimento -->
            foreach ($vendas->getPorEstabAgrupadoData($value['empresacnpj_estab']) as $item):

                $produtoDados = $produtos->findId($item['vendascod_produto']);

                $total = $vendas->somaVendasEstabelecimentoProdutoData($item['vendascnpj_estab'], $item['vendascod_produto'], $item['vendasdt_docto'], $item['vendasgerente'], $item['vendascod_unid_neg'], $item['vendasnome_vendedor'], $produtoDados['produtocod_grup_prod']);

                $metaTotal = $metas->somaValorPorProduto($item['vendascnpj_estab'], $item['vendascod_produto']);
                $metaTotalDiaria = $metaTotal['soma'] / $dias;

                $metaValorMinimo = $metas->somaValorMinimoPorProduto($item['vendascnpj_estab'], $item['vendascod_produto']);
                $metaValorMinimoDiaria = $metaValorMinimo['soma'] / $dias;

                $metaValorVariavel = $metas->somaValorVariavelPorProduto($item['vendascnpj_estab'], $item['vendascod_produto']);
                $metaValorVariavelDiaria = $metaValorVariavel['soma'] / $dias;

                $metaValorAdicional = $metas->somaValorAdicionalPorProduto($item['vendascnpj_estab'], $item['vendascod_produto']);
                $metaValorAdicionalDiaria = $metaValorAdicional['soma'] / $dias;

                $metaValorBonus = $metas->somaValorBonusPorProduto($item['vendascnpj_estab'], $item['vendascod_produto']);
                $metaValorBonusDiaria = $metaValorBonus['soma'] / $dias;

                $res_produto->res_produtocnpj_estab = $item['vendascnpj_estab'];
                $res_produto->res_produtovl_meta = $metaTotalDiaria;
                $res_produto->res_produtovl_minimo = $metaValorMinimoDiaria;
                $res_produto->res_produtovl_variavel = $metaValorVariavelDiaria;
                $res_produto->res_produtovl_adic = $metaValorAdicionalDiaria;
                $res_produto->res_produtovl_bonus = $metaValorBonusDiaria;
                $res_produto->res_produtodt_movto = $item['vendasdt_docto'];
                $res_produto->res_produtocod_unid_neg = $item['vendascod_unid_neg'];
                $res_produto->res_produtocod_gerente = $item['vendasgerente'];
                $res_produto->res_produtocod_vendedor = $item['vendasnome_vendedor'];
                $res_produto->res_produtocod_gru_prod = $produtoDados['produtocod_grup_prod'];
                $res_produto->res_produtocod_produto = $item['vendascod_produto'];
                $res_produto->res_produtovl_real = $total['soma'];

                $res_produto->save();


            endforeach;
            // Vendas por estabelecimento -->

        endforeach;

//<!-- RES_UNIDADE -->

        //Limpa os dados
        $resUnidNeg->deleteLimpa();

        $mes = ltrim(date("m"), 0);
        $ano = date("Y");
        $dias = cal_days_in_month(CAL_GREGORIAN, $mes, $ano);


        foreach ($vendas->agrupadoEstabDataUnid() as $value):

            $total = $vendas->somaEstabDataUnid($value['vendascnpj_estab'], $value['vendasdt_docto'], $value['vendascod_unid_neg']);

            //Valores das metas
            $metaTotal = $metas->somaUnidValorTotal($value['vendascod_unid_neg']);
            $metaTotalDiaria = $metaTotal['soma'] / $dias;

            $metaValorMinimo = $metas->somaUnidValorMinimo($value['vendascod_unid_neg']);
            $metaValorMinimoDiario = $metaValorMinimo['soma'] / $dias;

            $metaValorVariavel = $metas->somaUnidValorVariavel($value['vendascod_unid_neg']);
            $metaValorVariavelDiario = $metaValorVariavel['soma'] / $dias;

            $metaValorAdicional = $metas->somaUnidValorAdicional($value['vendascod_unid_neg']);
            $metaValorAdicionalDiario = $metaValorAdicional['soma'] / $dias;

            $metaValorBonus = $metas->somaUnidValorBonus($value['vendascod_unid_neg']);
            $metaValorBonusDiario = $metaValorBonus['soma'] / $dias;

            //Valores das metas

            $resUnidNeg->res_unid_negcnpj_estab = $value['vendascnpj_estab'];
            $resUnidNeg->res_unid_negvl_meta = $metaTotalDiaria;
            $resUnidNeg->res_unid_negvl_minimo = $metaValorMinimoDiario;
            $resUnidNeg->res_unid_negvl_variavel = $metaValorVariavelDiario;
            $resUnidNeg->res_unid_negvl_adic = $metaValorAdicionalDiario;
            $resUnidNeg->res_unid_negvl_bonus = $metaValorBonusDiario;
            $resUnidNeg->res_unid_negdt_movto = $value['vendasdt_docto'];
            $resUnidNeg->res_unid_negcod_unid_neg = $value['vendascod_unid_neg'];
            $resUnidNeg->res_unid_negvl_real = $total['soma'];

            $resUnidNeg->save();


        endforeach;

//<!-- RES_UNIDADE -->

        //<!-- RES_VENDEDOR -->

//Limpa dados
        $res_vendedor->deleteLimpa();

        $mes = ltrim(date("m"), 0);
        $ano = date("Y");
        $dias = cal_days_in_month(CAL_GREGORIAN, $mes, $ano);

        foreach ($vendas->agrupadoVendedor() as $value) {

            $metaVendedorTotal = $metas->somaValorTotalVendedor($value['vendasnome_vendedor'], $value['vendascnpj_estab'], $value['vendascod_unid_neg']);
            $metaVendedorTotalDiaria = $metaVendedorTotal['soma'] / $dias;

            $metaVendedorMinimo = $metas->somaValorTotalVendedorMinimo($value['vendasnome_vendedor'], $value['vendascnpj_estab'], $value['vendascod_unid_neg']);
            $metaVendedorMinimoDiaria = $metaVendedorMinimo['soma'] / $dias;

            $metaVendedorVariavel = $metas->somaValorTotalVendedorVariavel($value['vendasnome_vendedor'], $value['vendascnpj_estab'], $value['vendascod_unid_neg']);
            $metaVendedorVariavelDiaria = $metaVendedorVariavel['soma'] / $dias;

            $metaVendedorAdicional = $metas->somaValorTotalVendedorAdicional($value['vendasnome_vendedor'], $value['vendascnpj_estab'], $value['vendascod_unid_neg']);
            $metaVendedorAdicionalDiaria = $metaVendedorAdicional['soma'] / $dias;

            $metaVendedorBonus = $metas->somaValorTotalVendedorBonus($value['vendasnome_vendedor'], $value['vendascnpj_estab'], $value['vendascod_unid_neg']);
            $metaVendedorBonusDiaria = $metaVendedorBonus['soma'] / $dias;

            //<!-- Venda por vendedor -->
            foreach ($vendas->agrupadoDataEstabUnidGerente($value['vendasnome_vendedor']) as $item) {

                $total = $vendas->somaVendedorDataEstabUnidGerente($value['vendasnome_vendedor'], $item['vendasdt_docto'], $item['vendascnpj_estab'], $item['vendascod_unid_neg'], $item['vendasgerente']);

                $res_vendedor->res_vendedorcnpj_estab = $item['vendascnpj_estab'];
                $res_vendedor->res_vendedorvl_meta = $metaVendedorTotalDiaria;
                $res_vendedor->res_vendedorvl_minimo = $metaVendedorMinimoDiaria;
                $res_vendedor->res_vendedorvl_variavel = $metaVendedorVariavelDiaria;
                $res_vendedor->res_vendedorvl_adic = $metaVendedorAdicionalDiaria;
                $res_vendedor->res_vendedorvl_bonus = $metaVendedorBonusDiaria;
                $res_vendedor->res_vendedordt_movto = $item['vendasdt_docto'];
                $res_vendedor->res_vendedorcod_unid_neg = $item['vendascod_unid_neg'];
                $res_vendedor->res_vendedorcod_gerente = $item['vendasgerente'];
                $res_vendedor->res_vendedorcod_vendedor = $value['vendasnome_vendedor'];
                $res_vendedor->res_vendedorvl_real = $total['soma'];

                $res_vendedor->save();

            }

            //<!-- RES_VENDEDOR -->


//!--RES GERENTE-->
            $mes = ltrim(date("m"), 0);
            $ano = date("Y");
            $dias = cal_days_in_month(CAL_GREGORIAN, $mes, $ano);

            //Limpa os dados
            $res_gerente->deleteLimpa();

            foreach ($vendas->agrupadoGerenteEstabDataUnid() as $value) {
                $total = $vendas->somaGerenteEstabDataUnid($value['vendasgerente'], $value['vendascnpj_estab'], $value['vendasdt_docto'], $value['vendascod_unid_neg']);

                $metaTotalGerente = $metas->somaValorTotalGerente($value['vendasgerente'], $value['vendascnpj_estab'], $value['vendascod_unid_neg']);
                $metaTotalGerenteDiaria = $metaTotalGerente['soma'] / $dias;

                $metaMinimoGerente = $metas->somaValorMinimoGerente($value['vendasgerente'], $value['vendascnpj_estab'], $value['vendascod_unid_neg']);
                $metaMinimoGerenteDiaria = $metaMinimoGerente['soma'] / $dias;

                $metaVariavelGerente = $metas->somaValorVariavelGerente($value['vendasgerente'], $value['vendascnpj_estab'], $value['vendascod_unid_neg']);
                $metaVariavelGerenteDiaria = $metaVariavelGerente['soma'] / $dias;

                $metaAdicionalGerente = $metas->somaValorAdicionalGerente($value['vendasgerente'], $value['vendascnpj_estab'], $value['vendascod_unid_neg']);
                $metaAdicionalGerenteDiaria = $metaAdicionalGerente['soma'] / $dias;

                $metaBonusGerente = $metas->somaValorBonusGerente($value['vendasgerente'], $value['vendascnpj_estab'], $value['vendascod_unid_neg']);
                $metaBonusGerenteDiaria = $metaBonusGerente['soma'] / $dias;

                $res_gerente->res_gerentecnpj_estab = $value['vendascnpj_estab'];
                $res_gerente->res_gerentevl_meta = $metaTotalGerenteDiaria;
                $res_gerente->res_gerentevl_minimo = $metaMinimoGerenteDiaria;
                $res_gerente->res_gerentevl_variavel = $metaVariavelGerenteDiaria;
                $res_gerente->res_gerentevl_adic = $metaAdicionalGerenteDiaria;
                $res_gerente->res_gerentevl_bonus = $metaBonusGerenteDiaria;
                $res_gerente->res_gerentedt_movto = $value['vendasdt_docto'];
                $res_gerente->res_gerentecod_unid_neg = $value['vendascod_unid_neg'];
                $res_gerente->res_gerentecod_gerente = $value['vendasgerente'];
                $res_gerente->res_gerentevl_real = $total['soma'];

                $res_gerente->save();

            }

//<!-- RES GERENTE -->

            //<!-- RES GRUPO PRODUTO -->
            $mes = ltrim(date("m"), 0);
            $ano = date("Y");
            $dias = cal_days_in_month(CAL_GREGORIAN, $mes, $ano);

            //Limpa dados
            $res_grupo->deleteLimpa();

            foreach ($vendas->agrupadoGrupoEstabDataUnidGerenteVendedor() as $value) {
                $total = $vendas->somaGrupoEstabDataUnidadeGerenteVendedor($value['vendasgrupoprod'], $value['vendascnpj_estab'], $value['vendasdt_docto'], $value['vendascod_unid_neg'], $value['vendasgerente'], $value['vendasnome_vendedor']);

                    $metaGrupoTotal = $metas->somaValorPorGrupo($value['vendascnpj_estab'], $value['vendasgrupoprod'], $value['vendascod_unid_neg']);
                    $metaGrupoTotalDiaria = $metaGrupoTotal['soma'] / $dias;

                    $metaGrupoMinimo = $metas->somaValorMinimoPorGrupo($value['vendascnpj_estab'], $value['vendasgrupoprod'], $value['vendascod_unid_neg']);
                    $metaGrupoMinimoDiaria = $metaGrupoMinimo['soma'] / $dias;

                    $metaGrupoVariavel = $metas->somaValorVariavelPorGrupo($value['vendascnpj_estab'], $value['vendasgrupoprod'], $value['vendascod_unid_neg']);
                    $metaGrupoVariavelDiaria = $metaGrupoVariavel['soma'] / $dias;

                    $metaGrupoAdicional = $metas->somaValorAdicionalPorGrupo($value['vendascnpj_estab'], $value['vendasgrupoprod'], $value['vendascod_unid_neg']);
                    $metaGrupoAdicionalDiaria = $metaGrupoAdicional['soma'] / $dias;

                    $metaGrupoBonus = $metas->somaValorBonusPorGrupo($value['vendascnpj_estab'], $value['vendasgrupoprod'], $value['vendascod_unid_neg']);
                    $metaGrupoBonusDiaria = $metaGrupoBonus['soma'] / $dias;

                    $res_grupo->res_gru_prodcnpj_estab = $value['vendascnpj_estab'];
                    $res_grupo->res_gru_prodvl_meta = $metaGrupoTotalDiaria;
                    $res_grupo->res_gru_prodvl_minimo = $metaGrupoMinimoDiaria;
                    $res_grupo->res_gru_prodvl_variavel = $metaGrupoVariavelDiaria;
                    $res_grupo->res_gru_prodvl_adic = $metaGrupoAdicionalDiaria;
                    $res_grupo->res_gru_prodvl_bonus = $metaGrupoBonusDiaria;
                    $res_grupo->res_gru_proddt_movto = $value['vendasdt_docto'];
                    $res_grupo->res_gru_prodcod_unid_neg = $value['vendascod_unid_neg'];
                    $res_grupo->res_gru_prodcod_gerente = $value['vendasgerente'];
                    $res_grupo->res_gru_prodcod_vendedor = $value['vendasnome_vendedor'];
                    $res_grupo->res_gru_prodcod_gru_prod = $value['vendasgrupoprod'];
                    $res_grupo->res_gru_prodvl_real = $total['soma'];

                    $res_grupo->save();

                }

            //<!-- RES GRUPO PRODUTO -->

//<!-- RES MATRIZ -->

        $mes = ltrim(date("m"), 0);
        $ano = date("Y");
        $dias = cal_days_in_month(CAL_GREGORIAN, $mes, $ano);

        $res_matriz->deleteLimpa();


        foreach ($vendas->agrupadoEstabData() as $value){
            $total = $vendas->somaMatrizData($value['empresacnpj_matriz'],$value['vendasdt_docto']);

            $metaMatrizTotal = $metas->somaValorMatriz($value['empresacnpj_matriz']);
            $metaMatrizTotalD = $metaMatrizTotal['soma'] / $dias;

            $metaMatrizMinimo = $metas->somaValorMinimoMatriz($value['empresacnpj_matriz']);
            $metaMatrizMinimoD = $metaMatrizMinimo['soma'] / $dias;

            $metaMatrizVariavel = $metas->somaValorVariavelMatriz($value['empresacnpj_matriz']);
            $metaMatrizVariavelD = $metaMatrizVariavel['soma'] / $dias;

            $metaMatrizAdicional = $metas->somaValorAdicionalMatriz($value['empresacnpj_matriz']);
            $metaMatrizAdicionalD = $metaMatrizAdicional['soma'] / $dias;

            $metaMatrizBonus = $metas->somaValorBonusMatriz($value['empresacnpj_matriz']);
            $metaMatrizBonusD = $metaMatrizBonus['soma'] / $dias;

            $res_matriz->res_matrizcnpj_matriz=$value['empresacnpj_matriz'];
            $res_matriz->res_matrizvl_meta=$metaMatrizTotalD;
            $res_matriz->res_matrizvl_minimo=$metaMatrizMinimoD;
            $res_matriz->res_matrizvl_variavel=$metaMatrizVariavelD;
            $res_matriz->res_matrizvl_adic=$metaMatrizAdicionalD;
            $res_matriz->res_matrizvl_bonus=$metaMatrizBonusD;
            $res_matriz->res_matrizdt_movto=$value['vendasdt_docto'];
            $res_matriz->res_matrizvl_real=$total['soma'];

            $res_matriz->save();

        }

        //<!-- RES MATRIZ -->

            $this->view('home/index');
        }
    }
}
?>
