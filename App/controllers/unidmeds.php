<?php

use \App\Core\Controller;
use App\Auth;

class unidmeds extends Controller{

    public function index()
    {
        Auth::checkLogin();

        $empresas = $this->model('Empresa');
        $dados = $empresas->getPorMatriz($_SESSION['matriz']);

        $this->view('unidmeds/index', $dados = ['registros' => $dados]);
    }


    public function ver($id = '')
    {
        Auth::checkLogin();

        $unid_med = $this->model('UnidMed');
        $dados = $unid_med->findId($id);

        $this->view('unidmeds/ver', $dados);
    }

    public function cadastrar()
    {
        Auth::checkLogin();

        $mensagem = array();

        if(isset($_POST['Cadastrar'])){

            $unid_med = $this->model('UnidMed');

            $unid_med->unid_medcnpj_estab = str_replace(array('.', '/', '-','_'), '', $_POST['cnpj_estab']);
            $unid_med->unid_medcod_unid_med = $_POST['cod_unid_med'];
            $unid_med->unid_meddescricao = $_POST['descricao'];
            $unid_med->unid_medind_ativo = $_POST['ind_ativo'];
            $unid_med->unid_medind_origem = $_POST['ind_origem'];
            $unid_med->unid_meddt_criacao = date("Y-m-d");
            $unid_med->unid_medhr_criacao = date("h:i:s");
            $unid_med->unid_meduser_criacao = $_SESSION['userId'];
            $unid_med->unid_meddt_altera = date("Y-m-d");
            $unid_med->unid_medhr_altera = date("h:i:s");
            $unid_med->unid_meduser_altera = $_SESSION['userId'];

            $mensagem[] = $unid_med->save();

            $ultimo = $unid_med->ultimo(str_replace(array('.', '/', '-','_'), '', $_POST['cnpj_estab']),$_SESSION['userId']);

            $unid_med_log = $this->model('UnidMedLog');

            $unid_med_log->unid_med_logcnpj_estab = str_replace(array('.', '/', '-','_'), '', $_POST['cnpj_estab']);
            $unid_med_log->unid_med_logcod_unid_med = $ultimo['unid_medid'];
            $unid_med_log->unid_med_logdt_emis = date("Y-m-d");
            $unid_med_log->unid_med_loghr_emis = date("h:i:s");
            $unid_med_log->unid_med_loguser_emis = $_SESSION['userId'];
            $unid_med_log->unid_med_logtitulo = "Inclusão";

            $mensagem[] = $unid_med_log->save();

        }

        $this->view('unidmeds/cadastrar', $dados = ['mensagem' => $mensagem]);

    }

    public function editar($id){

        Auth::checkLogin();

        $mensagem = array();
        $unid_med = $this->model('UnidMed');

        if(isset($_POST['Atualizar'])){

            $unid_med->unid_medcnpj_estab = str_replace(array('.', '/', '-','_'), '', $_POST['cnpj_estab']);
            $unid_med->unid_medcod_unid_med = $_POST['cod_unid_med'];
            $unid_med->unid_meddescricao = $_POST['descricao'];
            $unid_med->unid_medind_ativo = $_POST['ind_ativo'];
            $unid_med->unid_medind_origem = $_POST['ind_origem'];
            $unid_med->unid_meddt_altera = date("Y-m-d");
            $unid_med->unid_medhr_altera = date("h:i:s");
            $unid_med->unid_meduser_altera = $_SESSION['userId'];

            $mensagem[] = $unid_med->update($id);

            $unid_med_log = $this->model('UnidMedLog');

            $unid_med_log->unid_med_logcnpj_estab = str_replace(array('.', '/', '-','_'), '', $_POST['cnpj_estab']);
            $unid_med_log->unid_med_logcod_unid_med = $id;
            $unid_med_log->unid_med_logdt_emis = date("Y-m-d");
            $unid_med_log->unid_med_loghr_emis = date("h:i:s");
            $unid_med_log->unid_med_loguser_emis = $_SESSION['userId'];
            $unid_med_log->unid_med_logtitulo = "Alteração";

            $mensagem[] = $unid_med_log->save();

        }

        $dados = $unid_med->findId($id);

        $this->view('unidmeds/editar', $dados = [ 'mensagem' => $mensagem, 'registros' => $dados]);
    }


    public function excluir($id = '')
    {

        Auth::checkLogin();

        $mensagem = array();
        $unid_med = $this->model('UnidMed');

        $mensagem[] = $unid_med->delete($id);

        $dados = $unid_med->getAll();

        $this->view('home/index', $dados = ['registros' => $dados, 'mensagem' => $mensagem]);
    }

}



