<?php

use \App\Core\Controller;
use App\Auth;

class userestabs extends Controller{

    public function index()
    {
        Auth::checkLogin();

        $user_estab = $this->model('UserEstab');
        $dados = $user_estab->getAll();

        $this->view('userestabs/index', $dados = ['registros' => $dados]);
    }

    public function usuario($id)
    {
        Auth::checkLogin();

        $user_estab = $this->model('UserEstab');
        $dados = $user_estab->findCod($id);

        $this->view('userestabs/index', $dados = ['registros' => $dados]);
    }


    public function ver($id = '')
    {
        Auth::checkLogin();

        $user_estab = $this->model('UserEstab');
        $dados = $user_estab->findId($id);

        $this->view('userestabs/ver', $dados);
    }

    public function cadastrar($id)
    {
        Auth::checkLogin();

        $mensagem = array();

        if(isset($_POST['Cadastrar'])){

            $user_estab = $this->model('UserEstab');

            $user_estab->user_estabcnpj_matriz = str_replace(array('.', '/', '-','_'), '', $_POST['cnpj_matriz']);
            $user_estab->user_estabcnpj_estab = str_replace(array('.', '/', '-','_'), '', $_POST['cnpj_estab']);
            $user_estab->user_estabcod_user = $id;
            $user_estab->user_estabnome_user = "NULL";
            $user_estab->user_estabvisao_global = $_POST['visao_global'];
            $user_estab->user_estabvisao_gerencial = $_POST['visao_gerencial'];
            $user_estab->user_estabvisao_vendas = $_POST['visao_vendas'];
            $user_estab->user_estabind_ativo = $_POST['ind_ativo'];
            $user_estab->user_estabind_origem = $_POST['ind_origem'];
            $user_estab->user_estabrecebe_metas = $_POST['recebe_metas'];
            $user_estab->user_estabdt_criacao = date("Y-m-d");
            $user_estab->user_estabhr_criacao = date("h:i:s");
            $user_estab->user_estabdt_altera = date("Y-m-d");
            $user_estab->user_estabhr_altera = date("h:i:s");
            $user_estab->user_estabuser_criacao = $_SESSION['userId'];
            $user_estab->user_estabuser_altera = $_SESSION['userId'];

            $mensagem[] = $user_estab->save();

            $user_estab_log = $this->model('UserEstabLog');

            $user_estab_log->user_estab_logcnpj_matriz = str_replace(array('.', '/', '-','_'), '', $_POST['cnpj_matriz']);
            $user_estab_log->user_estab_logcod_user = $id;
            $user_estab_log->user_estab_logdt_emis = date("Y-m-d");
            $user_estab_log->user_estab_loghr_emis = date("h:i:s");
            $user_estab_log->user_estab_loguser_emis = $_SESSION['userId'];
            $user_estab_log->user_estab_logtitulo = "Inclusão";
            $user_estab_log->user_estab_lognome_user = "NULL";

            $mensagem[] = $user_estab_log->save();

        }

        $user_perfil = $this->model('UserPerfil');
        $dados = $user_perfil->findCod($id);
        $this->view('userestabs/cadastrar', $dados = [ 'mensagem' => $mensagem, 'registros' => $dados]);

    }

    public function editar($id){

        Auth::checkLogin();

        $mensagem = array();
        $user_estab = $this->model('UserEstab');
        $dados = $user_estab->findId($id);


        if(isset($_POST['Atualizar'])){

            $user_estab->user_estabvisao_global = $_POST['visao_global'];
            $user_estab->user_estabvisao_gerencial = $_POST['visao_gerencial'];
            $user_estab->user_estabvisao_vendas = $_POST['visao_vendas'];
            $user_estab->user_estabind_ativo = $_POST['ind_ativo'];
            $user_estab->user_estabind_origem = $_POST['ind_origem'];
            $user_estab->user_estabrecebe_metas = $_POST['recebe_metas'];
            $user_estab->user_estabdt_altera = date("Y:m:d");
            $user_estab->user_estabhr_altera = date("h:i:s");
            $user_estab->user_estabuser_altera = $_SESSION['userId'];

            $mensagem[] = $user_estab->update($id);

            $user_estab_log = $this->model('UserEstabLog');

            $user_estab_log->user_estab_logcnpj_matriz = str_replace(array('.', '/', '-','_'), '', $dados['user_estabcnpj_matriz']);
            $user_estab_log->user_estab_logcod_user = $id;
            $user_estab_log->user_estab_logdt_emis = date("Y-m-d");
            $user_estab_log->user_estab_loghr_emis = date("h:i:s");
            $user_estab_log->user_estab_loguser_emis = $_SESSION['userId'];
            $user_estab_log->user_estab_logtitulo = "Alteração";
            $user_estab_log->user_estab_lognome_user = "NULL";

            $mensagem[] = $user_estab_log->save();

        }

        $this->view('userestabs/editar', $dados = [ 'mensagem' => $mensagem, 'registros' => $dados]);
    }


    public function excluir($id = '')
    {
        Auth::checkLogin();

        $mensagem = array();
        $user_estab = $this->model('UserEstab');

        $mensagem[] = $user_estab->delete($id);

        $dados = $user_estab->getAll();

        $this->view('home/index', $dados = ['registros' => $dados, 'mensagem' => $mensagem]);
    }

}



