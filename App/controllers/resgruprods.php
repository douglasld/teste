<?php

use \App\Core\Controller;
use App\Auth;

class resgruprods extends Controller{

    public function index()
    {
        Auth::checkLogin();

        $empresas = $this->model('Empresa');
        $dados = $empresas->getPorMatriz($_SESSION['matriz']);

        $this->view('resgruprods/index', $dados = ['registros' => $dados]);
    }

    public function ver($id = '')
    {
        Auth::checkLogin();

        $res_gru_prod = $this->model('ResGruProd');
        $dados = $res_gru_prod->findId($id);

        $this->view('resgruprods/ver', $dados = ['registros' => $dados] );
    }

    public function cadastrar()
    {
        Auth::checkLogin();

        $mensagem = array();

        if(isset($_POST['Cadastrar'])){

            $res_gru_prod = $this->model('ResGruProd');

            $cnpj_estab = str_replace(array('.', '/', '-', '_'), '', $_POST['cnpj_estab']);
            $res_gru_prod->res_gru_prodcnpj_estab = substr($cnpj_estab,0, 14); 

            $res_gru_prod->res_gru_proddt_movto = $_POST['dt_movto'];
            $res_gru_prod->res_gru_prodcod_unid_neg = $_POST['cod_unid_neg'];
            $res_gru_prod->res_gru_prodcod_gerente = $_POST['cod_gerente'];
            $res_gru_prod->res_gru_prodcod_vendedor = $_POST['cod_vendedor'];
            $res_gru_prod->res_gru_prodcod_gru_prod = $_POST['cod_gru_prod'];

            $res_gru_prod->res_gru_prodvl_meta = str_replace(array('.', ','), array('', '.'), $_POST['vl_meta']);
            $res_gru_prod->res_gru_prodvl_minimo = str_replace(array('.', ','), array('', '.'), $_POST['vl_minimo']);
            $res_gru_prod->res_gru_prodvl_variavel = str_replace(array('.', ','), array('', '.'), $_POST['vl_variavel']);
            $res_gru_prod->res_gru_prodvl_adic = str_replace(array('.', ','), array('', '.'), $_POST['vl_adic']);
            $res_gru_prod->res_gru_prodvl_bonus = str_replace(array('.', ','), array('', '.'), $_POST['vl_bonus']);
            $res_gru_prod->res_gru_prodvl_real = str_replace(array('.', ','), array('', '.'), $_POST['vl_real']);

            $mensagem[] = $res_gru_prod->save();
        }

        $this->view('resgruprods/cadastrar', $dados = [ 'mensagem' => $mensagem]);

    }

    public function editar($id){

        Auth::checkLogin();

        $mensagem = array();
        $res_gru_prod = $this->model('ResGruProd');

        if(isset($_POST['Atualizar'])){

            $cnpj_estab = str_replace(array('.', '/', '-', '_'), '', $_POST['cnpj_estab']);
            $res_gru_prod->res_gru_prodcnpj_estab = substr($cnpj_estab,0, 14); 

            $res_gru_prod->res_gru_proddt_movto = $_POST['dt_movto'];
            $res_gru_prod->res_gru_prodcod_unid_neg = $_POST['cod_unid_neg'];
            $res_gru_prod->res_gru_prodcod_gerente = $_POST['cod_gerente'];
            $res_gru_prod->res_gru_prodcod_vendedor = $_POST['cod_vendedor'];
            $res_gru_prod->res_gru_prodcod_gru_prod = $_POST['cod_gru_prod'];
            $res_gru_prod->res_gru_prodvl_real = str_replace(array('.', ','), array('', '.'), $_POST['vl_real']);

            $mensagem[] = $res_gru_prod->update($id);

        }

        $dados = $res_gru_prod->findId($id);

        $this->view('resgruprods/editar', $dados = [ 'mensagem' => $mensagem, 'registros' => $dados]);
    }


    public function excluir($id = '')
    {
        Auth::checkLogin();

        $mensagem = array();
        $res_gru_prod = $this->model('ResGruProd');

        $mensagem[] = $res_gru_prod->delete($id);

        
        $empresas = $this->model('Empresa');
        $dados = $empresas->getPorMatriz($_SESSION['matriz']);

        $this->view('resgruprods/index', $dados = ['registros' => $dados]);
    }

}







