<?php

use \App\Core\Controller;
use App\Auth;

class catpai extends Controller
{

    public function index()
    {
        Auth::checkLogin();
        $CatPai = $this->model('CatPaisModel');

        $dados = $CatPai->GetAll();

        $this->view('catpai/index', $dados = ['registros' => $dados]);
    }

    public function ver($id = '')
    {
        Auth::checkLogin();
        $CatPai = $this->model('CatPaisModel');

        $dados = $CatPai->getId($id);

        $this->view('catpai/ver', $dados = ['registros' => $dados]);
    }

    public function cadastrar()
    {
        Auth::checkLogin();
        $CatPai = $this->model('CatPaisModel');
        $mensagem = array();

        if (isset($_POST['Cadastrar'])) {

            //recebe posts
            $CatPai->nome_cat_p = $_POST['nome_cat_p'];
            $CatPai->user_criacao = $_SESSION['userId'];
            $CatPai->dt_criacao = date("Y-m-d");
            $CatPai->hr_criacao = date("H:i:s");

            //trata ind Ativo]
            if ($_POST['ind_ativo'] == "on") {
                $CatPai->ind_ativo = '0';
            } else {
                $CatPai->ind_ativo = '1';
            }
            $mensagem[] = $CatPai->save();
        }
        $dados = $CatPai->GetUltimoCod();

        $this->view('catpai/cadastrar', $dados = ['registros' => $dados, 'mensagem' => $mensagem]);
    }

    public function editar($id)
    {

        Auth::checkLogin();
        $CatPai = $this->model('CatPaisModel');
        $mensagem = array();

        if (isset($_POST['Atualizar'])) {

            $CatPai->nome_cat_p = $_POST['nome_cat_p'];

            //trata ind Ativo]
            if ($_POST['ind_ativo'] == "on") {
                $CatPai->ind_ativo = '0';
            } else {
                $CatPai->ind_ativo = '1';
            }

            $mensagem[] = $CatPai->update($id);
        }

        $dados = $CatPai->getId($id);

        $this->view('catpai/ver', $dados = ['mensagem' => $mensagem, 'registros' => $dados]);
    }


    public function excluir($id = '')
    {
        Auth::checkLogin();

        $mensagem = array();
        $CatPai = $this->model('CatPaisModel');

        if ($CatPai->VerificaExcluir($id)) {
            $mensagem[] = $CatPai->delete($id);
        } else {
            $mensagem[] = "6 - Existem produtos vinculados a essa categoria!";
        }

        $dados = $CatPai->GetAll();

        $this->view('catpai/index', $dados = ['mensagem' => $mensagem, 'registros' => $dados]);
    }
}
