<?php

use App\Core\Controller;
use App\Auth;

class turnoslogs extends Controller{

    public function index($estabelecimento = '',$horaaInicio = '',$horaFinal = '')
    {
        Auth::checkLogin();

        $turno_log = $this->model('TurnoLog');
        $dados = $turno_log->getAll($estabelecimento,$horaaInicio,$horaFinal);

        $this->view('turnoslogs/index', $dados = ['registros' => $dados]);
    }

}


