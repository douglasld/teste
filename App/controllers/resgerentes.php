<?php

use \App\Core\Controller;
use App\Auth;

class resgerentes extends Controller
{

    public function index()
    {
        Auth::checkLogin();

        $empresas = $this->model('Empresa');
        $dados = $empresas->getPorMatriz($_SESSION['matriz']);

        $this->view('resgerentes/index', $dados = ['registros' => $dados]);
    }

    public function ver($id = '')
    {
        Auth::checkLogin();

        $res_gerente = $this->model('ResGerente');
        $dados = $res_gerente->findId($id);
        $this->view('resgerentes/ver', $dados = ['registros' => $dados]);
    }

    public function cadastrar()
    {
        Auth::checkLogin();

        $mensagem = array();

        if (isset($_POST['Cadastrar'])) {

            $res_gerente = $this->model('ResGerente');

            //trata o cnpj do estab
            $cnpj_estab = str_replace(array('.', '/', '-', '_'), '', $_POST['cnpj_estab']);
            $res_gerente->res_gerentecnpj_estab = substr($cnpj_estab, 0, 14);

            //trata o cod_unid_neg
            $partes = explode(" -", $_POST['cod_unid_neg']);
            $res_gerente->res_gerentecod_unid_neg = $partes[0];

            $res_gerente->res_gerentedt_movto = $_POST['dt_movto'];
            $res_gerente->res_gerentecod_gerente = $_POST['cod_gerente'];
            $res_gerente->res_gerentevl_real = str_replace(array('.', ','), array('', '.'), $_POST['vl_real']);

            $mensagem[] = $res_gerente->save();
        }

        $this->view('resgerentes/cadastrar', $dados = ['mensagem' => $mensagem]);
    }

    public function editar($id)
    {

        Auth::checkLogin();

        $mensagem = array();
        $res_gerente = $this->model('ResGerente');

        if (isset($_POST['Atualizar'])) {

            //trata cnpj
            $cnpj_estab = str_replace(array('.', '/', '-', '_'), '', $_POST['cnpj_estab']);
            $res_gerente->res_gerentecnpj_estab = substr($cnpj_estab, 0, 14);

            //trata o cod_unid_neg
            $partes = explode(" -", $_POST['cod_unid_neg']);
            $res_gerente->res_gerentecod_unid_neg = $partes[0];

            $res_gerente->res_gerentedt_movto = $_POST['dt_movto'];
            $res_gerente->res_gerentecod_gerente = $_POST['cod_gerente'];
            $res_gerente->res_gerentevl_real = str_replace(array('.', ','), array('', '.'), $_POST['vl_real']);

            $mensagem[] = $res_gerente->update($id);
        }

        $dados = $res_gerente->findId($id);

        $this->view('resgerentes/editar', $dados = ['mensagem' => $mensagem, 'registros' => $dados]);
    }


    public function excluir($id = '')
    {
        Auth::checkLogin();

        $mensagem = array();
        $res_gerente = $this->model('ResGerente');

        $mensagem[] = $res_gerente->delete($id);

        $empresas = $this->model('Empresa');
        $dados = $empresas->getPorMatriz($_SESSION['matriz']);

        $this->view('resgerentes/index', $dados = ['registros' => $dados]);
    }
}
