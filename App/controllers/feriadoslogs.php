<?php

use App\Core\Controller;
use App\Auth;

class feriadoslogs extends Controller{

    public function index($dataFeriado = '',$cnpjEstabelecimento = '')
    {
        Auth::checkLogin();

        $feriado_log = $this->model('FeriadoLog');
        $dados = $feriado_log->getAll($dataFeriado,$cnpjEstabelecimento);

        $this->view('feriadoslogs/index', $dados = ['registros' => $dados]);
    }

}

