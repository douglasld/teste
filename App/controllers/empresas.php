<?php

use App\Core\Controller;
use App\Auth;

class empresas extends Controller
{

    public function index()
    {
        Auth::checkLogin();

        $empresas = $this->model('Empresa');
        $dados = $empresas->getPorMatriz($_SESSION['matriz']);
        $this->view('empresas/index', $dados = ['registros' => $dados]);
    }

    public function ver($id = '')
    {
        Auth::checkLogin();

        $empresa = $this->model('Empresa');
        $dados['empresa'] = $empresa->findId($id);
        $empresa_log = $this->model('EmpresaLog');
        $dados['logs'] = $empresa_log->findId($id);
        $unid_neg = $this->model('UnidNeg');
        $dados['unid_neg'] = $unid_neg->getAllEstabelecimento($id);
        // print_r(var_dump($empresa_log), true); die;
        $this->view('empresas/ver', $dados);
    }

    public function cadastrar()
    {

        Auth::checkLogin();

        $empresa = $this->model('Empresa');
        $user_perfil = $this->model('UserPerfil');

        $mensagem = array();

        if (isset($_POST['Cadastrar'])) {

            /* $checaMatriz = $empresa->findMatriz(str_replace(array('.', '/', '-', '_'), '', $_POST['cnpj_matriz']));
            $checaEstab = $empresa->findEstab(str_replace(array('.', '/', '-', '_'), '', $_POST['cnpj_estab']));
            $checaEmail = $user_perfil->contaEmail($_POST['e_email']);

            if (empty($_POST['nome_abrev'])) {
                $mensagem[] = "<div class=\"alert alert-warning alert-dismissible \" role=\"alert\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span>
                    </button>
                    <strong>Cancelado!</strong> O campo Nome não pode ser em branco.
                  </div>";
            } elseif (strlen($_POST['cnpj_matriz'] < 11) or strlen($_POST['cnpj_estab'] < 11)) {
                $mensagem[] = "<div class=\"alert alert-warning alert-dismissible \" role=\"alert\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span>
                    </button>
                    <strong>Cancelado!</strong> O campo CNPJ não está correto.
                  </div>";
            } elseif ($checaEstab > 0) {
                $mensagem[] = "<div class=\"alert alert-warning alert-dismissible \" role=\"alert\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span>
                    </button>
                    <strong>Cancelado!</strong> O campo CNPJ do Estabelecimento já está em uso.
                  </div>";
            } elseif ($checaEmail > 0) {
                $mensagem[] = "<div class=\"alert alert-warning alert-dismissible \" role=\"alert\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span>
                    </button>
                    <strong>Cancelado!</strong> O e-mail já está em uso!
                  </div>";
            } else {
                //upload
                if (!empty($_POST['foo'])) {

                    $storage = new \Upload\Storage\FileSystem('images');
                    $file = new \Upload\File('foo', $storage);

                    // Optionally you can rename the file on upload
                    $new_filename = uniqid();
                    $file->setName($new_filename);

                    // Validate file upload
                    // MimeType List => http://www.iana.org/assignments/media-types/media-types.xhtml
                    $file->addValidations(array(
                        // Ensure file is of type "image/png"
                        new \Upload\Validation\Mimetype(array('image/png', 'image/gif', 'image/jpeg')),

                        //You can also add multi mimetype validation
                        //new \Upload\Validation\Mimetype(array('image/png', 'image/gif'))

                        // Ensure file is no larger than 5M (use "B", "K", M", or "G")
                        new \Upload\Validation\Size('5M')
                    ));

                    // Access data about the file that has been uploaded
                    $data = array(
                        'name' => $file->getNameWithExtension(),
                        'extension' => $file->getExtension(),
                        'mime' => $file->getMimetype(),
                        'size' => $file->getSize(),
                        'md5' => $file->getMd5(),
                        'dimensions' => $file->getDimensions()
                    );

                    //Para gravar no banco de dados
                    $nome_arquivo = "images/" . $data['name'];

                    // Try to upload file
                    try {
                        // Success!
                        $file->upload();
                        $mensagem[] = "Upload feito com sucesso!";
                    } catch (\Exception $e) {
                        // Fail!
                        $errors = $file->getErrors();
                        $mensagem[] = implode("<br>", $errors);
                    }
                } */

                $empresa->empresacnpj_matriz = str_replace(array('.', '/', '-', '_'), '', $_POST['cnpj_matriz']);

                $cnpj_estab = str_replace(array('.', '/', '-', '_'), '', $_POST['cnpj_estab']);
                $empresa->empresacnpj_estab = substr($cnpj_estab, 0, 14);
                $empresa->empresanome_abrev = $_POST['nome_abrev'];
                $empresa->empresarazao_social = $_POST['razao_social'];
                $empresa->empresaramo_atividade = $_POST['ramo_atividade'];
                $empresa->empresarua = $_POST['rua'];
                $empresa->empresanumero = $_POST['numero'];
                $empresa->empresacomplemento = $_POST['complemento'];
                $empresa->empresacidade = $_POST['cidade'];
                $empresa->empresaestado = $_POST['estado'];
                $empresa->empresabairro = $_POST['bairro'];
                $empresa->empresapais = $_POST['pais'];
                $empresa->empresacep = str_replace(array('.', '-'), '', $_POST['cep']);

                $empresa->empresatelefone = str_replace(array('(', ')', '-', ' '), '', $_POST['telefone']);
                $empresa->empresacelular = str_replace(array('(', ')', '-', ' '), '', $_POST['celular']);

                $empresa->empresaurl = $_POST['url'];
                $empresa->empresae_mail = $_POST['e_mail'];
                $empresa->empresae_mail_nfe = $_POST['e_mail_nfe'];
                $empresa->empresaind_ativo = $_POST['ind_ativo'];
                if ($empresa->empresaind_ativo == "on") {
                    $empresa->empresaind_ativo = "0";
                } else {
                    $empresa->empresaind_ativo = "1";
                }    
                $empresa->empresalogo_marca = "NULL";
                
                $empresa->empresaind_origem = "0";
                $empresa->empresadt_criacao = date("Y-m-d");
                $empresa->empresahr_criacao = date("H:i:s");
                $empresa->empresadt_altera = date("Y-m-d");
                $empresa->empresahr_altera = date("H:i:s");
                $empresa->empresauser_criacao = $_SESSION['userId'];
                $empresa->empresauser_altera = $_SESSION['userId'];

                $mensagem[] = $empresa->save();

               /* 
                $empresa_log = $this->model('EmpresaLog');
                $empresa_log->empresa_logcnpj_matriz = str_replace(array('.', '/', '-','_'), '', $_POST['cnpj_matriz']);
                $empresa_log->empresa_logcnpj_estab = str_replace(array('.', '/', '-','_'), '', $_POST['cnpj_estab']);
                $empresa_log->empresa_logdt_emis = date("Y-m-d");
                $empresa_log->empresa_loghr_emis = date("H:i:s");
                $empresa_log->empresa_loguser_emis = $_SESSION['userId'];
                $empresa_log->empresa_logtitulo = "Inclusão";

                $mensagem[] = $empresa_log->save();

                
            /*}   */

         } 

        $this->view('empresas/cadastrar', $dados = ['mensagem' => $mensagem]);
    }


    public function editar($id)
    {

        Auth::checkLogin();

        $mensagem = array();
        $empresa = $this->model('Empresa');

        if (isset($_POST['Atualizar'])) {

            $empresa->empresacnpj_matriz = str_replace(array('.', '/', '-', '_'), '', $_POST['cnpj_matriz']);

            $cnpj_estab = str_replace(array('.', '/', '-', '_'), '', $_POST['cnpj_estab']);
            $empresa->empresacnpj_estab = substr($cnpj_estab, 0, 14);

            $empresa->empresanome_abrev = $_POST['nome_abrev'];
            $empresa->empresarazao_social = $_POST['razao_social'];
            $empresa->empresaramo_atividade = $_POST['ramo_atividade'];
            $empresa->empresarua = $_POST['rua'];
            $empresa->empresanumero = $_POST['numero'];
            $empresa->empresacomplemento = $_POST['complemento'];
            $empresa->empresacidade = $_POST['cidade'];
            $empresa->empresaestado = $_POST['estado'];
            $empresa->empresapais = $_POST['pais'];
            $empresa->empresacep = str_replace(array('.', '-'), '', $_POST['cep']);
            $empresa->empresatelefone = $_POST['telefone'];
            $empresa->empresacelular = $_POST['celular'];
            $empresa->empresabairro = $_POST['bairro'];
            $empresa->empresaurl = $_POST['url'];
            $empresa->empresae_mail = $_POST['e_mail'];
            $empresa->empresae_mail_nfe = $_POST['e_mail_nfe'];
            $empresa->empresaind_ativo = $_POST['ind_ativo'];
            if ($empresa->empresaind_ativo == "on") {
                $empresa->empresaind_ativo = "0";
            } else {
                $empresa->empresaind_ativo = "1";
            }

            $empresa->empresaind_origem = "0";
            $empresa->empresadt_altera = date("Y-m-d");
            $empresa->empresahr_altera = date("H:i:s");
            $empresa->empresauser_altera = $_SESSION['userId'];



            $mensagem[] = $empresa->update($id);

/*             //Gera o log

            $empresa_log = $this->model('EmpresaLog');

            $empresa_log->empresa_logcnpj_matriz = $empresa->empresacnpj_matriz;
            $empresa_log->empresa_logcnpj_estab = $empresa->empresacnpj_estab;
            $empresa_log->empresa_logdt_emis = date("Y-m-d");
            $empresa_log->empresa_loghr_emis = date("H:i:s");
            $empresa_log->empresa_loguser_emis = $_SESSION['matriz'];
            $empresa_log->empresa_logtitulo = "Alteração";


            $mensagem[] = $empresa_log->save(); */
        }

        $dados = $empresa->findId($id);

        $this->view('empresas/ver', $dados = ['mensagem' => $mensagem, 'empresa' => $dados]);
    }


    public
    function excluir($id = '')
    {
        Auth::checkLogin();

        $mensagem = array();
        $erro_empresa = array();
        $erro_estab = array();

        $empresa = $this->model('Empresa');
        $user_estab = $this->model('UserEstab');
        $meta = $this->model('Meta');
        $produto = $this->model('Produto');
        $gru_prod = $this->model('GrupoProd');
        $unid_med = $this->model('UnidMed');
        $param_estab = $this->model('ParamEstab');
        $unid_neg = $this->model('UnidNeg');




        $dados = $empresa->findId($id);

        //EMPRESA MÃE
        if ($dados['empresacnpj_matriz'] == $dados['empresacnpj_estab']) {
            $cnpj_matriz = $dados['empresacnpj_matriz'];
            $verifica_empresas_vinculadas[] = $empresa->getPorMatriz($cnpj_matriz);
            $estab_vinculado[] = $user_estab->BuscageralMatriz($cnpj_matriz);

            //verifica se existe algum dado
            if (!empty($verifica_empresas_vinculadas) && !empty($estab_vinculado)) {
                $erro_empresa = $verifica_empresas_vinculadas;
                $erro_estab = $estab_vinculado;
            }
        } else {
            $cnpj_estabe = $dados['empresacnpj_estab'];
            $meta_vinculada = $meta->getAllEstabelecimento($cnpj_estabe);
            $estab_vinculado = $user_estab->BuscageralEstab($cnpj_estabe);
            $prod_vinculado = $produto->BuscageralEstab($cnpj_estabe);
            $gru_prod_vinculado = $gru_prod->BuscageralEstab($cnpj_estabe);
            $unid_med_vinculada = $unid_med->BuscageralEstab($cnpj_estabe);
            $param_estab_vinculado = $param_estab->BuscageralEstab($cnpj_estabe);
            $unid_neg_vinculado = $unid_neg->BuscageralEstab($cnpj_estabe);

            if (!empty($meta_vinculada) && !empty($estab_vinculado)  && !empty($prod_vinculado) && !empty($gru_prod_vinculado) && !empty($unid_med_vinculada) && !empty($param_estab_vinculado) && !empty($unid_neg_vinculado)) {
                /*  $mensagem[] = $empresa->delete($id); */
                echo "ok";
            } else {
                echo "er";
            }
        }

        $dados = $empresa->getAll();



        $this->view('empresas/index', $dados = [
            'registros' => $dados, 'mensagem' => $mensagem,
            'erro_empresa' => $erro_empresa, 'erro_estab' => $erro_estab, 'erro_prod' => $prod_vinculado, 'erro_gp' => $gru_prod_vinculado,
            'erro_uni_med' => $unid_med, 'par_estab' => $param_estab_vinculado, 'unid_neg' => $unid_neg_vinculado
        ]);
    }

    public
    function editarlogo($id)
    {

        Auth::checkLogin();

        $mensagem = array();
        $empresa = $this->model('Empresa');

        if (isset($_POST['AtualizarLogo'])) {
            //upload
            if (empty($_POST['foo'])) {
                $storage = new \Upload\Storage\FileSystem('images');
                $file = new \Upload\File('foo', $storage);

                // Optionally you can rename the file on upload
                $new_filename = uniqid();
                $file->setName($new_filename);

                // Validate file upload
                // MimeType List => http://www.iana.org/assignments/media-types/media-types.xhtml
                $file->addValidations(array(
                    // Ensure file is of type "image/png"
                    new \Upload\Validation\Mimetype(array('image/png', 'image/gif', 'image/jpeg')),

                    //You can also add multi mimetype validation
                    //new \Upload\Validation\Mimetype(array('image/png', 'image/gif'))

                    // Ensure file is no larger than 5M (use "B", "K", M", or "G")
                    new \Upload\Validation\Size('5M')
                ));

                // Access data about the file that has been uploaded
                $data = array(
                    'name' => $file->getNameWithExtension(),
                    'extension' => $file->getExtension(),
                    'mime' => $file->getMimetype(),
                    'size' => $file->getSize(),
                    'md5' => $file->getMd5(),
                    'dimensions' => $file->getDimensions()
                );

                //Para gravar no banco de dados
                //$nome_arquivo = "images/" . $data['name'];

                // Try to upload file
                try {
                    // Success!
                    $file->upload();
                    $mensagem[] = "Upload feito com sucesso!";
                } catch (\Exception $e) {
                    // Fail!
                    $errors = $file->getErrors();
                    $mensagem[] = implode("<br>", $errors);
                }
            }

            if (!empty($data['name'])) {
                $empresa->logo_marca = $data['name'];
            } else {
                $empresa->foto = "NULL";
            }
            $mensagem[] = $empresa->updatelogo($id);
        }

        $dados = $empresa->findId($id);

        $empresa_log = $this->model('EmpresaLog');
        $empresa_log->cnpj_matriz = $dados['empresacnpj_matriz'];
        $empresa_log->cnpj_estab = $dados['empresacnpj_estab'];
        $empresa_log->dt_emis = date("Y-m-d");
        $empresa_log->hr_emis = date("H:i:s");
        $empresa_log->user_emis = $_SESSION['matriz'];
        $empresa_log->titulo = "Alteração";
        $empresa_log->save();

        $this->view('empresas/editarlogo', $dados = ['mensagem' => $mensagem, 'registros' => $dados]);
    }

    public
    function pesquisar()
    {
        Auth::checkLogin();

        $busca = isset($_POST['texto']) ? $_POST['texto'] : $_SESSION['texto'];
        $_SESSION['texto'] = $busca;

        $empresa = $this->model('Empresa');
        $dados = $empresa->pesquisar($busca);

        $this->view('empresas/index', $dados = ['registros' => $dados]);
    }
}
