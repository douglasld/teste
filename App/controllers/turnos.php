<?php

use \App\Core\Controller;
use App\Auth;

class turnos extends Controller{

    public function index()
    {
        Auth::checkLogin();

        $turnos = $this->model('Turno');
        $dados = $turnos->getAll();

        $this->view('turnos/index', $dados = ['registros' => $dados]);
    }

    public function ver($id = '')
    {
        Auth::checkLogin();

        $turnos = $this->model('Turno');
        $dados = $turnos->findId($id);

        $this->view('turnos/ver',$dados = ['registros' => $dados]);
    }

    public function cadastrar()
    {
        Auth::checkLogin();

        $mensagem = array();

        if(isset($_POST['Cadastrar'])){

            $turnos = $this->model('Turno');

            $cnpj_estab = str_replace(array('.', '/', '-', '_'), '', $_POST['cnpj_estab']);
            $turnos->turnocnpj_estab = substr($cnpj_estab,0, 14); 
            $turnos->turnodescricao = $_POST['descricao'];
            $turnos->turnoind_ativo = $_POST['ind_ativo'];
            if($turnos->turnoind_ativo == "on"){
                $turnos->turnoind_ativo = "0";
            }else{
                $turnos->turnoind_ativo = "1";
            }
            $turnos->turnoind_origem = "0";
            $turnos->turnodt_criacao = date("Y-m-d");
            $turnos->turnohr_criacao = date("h:i:s");
            $turnos->turnouser_criacao = $_SESSION['userId'];
            $turnos->turnodt_altera = date("Y-m-d");
            $turnos->turnohr_altera = date("h:i:s");
            $turnos->turnouser_altera = $_SESSION['userId'];
            $turnos->turnohr_ini = $_POST['hr_ini'];
            $turnos->turnohr_fim = $_POST['hr_fim'];
            $turnos->turnoperc_red_meta_turno = str_replace(array('.', ','), array('', '.'), $_POST['perc_red_meta_turno']);

            $mensagem[] = $turnos->save();

            $turno_log = $this->model('TurnoLog');

            $cnpj_estab = str_replace(array('.', '/', '-', '_'), '', $_POST['cnpj_estab']);
            $turno_log->turno_logcnpj_estab = substr($cnpj_estab,0, 14); 
            $turno_log->turno_loguser_emis = $_SESSION['userId'];
            $turno_log->turno_logdt_emis = date("Y-m-d");
            $turno_log->turno_loghr_emis = date("h:i:s");
            $turno_log->turno_logtitulo = "Inclusão";
            $turno_log->turno_loghr_ini = $_POST['hr_ini'];
            $turno_log->turno_loghr_fim = $_POST['hr_fim'];

            $mensagem[] = $turno_log->save();

        }

        $this->view('turnos/cadastrar', $dados = [ 'mensagem' => $mensagem]);

    }

    public function editar($id){

        Auth::checkLogin();

        $mensagem = array();
        $turnos = $this->model('Turno');    

        if(isset($_POST['Atualizar'])){

            $cnpj_estab = str_replace(array('.', '/', '-', '_'), '', $_POST['cnpj_estab']);
            $turnos->turnocnpj_estab = substr($cnpj_estab,0, 14); 
            $turnos->turnodescricao = $_POST['descricao'];
            $turnos->turnoind_ativo = $_POST['ind_ativo'];
            if($turnos->turnoind_ativo == "on"){
                $turnos->turnoind_ativo = "0";
            }else{
                $turnos->turnoind_ativo = "1";
            }
            $turnos->turnoind_origem = "0";
            $turnos->turnodt_altera = date("Y-m-d");
            $turnos->turnohr_altera = date("h:i:s");
            $turnos->turnouser_altera = $_SESSION['userId'];
            $turnos->turnohr_ini = $_POST['hr_ini'];
            $turnos->turnohr_fim = $_POST['hr_fim'];
            $turnos->turnoperc_red_meta_turno = str_replace(array('.', ','), array('', '.'), $_POST['perc_red_meta_turno']);

            $mensagem[] = $turnos->update($id);

            $turno_log = $this->model('TurnoLog');

            $cnpj_estab = str_replace(array('.', '/', '-', '_'), '', $_POST['cnpj_estab']);
            $turno_log->turno_logcnpj_estab = substr($cnpj_estab,0, 14); 
            $turno_log->turno_loguser_emis = $_SESSION['userId'];
            $turno_log->turno_logdt_emis = date("Y-m-d");
            $turno_log->turno_loghr_emis = date("h:i:s");
            $turno_log->turno_logtitulo = "Alteração";
            $turno_log->turno_loghr_ini = $_POST['hr_ini'];
            $turno_log->turno_loghr_fim = $_POST['hr_fim'];

            $mensagem[] = $turno_log->save();

        }

        $dados = $turnos->findId($id);

        $this->view('turnos/editar', $dados = [ 'mensagem' => $mensagem, 'registros' => $dados]);
    }


    public function excluir($id = '')
    {
        Auth::checkLogin();

        $mensagem = array();
        $turnos = $this->model('Turno');

        $mensagem[] = $turnos->delete($id);

        $dados = $turnos->getAll();

        $this->view('turnos/index', $dados = ['registros' => $dados, 'mensagem' => $mensagem]);
    }

}





