<?php

use \App\Core\Controller;
use App\Auth;

class vendprod extends Controller
{

    public function index()
    {
        Auth::checkLogin();

        $prods = $this->model('VendProds');

        $dados = $prods->GetAll();

        $this->view('vendprod/index', $dados = ['registros' => $dados]);
    }

    public function direciona()
    {
        Auth::checkLogin();

        if (isset($_POST['nome_prod'])) {
            //includes
            $prods = $this->model('VendProds');

            //trata post
            $cod_prod = $_POST['nome_prod'];


            //verifica produto
            if ($verifica_prod = $prods->VerificaProd($cod_prod)) {
                $verifica_prod = $verifica_prod[0];
                $cod_prod = $verifica_prod['cod_prod'];
                $cod_cat_p = $verifica_prod['cod_cat_p'];


                //verifica a pagina a ser direcionada
                if ($cod_cat_p == 1) {
                    $this->view('vendprod/artesanato', $dados = ['registros' => $verifica_prod]);
                }
                if ($cod_cat_p == 2) {
                    $this->view('vendprod/tabacaria', $dados = ['registros' => $verifica_prod]);
                }
                if ($cod_cat_p == 3) {
                    $this->view('vendprod/malha', $dados = ['registros' => $verifica_prod]);
                }
                if ($cod_cat_p == 4) {
                    $this->view('vendprod/pintura', $dados = ['registros' => $verifica_prod]);
                }
                if ($cod_cat_p == 5) {
                    $this->view('vendprod/modelagem', $dados = ['registros' => $verifica_prod]);
                }
                if ($cod_cat_p == 6) {
                    $this->view('vendprod/estetica', $dados = ['registros' => $verifica_prod]);
                }
                if ($cod_cat_p == 7) {
                    $this->view('vendprod/higiene', $dados = ['registros' => $verifica_prod]);
                }
            }
        }
    }
}
