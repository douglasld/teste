<?php

use \App\Core\Controller;
use App\Auth;

class gruposprodutos extends Controller
{

    public function index()
    {
        Auth::checkLogin();

        $empresas = $this->model('GrupoProd');

        $dados = $empresas->getPorMatriz($_SESSION['matriz']);
       
        $this->view('gruposprodutos/index', $dados = ['registros' => $dados]);
    }



    public function ver($id = '')
    {
        Auth::checkLogin();

        $grupos = $this->model('GrupoProd');
        $dados = $grupos->findId($id);

    

        $this->view('gruposprodutos/ver', $dados);

        
    }

    public function cadastrar()
    {
        Auth::checkLogin();

        $mensagem = array();

        if (isset($_POST['Cadastrar'])) {

            if (empty($_POST['descricao'])) {
                $mensagem[] = "O descricao não pode ser em branco";
            } else {
                $grupos = $this->model('GrupoProd');

                $grupos->grupo_prodcnpj_estab = str_replace(array('.', '/', '-', '_'), '', $_POST['cnpj_estab']);
                $grupos->grupo_prodcod_grupo_prod = $_POST['cod_grupo_prod'];
                $grupos->grupo_proddescricao = $_POST['descricao'];
                $grupos->grupo_prodind_ativo = $_POST['ind_ativo'];
                
                if($grupos->grupo_prodind_ativo == "on"){
                    $grupos->grupo_prodind_ativo = "0";
                }else{
                    $grupos->grupo_prodind_ativo = "1";
                }

                $grupos->grupo_prodind_origem = "0";
                $grupos->grupo_proddt_criacao = date("Y-m-d");
                $grupos->grupo_prodhr_criacao = date("H:i:s");
                $grupos->grupo_produser_criacao = $_SESSION['userId'];
                $grupos->grupo_proddt_altera = date("Y-m-d");
                $grupos->grupo_prodhr_altera = date("H:i:s");
                $grupos->grupo_produser_altera = $_SESSION['userId'];
                
         

                $mensagem[] = $grupos->save();

                $grupo_prod_log = $this->model('GrupoProdLog');
                $grupo_prod_log->grupo_prod_logcnpj_estab = str_replace(array('.', '/', '-', '_'), '', $_POST['cnpj_estab']);
                $grupo_prod_log->grupo_prod_logcod_grupo_prod = $_POST['cod_grupo_prod'];
                $grupo_prod_log->grupo_prod_logdt_emis = date("Y-m-d");
                $grupo_prod_log->grupo_prod_loghr_emis = date("H:i:s");
                $grupo_prod_log->grupo_prod_loguser_emis = $_SESSION['userId'];
                $grupo_prod_log->grupo_prod_logtitulo = "Inclusão";

                $mensagem[] = $grupo_prod_log->save();
            }
        }


        $this->view('gruposprodutos/cadastrar', $dados = ['mensagem' => $mensagem]);
    }

    public function editar($id)
    {

        Auth::checkLogin();

        $mensagem = array();
        $grupos = $this->model('GrupoProd');
        $dados = $grupos->findId($id);

        if (isset($_POST['Atualizar'])) {

            $grupos->grupo_prodcnpj_estab = str_replace(array('.', '/', '-', '_'), '', $_POST['cnpj_estab']);
            $grupos->grupo_prodcod_grupo_prod = $_POST['cod_grupo_prod'];
            $grupos->grupo_proddescricao = $_POST['descricao'];
            $grupos->grupo_prodind_ativo = $_POST['ind_ativo'];
            if($grupos->grupo_prodind_ativo == "on"){
                $grupos->grupo_prodind_ativo = "0";
            }else{
                $grupos->grupo_prodind_ativo = "1";
            }


            $grupos->grupo_prodind_origem = "0";
            $grupos->grupo_proddt_altera = date("Y-m-d");
            $grupos->grupo_prodhr_altera = date("H:i:s");
            $grupos->grupo_produser_altera = $_SESSION['userId'];


            $mensagem[] = $grupos->update($id);

            $grupo_prod_log = $this->model('GrupoProdLog');
            $grupo_prod_log->grupo_prod_logcnpj_estab = str_replace(array('.', '/', '-', '_'), '', $dados['grupo_prodcnpj_estab']);
            $grupo_prod_log->grupo_prod_logcod_grupo_prod = $dados['grupo_prodcod_grupo_prod'];
            $grupo_prod_log->grupo_prod_logdt_emis = date("Y-m-d");
            $grupo_prod_log->grupo_prod_loghr_emis = date("H:i:s");
            $grupo_prod_log->grupo_prod_loguser_emis = $_SESSION['userId'];
            $grupo_prod_log->grupo_prod_logtitulo = "Alteração";

            $mensagem[] = $grupo_prod_log->save();
        }

        $this->view('gruposprodutos/editar', $dados = ['mensagem' => $mensagem, 'registros' => $dados]);
    }

    public function copiar($id)
    {

        Auth::checkLogin();

        $mensagem = array();
        $grupos = $this->model('GrupoProd');
        $dados = $grupos->findId($id);

        if (isset($_POST['Cadastrar'])) {

            $grupos->grupo_prodcnpj_estab = str_replace(array('.', '/', '-', '_'), '', $_POST['cnpj_estab']);
            $grupos->grupo_prodcod_grupo_prod = $_POST['cod_grupo_prod'];
            $grupos->grupo_proddescricao = $_POST['descricao'];
            $grupos->grupo_prodind_ativo = $_POST['ind_ativo'];
            $grupos->grupo_prodind_origem = $_POST['ind_origem'];
            $grupos->grupo_proddt_altera = date("Y-m-d");
            $grupos->grupo_prodhr_altera = date("H:i:s");
            $grupos->grupo_produser_altera = $_SESSION['userId'];

            $mensagem[] = $grupos->save();

            $grupo_prod_log = $this->model('GrupoProdLog');
            $grupo_prod_log->grupo_prod_logcnpj_estab = str_replace(array('.', '/', '-', '_'), '', $dados['grupo_prodcnpj_estab']);
            $grupo_prod_log->grupo_prod_logcod_grupo_prod = $dados['grupo_prodcod_grupo_prod'];
            $grupo_prod_log->grupo_prod_logdt_emis = date("Y-m-d");
            $grupo_prod_log->grupo_prod_loghr_emis = date("H:i:s");
            $grupo_prod_log->grupo_prod_loguser_emis = $_SESSION['userId'];
            $grupo_prod_log->grupo_prod_logtitulo = "Alteração";

            $mensagem[] = $grupo_prod_log->save();
        }

        $this->view('gruposprodutos/copiar', $dados = ['mensagem' => $mensagem, 'registros' => $dados]);
    }

    public function excluir($id = '')
    {
        Auth::checkLogin();
        die();

        $mensagem = array();
        $grupos = $this->model('GrupoProd');

        $mensagem[] = $grupos->delete($id);

        $dados = $grupos->getAll();

        $this->view('home/index', $dados = ['registros' => $dados, 'mensagem' => $mensagem]);
    }

    public function pesquisar()
    {
        Auth::checkLogin();

        $busca = isset($_POST['texto']) ? $_POST['texto'] : $_SESSION['texto'];
        $_SESSION['texto'] = $busca;

        $empresas = $this->model('Empresa');
        $dados = $empresas->getPorMatriz($_SESSION['matriz']);

        $this->view('gruposprodutos/index', $dados = ['registros' => $dados]);
    }
}
